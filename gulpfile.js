var gulp = require('gulp');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var cssnano = require('cssnano');
var beep = require('beepbeep');
var colors = require('colors');

var cssDir = 'httpdocs/themes/custom/css';  // do *not* put "./" in front of path -- it causes gulp.watch to ignore subdirectories for some weird reason
var sassDir = 'httpdocs/themes/custom/css/sass';  // do *not* put "./" in front of path -- it causes gulp.watch to ignore subdirectories for some weird reason

sass.compiler = require('node-sass'); // default, but recommended to  be set explicitly (https://www.npmjs.com/package/gulp-sass)

gulp.task('sass', async function() {
	gulp.src(sassDir + '/*.scss')
		.pipe(sass.sync().on('error', function(error) {
			console.log(error.message.bold.red);
			beep();
		}))
		.pipe(postcss([
			autoprefixer({ // see "browserslist" in package.json for targeted browsers
				grid: 'autoplace' // for IE11 support (https://github.com/postcss/autoprefixer#does-autoprefixer-polyfill-grid-layout-for-ie and https://github.com/postcss/autoprefixer#does-autoprefixer-polyfill-grid-layout-for-ie)
			}),
			cssnano({
				reduceIdents: {keyframes: false} // `reduceIdents` used because w/o it, cssnano renames css keyframes: https://github.com/cssnano/cssnano/issues/247
			})
		]))
		.pipe(gulp.dest(cssDir));
});

gulp.task('watch', function() {
	console.log(('\nWatching for changes.\nPress ^C at any time to quit.\n').green);
	gulp.watch(sassDir + '/**/*.scss', gulp.series(['sass']));
});

gulp.task('default', gulp.series(['watch', 'sass']));