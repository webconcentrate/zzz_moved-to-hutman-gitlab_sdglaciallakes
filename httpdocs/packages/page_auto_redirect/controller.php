<?php     defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * Class that is used to redirect to the newest child page
 * @package Page Auto Redirect
 * @author Aaron Ostrowsky <aaronostrowsky@gmail.com>
 * @category Packages
 * @copyright  Copyright (c) 2011 Aaron Ostrowsky
 */
class PageAutoRedirectPackage extends Package {

	protected $pkgHandle = 'page_auto_redirect';
	protected $appVersionRequired = '5.4.2';
	protected $pkgVersion = '2.0';
	
	public function getPackageDescription() {
		return t("Adds a page attribute for redirecting page to child page.  Options for redirection: first, newest, or random child page, and parent page.");
	}
	
	public function getPackageName() {
		return t("Page Auto Redirect");
	}
	
	public function install() {
		$pkg = parent::install();
		Loader::model('collection_attributes');
		
		# install my own attribute type select
		$MyOwnAttrKey=CollectionAttributeKey::getByHandle('page_redirect_selector');
		
		# test if does not alreday exist
		if( !$MyOwnAttrKey || !intval($MyOwnAttrKey->getAttributeKeyID()) ) {
			$MyOwnAttrKey = CollectionAttributeKey::add('page_redirect_selector', t('Page Auto Redirect'), false, null, 'SELECT');
			
			# fill the value inside the background attribute
			$MyOwnAttributeTypeOption = SelectAttributeTypeOption::add( $MyOwnAttrKey, 'First child page', 0, 0);
			$MyOwnAttributeTypeOption = SelectAttributeTypeOption::add( $MyOwnAttrKey, 'Newest child page', 0, 0);
			$MyOwnAttributeTypeOption = SelectAttributeTypeOption::add( $MyOwnAttrKey, 'Random child page', 0, 0);
			$MyOwnAttributeTypeOption = SelectAttributeTypeOption::add( $MyOwnAttrKey, 'Parent page', 0, 0);
		
		}
		
		//WC CUSTOMIZATION: Add the attribute to the "Navigation and Indexing" set
		if (version_compare(APP_VERSION, '5.5.0', '>=')) {
			$as = AttributeSet::getByHandle('navigation');
			if (!is_null($as)) {
				$as->addKey($MyOwnAttrKey);
			}
		}
	}
	
	public function on_start() {
		$url = Loader::helper('concrete/urls')->getPackageURL(Package::getByHandle('page_auto_redirect'));
		Events::extend('on_start', 'PageAutoRedirect', 'checkRedirect', DIRNAME_PACKAGES . '/' . $this->pkgHandle . '/models/page_auto_redirect.php');
	}
}