<?php     defined('C5_EXECUTE') or die("Access Denied.");

/**
 * Class that is used to automatically redirect to a child page, can be either the first, newest, or random child page.
 * @package Page Auto Redirect
 * @author Aaron Ostrowsky <aaronostrowsky@gmail.com>
 * @category Packages
 * @copyright  Copyright (c) 2011 Aaron Ostrowsky
 */

class PageAutoRedirect {

	public function checkRedirect() {
		# get the current page
		$cobj = Page::getCurrentPage();
		$cobj_id = $cobj->getCollectionID();
		
		# check for redirect selection attribute
		$akRedirect = 'page_redirect_selector';
		$redirectVal = strtolower($cobj->getCollectionAttributeValue($akRedirect));
		
		
		# redirect logic
		if( $redirectVal != '' ){
				# get list of pages
				 Loader::model('page_list');
			     $pl = new PageList();
			     $pl->setItemsPerPage(1);
		         $pl->filterByParentID($cobj_id);
				
				# sort pages
				switch($redirectVal){
					case 'first child page' :
						$pl->sortByDisplayOrder();
						break;
					case 'newest child page' :
						$pl->sortByPublicDateDescending();
				 		break;
	
				}
				
				# shuffle pages
				if( $redirectVal == 'random child page' ){
					$pages = $pl->get();					
					if (!is_array($pages)) $page = $pages;
					$keys = array_keys($pages);
					shuffle($keys);
					$random = array();
					foreach ($keys as $key) {
					$shuffle[] = $pages[$key];
					}
					$page = $shuffle[0];
					
				# find parent page
				} elseif( $redirectVal == 'parent page' ){
					$parent_id = $cobj->getCollectionParentID();
					$page = Page::getById($parent_id);
																		
				# default				
				} else {
					$pages = $pl->get($itemsToGet = 1, $offset = 0);
					$page = $pages[0];
				}
				
				
		
				
				# do redirect
				if(is_object($page) && !$page->isError()) {
					# get the page path
					$page_target = $page->getcollectionpath();
					# load the controller just incase
					$pcontroller = Loader::controller($page);
					# redirect
					if(!$page->isExternalLink()) {
						$nh = Loader::helper('navigation');
						header('Location: '. ($nh->getLinkToCollection($page)));
						exit;
					} else {
						header('Location: '.$page->getCollectionPointerExternalLink());
						exit;
					}
				}
				
								
		}
	}
}

?>