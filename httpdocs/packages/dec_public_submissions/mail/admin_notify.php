<?php defined('C5_EXECUTE') or die("Access Denied.");

$th = Loader::helper('text');
	

$subject = '['.SITE.'] New Public Event Submission';

$body = "A new event has been submitted by a public website visitor...\n\n";
$body .= "Business/Organization Name: {$submitted_by}\n\n";
$body .= str_replace("\n", "\n\n", $submitter_info); //this one contains its own line labels
$body .= "\n\n";
$body .= 'Event Title: ' . $event->getTitle() . "\n\n";
$body .= 'Venue Info: ' . $event->getSubtitle() . "\n\n";

$body .= 'Event Date: ';
if ($event->getLengthInDays() == 1) {
	$body .= $event->getStartFormatted('n/j/y (D)');
} else {
	$body .= $event->getStartFormatted('n/j/y (D)');
	$body .= ' - ';
	$body .= $event->getEndFormatted('n/j/y (D)');
}
$body .= "\n\n";

$body .= 'Event Time: ';
if ($event->isAllDay()) {
	$body .= 'All Day';
} else {
	$body .= $event->getStartFormatted('g:i a');
	$body .= ' - ';
	$body .= $event->getEndFormatted('g:i a');
}
$body .= "\n\n";

//$body .= 'Event Repeat: ' . $event->getRepeatSummary() . "\n";
$body .= 'Event Description: ' . $event->getDescription() . "\n\n";
//$body .= 'Event Categories: ' . implode(', ', $event->getCategoryNames()) . "\n";

$body .= "\n\n";
$body .= 'Go to ' . BASE_URL . View::url('/dashboard/event_calendar/public_submissions/') . ' to approve or reject this submission.';
$body .= "\n\n";

$body = strip_tags($body);

$bodyHTML = nl2br($th->entities($body));