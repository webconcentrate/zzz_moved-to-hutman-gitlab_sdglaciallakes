<?php defined('C5_EXECUTE') or die(_("Access Denied."));

$dh = Loader::helper('concrete/dashboard');
$ih = Loader::helper('concrete/interface');
?>


<?php echo $dh->getDashboardPaneHeaderWrapper('Review Public Submissions'); ?>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Submitted At</th>
			<th>Submitted By</th>
			<th>Info</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($events as $id => $event): ?>
			<tr>
				<td><?=$misc[$id]['submitted_at']?></td>
				<td>
					<?=h($misc[$id]['submitted_by'])?>
					<br>
					<?=nl2br(h($misc[$id]['submitter_info']))?>
				</td>
				<td>
					<?php if ($misc[$id]['is_error']): ?>
						<p style="color: red; font-weight: bold;">Error: This submission contains invalid data.</p>
					<?php else: ?>
						<table border="0">
							<tr>
								<td>Title:</td>
								<td><?=$event->getTitle()?></td>
							</tr>
							<tr>
								<td>Date:</td>
								<td><?php
									if ($event->getLengthInDays() == 1) {
										echo $event->getStartFormatted('n/j/y (D)');
									} else {
										echo $event->getStartFormatted('n/j/y (D)');
										echo ' - ';
										echo $event->getEndFormatted('n/j/y (D)');
									}
								?></td>
							</tr>
							<tr>
								<td>Time:</td>
								<td><?php
									if ($event->isAllDay()) {
										echo 'All Day';
									} else {
										echo $event->getStartFormatted('g:i a');
										echo ' - ';
										echo $event->getEndFormatted('g:i a');
									}
								?></td>
							</tr>
							<?php /*
							<tr>
								<td>Repeat:</td>
								<td><?=$event->getRepeatSummary()?></td>
							</tr>
							*/ ?>
							<tr>
								<td>Description:</td>
								<td><?=$event->getDescription()?></td>
							</tr>
							<?php /*
							<tr>
								<td>Categories:</td>
								<td><?=implode(', ', $event->getCategoryNames())?></td>
							</tr>
							*/ ?>
						</table>
					<?php endif; ?>
				</td>
				
				<td class="action"><a class="btn" href="<?php echo $this->action('approve', $id); ?>"><i class="icon-ok"></i> Approve</a></td>
				<td class="action"><a class="btn" href="<?php echo $this->action('reject', $id); ?>"><i class="icon-remove"></i> Reject</a></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
	
<?php echo $dh->getDashboardPaneFooterWrapper(); ?>
