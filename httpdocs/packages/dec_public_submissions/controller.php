<?php defined('C5_EXECUTE') or die(_("Access Denied."));

class DecPublicSubmissionsPackage extends Package {
	
	protected $pkgHandle = 'dec_public_submissions';
	protected $pkgName = 'Event Calendar Public Submissions';
	protected $pkgDescription = 'Allows website visitors to submit calendar events, which can then be reviewed by administrators for approval/rejection';
	protected $appVersionRequired = '5.6';
	protected $pkgVersion = '0.3.5'; //Compatible with DEC v0.8.8
	
	public function install() {
		if (!Package::getByHandle('designer_event_calendar')) {
			throw new Exception(t('Designer Event Calendar must be installed before this addon can be installed.'));
			exit;
		}
		
		$pkg = parent::install(); //this will automatically install our package-level db.xml schema for us (among other things)
		$this->installOrUpgrade($pkg);
	}
	
	public function upgrade() {
		$this->installOrUpgrade($this);
		parent::upgrade();
	}
	
	//Put most installation tasks here -- makes development easier
	// (just make sure the actions you perform are "non-destructive",
	//  for example, check if a page exists before adding it).
	private function installOrUpgrade($pkg) {
		
		//Blocks
		$this->getOrInstallBlockType($pkg, 'dec_public_submissions');
		
		//Dashboard Pages:
		//Install one page for each *controller* (not each view),
		// plus one at the top-level to serve as a placeholder in the dashboard menu
		// ^^Although in this specific case we are NOT installing a top-level placeholder page
		//   because we're adding to the existing "event_calendar" section of the dashboard
		//   (so a top-level placeholder page already exists).
		$this->getOrAddSinglePage($pkg, '/dashboard/event_calendar/public_submissions', 'Review Public Submissions');		
	}
	
	
/*** UTILITY FUNCTIONS ***/
	private function getOrAddSinglePage($pkg, $cPath, $cName = '', $cDescription = '') {
		Loader::model('single_page');
		
		$sp = SinglePage::add($cPath, $pkg);
		
		if (is_null($sp)) {
			//SinglePage::add() returns null if page already exists
			$sp = Page::getByPath($cPath);
		} else {
			//Set page title and/or description...
			$data = array();
			if (!empty($cName)) {
				$data['cName'] = $cName;
			}
			if (!empty($cDescription)) {
				$data['cDescription'] = $cDescription;
			}
			
			if (!empty($data)) {
				$sp->update($data);
			}
		}
		
		return $sp;
	}
	
	public function getOrInstallBlockType($pkg, $btHandle) {
		$bt = BlockType::getByHandle($btHandle);
		if (empty($bt)) {
			BlockType::installBlockTypeFromPackage($btHandle, $pkg);
			$bt = BlockType::getByHandle($btHandle);
		}
		return $bt;
	}
	
}
