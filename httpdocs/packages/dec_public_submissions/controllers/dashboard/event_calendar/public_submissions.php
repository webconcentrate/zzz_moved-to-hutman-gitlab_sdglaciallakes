<?php defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('event', 'designer_event_calendar');

Loader::library('wc_crud/better_controller', 'wc_utils'); //Superset of Concrete5's Controller class -- provides simpler interface and some extra useful features.
class DashboardEventCalendarPublicSubmissionsController extends BetterController {

	public function view() {
		list($events, $misc) = $this->getEventsAndMiscellaneousData();
		$this->set('events', $events);
		$this->set('misc', $misc);
	}
	
	private function getEventsAndMiscellaneousData() {
		$records = Loader::db()->GetAll("SELECT * FROM dec_public_submissions ORDER BY start_date ASC, submitted_at ASC");
		$events = array();
		$misc = array();
		foreach ($records as $record) {
			$id = $record['submission_id'];
			$event = $this->getEventFromSubmissionRecord($record);
			$events[$id] = $event;
			$misc[$id] = array(
				'submitted_at' => $record['submitted_at'],
				'submitted_by' => $record['submitted_by'],
				'submitter_info' => $record['submitter_info'],
				'is_error' => is_null($event), //this should never happen, because the event should have been validated when first submitted. But just in case...
			);
		}
		
		return array($events, $misc);
	}
	
	private function getEventFromSubmissionRecord($record) {
		//convert comma-separated strings to arrays
		$record['repeat_weekly_on'] = explode(',', $record['repeat_weekly_on']);
		$record['category_ids'] = explode(',', $record['category_ids']);
		
		//Concatenate all public submitter info fields
		// (in case this will be saved to the actual event record upon approval)
		$record['public_submitter_info'] = "Submitted At: {$record['submitted_at']}\nSubmitted By: {$record['submitted_by']}\n{$record['submitter_info']}";

		
		if (EventRecord::validatePOST($record)->has()) {
			return null;
		}
		
		return EventRecord::loadFromPOST($record);
	}
	
	public function approve($submission_id) {
		$record = $this->getSubmissionRecord($submission_id);
		$event = $this->getEventFromSubmissionRecord($record);
		if (is_null($event)) {
			//This should never happen,
			// because the event should have been validated when first submitted.
			// But just in case...
			$msg = 'The event could not be approved because it contains invalid data.';
			$this->flash($msg); //Don't use 'error' style, because we're using that for rejections. Instead just use the default (blue "message" style).
		} else {
			$event->save();
			//$msg = 'Event "' . $event->getTitle() . '" has been approved.';
			//$this->flash($msg, 'success');
			$this->deleteSubmissionRecord($submission_id);
			
			//SDGL CUSTOMIZATION: Redirect to the event edit page so they can do the address lat/lng lookup and assign members
			$this->flash('This event has been approved. Please scroll down to the bottom of this page and assign it to one or more Member(s)', 'success');
			$abs_event_url = BASE_URL . View::url('/dashboard/event_calendar/events/edit', $event->getId());
			header("Location: {$abs_event_url}");
			exit;
		}
		
		$this->redirect('view');
	}
	
	public function reject($submission_id) {
		$record = $this->getSubmissionRecord($submission_id);
		$this->deleteSubmissionRecord($submission_id);
		$msg = 'Event "' . h($record['title']) . '" has been rejected.';
		$this->flash($msg, 'error');
		$this->redirect('view');
	}
	
	private function getSubmissionRecord($submission_id) {
		$db = Loader::db();
		$sql = 'SELECT * FROM dec_public_submissions WHERE submission_id = ?';
		$vals = intval($submission_id);
		$record = $db->GetRow($sql, $vals);
		return $record;
	}
	
	private function deleteSubmissionRecord($submission_id) {
		$db = Loader::db();
		$sql = 'DELETE FROM dec_public_submissions WHERE submission_id = ?';
		$vals = array(intval($submission_id));
		$db->Execute($sql, $vals);
	}
}