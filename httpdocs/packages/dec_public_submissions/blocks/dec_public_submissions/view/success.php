<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<div class="dec-public-form">
	<div class="dec-public-form__form-success">
		<p>Thank you! Your event has been submitted and will be reviewed by us shortly.</p>
		<p><a href="<?php echo Loader::helper('navigation')->getLinkToCollection(Page::getCurrentPage()); ?>">Click here to submit another event</a></p>
	</div>
</div>
