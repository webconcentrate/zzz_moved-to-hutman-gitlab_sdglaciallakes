<?php defined('C5_EXECUTE') or die(_("Access Denied."));

//DEV NOTE: This file is mostly a copy of:
// /packages/designer_event_calendar/single_pages/dashbaord/event_calendar/events/edit.php
//The differences are:
// * commented out the dashboard style stuff
// * submit to the block action
// * display our own success/error message
// * changed the "Description" field from a WYSIWYG editor to a plain textbox
// * added submitted_by text field
// * do not pass in existing values to form fields (since this form is always "add new", never "edit existing")
// * added some SDGL-specific fields (e.g. venu info)

// $dh = Loader::helper('concrete/dashboard');
// $ih = Loader::helper('concrete/interface');
$th = Loader::helper('text');
$form = Loader::helper('form');

// Loader::element('editor_config'); //for wysiwyg editor

// $is_new = empty($id);
// $heading = $is_new ? t('Add New Event') : t('Edit Event');
// $action = $is_new ? $this->action('add') : $this->action('edit', (int)$id);
?>

<div class="dec-public-form">
<?php /* echo $dh->getDashboardPaneHeaderWrapper($heading, false, 'span12', false); */ ?>
	
	<?php if (!empty($error)) { ?>
		<div class="dec-public-form__form-error">
			<?php Loader::element('system_errors', array('error' => $error)); ?>
		</div>
	<?php } ?>
	
	<form novalidate class="dec-public-form__form" method="post" action="<?php echo $this->action('submit'); ?>">
		<?php echo $token; ?>
		<?php /* echo $form->hidden('id', (int)$id); /* redundant, but simplifies processing */ ?>
	
			<fieldset class="dec-public-form__fieldset">
				<legend class="dec-public-form__fieldset-legend">Event Information</legend>
				
				<div class="dec-public-form__input">
					<div class="dec-public-form__input-label">
						<label for="title"><?php echo t('Event Title'); ?> *</label>
					</div>
					<div class="dec-public-form__input-field">
						<?php echo $form->text('title', null, array('maxlength' => '255')); ?>
					</div>
				</div>
				
				<div class="dec-public-form__input">
					<div class="dec-public-form__input-label">
						<label for="sdgl_event_phone">Event Contact Phone # *</label>
					</div>
					<div class="dec-public-form__input-field">
						<?php echo $form->text('sdgl_event_phone', null, array('maxlength' => '25')); ?>
					</div>
				</div>
				
				<div class="dec-public-form__input">
					<div class="dec-public-form__input-label">
						<label for="sdgl_event_website">Event Website URL</label>
					</div>
					<div class="dec-public-form__input-field">
						<?php echo $form->text('sdgl_event_website'); ?>
					</div>
				</div>
				
				<div class="dec-public-form__input">
					<div class="dec-public-form__input-label">
						<label for="sdgl_venue_name">Venue Name *</label>
					</div>
					<div class="dec-public-form__input-field">
						<?php echo $form->text('sdgl_venue_name', null, array('maxlength' => '100')); ?>
					</div>
				</div>
				
				<div class="dec-public-form__input">
					<div class="dec-public-form__input-label">
						<label for="sdgl_venue_address">Venue Address *</label>
					</div>
					<div class="dec-public-form__input-field">
						<?php echo $form->text('sdgl_venue_address', null, array('maxlength' => '350')); ?>
					</div>
				</div>
				
				<div class="dec-public-form__input">
					<div class="dec-public-form__input-field">
						<?php echo $form->checkbox('is_all_day', 1, false, array()); ?> 
						<label for="is_all_day"><?php echo t('All Day Event'); ?>?</label>
					</div>
				</div>
				
				<div class="dec-public-form__input">
					<div class="dec-public-form__input-label">
						<label for="start_date"><?php echo t('Date/Time'); ?></label>
					</div>
					<div class="dec-public-form__input-field dec-public-form__input-field--small-input">
						<div class="dec-datepair">
							<?php echo $form->text('start_date', $start_date, array('class' => 'date start')); ?>
							<?php echo $form->text('start_time', $start_time, array('class' => 'time start ui-timepicker-input', 'autocomplete' => 'off')); ?>
							<span>to</span>
							<?php echo $form->text('end_time', $end_time, array('class' => 'time end ui-timepicker-input', 'autocomplete' => 'off')); ?>
							<?php echo $form->text('end_date', $end_date, array('class' => 'date end')); ?>
						</div>
					</div>
				</div>
				
				<?php /*
				<div class="dec-public-form__input">
					<div class="dec-public-form__input-field">
						<?php echo $form->checkbox('is_repeating', 1, false, array()); ?>
						<label for="is_repeating"><?php echo t('Repeating Event'); ?>?</label>
					</div>
				</div>

				<div class="repeat_controls hide-if-no-js dec-public-form__input dec-public-form__input-field--small-input">
					<div class="dec-public-form__input-label">
						<label for="repeat_unit"><?php echo t('Repeats'); ?></label>
					</div>
					<div class="dec-public-form__input-field">
						<?php
							$repeat_unit_options = array(
								'daily' => t('Daily'),
								'weekly' => t('Weekly'),
								'monthly' => t('Monthly'),
								'yearly' => t('Yearly'),
							);
							echo $form->select('repeat_unit', $repeat_unit_options, null, array()); 
						?>
					</div>
				</div>
				
				<div class="repeat_controls hide-if-no-js dec-public-form__input dec-public-form__input-field--small-input">
					<div class="dec-public-form__input-label">
						<label for="repeat_frequency"><?php echo t('Repeat Every'); ?></label>
					</div>
					<div class="dec-public-form__input-field">
						<?php echo $form->select('repeat_frequency', array_combine(range(1, 30), range(1, 30)), null, array()); ?>
						<span id="repeat_frequency_suffix"></span>
					</div>
				</div>
					
				<div class="repeat_controls hide-if-no-js dec-public-form__input">
					<div id="repeat_weekly_on_container">
						<div class="dec-public-form__input-label">
							<?php echo t('Repeat On'); ?>
						</div>
						<div class="dec-public-form__input-field">
							<?php
								$repeat_weekly_on_options = array(
									1 => t('Monday'),
									2 => t('Tuesday'),
									3 => t('Wednesday'),
									4 => t('Thursday'),
									5 => t('Friday'),
									6 => t('Saturday'),
									7 => t('Sunday'),
								);
							?>
							<?php foreach ($repeat_weekly_on_options as $index => $label): ?>
								<input type="checkbox" name="repeat_weekly_on[]" id="repeat_weekly_on_<?php echo $index; ?>" value="<?php echo $index; ?>" <?php echo (!is_null($repeat_weekly_on) && in_array($index, $repeat_weekly_on)) ? 'checked="checked"' : ''; ?>>
								<label for="repeat_weekly_on_<?php echo $index; ?>"><?php echo $label; ?></label><br>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
				
				
				<div class="repeat_controls hide-if-no-js dec-public-form__input">
					<div id="repeat_monthly_by_container">
						<div class="dec-public-form__input-label">
							<?php echo t('Repeat By'); ?>
						</div>
						<div class="dec-public-form__input-field">
							<?php
								$repeat_monthly_by_options = array(
									'day_of_month' => t('day of the month'),
									'day_of_week_from_first' => t('day of the week'),
									'day_of_week_from_last' => t('day of the week (from end of month)'),
								);
							?>
							<?php foreach ($repeat_monthly_by_options as $index => $label): ?>
								<input type="radio" name="repeat_monthly_by" id="repeat_monthly_by_<?php echo $index; ?>" value="<?php echo $index; ?>" <?php echo ($index == $repeat_monthly_by) ? 'checked="checked"' : ''; ?>>
								<label for="repeat_monthly_by_<?php echo $index; ?>"><?php echo $label; ?></label><br>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
				
				
				<div class="repeat_controls hide-if-no-js dec-public-form__input">
					<div class="dec-public-form__input-label">
						<?php echo t('Ends'); ?>
					</div>
					<div class="dec-public-form__input-field dec-public-form__input-field--small-input">
						<div id="repeat_ends_never_wrapper">							
							<?php echo $form->radio('repeat_ends', 'never', null, array()); ?>
							<label for="repeat_ends1"><?php echo t('Never'); ?></label>
						</div>
					</div>
					<div class="dec-public-form__input-field dec-public-form__input-field--small-input">
						<?php echo $form->radio('repeat_ends', 'after', null, array()); ?>
						<label for="repeat_ends2"><?php echo t('After'); ?></label>
						<?php echo $form->text('repeat_ends_after_count', null, array()); ?>
						<label for="repeat_ends_after_count"><?php echo t('Occurrences'); ?></label>
					</div>
					<div class="dec-public-form__input-field dec-public-form__input-field--small-input">
						<?php echo $form->radio('repeat_ends', 'on', null, array()); ?>
						<label for="repeat_ends3"><?php echo t('On'); ?></label>
						<span class="dec-datepicker">
							<?php echo $form->text('repeat_ends_on_date', $repeat_ends_on_date, array('class' => 'date')); ?>
						</span>
					</div>
				</div>
							
				<div class="repeat_controls hide-if-no-js dec-public-form__input dec-public-form__input--repeat-summary">
					<div class="dec-public-form__input-label">
						<?php echo t('Repeat Summary'); ?>
					</div>
					<div class="dec-public-form__input-field">
						<span id="repeat_summary"></span>
					</div>
				</div>
				*/ ?>
			
				<div class="dec-public-form__input">
					<div class="dec-public-form__input-label">
						<label for="description"><?php echo t('Description'); ?></label>
					</div>
					<div class="dec-public-form__input-field">
						<?php /* Loader::element('editor_controls'); */ ?>
						<?php /* echo $form->textarea('description', $description, array('class' => 'ccm-advanced-editor')); */ ?>
						<?php echo $form->textarea('description', $description, array()); ?>
					</div>
					<p style="font-style: italic; line-height: 1.2;">Please include info about admission fees (if applicable), wheelchair access, and whether or not this is an annual event.</p>
				</div>
				
				<?php /*
				<div class="dec-public-form__input">
					<div class="dec-public-form__input-label">
						<?php echo t('Categories'); ?>
					</div>
					<div class="dec-public-form__input-field">
						<?php foreach ($categories as $index=>$category): ?>
							<input type="checkbox" name="category_ids[]" id="category_ids_<?php echo $index; ?>" value="<?php echo $category['id']; ?>" <?php echo empty($category['has']) ? '' : 'checked="checked"'; ?>>
							<label for="category_ids_<?php echo $index; ?>"><?php echo $th->entities($category['name']); ?></label><br>
						<?php endforeach; ?>
					</div>
				</div>
				*/ ?>
				
			</fieldset>
			
			<fieldset class="dec-public-form__fieldset">
				<legend class="dec-public-form__fieldset-legend">Contact Information</legend>
				
				<p style="font-style: italic;">For internal use only -- will not be publicly displayed.</p>
				
				<div class="dec-public-form__input-grouping">
					<div class="dec-public-form__input">
						<div class="dec-public-form__input-label">
							<label for="submitted_by"><?php echo t('Business / Organization Name'); ?> *</label>
						</div>
						<div class="dec-public-form__input-field">
							<?php echo $form->text('submitted_by', null, array('maxlength' => '255')); ?>
						</div>
					</div>
					<div class="dec-public-form__input">
						<div class="dec-public-form__input-label">
							<label for="submitter_name"><?php echo t('Contact Person Name'); ?> *</label>
						</div>
						<div class="dec-public-form__input-field">
							<?php echo $form->text('submitter_name', null, array('maxlength' => '255')); ?>
						</div>
					</div>
				</div>
				<div class="dec-public-form__input-grouping">
					<div class="dec-public-form__input">
						<div class="dec-public-form__input-label">
							<label for="submitter_email"><?php echo t('Contact Person Email'); ?> *</label>
						</div>
						<div class="dec-public-form__input-field">
							<?php echo $form->text('submitter_email', null, array('maxlength' => '255')); ?>
						</div>
					</div>
					<div class="dec-public-form__input">
						<div class="dec-public-form__input-label">
							<label for="submitter_phone"><?php echo t('Contact Person Phone #'); ?> *</label>
						</div>
						<div class="dec-public-form__input-field">
							<?php echo $form->text('submitter_phone', null, array('maxlength' => '255')); ?>
						</div>
					</div>
				</div>
			</fieldset>
			
			<?php if (defined('DEC_PUBLIC_SUBMISSIONS_RECAPTCHA_ENABLED') && DEC_PUBLIC_SUBMISSIONS_RECAPTCHA_ENABLED) { ?>
				<script src="https://www.google.com/recaptcha/api.js" async defer></script>
				<div class="g-recaptcha" data-sitekey="<?=DEC_PUBLIC_SUBMISSIONS_RECAPTCHA_SITE_KEY?>"></div>
				<br>
			<?php } ?>
			
			<div class="dec-public-form__actions">
				<button>Submit</button>
			</div>

		
	</form>
	
<?php /* echo $dh->getDashboardPaneFooterWrapper(); */ ?>
</div>
	
<script type="text/javascript">
	$(document).ready(function() {
		$('#is_all_day').on('change', toggleTimePickers);
		toggleTimePickers();
		
		$('#is_repeating').on('change', toggleRepeatControls);
		toggleRepeatControls();
		
		$('#repeat_unit').on('change', updateRepeatFrequencyLabel);
		updateRepeatFrequencyLabel();
		
		$('#repeat_unit').on('change', toggleRepeatWeeklyMonthlyOptions);
		toggleRepeatWeeklyMonthlyOptions();
		
		$('input[name="repeat_ends"], #end_date').on('change', toggleRepeatEndsOptions);
		$('#start_date').on('change', function() { setTimeout(toggleRepeatEndsOptions, 100); }); //give datepair.js a chance to update the "to" date first
		toggleRepeatEndsOptions();
		
		$('.repeat_controls input, .repeat_controls select, #is_repeating, #start_date').on('change', updateRepeatSummary);
		updateRepeatSummary();
	});
	
	function toggleTimePickers() {
		var is_all_day = $('#is_all_day').prop('checked');
		$('#start_time, #end_time').toggle(!is_all_day);
	}
	
	function toggleRepeatControls() {
		var is_repeating = $('#is_repeating').prop('checked');
		$('.repeat_controls').toggle(is_repeating);
	}
	
	function updateRepeatFrequencyLabel() {
		var localized_labels = {
			'daily': '<?php echo t('days'); ?>',
			'weekly': '<?php echo t('weeks'); ?>',
			'monthly': '<?php echo t('months'); ?>',
			'yearly': '<?php echo t('years'); ?>'
		};
		var repeat_unit = $('#repeat_unit').val();
		$('#repeat_frequency_suffix').html(localized_labels[repeat_unit]);
	}
	
	function toggleRepeatWeeklyMonthlyOptions() {
		var repeat_unit = $('#repeat_unit').val();
		var is_weekly = (repeat_unit == 'weekly');
		var is_monthly = (repeat_unit == 'monthly');
		$('#repeat_weekly_on_container').toggle(is_weekly);
		$('#repeat_monthly_by_container').toggle(is_monthly);
	}
	
	function toggleRepeatEndsOptions() {
		var repeat_ends = $('input[name="repeat_ends"]:checked').val();
		
		if (repeat_ends == 'after') {
			$('#repeat_ends_after_count').removeAttr('disabled');
		} else {
			$('#repeat_ends_after_count').attr('disabled', 'disabled');
		}
		
		if (repeat_ends == 'on') {
			$('#repeat_ends_on_date').removeAttr('disabled');
		} else {
			$('#repeat_ends_on_date').attr('disabled', 'disabled');
		}
		
		//show/hide "never" option depending on if this is a multiday event or not
		var $repeat_ends_never_wrapper = $('#repeat_ends_never_wrapper');
		var $repeat_ends_never_control = $('input[name="repeat_ends"][value="never"]');
		var is_multi_day = Boolean(new Date($('#end_date').val()) - new Date($('#start_date').val()));
		if (is_multi_day) {
			$repeat_ends_never_wrapper.hide();
			$repeat_ends_never_control.prop('checked', false);
		} else {
			$repeat_ends_never_wrapper.show();
			$repeat_ends_never_control.prop('checked', (typeof(repeat_ends) === 'undefined' || repeat_ends == 'never'));
		}
	}
	
	<?php /* WARNING: THIS LOGIC IS DUPLICATED IN THE BACK-END PHP CODE (models/event.php) */ ?>
	function updateRepeatSummary() {
		var summary = '';
		
		if ($('#is_repeating').prop('checked')) {
			<?php /* Note that Sunday is repeated, to account for the difference between php (7) and JS (0) */ ?>
			var days_of_week = ['<?php echo t('Sunday'); ?>', '<?php echo t('Monday'); ?>', '<?php echo t('Tuesday'); ?>', '<?php echo t('Wednesday'); ?>', '<?php echo t('Thursday'); ?>', '<?php echo t('Friday'); ?>', '<?php echo t('Saturday'); ?>', '<?php echo t('Sunday'); ?>'];
			
			var start_date = new Date($('#start_date').val());
			
			var repeat_unit = $('#repeat_unit').val();
			var repeat_frequency = parseInt($('#repeat_frequency').val());
			if (repeat_unit == 'daily') {
				if (repeat_frequency == 1) {
					summary += '<?php echo t('Daily'); ?>';
				} else {
					summary += '<?php echo t('Every'); ?> ' + repeat_frequency + ' <?php echo t('days'); ?>';
				}
			} else if (repeat_unit == 'weekly') {
				if (repeat_frequency == 1) {
					summary += '<?php echo t('Weekly'); ?>';
				} else {
					summary += '<?php echo t('Every'); ?> ' + repeat_frequency + ' <?php echo t('weeks'); ?>';
				}
				
				var $repeat_weekly_on_selections = $('input[name="repeat_weekly_on[]"]:checked');
				if ($repeat_weekly_on_selections.length == 0) {
					var start_date_day_of_week = start_date.getDay();
					summary += ' <?php echo t('on'); ?> ' + days_of_week[start_date_day_of_week];
				} else if ($repeat_weekly_on_selections.length == 7) {
					summary += ' <?php echo t('on all days'); ?>';
				} else {
					summary += ' <?php echo t('on'); ?> ' + $repeat_weekly_on_selections.map(function() { return days_of_week[$(this).val()] }).get().join(', ');
				}
			} else if (repeat_unit == 'monthly') {
				if (repeat_frequency == 1) {
					summary += '<?php echo t('Monthly'); ?>';
				} else {
					summary += '<?php echo t('Every'); ?> ' + repeat_frequency + ' <?php echo t('months'); ?>';
				}
				
				var repeat_monthly_by = $('input[name="repeat_monthly_by"]:checked').val();
				var start_date_day_of_month = start_date.getDate();
				var start_date_day_of_week = start_date.getDay();
				
				if (repeat_monthly_by == 'day_of_week_from_first') {
					var start_date_day_of_week_ordinal = Math.floor(start_date_day_of_month / 7);
					var ordinal_names = ['<?php echo t('first'); ?>', '<?php echo t('second'); ?>', '<?php echo t('third'); ?>', '<?php echo t('fourth'); ?>', '<?php echo t('fifth'); ?>', '<?php echo t('sixth'); ?>'];
					summary += ' <?php echo t('on the'); ?> ' + ordinal_names[start_date_day_of_week_ordinal] + ' ' + days_of_week[start_date_day_of_week];
				} else if (repeat_monthly_by == 'day_of_week_from_last') {
					var month = start_date.getMonth();
					var year = start_date.getFullYear();
					var last_day_of_month = new Date(year, month + 1, 0).getDate(); //YES, this works even if month + 1 goes into next year!
					var start_date_ordinal_from_end_of_month = Math.floor((last_day_of_month - start_date_day_of_month) / 7);
					var ordinal_names = ['<?php echo t('last'); ?>', '<?php echo t('second-to-last'); ?>', '<?php echo t('third-to-last'); ?>', '<?php echo t('fourth-to-last'); ?>', '<?php echo t('fifth-to-last'); ?>', '<?php echo t('sixth-to-last'); ?>'];
					summary += ' <?php echo t('on the'); ?> ' + ordinal_names[start_date_ordinal_from_end_of_month] + ' ' + days_of_week[start_date_day_of_week];
				} else {
					summary += ' <?php echo t('on day'); ?> ' + start_date_day_of_month;
				}
			} else if (repeat_unit == 'yearly') {
				if (repeat_frequency == 1) {
					summary += '<?php echo t('Annually'); ?>';
				} else {
					summary += '<?php echo t('Every'); ?> ' + repeat_frequency + ' <?php echo t('years'); ?>';
				}
				var month_names = ['<?php echo t('January'); ?>', '<?php echo t('February'); ?>', '<?php echo t('March'); ?>', '<?php echo t('April'); ?>', '<?php echo t('May'); ?>', '<?php echo t('June'); ?>', '<?php echo t('July'); ?>', '<?php echo t('August'); ?>', '<?php echo t('September'); ?>', '<?php echo t('October'); ?>', '<?php echo t('November'); ?>', '<?php echo t('December'); ?>'];
				var month_number = start_date.getMonth();
				var day_number = start_date.getDate();
				summary += ' <?php echo t('on'); ?> ' + month_names[month_number] + ' ' + day_number;
			}
			
			var repeat_ends = $('input[name="repeat_ends"]:checked').val();
			if (repeat_ends == 'after') {
				var repeat_ends_after_count = $('#repeat_ends_after_count').val();
				if (repeat_ends_after_count.length) {
					repeat_ends_after_count = parseInt(repeat_ends_after_count);
					if (repeat_ends_after_count > 1) {
						summary += ', ' + repeat_ends_after_count + ' <?php echo t('times'); ?>';
					} else if (repeat_ends_after_count == 1) {
						summary = '<?php echo t('Once'); ?>'; //override the entire string (this is the same as not repeating at all)
					}
				}
			} else if (repeat_ends == 'on') {
				var repeat_ends_on_date = $('#repeat_ends_on_date').val();
				if (repeat_ends_on_date.length) {
					summary += ', <?php echo t('until'); ?> ' + (new Date(repeat_ends_on_date)).toDateString();
				}
			}
		}
		
		$('#repeat_summary').html(summary);
	}
</script>
