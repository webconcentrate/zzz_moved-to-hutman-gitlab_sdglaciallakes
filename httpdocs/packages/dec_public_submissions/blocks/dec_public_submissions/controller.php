<?php defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('event', 'designer_event_calendar');

class DecPublicSubmissionsBlockController extends BlockController {
	
	protected $btName = 'Event Calendar Public Submission Form';
	
	protected $btCacheBlockRecord = false;
	protected $btCacheBlockOutput = false;
	protected $btCacheBlockOutputOnPost = false;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;
	
	public function on_page_view() {
		//date/time picker...
		$html = Loader::helper('html');
		$this->addHeaderItem($html->css('../js/datetimepicker/datetimepair.min.css', 'designer_event_calendar'));
		$this->addFooterItem($html->javascript('datetimepicker/datetimepair.min.js', 'designer_event_calendar'));
	}
	
	public function view() {
		if ($this->get('action_submit_was_called')) {
			return;
		}
		
		$this->initCSRFToken();
		
		if (array_key_exists('dec_public_submissions_success', $_SESSION)) {
			unset($_SESSION['dec_public_submissions_success']);
			$this->set('success', true);
		}
		
		$default_start_ts = $this->getDefaultStartTimestamp();
		$start_date = date(DEC_DATETIMEPICKER_DATE_FORMAT, $default_start_ts);
		$start_time = date(DEC_DATETIMEPICKER_TIME_FORMAT, $default_start_ts);
		$default_end_ts = $default_start_ts + 3600; //1 hour past default start time
		$end_date = date(DEC_DATETIMEPICKER_DATE_FORMAT, $default_end_ts);
		$end_time = date(DEC_DATETIMEPICKER_TIME_FORMAT, $default_end_ts);
		$this->set('start_date', $start_date);
		$this->set('start_time', $start_time);
		$this->set('end_date', $end_date);
		$this->set('end_time', $end_time);
		
		$this->set('categories', $this->getCategories());
	}
	
	public function action_submit() {
		$this->initCSRFToken();

		$submitter_info_fields = array(
			'submitter_name' => 'Contact Name',
			'submitter_email' => 'Contact Email',
			'submitter_phone' => 'Contact Phone #',
		);
		
		$post = $this->post();
		if ($post) {
			//Since 'description' is a WYSIWYG field on the back-end,
			// its data is not html-escaped on output...
			// but we only provide a plain textbox on the public submission form
			// so we must remove any html tags to prevent XSS attacks.
			$post['description'] = strip_tags($post['description']);
			
			$error = EventRecord::validatePOST($post);
			if (empty($post['submitted_by'])) {
				$error->add('Business/Organization Name is required');
			}
			
			foreach ($submitter_info_fields as $field_name => $field_label) {
				if (empty($post[$field_name])) {
					$error->add($field_label . ' is required');
				}
			}
			
			//Validate SDGL-specific fields
			$sdgl_venue_fields = array(
				'sdgl_venue_name' => 'Venue Name',
				'sdgl_venue_address' => 'Venue Address',
				'sdgl_event_phone' => 'Event Contact Phone #',
			);
			foreach ($sdgl_venue_fields as $field_name => $field_label) {
				if (empty($post[$field_name])) {
					$error->add($field_label . ' is required');
				}
			}
			
			if (defined('DEC_PUBLIC_SUBMISSIONS_RECAPTCHA_ENABLED') && DEC_PUBLIC_SUBMISSIONS_RECAPTCHA_ENABLED) {
				Loader::library('recaptcha/vendor/google/recaptcha/src/autoload');
				if (empty($_POST['g-recaptcha-response'])) {
					$error->add('You must complete the spammer verification (reCAPTCHA check).');
				} else {
					$recaptcha = new \ReCaptcha\ReCaptcha(DEC_PUBLIC_SUBMISSIONS_RECAPTCHA_SECRET_KEY);
					$resp = $recaptcha->verify($_POST['g-recaptcha-response'], Loader::helper('validation/ip')->getRequestIP());
					if (!$resp->isSuccess()) {
						$error->add('Our spam/abuse detection tool flagged this submission. If you believe this is an error, please contact us.');
					}
					/*
					ob_start();
					print_r($resp);
					$log_content = ob_get_contents();
					ob_end_clean();
					$log_content .= "\r\n";
					$log_file = dirname(__FILE__) . '/log.txt';
					touch($log_file);
					$fp = fopen($log_file, 'a');
					fwrite($fp, $log_content);
					fclose($fp);
					*/
				}
			}
	
			
		//Validation failed (so re-display the form)...
			if ($error->has()) {
				$this->set('error', $error);
				
				//C5 form helpers will automatically repopulate form fields from $_POST data
				
				//but we need to manually repopulate radio buttons...
				$this->set('repeat_weekly_on', $post['repeat_weekly_on']);
				$this->set('repeat_monthly_by', $post['repeat_monthly_by']);
				
				//...and the checkbox list of categories
				$categories = $this->getCategories();
				$chosen_category_ids = $this->post('category_ids', array());
				foreach ($categories as $key => $category) {
					$categories[$key]['has'] = in_array($category['id'], $chosen_category_ids);
				}
				$this->set('categories', $categories);
				
				
				$this->set('action_submit_was_called', true); //work around C5 weirdness (when view() is subsequently called as a separate instance)
				
		//Validation passed (so save to db and redirect)...
			} else {
				$submitter_info = array();
				foreach ($submitter_info_fields as $field_name => $field_label) {
					$submitter_info[] = $field_label . ': ' . $post[$field_name];
				}
				$submitter_info = implode("\n", $submitter_info);
				
				//Combine 3 SDGL-specific fields into the event subtitle field:
				$sdgl_venue_info = array();
				foreach ($sdgl_venue_fields as $field_name => $field_label) {
					$sdgl_venue_info[] = $this->post($field_name);
				}
				$post['subtitle'] = implode(' - ', $sdgl_venue_info);
				
				//Append event website URL to description
				if (!empty($post['sdgl_event_website'])) {
					$sdgl_event_website = h(trim(strip_tags($post['sdgl_event_website'])));
					if ($sdgl_event_website) {
						$post['description'] .= ' <p><a href="' . $sdgl_event_website . '" target="_blank" rel="noopener">' . $sdgl_event_website . '</a></p>';
					}
				}
				
				$event = EventRecord::loadFromPOST($post);
				$submission_id = $this->saveEvent($event, $post['submitted_by'], $submitter_info);
				$this->sendNotificationEmail($submission_id);
				$_SESSION['dec_public_submissions_success'] = 1;
				$this->redirectToViewAndExit();
			}
		} else {
			$this->redirectToViewAndExit();
		}
	}
	
	private function getCategories() {
		Loader::model('category', 'designer_event_calendar');
		return CategoryModel::factory()->getAll();
	}
	
	private function getCalendarOptions() {
		Loader::model('calendar', 'designer_event_calendar');
		return CalendarModel::factory()->getSelectOptions(array(0 => '&lt;Choose One&gt;'));
	}
	
	private function getDefaultStartTimestamp() {
		//default the start time to an hour after the next half-hour increment
		// (e.g. if current time is between 7:00-7:29 then default to 8:00,
		//  or if current time is between 7:30-7:59 then default to 8:30)
		$now_ts = time();
		$diff_between_now_and_previous_half_hour = $now_ts % 1800;
		$previous_half_hour_ts = $now_ts - $diff_between_now_and_previous_half_hour;
		$next_hour_after_previous_half_hour = $previous_half_hour_ts + 3600;
		return $next_hour_after_previous_half_hour;
	}

	private function redirectToViewAndExit() {
		$c = Page::getCurrentPage();
		$nh = Loader::helper('navigation');
		$url = $nh->getLinkToCollection($c, true); //pass true as 2nd arg to get full absolute url (with BASE_URL), which is required for "location" headers
		header("Location: {$url}");
		exit;
	}
	
	private function initCSRFToken() {
		$token = Loader::helper('validation/token');
		if (!empty($_POST) && !$token->validate()) {
			die($token->getErrorMessage());
		}
		$this->set('token', $token->output('', true));
	}
		
	private function saveEvent(&$event, $submitted_by, $submitter_info) {
		$record = array(
			'submitted_at' => date('Y-m-d H:i:s'),
			'submitted_by' => $submitted_by,
			'submitter_info' => $submitter_info,
			'title' => $event->getTitle(),
			'subtitle' => $event->getSubtitle(),
			'description' => nl2br($event->getDescription()),
			'start_date' => $event->getStartFormatted('Y-m-d'),
			'start_time' => $event->getStartFormatted('H:i:s'),
			'end_date' => $event->getEndFormatted('Y-m-d'),
			'end_time' => $event->getEndFormatted('H:i:s'),
			'is_all_day' => $event->isAllDay(),
			'is_repeating' => $event->isRepeating(),
			'repeat_unit' => $event->getRepeatUnit(),
			'repeat_frequency' => $event->getRepeatFrequency(),
			'repeat_weekly_on' => implode(',', $event->getRepeatWeeklyOn()),
			'repeat_monthly_by' => $event->getRepeatMonthlyBy(),
			'repeat_ends' => $event->getRepeatEnds(),
			'repeat_ends_after_count' => $event->getRepeatEndsAfterCount(),
			'repeat_ends_on_date' => $event->getRepeatEndsOnFormatted(DEC_DATETIMEPICKER_DATE_FORMAT),
			'category_ids' => implode(',', $event->getCategoryIds()),
		);
		
		$db = Loader::db();
		$db->AutoExecute('dec_public_submissions', $record, 'INSERT');
		return $db->Insert_ID();
	}
	
	private function sendNotificationEmail($submission_id) {
		//Get submission record from db
		$sql = "SELECT * FROM dec_public_submissions WHERE submission_id = ?";
		$vals = array((int)$submission_id);
		$record = Loader::db()->GetRow($sql, $vals);
		
		//Convert the submission data into an event object
		$record['repeat_weekly_on'] = explode(',', $record['repeat_weekly_on']);
		$record['category_ids'] = explode(',', $record['category_ids']);
		if (EventRecord::validatePOST($record)->has()) {
			return;
		}
		$event = EventRecord::loadFromPOST($record);
		
		//Send email
		$mh = Loader::helper('mail');
		$mh->to(DEC_PUBLIC_SUBMISSIONS_NOTIFICATION_EMAIL_TO);
		$mh->from(EMAIL_DEFAULT_FROM_ADDRESS);
		
		$mh->addParameter('submitted_by', $record['submitted_by']);
		$mh->addParameter('submitter_info', $record['submitter_info']);
		
		$mh->addParameter('event', $event);
		
		$mh->load('admin_notify', 'dec_public_submissions');
		@$mh->sendMail(); 
	}
	
}