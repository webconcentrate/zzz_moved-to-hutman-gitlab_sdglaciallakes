<?php defined('C5_EXECUTE') or die(_("Access Denied."));

if (isset($success)) {
	include('view/success.php');
} else {
	include('view/form.php');
}
