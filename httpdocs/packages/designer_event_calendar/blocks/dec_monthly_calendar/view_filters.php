<?php defined('C5_EXECUTE') or die("Access Denied.");

/*
Note that we are not using C5's form helpers,
because they automatically populate with values
in $_GET (which might be from another block instance
if there were 2 blocks of this type on the same page).

Other notes (see number references in code below):

[1] Leave the form action blank (do not use $this->action),
    because method="GET" forms do not work with $this->action()
    (because it relies on putting querystring args into the url,
    which the browser will replace with actual form fields upon submission).

[2] Include the 'cID' in a hidden field in case the page is
    being viewed at its "non-pretty" url (e.g. index.php?cID=147).

[3] Include the 'bID' in a hidden field so the controller knows
    which block should be filtered (in case there are 2 or more blocks
    of this type on the same page). UPDATE: We also need it for ajax!

[4] Submit button is in a <noscript> tag because
    the form auto-submits if javascript is enabled.

*/

if (!$show_category_filter) {
	return;
}
?>

<form class="js-ajax-category-form" method="get" action="<?php /*[1]*/ ?>">
	
	<?php if (!empty($_GET['cID'])): /*[2]*/ ?>
		<input type="hidden" name="cID" value="<?php echo intval($_GET['cID']); ?>" />
	<?php endif; ?>
	
	<input type="hidden" name="bID" value="<?php echo $bID; /*[3]*/ ?>" />
	<input type="hidden" name="year" value="<?php echo $current_year; ?>" />
	<input type="hidden" name="month" value="<?php echo $current_month; ?>" />
	
	<img class="js-ajax-loading-indicator" style="display: none;" src="<?=ASSETS_URL_IMAGES?>/throbber_white_16.gif" width="16" height="16" alt="">
	
	<select name="cat">
		<?php foreach ($category_options as $id => $h_name): ?>
			<option value="<?php echo $id; ?>" <?php echo ($id == $filter_category_id) ? 'selected="selected"' : ''; ?>>
				<?php echo $h_name; ?>
			</option>
		<?php endforeach; ?>
	</select>
	
	<noscript><?php /*[4]*/ ?>
		<input type="submit" value="<?php echo t('Go'); ?>" />
	</noscript>
</form>
