<?php defined('C5_EXECUTE') or die("Access Denied.");

if (empty($_GET['bID']) || ((int)$_GET['bID'] < 1)) {
	return;
}

$b = Block::GetById($_GET['bID']);
if (!$b) {
	return;
}

$bc = new DecMonthlyCalendarBlockController($b);

//this is so hacky, but we need to work around block caching...
$bc->set('isAjaxNav', true);
$bc->view();
extract($bc->getSets());
require __DIR__ . '/../view.php';

exit;