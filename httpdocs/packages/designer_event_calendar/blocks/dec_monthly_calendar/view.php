<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<div class="js-dec-monthly-calendar-ajax-navigation-wrapper">
	<div class="calendar js-ajax-content">
		<table>
			<caption>
				<div>
					<div>
						<?php echo gmdate('F Y', $current_timestamp); ?>
						<a href="<?php echo $pagination_prev_link; ?>" class="prev-month js-ajax-nav">&#x25C4;</a>
						<a href="<?php echo $pagination_next_link; ?>" class="next-month js-ajax-nav">&#x25BA;</a>
						<img class="js-ajax-loading-indicator" style="display: none;" src="<?=ASSETS_URL_IMAGES?>/throbber_white_16.gif" width="16" height="16" alt="">
					</div>
					<?php include dirname(__FILE__) . '/view_filters.php'; ?>
				</div>
			</caption>
			<thead>
				<tr>
					<?php foreach (array_slice(array_keys($events_per_day), 0, 7) as $day_timestamp) { ?>
						<th><?php echo gmdate('D', $day_timestamp); ?></th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php
				$column = 1;
				foreach ($events_per_day as $day_timestamp => $day_events) {
					if ($column == 1) {
						echo "<tr>\n";
					}
					
					$is_current_month = ($current_month == gmdate('n', $day_timestamp));
					
					if ($is_current_month) {
						echo '<td>';
					} else {
						echo '<td class="off">';
					}
					
					echo '<div class="day-num">' . gmdate('j', $day_timestamp) . '</div>';
					
					if ($day_events && $is_current_month) {
						echo '<ul>';
						foreach ($day_events as $event) {
							echo ($event->isAllDay() ? '<li class="all-day">' : '<li>');
							
							if ($event->isAllDay()) {
								//do not output a time for all-day events
							} else if ($event->getDayInSequence() > 1) {
								echo '12a'; //event continues from previous day
							} else {
								//output compact time format (e.g. "3p" or "12:30a")...
								$start_hour = $event->getStartFormatted('g');
								$start_minute = (int)$event->getStartFormatted('i');
								$start_ampm = rtrim($event->getStartFormatted('a'), 'm'); //remove trailing "m" from "am" or "pm" (so we only show "3p" instead of "3pm", for example)
								
								echo $start_hour;
								if ($start_minute > 0) {
									echo ':' . $start_minute;
								}
								echo $start_ampm;
							}
							
							echo ' ';
							
							if ($event->isLinkToNothing()) {
								echo $event->getTitle();
							} else {
								echo '<a href="' . $event->getLinkToDetailsPage() . '">' . $event->getTitle() . '</a>';
							}
							
							echo '</li>';
						}
						echo '</ul>';
					}
					
					echo "</td>\n";
					
					if ($column == 7) {
						echo "</tr>\n";
					}
					
					$column = ($column == 7 ? 1 : $column + 1);
				}
				?>
			</tbody>
		</table>
	</div>
</div>