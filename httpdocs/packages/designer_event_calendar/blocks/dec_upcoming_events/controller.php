<?php defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('category', 'designer_event_calendar');

class DecUpcomingEventsBlockController extends BlockController {

	public function getBlockTypeName() { return t('Upcoming Events'); }
	public function getBlockTypeDescription() { return t('Displays a limited number of calendar events that are happening soon.'); }
	
	protected $btInterfaceWidth = 400;
	protected $btInterfaceHeight = 400;
	
	protected $btTable = 'btDecUpcomingEvents';
	protected $btCacheBlockRecord = true;
	
	//disable output caching because different events will be shown depending on the time of request
	protected $btCacheBlockOutput = false;
	protected $btCacheBlockOutputOnPost = false;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;
	
	private $default_event_count = 3;
	
	public function add() {
		//set default values for new blocks
		$this->set('event_count', $this->default_event_count);
		$this->set('category_ids', array());
		
		$this->edit();
	}
	
	public function edit() {
		$this->set('category_options', CategoryModel::factory()->getSelectOptions());
		$this->set('category_ids', $this->getCategoryIdsArray());
	}
	
	public function save($args) {
		//use default if event_count is invalid (instead of showing error message to user)
		$args['event_count'] = ctype_digit($args['event_count']) ? (int)$args['event_count'] : $this->default_event_count;
		
		//convert checked categories array into a comma-separated string
		$safe_category_ids = array();
		if (!empty($args['category_ids']) && is_array($args['category_ids'])) {
			foreach ($args['category_ids'] as $category_id) {
				$category_id = (int)$category_id;
				if ($category_id > 0) {
					$safe_category_ids[] = $category_id;
				}
			}
		}
		$args['category_ids'] = implode(',', $safe_category_ids);
		
		parent::save($args);
	}
	
	public function view() {
		Loader::model('event', 'designer_event_calendar');
		
		$from_date = date(DEC_DATETIMEPICKER_DATE_FORMAT); //today
		
		$list_category_ids = $this->getCategoryIdsArray();

		$events = EventOccurrence::getByCount($from_date, $this->event_count, $list_category_ids);
		$this->set('events', $events);
	}
	
	private function getCategoryIdsArray() {
		$category_ids = array();
		if (!empty($this->category_ids)) {
			$category_ids = explode(',', $this->category_ids);
		}
		return $category_ids;
	}
}