<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<div class="dec-upcoming-events-container">
	
	<?php if (count($events)): ?>
		<div class="dec-upcoming-events-list">
		<?php foreach ($events as $event): ?>
			<div class="dec-upcoming-event-item">
				<h3><?php echo $event->getDayInSequenceFormatted('M jS'); ?></h3>
				
				<p><?php
				$start_date = $event->getStartFormatted('M jS');
				$start_time = $event->getStartFormatted('g:i a');
				$end_date = $event->getEndFormatted('M jS');
				$end_time = $event->getEndFormatted('g:i a');
				
				if ($event->isMultiDay() && $event->isAllDay()) {
					echo $start_date t('%s thru %s (all day)', $start_date, $end_date);
				} else if ($event->isMultiDay() && !$event->isAllDay()) {
					echo $start_time . ' on ' . $start_date . ' thru ';
					if (!$event->isEndTimeHidden()) {
						echo $end_time . ' on ';
					}
					echo $end_date;
				} else if (!$event->isMultiDay() && $event->isAllDay()) {
					echo $start_date .' (all day)';
				} else if (!$event->isMultiDay() && !$event->isAllDay()) {
					echo $start_date;
					if ($event->isEndTimeHidden()) {
						echo ' at ' . $start_time;
					} else {
						echo ', ' . $start_time . ' - ' . $end_time;
					}
				}
				?></p>
				
				<h4><?php
					echo $event->isLinkToNothing() ? $event->getTitle() : '<a href="' . $event->getLinkToDetailsPage() . '">' . $event->getTitle() . '</a>';
				?></h4>
				
				<?php if ($event->hasSubtitle()): ?>
					<h5><i><?php echo $event->getSubtitle(); ?></i></h5>
				<?php endif; ?>
				
				<?php if ($event->hasLocation()): ?>
					<p><?php echo $event->getLocationLinkTag(); ?></p>
				<?php endif; ?>
				
			</div><!-- .dec-upcoming-event-item -->
		<?php endforeach; ?>
		</div><!-- dec-upcoming-events-list -->
		
	<?php else: ?>
		<p><em><?php echo t('No upcoming events.'); ?></em></p>
	<?php endif; ?>
	
</div><!-- .dec-upcoming-events-container -->