<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<div class="event-list__pagination">
	<span class="event-list__pagination-previous"><?php
		if ($pagination_prev_link) {
			echo '<a href="' . $pagination_prev_link . '" class="js-ajax-nav ' . ($ajax_nav_is_below_content ? 'js-ajax-nav-scroll-on-click' : '') . '">Past Events</a>';
		} else {
			echo 'Past Events';
		}
	?></span>
	<span class="event-list__pagination-next"><?php
		if ($pagination_next_link) {
			echo '<a href="' . $pagination_next_link . '" class="js-ajax-nav ' . ($ajax_nav_is_below_content ? 'js-ajax-nav-scroll-on-click' : '') . '">Future Events</a>';
		} else {
			echo 'Future Events';
		}
	?></span>
	<img class="js-ajax-loading-indicator" style="display: none;" src="<?=ASSETS_URL_IMAGES?>/throbber_white_16.gif" width="16" height="16" alt="">
</div>
