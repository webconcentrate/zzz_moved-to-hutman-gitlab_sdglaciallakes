<?php defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('category', 'designer_event_calendar');

class DecEventListBlockController extends BlockController {

	public function getBlockTypeName() { return t('Event List'); }
	public function getBlockTypeDescription() { return t('Displays a list of calendar events.'); }
	
	protected $btInterfaceWidth = 500;
	protected $btInterfaceHeight = 400;
	
	protected $btTable = 'btDecEventList';
	protected $btCacheBlockRecord = true;
	
	//Note that output caching is disabled because different filter selections will show different events.
	// (If we only cared about ajax then we could enable it, but we want it to work sans javascript too).
	protected $btCacheBlockOutput = false;
	protected $btCacheBlockOutputOnPost = false;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;
	
	private $default_day_count = 7;
	
	
	//////////////
	// ADD/EDIT //
	///////////////////////////////////////////////////////////////////////////
	
	public function add() {
		//set default values for new blocks
		$this->set('day_count', $this->default_day_count);
		$this->set('category_ids', array());
		$this->set('show_category_filter', 1);
		$this->set('show_event_descriptions', 1);
		
		$this->edit();
	}
	
	public function edit() {
		$this->set('category_options', CategoryModel::factory()->getSelectOptions());
		$this->set('category_ids', $this->getCategoryIdsArray());
		$this->set('allow_description_hiding', $this->allowDescriptionHiding());
	}
	
	public function save($args) {
		//use default if day_count is invalid (instead of showing error message to user)
		$args['day_count'] = ctype_digit($args['day_count']) ? (int)$args['day_count'] : $this->default_day_count;
		$args['day_count'] = ($args['day_count'] > 90) ? 90 : $args['day_count'];
		
		//convert checked categories array into a comma-separated string
		$safe_category_ids = array();
		if (!empty($args['category_ids']) && is_array($args['category_ids'])) {
			foreach ($args['category_ids'] as $category_id) {
				$category_id = (int)$category_id;
				if ($category_id > 0) {
					$safe_category_ids[] = $category_id;
				}
			}
		}
		$args['category_ids'] = implode(',', $safe_category_ids);
		
		parent::save($args);
	}
	
	
	//////////
	// VIEW //
	///////////////////////////////////////////////////////////////////////////	
	
	public function on_page_view() {
		//Load the datepicker widget js+css
		$html = Loader::helper('html');
		$this->addHeaderItem($html->css('../js/datetimepicker/datepickeronly.min.css', 'designer_event_calendar'));
		$this->addFooterItem($html->javascript('datetimepicker/datepickeronly.min.js', 'designer_event_calendar'));
	}
	
	public function view() {
		Loader::model('event', 'designer_event_calendar');
		
		//if block caching is enabled,
		// then only apply filters for ajax requests
		// (otherwise C5 might cache a non-default page view
		// depending on what's being shown whenever cache goes stale)
		if ($this->btCacheBlockOutput && !$this->get('isAjaxNav')) {
			$ignore_filters = true;
			
		//if a block id was provided in the querystring,
		// only grab filter args if they were submitted for this block
		// (in case two blocks of this type were added to the same page)
		} else if (!empty($_GET['bID']) && ($_GET['bID'] != $this->bID)) {
			$ignore_filters = true;
			//dev note: the reason we don't ignore the filters when no bID is provided
			// is so a particular category can be linked to from another page
			// (without that page having to figure out what the block id is).
		} else {
			$ignore_filters = false;
		}
		
		$filter_category_id = 0;
		if ($this->show_category_filter && !$ignore_filters && !empty($_GET['cat']) && (int)$_GET['cat'] > 0) {
			$filter_category_id = (int)$_GET['cat'];
		}
		
		$from_date = (empty($_GET['day']) || $ignore_filters) ? date(DEC_DATETIMEPICKER_DATE_FORMAT) : $_GET['day'];
		
		//SDGL CUSTOMIZATION: Use member categories instead of event categories
		/*
		$allowable_category_ids = $this->getCategoryIdsArray();
		$list_category_ids = $filter_category_id ? $filter_category_id : $allowable_category_ids;
		*/
		//END SDGL CUSTOMIZATION
		
		$reverse = !empty($_GET['reverse']);
		//SDGL CUSTOMIZATION: Use member categories instead of event categories
		//$events_per_day = EventOccurrence::getByDaysHavingEvents($from_date, $this->day_count, $reverse, $list_category_ids);
		$events_per_day = EventOccurrence::getByDaysHavingEvents($from_date, $this->day_count, $reverse, null, 365, $filter_category_id);
		//END SDGL CUSTOMIZATION
		list($pagination_prev_link, $pagination_next_link) = $this->getPaginationLinks($events_per_day, $from_date, $reverse, $filter_category_id);
		
		$this->set('events_per_day', $events_per_day);
		$this->set('pagination_prev_link', $pagination_prev_link);
		$this->set('pagination_next_link', $pagination_next_link);
		
		$event_count = 0;
		foreach ($events_per_day as $events) {
			$event_count += count($events);
		}
		$this->set('event_count', $event_count);
		
		$category_options = array();
		if ($this->show_category_filter) {
			//SDGL CUSTOMIZATION: Use member categories instead of event categories
			/*
			if ($allowable_category_ids) {
				$all_category_options = CategoryModel::factory()->getSelectOptions();
				$category_options[0] = t('All Categories');
				foreach ($allowable_category_ids as $category_id) {
					$category_options[$category_id] = $all_category_options[$category_id];
				}
			} else {
				//admin didn't specify allowable categories,
				// which means to include all categories
				$category_options = CategoryModel::factory()->getSelectOptions(array(0 => t('All Categories')));
			}
			*/
			Loader::model('subcategory/query', 'sdgl_members');
			$category_options = SubcategoryQuery::allGroupedByCategory();
			//END SDGL CUSTOMIZATION
		}
		
		$this->set('selected_day', $from_date);
		$this->set('category_options', $category_options);
		$this->set('filter_category_id', $filter_category_id);
		
		if (!$this->allowDescriptionHiding()) {
			//override saved setting if pkg config doesn't allow description hiding
			$this->set('show_event_descriptions', true);
		}
	}
	
	private function getPaginationLinks($events_per_day, $from_date, $reverse, $filter_category_id) {
		$link = "?bID={$this->bID}&cat={$filter_category_id}";
		if (!empty($_GET['cID'])) {
			$link .= '&cID=' . intval($_GET['cID']);
		}
		
		if ($events_per_day) {
			reset($events_per_day);
			$initial_day_timestamp = key($events_per_day);
			end($events_per_day);
			$final_day_timestamp = key($events_per_day);
			reset($events_per_day);
			
			$prev_date = gmdate(DEC_DATETIMEPICKER_DATE_FORMAT, TimezoneHelper::gmstrtotime('-1 day', $initial_day_timestamp));
			$next_date = gmdate(DEC_DATETIMEPICKER_DATE_FORMAT, TimezoneHelper::gmstrtotime('+1 day', $final_day_timestamp));
			
			$prev_link = $link . '&day=' . urlencode($prev_date) . '&reverse=1';
			$next_link = $link . '&day=' . urlencode($next_date);
		} else {
			//If there are no events, that means we've reached the "end of the line",
			// so we only provide 1 pagination link (back towards the direction that events exist),
			// and we must base it on the original "from_date" (since we can't get a date from the empty $events_per_day array)...
			$prev_link = null;
			$next_link = null;
			if ($reverse) {
				$next_date = gmdate(DEC_DATETIMEPICKER_DATE_FORMAT, TimezoneHelper::gmstrtotime('+1 day', TimezoneHelper::gmstrtotime($from_date)));
				$next_link = $link . '&day=' . urlencode($next_date);
			} else {
				$prev_date = gmdate(DEC_DATETIMEPICKER_DATE_FORMAT, TimezoneHelper::gmstrtotime('-1 day', TimezoneHelper::gmstrtotime($from_date)));
				$prev_link = $link . '&day=' . urlencode($prev_date) . '&reverse=1';
			}
		}
				
		return array($prev_link, $next_link);
	}
	
	private function getCategoryIdsArray() {
		$category_ids = array();
		if (!empty($this->category_ids)) {
			$category_ids = explode(',', $this->category_ids);
		}
		return $category_ids;
	}
	
	private function allowDescriptionHiding() {
		$dec_pkg = Package::getByHandle('designer_event_calendar');
		$allow = $dec_pkg->config('dec_event_list_allow_description_hiding');
		return (bool)$allow;
	}

}