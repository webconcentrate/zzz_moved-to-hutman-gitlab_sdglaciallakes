<?php defined('C5_EXECUTE') or die("Access Denied.");

//CUSTOMIZED FOR SDGL: "Category" FILTER IS FOR MEMBER CATEGORIES (not calendar categories)!

/*
Note that we are not using C5's form helpers,
because they automatically populate with values
in $_GET (which might be from another block instance
if there were 2 blocks of this type on the same page).

Other notes (see number references in code below):

[1] Leave the form action blank (do not use $this->action),
    because method="GET" forms do not work with $this->action()
    (because it relies on putting querystring args into the url,
    which the browser will replace with actual form fields upon submission).

[2] Include the 'cID' in a hidden field in case the page is
    being viewed at its "non-pretty" url (e.g. index.php?cID=147).

[3] Include the 'bID' in a hidden field so the controller knows
    which block should be filtered (in case there are 2 or more blocks
    of this type on the same page). UPDATE: We also need it for ajax!

[4] Do not remove the "dec-datepicker" and "date" classes
    because they are required by the calendar widget javascript.
*/

$th = Loader::helper('text');

if (!$show_category_filter) {
	return;
}
?>

<div class="event-list__filters">
	<form class="event-list__filters-form js-ajax-category-form" method="get" action="<?php /*[1]*/ ?>">
		
		<?php if (!empty($_GET['cID'])) { /*[2]*/ ?>
			<input type="hidden" name="cID" value="<?=(int)$_GET['cID']?>">
		<?php } ?>
		<input type="hidden" name="bID" value="<?=$bID /*[3]*/?>">
		
		
		<div class="event-list__filters-input">
			<div class="event-list__filters-input-label">
				<label for="event-list-filter-date">Date</label>
			</div>
			<div class="event-list__filters-input-field">
				<span class="dec-datepicker"><?php /*[4]*/ ?>
					<input id="event-list-filter-date" type="text" class="date" name="day" value="<?php echo $th->entities($selected_day); ?>" />
				</span>
			</div>
		</div>

		<div class="event-list__filters-input">
			<div class="event-list__filters-input-label">
				<label for="event-list-filter-category">Category</label>
			</div>
			<div class="event-list__filters-input-field">
				<select name="cat" id="event-list-filter-category">
					<option value="">All Categories</option>
					<?php foreach ($category_options as $category) { ?>
						<optgroup label="<?=$category->title?>">
						<?php foreach ($category->subcategories as $subcategory) { ?>
							<option value="<?=$subcategory->id?>" <?=(($subcategory->id == $filter_category_id) ? 'selected="selected"' : '')?>>
								<?=$subcategory->title?>
							</option>
						<?php } ?>
						</optgroup>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="event-list__filters-loader">
			<img class="js-ajax-loading-indicator" style="display: none;" src="<?=ASSETS_URL_IMAGES?>/throbber_white_16.gif" width="16" height="16" alt="">
		</div>
		<div class="event-list__filters-submit">
			<button>Go</button>
		</div>
	</form>
</div>
