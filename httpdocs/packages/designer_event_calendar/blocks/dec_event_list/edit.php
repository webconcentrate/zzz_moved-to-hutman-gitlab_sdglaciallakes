<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
		
<div class="ccm-ui">
	<div class="form-horizontal">
		
		<div class="control-group">
			<label class="control-label" for="day_count"><?php echo t('# of days'); ?></label>
			<div class="controls">
				<?php echo $form->text('day_count', $day_count, array('style' => 'width: 25px; text-align: center;', 'maxlength' => '2')); ?>
			</div>
		</div>
		
		<?php if ($allow_description_hiding) { ?>
			<div class="control-group">
				<label class="control-label" for="show_event_descriptions"><?php echo t('Show event descriptions'); ?></label>
				<div class="controls">
					<?php echo $form->select('show_event_descriptions', array(
						1 => 'Yes',
						0 => 'No',
					), ($show_event_descriptions ? 1 : 0), array('style' => 'width: auto;')); ?>
				</div>
			</div>
		<?php } else { ?>
			<input type="hidden" name="show_event_descriptions" value="1">
		<?php } ?>
		
		<div class="control-group">
			<label class="control-label" for="show_category_filter"><?php echo t('Show category filter'); ?></label>
			<div class="controls">
				<?php echo $form->select('show_category_filter', array(
					1 => 'Yes (users can filter events via dropdown list)',
					0 => 'No (users always see events from all included categories)',
				), ($show_category_filter ? 1 : 0), array('style' => 'width: auto;')); ?>
			</div>
		</div>
		
		<?php /* CUSTOM FOR SDGL: HIDE THIS SO USER CANNOT CHANGE IT... */ ?>
		<div class="control-group" style="display: none;">
			<label class="control-label" for="is_all_categories"><?php echo t('Include events from'); ?></label>
			<div class="controls">
				<?php echo $form->select('is_all_categories', array(
					1 => 'all categories.',
					0 => 'selected categories...',
				), (empty($category_ids) ? 1 : 0)); ?>
				
				<ul id="category_ids" style="display: <?php echo (empty($category_ids) ? 'none' : 'block'); ?>; list-style: none; margin: 0; padding: 0;">
					<?php foreach ($category_options as $category_id => $category_name) { ?>
						<li>
							<label>
								<input type="checkbox" name="category_ids[]" value="<?php echo $category_id; ?>" <?php echo (in_array($category_id, $category_ids) ? 'checked' : ''); ?>>
								<?php echo $category_name; ?>
							</label>
						</li>
					<?php } ?>
				</ul>
				
				<script>
					$('#is_all_categories').on('change', function() {
						var show_categories = !parseInt($(this).val(), 10);
						$('#category_ids').toggle(show_categories);
					});
					$('#is_all_categories').closest('form').on('submit', function() {
						if (parseInt($('#is_all_categories').val(), 10)) {
							//explicitly uncheck all categories
							// (because this "is_all_categories" thing
							// isn't an actual db field... just an editing UI convenience)
							$('input[name="category_ids[]"]').prop('checked', false);
						}
					});
				</script>
			</div>
		</div>
		
	</div>
</div>
