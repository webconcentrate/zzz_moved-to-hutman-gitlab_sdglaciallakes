<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<div class="js-dec-event-list-ajax-navigation-wrapper">
	<div class="event-list js-ajax-content">
		<?php include dirname(__FILE__) . '/view_filters.php'; ?>
		
		<ul class="event-list__list event-list__list--multicolumn">
			<?php foreach ($events_per_day as $day_ts => $events) { ?>
				<?php foreach ($events as $event) { ?>
					<?php if ($event->getDayInSequence() == 1) { ?>
						<li class="event-list__item">
							<div class="event-list__item-summary-date">
								<div class="event-list__item-summary-month">
									<?=$event->getStartFormatted('M')?>
								</div>
								<div class="event-list__item-summary-day">
									<?=$event->getStartFormatted('j')?>
								</div>
							</div>
							<div class="event-list__item-details">
								<div class="event-list__item-title">
									<?php if ($event->isLinkToNothing()) { ?>
										<span><?=$event->getTitle()?></span>
									<?php } else { ?>
										<a href="<?=$event->getLinkToDetailsPage()?>">
											<?=$event->getTitle()?>
										</a>
									<?php } ?>
								</div>
								
								<div class="event-list__item-date">
									<?php
										$start_date = $event->getStartFormatted('l, F d');
										$start_time = $event->getStartFormatted('g:ia');
										$end_date = $event->getEndFormatted('l, F d');
										$end_time = $event->getEndFormatted('g:ia');
										
										if (!$event->isAllDay()) {
											echo $start_time;
											if (!$event->isEndTimeHidden()) {
												echo ' - ' . $end_time;
											}
											if ($event->isMultiDay()) {
												echo ' (' . $end_date . ')';
											}
										} else if ($event->isMultiDay()) {
											echo "through {$end_date}";
										}
									?>
								</div>
								
								<?php if ($event->hasSubtitle()) { ?>
									<div class="event-list__item-location">
										<?=$event->getSubtitle()?>
									</div>
								<?php } ?>
								
							</div>
						</li>
					<?php } ?>
				<?php } ?>
			<?php } ?>
		</ul>
		
		<?php if (!$event_count) { ?>
			<p><em>No events match the chosen criteria.</em></p>
		<?php } ?>

		<?php
		$ajax_nav_is_below_content = true; //so the js knows that it should scroll to the top of the list after navigation
		include dirname(__FILE__) . '/view_pagination.php';
		?>
		
	</div>
</div>