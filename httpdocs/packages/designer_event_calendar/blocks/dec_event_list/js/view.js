$(function() {
	var wrapperSelector = '.js-dec-event-list-ajax-navigation-wrapper';
	
	$(wrapperSelector).on('click', '.js-ajax-nav', function(event) {
		event.preventDefault();
		
		$(this).siblings('.js-ajax-loading-indicator').css('display', 'inline');
		
		var $wrapper = $(this).closest(wrapperSelector);
		var qs = $(this).attr('href');
		var scrollToTop = $(this).hasClass('js-ajax-nav-scroll-on-click');
		replaceCalendarView($wrapper, qs, scrollToTop);
	});
	
	$(wrapperSelector).on('submit', '.js-ajax-category-form', function(event) {
		event.preventDefault();
		$(this).find('.js-ajax-loading-indicator').show();
		
		var $wrapper = $(this).closest(wrapperSelector);
		var qs = $(this).serialize();
		replaceCalendarView($wrapper, qs);
	});

	function replaceCalendarView($wrapper, qs, scrollToTop) {
		qs = '?' + qs.substring(qs.indexOf('?') + 1); //normalize leading question mark
		
		var url = CCM_REL + '/index.php/tools/blocks/dec_event_list/ajax_nav' + qs;
		
		$.get(url, function(response) {
			//Only replace the inner contents of the container
			// (not the container itself)
			// because we don't want to lose its event listeners.
			var $inner = $(response).find('.js-ajax-content');
			if ($inner.length) {
				$wrapper.html($inner);
			} else {
				$wrapper.html('ERROR: Could not retrieve event list');
			}
			
			if (scrollToTop) {
				var buffer = 50; //pixels above the scroll-to position (if you want to scroll to slightly above the target)
				var duration = 250; //milliseconds
				$('body,html').animate({scrollTop : ($wrapper.offset().top - buffer)}, duration);
			}
			$(function(){$(".dec-datepicker input.date").datepicker({format:"m/d/yyyy",autoclose:true})})
		});
	}

	//auto-submit form when category filter is changed
	$(wrapperSelector).on('change', '.js-ajax-category-form input, .js-ajax-category-form select', function() {
		$(this).closest('form').trigger('submit');
	});
});
