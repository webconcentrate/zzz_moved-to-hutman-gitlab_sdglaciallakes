$(function() {
	var wrapperSelector = '.js-dec-mini-calendar-ajax-navigation-wrapper';
	
	
	//adjust font-size of calendar based on its width
	function wsMiniCalSetFontSize() {
		$('.ws-mini-cal').each(function() {
			var elWidth = $(this).width();
			var fontSize = elWidth / 20; //font-size should be 5% of the width of the calendar div
			$(this).css('font-size', fontSize + 'px');
		});
	}
	$(window).on('resize', wsMiniCalSetFontSize);
	wsMiniCalSetFontSize();
	
	
	//toggle event info rows when date is clicked
	$(wrapperSelector).on('click', '.ws-mini-cal .events', function() {
		$('.ws-mini-cal .event-details').hide();
		$('.ws-mini-cal .event-details ul').hide();

		$('.ws-mini-cal .events').removeClass('selected');
		$(this).addClass('selected');
		
		var day_attr = $(this).attr('data-day');
		var $info_container = $('ul[data-day="' + day_attr + '"]');
		$info_container.show();
		$info_container.closest('.event-details').css('display', 'table-row');
	});
	
	
	//ajax navigation
	$(wrapperSelector).on('click', '.js-ajax-nav', function(event) {
		event.preventDefault();
		
		$(this).siblings('.js-ajax-current-month-display').css('display', 'none');
		$(this).siblings('.js-ajax-loading-indicator').css('display', 'inline');
		
		var $wrapper = $(this).closest(wrapperSelector);
		var qs = $(this).attr('href');
		replaceCalendarView($wrapper, qs);
	});
	
	function replaceCalendarView($wrapper, qs) {
		qs = '?' + qs.substring(qs.indexOf('?') + 1); //normalize leading question mark
		
		var url = CCM_REL + '/index.php/tools/blocks/dec_mini_calendar/ajax_nav' + qs;
		
		$.get(url, function(response) {
			//Only replace the inner contents of the container
			// (not the container itself)
			// because we don't want to lose its event listeners.
			var $inner = $(response).find('.js-ajax-content');
			if ($inner.length) {
				$wrapper.html($inner);
				wsMiniCalSetFontSize();
			} else {
				$wrapper.html('ERROR: Could not retrieve calendar');
			}
		});
	}
});
