<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<div class="js-dec-mini-calendar-ajax-navigation-wrapper">
	<div class="ws-mini-cal js-ajax-content">

		<table>
			<caption>
				<div>
					<a href="<?php echo $pagination_prev_link; ?>" class="prev-month js-ajax-nav">&#x25C4;</a>
					<span class="js-ajax-current-month-display"><?php echo gmdate('F Y', $current_timestamp); ?></span>
					<img class="js-ajax-loading-indicator" style="display: none;" src="<?=ASSETS_URL_IMAGES?>/throbber_white_16.gif" width="16" height="16" alt="">
					<a href="<?php echo $pagination_next_link; ?>" class="next-month js-ajax-nav">&#x25BA;</a>
				</div>
			</caption>
			<thead>
				<tr>
					<?php foreach (array_slice(array_keys($events_per_day), 0, 7) as $day_timestamp) { ?>
						<th><?php echo gmdate('D', $day_timestamp); ?></th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php
				$column = 1;
				$calendar_dates_html = '';
				$event_content_html = '';
				foreach ($events_per_day as $day_timestamp => $day_events) {
					if ($column == 1) {
						$calendar_dates_html .= "<tr>\n";
						$event_content_html .= '<tr class="event-details"><td colspan="7">' . "\n";
					}
					
					$is_current_month = ($current_month == gmdate('n', $day_timestamp));
					
					$td_class = '';
					if (!$is_current_month) {
						$td_class = 'off';
					} else if ($day_events) {
						$td_class = 'events';
					}
					
					$day_attr = gmdate('Y-m-d', $day_timestamp);
					
					$calendar_dates_html .= '<td class="' . $td_class . '" data-day="' . $day_attr . '">';
					$calendar_dates_html .= gmdate('j', $day_timestamp);
					$calendar_dates_html .= '</td>';
					
					if ($day_events && $is_current_month) {
						$event_content_html .= '<ul data-day="' . $day_attr . '">';
						foreach ($day_events as $event) {
							$event_content_html .= ($event->isAllDay() ? '<li class="all-day">' : '<li>');
							
							if ($event->isAllDay()) {
								//do not output a time for all-day events
							} else if ($event->getDayInSequence() > 1) {
								$event_content_html .= '12a'; //event continues from previous day
							} else {
								//output compact time format (e.g. "3p" or "12:30a")...
								$start_hour = $event->getStartFormatted('g');
								$start_minute = (int)$event->getStartFormatted('i');
								$start_ampm = rtrim($event->getStartFormatted('a'), 'm'); //remove trailing "m" from "am" or "pm" (so we only show "3p" instead of "3pm", for example)
								
								$event_content_html .= $start_hour;
								if ($start_minute > 0) {
									$event_content_html .= ':' . $start_minute;
								}
								$event_content_html .= $start_ampm;
							}
							
							$event_content_html .= ' ';
							
							if ($event->isLinkToNothing()) {
								$event_content_html .= $event->getTitle();
							} else {
								$event_content_html .= '<a href="' . $event->getLinkToDetailsPage() . '">' . $event->getTitle() . '</a>';
							}
							
							$event_content_html .= '</li>';
						}
						$event_content_html .= "</ul>\n";
					}
					
					
					if ($column == 7) {
						$calendar_dates_html .= "</tr>\n";
						$event_content_html .= "</td></tr>\n";
						
						echo $calendar_dates_html;
						echo $event_content_html;
						
						$calendar_dates_html = '';
						$event_content_html = '';
					}
					
					$column = ($column == 7 ? 1 : $column + 1);
				}
				
				//Assume that controller always includes a multiple of 7 in $events_per_day
				// (otherwise we'd want to explicitly output $calendar_dates_html
				// and $event_content_html here in case we didn't hit column 7 in the loop above)
				?>
			</tbody>
		</table>
	</div>
</div>
