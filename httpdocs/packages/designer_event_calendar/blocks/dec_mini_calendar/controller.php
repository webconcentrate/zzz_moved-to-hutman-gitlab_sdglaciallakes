<?php defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('category', 'designer_event_calendar');

class DecMiniCalendarBlockController extends BlockController {

	public function getBlockTypeName() { return t('Mini Event Calendar'); }
	public function getBlockTypeDescription() { return t('Displays events in a compact monthly calendar.'); }
	
	protected $btInterfaceWidth = 500;
	protected $btInterfaceHeight = 400;
	
	protected $btTable = 'btDecMiniCalendar';
	protected $btCacheBlockRecord = true;
	
	//Note that output caching is disabled because different filter selections will show different events.
	// (If we only cared about ajax then we could enable it, but we want it to work sans javascript too).
	protected $btCacheBlockOutput = false;
	protected $btCacheBlockOutputOnPost = false;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;
	
	
	private $first_day_of_week_is_sunday = true; //false for monday
	
	
	//////////////
	// ADD/EDIT //
	///////////////////////////////////////////////////////////////////////////
	
	public function add() {
		//set default values for new blocks
		$this->set('category_ids', array());
		
		$this->edit();
	}
	
	public function edit() {
		$this->set('category_options', CategoryModel::factory()->getSelectOptions());
		$this->set('category_ids', $this->getCategoryIdsArray());
	}
	
	public function save($args) {
		//convert checked categories array into a comma-separated string
		$safe_category_ids = array();
		if (!empty($args['category_ids']) && is_array($args['category_ids'])) {
			foreach ($args['category_ids'] as $category_id) {
				$category_id = (int)$category_id;
				if ($category_id > 0) {
					$safe_category_ids[] = $category_id;
				}
			}
		}
		$args['category_ids'] = implode(',', $safe_category_ids);
		
		parent::save($args);
	}
	
	
	//////////
	// VIEW //
	///////////////////////////////////////////////////////////////////////////	
	
	public function view() {
		Loader::model('event', 'designer_event_calendar');
		
		//if block caching is enabled,
		// then only apply filters for ajax requests
		// (otherwise C5 might cache a non-default page view
		// depending on what's being shown whenever cache goes stale)
		if ($this->btCacheBlockOutput && !$this->get('isAjaxNav')) {
			$ignore_filters = true;
			
		//if a block id was provided in the querystring,
		// only grab filter args if they were submitted for this block
		// (in case two blocks of this type were added to the same page)
		} else if (!empty($_GET['bID']) && ($_GET['bID'] != $this->bID)) {
			$ignore_filters = true;
			//dev note: the reason we don't ignore the filters when no bID is provided
			// is so a particular category can be linked to from another page
			// (without that page having to figure out what the block id is).
		} else {
			$ignore_filters = false;
		}
		
		$list_category_ids = $this->getCategoryIdsArray();
		
		if (!$ignore_filters && !empty($_GET['year']) && (int)$_GET['year'] >= 1900 && (int)$_GET['year'] < 2200) {
			$current_year = (int)$_GET['year'];
		} else {
			$current_year = (int)date('Y');
		}
		
		if (!$ignore_filters && !empty($_GET['month']) && (int)$_GET['month'] >= 1 && (int)$_GET['month'] <= 12) {
			$current_month = (int)$_GET['month'];
		} else {
			$current_month = (int)date('n');
		}
		
		$current_timestamp = TimezoneHelper::gmstrtotime("{$current_month}/1/{$current_year}");
		
		$this->set('current_year', $current_year);
		$this->set('current_month', $current_month);
		$this->set('current_timestamp', $current_timestamp);
		
		list($from_date, $to_date) = TimestampConvert::firstAndLastDayOfMonthCalendar($current_month, $current_year, $this->first_day_of_week_is_sunday);
		
		$events_per_day = EventOccurrence::getByDateRange($from_date, $to_date, $list_category_ids);
		list($pagination_prev_link, $pagination_next_link) = $this->getPaginationLinks($current_year, $current_month);
		
		$this->set('events_per_day', $events_per_day);
		$this->set('pagination_prev_link', $pagination_prev_link);
		$this->set('pagination_next_link', $pagination_next_link);
	}
	
	private function getPaginationLinks($current_year, $current_month) {
		$link = "?bID={$this->bID}";
		if (!empty($_GET['cID'])) {
			$link .= '&cID=' . intval($_GET['cID']);
		}
		
		$current_is_january = ($current_month == 1);
		$current_is_december = ($current_month == 12);
		$prev_year = ($current_is_january ? $current_year - 1 : $current_year);
		$prev_month = ($current_is_january ? 12 : $current_month - 1);
		$next_year = ($current_is_december ? $current_year + 1 : $current_year);
		$next_month = ($current_is_december ? 1 : $current_month + 1);
		
		$prev_link = $link . "&year={$prev_year}&month={$prev_month}";
		$next_link = $link . "&year={$next_year}&month={$next_month}";
		
		return array($prev_link, $next_link);
	}
	
	private function getCategoryIdsArray() {
		$category_ids = array();
		if (!empty($this->category_ids)) {
			$category_ids = explode(',', $this->category_ids);
		}
		return $category_ids;
	}
}