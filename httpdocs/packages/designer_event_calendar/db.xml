<?xml version="1.0"?>
<schema version="0.3">
	
	<table name="dec_events">
		<field name="id" type="I"><UNSIGNED/><KEY/><AUTOINCREMENT/></field>
		<field name="title" type="C" size="255"><NOTNULL/></field>
		<field name="subtitle" type="C" size="500"></field><!-- SDGL CUSTOMIZATION: BUMP THIS UP TO 500 (FROM 255) -->
		<field name="public_submitter_info" type="X"></field><!-- SDGL CUSTOMIZATION: Store info from dec_public_submissions front-end form (for internal use, only visible to admins in the dashboard) -->
		<field name="location_name" type="C" size="255"></field>
		<field name="location_url" type="X"></field>
		<field name="description" type="X2"></field>
		<field name="link_to_cID" type="I"><UNSIGNED/></field>
		<field name="link_to_url" type="C" size="255"></field>
		<field name="link_to_nothing" type="L"></field><!-- default behavior is that events in lists/calendars link to a page or url (if link_to_cID or link_to_url is provided), otherwise link to the event details single_page... but if this field is true then it indicates to the frontend templates that the event should not link to anywhere. -->
		
		<field name="start_time" type="I"><UNSIGNED/></field><!-- number of seconds from midnight on the first (or only) day of event, or null for all-day events -->
		<field name="end_time" type="I"><UNSIGNED/></field><!-- number of seconds from midnight of the last (or only) day of event, or null for all-day events -->
		
		<field name="repeat_interval_days" type="I"><UNSIGNED/></field><!-- Number of days between event repetition. THIS IS USED FOR DAYS AND WEEKS! -->
		<field name="repeat_interval_weeks" type="I"><UNSIGNED/></field><!-- FOR EDITING UI ONLY (not calculations): number of weeks between event repetition -->
		<field name="repeat_interval_months" type="I"><UNSIGNED/></field><!-- Number of months between event repetition (e.g. "every month" or "every 3 months") -->
		<field name="repeat_interval_years" type="I"><UNSIGNED/></field><!-- Number of years between event repetition (e.g. "every year" or "every 2 years") -->
		<!-- non-recurring events will have NULL for all 4 of the above fields -->
		
		<field name="repeat_or_occur_until_day" type="I"><UNSIGNED/></field><!-- For repeating events, this specifies a limiting date after which no recurrences will be generated for this event. Set this column to NULL for no limit (infinitely-recurring events). For non-recurring events, this is the ending date of the event (so for non-recurring single-day events it equals the start_day, and for non-recurring multi-day events it equals start_day plus length_extra_days). Stored as number of days since unix epoch (so unix timestamp at midnight divided by 86,400). -->
		<field name="repeat_count" type="I"><UNSIGNED/></field><!-- FOR EDITING UI ONLY (not calculations): number of times to repeat the event. At time of save, this will be converted to a "repeat_or_occur_until_day" value, as that field is always used for calculations (but we retain this number separately so we can put it into the edit interface for the user when they bring up this event again for editing). -->
		<field name="length_extra_days" type="I"><UNSIGNED/><NOTNULL/><DEFAULT value="0"/></field><!-- number of days past the first day that an occurrence of a multi-day event lasts. 0 for single-day events, 1 for 2-day events, 2 for 3-day events, etc. Note that this field isn't queried on (because non-repeating multi-day events store their "end date" in the repeat_or_occur_until_day field, and repeating multi-day events store this information in the cache table), but it is used by the editing UI to know what the user chose (and to populate the repeat_or_occur_until_day field or the cache table upon event save), and by the view ui to quickly display an event's "thru" date. -->
		<field name="is_final_day_ignored_by_query" type="L"><NOTNULL/><DEFAULT value="0"/></field><!-- if true (and event is multi-day and has start/end times [not an "all-day" event]), then final day of the event will be ignored when querying for events on a particular date. This is useful for "late-night" events that extend an hour or two past midnight, so technically they end the following day but you only want the event listed on the starting day. Note that this ONLY affects when the event gets retrieved in the getByDay() query... it does not alter its actual ending date or length_extra_days values. -->
		<field name="is_end_time_hidden" type="L"><NOTNULL/><DEFAULT value="0"/></field><!-- custom for sdgl: if true, the event end time will not be displayed on the front-end -->
	</table>
	
	<table name="dec_event_recurrences">
		<field name="event_id" type="I"><UNSIGNED/><KEY/></field>
		
		<field name="start_day" type="I"><UNSIGNED/><KEY/></field><!-- The first (or only) date that an event occurs. Stored as number of days since unix epoch (so unix timestamp at midnight divided by 86,400). FYI, the reason this field is in this table and not the primary "events" table is to facilitate things like "repeats every Tuesday and Thursday" (which would have 2 records in this table, one for the recurrence that starts on Tuesday, and one for the recurrence that starts on Thursday). -->
		<field name="start_month" type="I"><UNSIGNED/><NOTNULL/></field><!-- Number of months since unix epoch. This facilitates queries for monthly-repeating events, but is just derived from the start_day upon save. -->
		<field name="start_year" type="I"><UNSIGNED/><NOTNULL/></field><!-- Number of years since unix epoch. This facilitates queries for yearly-repeating events, but is just derived from the start_day upon save. -->
		
		<!-- the following 2 fields are ONLY used for events that repeat monthly on a relative day (e.g. "1st Thursday of the month", or "2nd-last Tuesday of the month") -->
		<!-- they are *not* used for weekly repetitions (e.g. "weekly on Tuesday and Thursday")... those weekly repetitions are instead based off the "start_day" of each event_recurrences record -->
		<field name="week_of_month" type="I1"></field><!-- works in conjunction with day_of_week to specify things like "1st Thursday of the month", "4th Thursday of the month", or -1 for "last Thursday of the month", -2 for "2nd-last Thursday in the month" -->
		<field name="day_of_week" type="I1"><UNSIGNED/></field><!-- 1=Monday, 2=Tuesday, ... 7=Sunday. Works in conjunction with week_of_month.-->
		<!-- the following 1 field is ONLY used for events that repeat monthly on an absolute day (e.g. "on the 1st of the month", or "on the 15th of the month") -->
		<field name="day_of_month" type="I1"></field><!-- e.g. 13th day of month (aka "every month on the 13th"), or negative to indicate "nth-last-day of month"). NOTE that "nth-last-day of month" is not currently supported in the editing UI, so for now this field should never contain a negative value (but in the future if the UI adds this, all the logic is set up for it to work) -->
		<!-- the following 1 field is ONLY used for events that repeat yearly (e.g. "annually on June 2nd") -->
		<field name="month_of_year" type="I1"><UNSIGNED/></field><!-- 1=January, 2=February, etc. Only applies to yearly rules. -->
	</table>
	
	<table name="dec_event_cancellations">
		<field name="event_id" type="I"><UNSIGNED/><KEY/></field>
		<field name="start_day" type="I"><UNSIGNED/><KEY/></field><!-- start_day of the event occurrence to exclude. Stored as number of days since unix epoch (so unix timestamp at midnight divided by 86,400). -->
	</table>
	
	<table name="dec_event_multiday_recurrence_cache"><!-- stores 1 record for every day AFTER the 1st day of a multi-day repeating event occurrence. Note that cancelled event instances must have their corresponding cache records deleted from this table! -->
		<field name="event_id" type="I"><UNSIGNED/><KEY/></field>
		<field name="occurs_on_day" type="I"><UNSIGNED/><KEY/></field><!-- Stored as number of days since unix epoch (so unix timestamp at midnight divided by 86,400). -->
		<field name="day_in_sequence" type="I"><UNSIGNED/><KEY/></field><!-- Denotes which day of the multi-day event this instance falls on. For example, this will be "2" for the 2nd day of the event, "3" for the 3rd day of the event, etc. This field serves 2 purposes: first, it prevents duplicate key errors when an event overlaps itself (for example, a 3-day event that repeats every 2 days); second, it allows the view layer to display to the end-user how long the entire event is (without having to perform additional lookups or math [because otherwise there is no way to know how many days prior or after this instance the multi-day event actually runs]). -->
	</table>
	
	<table name="dec_categories">
		<field name="id" type="I"><UNSIGNED/><KEY/><AUTOINCREMENT/></field>
		<field name="name" type="C" size="255"><NOTNULL/></field>
		<field name="display_order" type="I"><UNSIGNED/><NOTNULL/></field>
	</table>
	
	<table name="dec_event_categories">
		<field name="event_id" type="I"><KEY/><UNSIGNED/></field>
		<field name="category_id" type="I"><KEY/><UNSIGNED/></field>
	</table>
	
	<!-- custom for SDGL... allows event to be associated with SDGL Member Locations -->
	<table name="dec_event_sdgl_members_locations">
		<field name="event_id" type="I"><KEY/><UNSIGNED/></field>
		<field name="sdgl_members_location_id" type="I"><KEY/><UNSIGNED/></field>
	</table>
	
</schema>

<!-- SEE http://phplens.com/lens/adodb/docs-datadict.htm FOR XML SCHEMA DOCS -->
