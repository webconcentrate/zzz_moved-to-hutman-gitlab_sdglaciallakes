// custom for designer_event_calendar
$(function() {
	$('.dec-datepicker input.date').datepicker({
		'format': 'm/d/yyyy', //should be the same as DATEPICKER_FORMAT in datepair.js
		autoclose: true
	});
});