<?php defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('event', 'designer_event_calendar');

Loader::library('crud_controller', 'designer_event_calendar'); //Superset of Concrete5's Controller class -- provides simpler interface and some extra useful features.
class CalendarEventDetailsController extends DecCrudController {
	
	public function view($event_id = null) {
		if (empty($event_id)) {
			$this->render404AndExit();
		}
		
		$event = EventRecord::getById($event_id);
		if (!$event) {
			$this->render404AndExit();
		}
		$sdgl_members_locations = array();
		$sdgl_members_location_ids = $event->getSdglMembersLocationIds();
		if ($sdgl_members_location_ids) {
			Loader::model('location/query', 'sdgl_members');
			$sdgl_members_locations = LocationQuery::byIds($sdgl_members_location_ids);
		}
		
		$this->set('pageTitle', $event->getTitle(false)); //send raw (unescaped) title because C5 escapes the $pageTitle var before outputting it
		$this->set('event', $event);
		$this->set('sdgl_members_locations', $sdgl_members_locations);
	}
	
}