<?php defined('C5_EXECUTE') or die(_("Access Denied."));

//This controller serves as a placeholder for the top-level item in the C5 dashboard.

Loader::library('crud_controller', 'designer_event_calendar');
class DashboardEventCalendarController extends DecCrudController {
	
	public function view() {
		$this->redirect('events');
	}
	
	//Utility method for refreshing the package schema
	// (go to http://localhost/your_site/dashboard/designer_event_calendar/refresh in your browser)
	public function refresh() {
		$u = new User;
		if ($u->isSuperUser() && Config::get('SITE_DEBUG_LEVEL')) {
			Package::getByHandle('designer_event_calendar')->upgrade();
			$this->flash('Package Schema Refreshed!');
			$this->view();
		}
	}
}
