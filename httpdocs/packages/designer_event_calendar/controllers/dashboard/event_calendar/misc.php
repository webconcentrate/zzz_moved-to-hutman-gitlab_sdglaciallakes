<?php defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('category', 'designer_event_calendar');

Loader::library('crud_controller', 'designer_event_calendar'); //Superset of Concrete5's Controller class -- provides simpler interface and some extra useful features.
class DashboardEventCalendarMiscController extends DecCrudController {
	
	public function on_before_render() {
		//Load css into the <head> and javascript into the footer of all views for this controller
		// (If you want to load js/css only for one action, put the addHeaderItem/addFooterItem call in that action's method instead)
		//DEV NOTE: we use "on_before_render()" instead of "on_page_view()" (on_page_view only works in block controllers [??])
		$html = Loader::helper('html');
		$this->addHeaderItem($html->css('dashboard.css', 'designer_event_calendar'));
		$this->addFooterItem($html->javascript('dashboard_sort.js', 'designer_event_calendar'));
	}
	
	public function view() {
		//this page just shows some links ot other pages, so we don't need to do anything here
	}
	
	
		
	/*** CATEGORIES **********************************************************/
	
	public function categories_view() {
		$this->set('categories', CategoryModel::factory()->getAll());
		$this->render('categories/view');
	}
	
	public function categories_add() {
		$this->categories_edit(null);
	}
	
	public function categories_edit($id = null) {
		$model = CategoryModel::factory();

		//This function serves several purposes:
		// * Display "add new" form
		// * Display "edit existing" form
		// * Process submitted form (validate data and save to db)
		//
		//We can determine which action to take based on a combination of
		// whether or not valid data was POST'ed and whether or not an $id was provided...
		if ($_POST) {
			$error = $model->validate($_POST);
			$result = $error->has() ? 'error' : 'success';
		} else {
			$result = empty($id) ? 'add' : 'edit';
		}


		//form was submitted and data is valid -- save to db and redirect...
		if ($result == 'success') {
			$id = $model->save($_POST);
			$this->flash(t('Category Saved!'));
			$this->redirect('categories_view');


		//form was submitted with invalid data -- display errors and repopulate form fields with user's submitted data...
		} else if ($result == 'error') {
			$this->set('error', $error); //C5 automagically displays these errors for us in the view

			//C5 form helpers will automatically repopulate form fields from $_POST data,
			// but we need to manually repopulate any data that isn't in $_POST,
			// or data that is used in places other than form fields...

			//[nothing needs to be done here]


		//form was not submitted, user wants to add a new record -- populate any form fields that should have default values...
		} else if ($result == 'add') {

			//[nothing needs to be done here]


		//form was not submitted, user wants to edit an existing record -- populate form fields with db data...
		} else if ($result == 'edit') {
			$record = $model->getById($id);
			if (!$record) {
				$this->render404AndExit();
			}

			$this->setArray($record); //sets variables for every field in $record
		}

		//now populate data that is the same regardless of the action taken...
		$this->set('id', $id);

		//finally, display the form with the data we populated above
		$this->render('categories/edit');
	}
	
	public function categories_sort() {
		if ($this->post()) {
			$ids = explode(',', $this->post('ids', ''));
			CategoryModel::factory()->setDisplayOrder($ids);
		}
		exit; //this is an ajax function, so no need to render anything
	}
	
	public function categories_delete($id) {
		if (empty($id) || !intval($id)) {
			$this->render404AndExit();
		}
		
		$model = CategoryModel::factory();
		
		if ($model->hasChildren($id)) {
			$this->set('error', t('This category cannot be deleted because one or more events is assigned to it.'));
			$this->set('disabled', true);
		} else if ($this->post()) {
			$model->delete($id);
			$this->flash(t('Category Deleted.'));
			$this->redirect('categories_view');
		}
		
		$this->setArray($model->getById($id));
		
		$this->render('categories/delete');
	}
	
	
	
	/*** CONFIG **************************************************************/
	public function config() {
		$configs = array(
			'dec_show_subtitle_field_in_dashboard',
			'dec_show_location_fields_in_dashboard',
			'dec_skip_empty_days_in_dashboard',
			'dec_list_number_of_days_in_dashboard',
			'dec_event_list_allow_description_hiding',
		);

		$pkg = Package::getByHandle('designer_event_calendar'); //don't use Loader::package() here because that doesn't set the pkgID properly for some bizarre reason
		
		if ($_POST) {
			foreach ($configs as $key) {
				if ($key == 'dec_list_number_of_days_in_dashboard') {
					$val = (int)$this->post($key);
					$val = ($val < 1 || $val > 365) ? 30 : $val;
				} else {
					$val = $this->post($key, 0);
				}
				$pkg->saveConfig($key, $val);
			}
			
			$this->flash(t('Settings Updated!'));
			$this->redirect('config');
		}
		
		foreach ($configs as $key) {
			$this->set($key, $pkg->config($key));
		}
		
		$this->render('config');
	}
	
}