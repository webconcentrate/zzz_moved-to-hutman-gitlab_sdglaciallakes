<?php defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('event', 'designer_event_calendar');
Loader::model('category', 'designer_event_calendar');

Loader::library('crud_controller', 'designer_event_calendar'); //Superset of Concrete5's Controller class -- provides simpler interface and some extra useful features.
class DashboardEventCalendarEventsController extends DecCrudController {

	public function on_before_render() {
		//Load css into the <head> and javascript into the footer of all views for this controller
		// (If you want to load js/css only for one action, put the addHeaderItem/addFooterItem call in that action's method instead)
		//DEV NOTE: we use "on_before_render()" instead of "on_page_view()" (on_page_view only works in block controllers [??])
		$html = Loader::helper('html');
		$this->addHeaderItem($html->css('dashboard.css', 'designer_event_calendar'));
	}
	
	public function view() {
		$category_options = CategoryModel::factory()->getSelectOptions(array(0 => t('&lt;All&gt;')));
		$this->set('category_options', $category_options);
		
		$category_id = empty($_GET['cat']) ? 0 : intval($_GET['cat']);
		$this->set('category_id', $category_id);
		
		$from_date = empty($_GET['day']) ? date(DEC_DATETIMEPICKER_DATE_FORMAT) : $_GET['day'];
		$this->set('day', $from_date);
		
		$dec_pkg = Package::getByHandle('designer_event_calendar');
		$skip_empty_days = $dec_pkg->config('dec_skip_empty_days_in_dashboard');
		$list_number_of_days = $dec_pkg->config('dec_list_number_of_days_in_dashboard');
		if ($skip_empty_days) {
			$reverse = !empty($_GET['reverse']);
			$max_days = 365; //how far into the past or future to continue searching for days having events before giving up (otherwise an infinite loop would occur if user paginates before the earliest event or after the latest event)
			$events_per_day = EventOccurrence::getByDaysHavingEvents($from_date, $list_number_of_days, $reverse, $category_id, $max_days);
			list($pagination_prev_link, $pagination_next_link) = $this->view_getPaginationLinksWithEmptyDaySkip($events_per_day, $from_date, $reverse);
			if (empty($events_per_day)) {
				$msg = 'There are no events within ' . $max_days . ' days ' . ($reverse ? 'before' : 'after') . ' the given date.';
				$this->set('no_events_message', $msg);
			}
		} else {
			$to_date = $this->view_getToDate($from_date, $list_number_of_days);
			$events_per_day = EventOccurrence::getByDateRange($from_date, $to_date, $category_id);
			list($pagination_prev_link, $pagination_next_link) = $this->view_getPaginationLinksNoEmptyDaySkip($from_date, $list_number_of_days);
		}
		
		$this->set('events_per_day', $events_per_day);
		$this->set('pagination_prev_link', $pagination_prev_link);
		$this->set('pagination_next_link', $pagination_next_link);
		$this->set('skip_empty_days', $skip_empty_days);
		$this->set('list_number_of_days', $list_number_of_days);
		
		$this->set('filter_querystring', $this->getFilterQuerystring());
		
		$html = Loader::helper('html');
		$this->addHeaderItem($html->css('../js/datetimepicker/datepickeronly.min.css', 'designer_event_calendar'));
		$this->addFooterItem($html->javascript('datetimepicker/datepickeronly.min.js', 'designer_event_calendar'));
	}
	
	private function view_getToDate($from_date, $list_number_of_days) {
		$diff_string = '+' . ($list_number_of_days - 1) . ' days';
		$to_date = date(DEC_DATETIMEPICKER_DATE_FORMAT, strtotime($diff_string, strtotime($from_date)));
		return $to_date;
	}
	
	private function view_getPaginationLinksNoEmptyDaySkip($from_date, $list_number_of_days) {
		$prev_date = date(DEC_DATETIMEPICKER_DATE_FORMAT, strtotime("-{$list_number_of_days} days", strtotime($from_date)));
		$next_date = date(DEC_DATETIMEPICKER_DATE_FORMAT, strtotime("+{$list_number_of_days} days", strtotime($from_date)));
		
		$prev_path = 'view' . $this->getFilterQuerystring($prev_date);
		$next_path = 'view' . $this->getFilterQuerystring($next_date);
		
		$prev_link = $this->url($prev_path);
		$next_link = $this->url($next_path);
		
		return array($prev_link, $next_link);
	}
	
	private function view_getPaginationLinksWithEmptyDaySkip($events_per_day, $from_date, $reverse) {
		if ($events_per_day) {
			reset($events_per_day);
			$initial_day_timestamp = key($events_per_day);
			end($events_per_day);
			$final_day_timestamp = key($events_per_day);
			reset($events_per_day);
			
			//not sure why we need to use gmdate here to get the correct result dates
			// (whereas we use regular `date()` in the `view_getPaginationLinksNoEmptyDaySkip()` function above)
			$prev_date = gmdate(DEC_DATETIMEPICKER_DATE_FORMAT, strtotime('-1 day', $initial_day_timestamp));
			$next_date = gmdate(DEC_DATETIMEPICKER_DATE_FORMAT, strtotime('+1 day', $final_day_timestamp));
			
			$prev_path = 'view' . $this->getFilterQuerystring($prev_date, true); //go backwards from the day before the initial date of the given sequence
			$next_path = 'view' . $this->getFilterQuerystring($next_date, false); //go forwards from the day after the final date of the given sequence
			
			$prev_link = $this->url($prev_path);
			$next_link = $this->url($next_path);
		} else {
			//If there are no events, that means we've reached the "end of the line",
			// so we only provide 1 pagination link (back towards the direction that events exist),
			// and we must base it on the original "from_date" (since we can't get a date from the empty $events_per_day array)...
			$prev_link = null;
			$next_link = null;
			if ($reverse) {
				$next_date = gmdate(DEC_DATETIMEPICKER_DATE_FORMAT, strtotime('+1 day', strtotime($from_date)));
				$next_path = 'view' . $this->getFilterQuerystring($next_date, false);
				$next_link = $this->url($next_path);
			} else {
				$prev_date = gmdate(DEC_DATETIMEPICKER_DATE_FORMAT, strtotime('-1 day', strtotime($from_date)));
				$prev_path = 'view' . $this->getFilterQuerystring($prev_date, true);
				$prev_link = $this->url($prev_path);
			}
		}
				
		return array($prev_link, $next_link);
	}
	
	public function search() {
		$category_options = CategoryModel::factory()->getSelectOptions(array(0 => t('&lt;All&gt;')));
		$this->set('category_options', $category_options);
		
		$category_id = empty($_GET['cat']) ? 0 : intval($_GET['cat']);
		$this->set('category_id', $category_id);
		
		$query = empty($_GET['q']) ? '' : $_GET['q'];
		$this->set('query', $query);
		
		$events = EventRecord::getByTitleSearch($query, $category_id);
		$this->set('events', $events);
		
		$this->render('search');
	}
	
	public function add() {
		$this->edit();
	}
	
	public function duplicate($event_id) {
		if (empty($event_id) || !intval($event_id)) {
			$this->render404AndExit();
		}
		
		$this->edit(null, $event_id);
	}
	
	public function edit($id = null, $duplicate_id = null) { //2nd arg is for duplicating records only (not adding new ones or editing existing ones)
		$post = $this->post();
		if ($post) {
			$post['link_to_nothing'] = ($post['link_type'] == 'nothing');
			$error = EventRecord::validatePOST($post);
			
		//Validation failed (so re-display the form)...
			if ($error->has()) {
				$this->set('error', $error); //C5 automagically displays these errors for us in the view
				
				//C5 form helpers will automatically repopulate form fields from $_POST data
				
				//but we need to manually repopulate radio buttons...
				$this->set('repeat_weekly_on', $post['repeat_weekly_on']);
				$this->set('repeat_monthly_by', $post['repeat_monthly_by']);
				//...and checkbox lists
				$categories = CategoryModel::factory()->getAll();
				$chosen_category_ids = $this->post('category_ids', array());
				foreach ($categories as $key => $category) {
					$categories[$key]['has'] = in_array($category['id'], $chosen_category_ids);
				}
				
				$sdgl_members_location_ids = $this->post('sdgl_members_location_ids', array());
				
				$this->set('public_submitter_info', $post['public_submitter_info']);

		//Validation passed (so save to db and redirect)...
			} else {
				$event = EventRecord::loadFromPOST($post);
				$event->save();
				$this->flash(t('Event Saved!'));
				$this->redirect('view' . $this->getFilterQuerystring($event->getStartFormatted(DEC_DATETIMEPICKER_DATE_FORMAT)));
			}
		
		//Add new event (so set default values)...
		} else if (empty($id) && empty($duplicate_id)) {
			
			$categories = CategoryModel::factory()->getAll();
			
			$default_start_ts = $this->getDefaultStartTimestamp();
			$start_date = date(DEC_DATETIMEPICKER_DATE_FORMAT, $default_start_ts);
			$start_time = date(DEC_DATETIMEPICKER_TIME_FORMAT, $default_start_ts);
			$default_end_ts = $default_start_ts + 3600; //1 hour past default start time
			$end_date = date(DEC_DATETIMEPICKER_DATE_FORMAT, $default_end_ts);
			$end_time = date(DEC_DATETIMEPICKER_TIME_FORMAT, $default_end_ts);
			$this->set('start_date', $start_date);
			$this->set('start_time', $start_time);
			$this->set('end_date', $end_date);
			$this->set('end_time', $end_time);
			
			//CUSTOM FOR SDGL:
			$this->set('is_all_day', 1);
			$this->set('link_to_nothing', true);
			
			$sdgl_members_location_ids = array();
			
		//Edit or duplicate existing event (so load data from db)...
		} else {
			$retrieve_id = empty($duplicate_id) ? $id : $duplicate_id;
			$event = EventRecord::getById($retrieve_id);
			if (!$event) {
				$this->render404AndExit();
			}
			$this->set('title', $event->getTitle());
			$this->set('subtitle', $event->getSubtitle());
			$this->set('public_submitter_info', $event->getPublicSubmitterInfo());
			$this->set('location_name', $event->getLocationName());
			$this->set('location_url', $event->getLocationUrl());
			$this->set('start_date', $event->getStartFormatted(DEC_DATETIMEPICKER_DATE_FORMAT));
			$this->set('start_time', $event->isAllDay() ? null : $event->getStartFormatted(DEC_DATETIMEPICKER_TIME_FORMAT));
			$this->set('end_date', $event->getEndFormatted(DEC_DATETIMEPICKER_DATE_FORMAT));
			$this->set('end_time', $event->isAllDay() ? null : $event->getEndFormatted(DEC_DATETIMEPICKER_TIME_FORMAT));
			$this->set('is_final_day_ignored_by_query', $event->isFinalDayIgnoredByQuery());
			$this->set('is_end_time_hidden', $event->isEndTimeHidden());
			$this->set('is_all_day', $event->isAllDay());
			$this->set('is_repeating', $event->isRepeating());
			$this->set('repeat_unit', $event->getRepeatUnit());
			$this->set('repeat_frequency', $event->getRepeatFrequency());
			$this->set('repeat_weekly_on', $event->getRepeatWeeklyOn());
			$this->set('repeat_monthly_by', $event->getRepeatMonthlyBy());
			$this->set('repeat_ends', $event->getRepeatEnds());
			$this->set('repeat_ends_after_count', $event->getRepeatEndsAfterCount());
			$this->set('repeat_ends_on_date', $event->getRepeatEndsOnFormatted(DEC_DATETIMEPICKER_DATE_FORMAT));
			$this->set('description', $event->getDescription(true)); //pass true for "edit mode" (because the description will be put into a WYSIWYG editor)
			$this->set('link_to_cID', $event->getLinkToCID());
			$this->set('link_to_url', $event->getLinkToURL());
			$this->set('link_to_nothing', $event->isLinkToNothing());
			
			$categories = CategoryModel::factory()->getAll();
			$chosen_category_ids = $event->getCategoryIds();
			foreach ($categories as $key => $category) {
				$categories[$key]['has'] = in_array($category['id'], $chosen_category_ids);
			}
			
			$sdgl_members_location_ids = $event->getSdglMembersLocationIds();
		}
		
		$this->set('id', $id);
		$this->set('categories', $categories);
		$this->set('cancellations', EventRecord::getCancellations($id)); //returns an empty array if $id is empty
		
		$this->set('filter_querystring', $this->getFilterQuerystring());
		
		$dec_pkg = Package::getByHandle('designer_event_calendar');
		$this->set('show_subtitle_field', (bool)$dec_pkg->config('dec_show_subtitle_field_in_dashboard'));
		$this->set('show_location_fields', (bool)$dec_pkg->config('dec_show_location_fields_in_dashboard'));

		Loader::model('subcategory/query', 'sdgl_members');
		$all_categories_and_subcategories = SubcategoryQuery::allGroupedByCategoryAsArraysForJSON();
		$this->set('all_categories_and_subcategories', $all_categories_and_subcategories);
		
		Loader::model('location/query', 'sdgl_members');
		$sdgl_members_locations = LocationQuery::byIdsAsArraysForJSON($sdgl_members_location_ids);
		$this->set('sdgl_members_locations', $sdgl_members_locations);
		
		$html = Loader::helper('html');
		$this->addHeaderItem($html->css('../js/datetimepicker/datetimepair.min.css', 'designer_event_calendar'));
		$this->addFooterItem($html->javascript('datetimepicker/datetimepair.min.js', 'designer_event_calendar'));
		
		$vue_js_filename = (Config::get('SITE_DEBUG_LEVEL') == DEBUG_DISPLAY_ERRORS ? 'vue.js' : 'vue.min.js');
		$this->addFooterItem($html->javascript($vue_js_filename, 'designer_event_calendar'));

		$this->render('edit');
	}
	
	public function edit_get_sdgl_members_locations($subcategory_id) {
		Loader::model('location/query', 'sdgl_members');
		$locations = LocationQuery::bySubcategoryAsArraysForJSON($subcategory_id); //<--this function sanitizes the input already (and returns empty array for non-integer-ish id) so we don't need to do it ourselves
		echo json_encode(array(
			'success' => true,
			'locations' => $locations,
		));
		exit;
	}
	
	public function uncancel_event_occurrence() {
		if ($this->post()) {
			$id = (int)$this->post('id');
			$day = (int)$this->post('day');
			if (!empty($id) && !empty($day)) {
				EventRecord::getById($id)->uncancelOccurrence($day);
			}
		}
		exit; //this is an ajax function, so no need to render anything
	}
	
	private function getDefaultStartTimestamp() {
		//default the start time to an hour after the next half-hour increment
		// (e.g. if current time is between 7:00-7:29 then default to 8:00,
		//  or if current time is between 7:30-7:59 then default to 8:30)
		$now_ts = time();
		$diff_between_now_and_previous_half_hour = $now_ts % 1800;
		$previous_half_hour_ts = $now_ts - $diff_between_now_and_previous_half_hour;
		$next_hour_after_previous_half_hour = $previous_half_hour_ts + 3600;
		return $next_hour_after_previous_half_hour;
	}
	
	//Pass in an occurrence date (the starting date of that occurrence, regardless of how many days long it is)
	// to delete just the occurrence (or all future occurrences).
	//Note that if you do not pass in an occurrence start date for a repeating event,
	// we will delete the entire event series!
	public function delete($id, $occurrence_start_date = null) {
		if (empty($id) || !intval($id)) {
			$this->render404AndExit();
		}
		
		$is_post = !empty($_POST);
		
		if (empty($occurrence_start_date) || $is_post) {
			$event = EventRecord::getById($id);
		} else {
			$event = EventOccurrence::getByDayAndEventId($occurrence_start_date, $id);
			if (!$event->isRepeating()) {
				//If this is not a repeating event,
				// redirect back here without the occurrence date
				// (and don't bother with the filter querystring stuff
				// since this is an edge case that won't normally happen).
				$this->redirect('delete', $id);
			}
		}
		
		if (!$event) {
			$this->render404AndExit();
		}
		
		if ($is_post) {
			$also_delete_all_future_occurrences = ($this->post('occurrences') == 'all');
			$event->delete($occurrence_start_date, $also_delete_all_future_occurrences); //passing in null for the first arg results in the entire event being deleted (which is what we want in that situation)
			
			if (empty($occurrence_start_date)) {
				$msg = t('Event Deleted.');
			} else if ($also_delete_all_future_occurrences) {
				$msg = t('Event Occurrences Deleted.');
			} else {
				$msg = t('Event Occurrence Deleted.');
			}
			$this->flash($msg);
			
			$redirect_to_path = 'view' . $this->getFilterQuerystring();
			$this->redirect($redirect_to_path);
		}
		
		$this->set('event', $event);
		$this->set('occurrence_start_date', $occurrence_start_date);
		
		$this->set('filter_querystring', $this->getFilterQuerystring());
		
		$this->render('delete');
	}
	
	private function getFilterQuerystring($override_date = null, $override_reverse = null) {
		$qs = '';
		
		if (!empty($_GET['cat'])) {
			$qs .= "&cat={$_GET['cat']}";
		}
		
		if (!empty($override_date)) {
			$qs .= '&day=' . urlencode($override_date);
		} else if (!empty($_GET['day'])) {
			$qs .= '&day=' . urlencode($_GET['day']);
		}
		
		if ($override_reverse || (is_null($override_reverse) && !empty($_GET['reverse']))) {
			$qs .= '&reverse=1';
		}
		
		if (!empty($qs)) {
			$qs = '?' . trim($qs, '&');
		}
		
		return $qs;
	}
}