<?php
defined('C5_EXECUTE') or die(_("Access Denied."));

class DesignerEventCalendarPackage extends Package {
	
	protected $pkgHandle = 'designer_event_calendar';
	public function getPackageName() { return t('Designer Event Calendar'); }
	public function getPackageDescription() { return t('Manage and display calendar events'); }
	protected $appVersionRequired = '5.6';
	protected $pkgVersion = '0.8.11'; //forked from version 0.8.8
	
	public function install() {
		$pkg = parent::install(); //this will automatically install our package-level db.xml schema for us (among other things)
		$this->installOrUpgrade($pkg);
	}
	
	public function upgrade() {
		$this->installOrUpgrade($this);
		parent::upgrade();
	}
	
	//Put most installation tasks here -- makes development easier
	// (just make sure the actions you perform are "non-destructive",
	//  for example, check if a page exists before adding it).
	private function installOrUpgrade($pkg) {
		
		//Frontend "Details" page (just one single_page controller that will get passed an event id -- this page does NOT list out events -- only blocks do that!)
		$event_details_toplevel_page = $this->getOrAddSinglePage($pkg, '/calendar-event-details', t('Event Details'));
		foreach (array(
			'exclude_nav',
			'exclude_page_list',
			'exclude_search_index',
			'exclude_sitemapxml',
		) as $attr) {
			$event_details_toplevel_page->setAttribute($attr, 1);
		}
		
		//Dashboard Pages:
		//Install one page for each *controller* (not each view),
		// plus one at the top-level to serve as a placeholder in the dashboard menu
		$this->getOrAddSinglePage($pkg, '/dashboard/event_calendar', 'Event Calendar'); //top-level pleaceholder
		$this->getOrAddSinglePage($pkg, '/dashboard/event_calendar/events', 'Manage Events');
		$this->getOrAddSinglePage($pkg, '/dashboard/event_calendar/misc', 'Misc. Settings'); //this one controller handles multiple entities (categories master list and interface settings)
		
		//Blocks
		$this->getOrInstallBlockType($pkg, 'dec_event_list');
		$this->getOrInstallBlockType($pkg, 'dec_upcoming_events');
		$this->getOrInstallBlockType($pkg, 'dec_monthly_calendar');
		$this->getOrInstallBlockType($pkg, 'dec_mini_calendar');
		
		//Configs (for interface settings)
		$this->getOrAddConfig($pkg, 'dec_show_subtitle_field_in_dashboard', 1);
		$this->getOrAddConfig($pkg, 'dec_show_location_fields_in_dashboard', 1);
		$this->getOrAddConfig($pkg, 'dec_skip_empty_days_in_dashboard', 1);
		$this->getOrAddConfig($pkg, 'dec_list_number_of_days_in_dashboard', 30);
		$this->getOrAddConfig($pkg, 'dec_event_list_allow_description_hiding', 1);
	}
	
	
/*** UTILITY FUNCTIONS ***/
	private function getOrAddSinglePage($pkg, $cPath, $cName = '', $cDescription = '') {
		Loader::model('single_page');
		
		$sp = SinglePage::add($cPath, $pkg);
		
		if (is_null($sp)) {
			//SinglePage::add() returns null if page already exists
			$sp = Page::getByPath($cPath);
		} else {
			//Set page title and/or description...
			$data = array();
			if (!empty($cName)) {
				$data['cName'] = $cName;
			}
			if (!empty($cDescription)) {
				$data['cDescription'] = $cDescription;
			}
			
			if (!empty($data)) {
				$sp->update($data);
			}
		}
		
		return $sp;
	}
	
	private function getOrInstallBlockType($pkg, $btHandle) {
		$bt = BlockType::getByHandle($btHandle);
		if (empty($bt)) {
			BlockType::installBlockTypeFromPackage($btHandle, $pkg);
			$bt = BlockType::getByHandle($btHandle);
		}
		return $bt;
	}
	
	private function getOrAddConfig($pkg, $key, $default_value_if_new = null) {
		$cfg = $pkg->config($key, true); //pass true to retrieve the full object (so we can differentiate between a non-existent config versus an existing config that has value set to null)
		if (is_null($cfg)) {
			$pkg->saveConfig($key, $default_value_if_new);
			return $default_value_if_new;
		} else {
			return $pkg->config($key);
		}
	}
}
