<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/*
You can access event data in two different ways:

1) Use the EventOccurrence class for read-only info about an event that occurs on a specific day
  (taking into account "repeating" event occurrences). Use this to populate lists and calendar views.

2) Use the EventRecord class for displaying a single event's details, and adding/editing/deleting events.

** Note that you should NEVER use the EventBase class directly -- it is for internal use only!

** EXAMPLE USAGE:

	Loader::model('event', 'designer_event_calendar');

	//Display events occurring the week of Jan. 5th - 11th, 2014:
	$events_per_day = EventOccurrence::getByDateRange('2014-01-05', '2014-01-11')
	foreach ($events_per_day as $day_timestamp => $events) {
		echo '<h2>' . gmdate('F j, Y') . '</h2>';
		echo '<ul>';
		foreach ($events as $event) {
			echo '<li>' . $event->getTitle() . '</li>';
		}
		echo '</ul>';
	}
	
	//Display an event's details:
	$event = EventRecord::getById(1234);
	echo $event->getTitle();
	
	//Save an event using POSTed data:
	if (!EventRecord::validatePOST($_POST)->has()) {
		EventRecord::loadFromPOST($_POST)->save();
	}
	
	//Delete an event:
	EventRecord::getById(1234)->delete();
	
	//Delete just one occurrence of an event:
	EventRecord::getById(1234)->delete('2014-05-03');
	
	//Delete one occurrence and all future occurrences of an event:
	EventRecord::getById(1234)->delete('2014-05-03', true);

*/

define('DEC_DATETIMEPICKER_DATE_FORMAT', 'n/j/Y'); //don't change this until datetimepicker.js becomes more flexible with its date formats (and allows them to be passed in as options)!!!
define('DEC_DATETIMEPICKER_TIME_FORMAT', 'g:ia'); //ditto

class EventOccurrence extends EventBase {
	
	/////////////////////
	// FACTORY METHODS //
	///////////////////////////////////////////////////////////////////////////
	
	public static function getByDateRange($from_date, $to_date, $category_id_or_ids = null) {
		$from_days = TimestampConvert::daysSinceEpoch(TimezoneHelper::gmstrtotime($from_date));
		$to_days = TimestampConvert::daysSinceEpoch(TimezoneHelper::gmstrtotime($to_date));
		
		$days = array();
		for ($day = $from_days; $day <= $to_days; $day++) {
			$timestamp = $day * 86400;
			$date = gmdate('Y-m-d', $timestamp);
			$days[$timestamp] = self::getByDay($date, $category_id_or_ids);
		}
		
		return $days;
	}
	
	//final arg ($sdgl_members_location_id_or_ids) is custom for SDGL
	public static function getByDaysHavingEvents($start_date, $number_of_days, $before_start_date_vs_after = false, $category_id_or_ids = null, $max_days = 365, $sdgl_members_location_id_or_ids = null) {
		$start_days = TimestampConvert::daysSinceEpoch(TimezoneHelper::gmstrtotime($start_date));
		
		$days_having_events = array();
		$current_day = $start_days;
		while (count($days_having_events) < $number_of_days && (abs($start_days - $current_day) <= $max_days)) {
			$timestamp = $current_day * 86400;
			$date = gmdate('Y-m-d', $timestamp);
			$day_events = self::getByDay($date, $category_id_or_ids, null, $sdgl_members_location_id_or_ids);
			if ($day_events) {
				$days_having_events[$timestamp] = $day_events;
			}
			
			if ($before_start_date_vs_after) {
				$current_day--;
			} else {
				$current_day++;
			}
		}
		
		if ($before_start_date_vs_after) {
			$days_having_events = array_reverse($days_having_events, true); //2nd arg is `$preserve_keys`
		}
		
		return $days_having_events;
	}
	
	//final arg ($sdgl_members_location_id_or_ids) is custom for SDGL
	public static function getByCount($from_date, $number_of_events, $category_id_or_ids = null, $max_days = 365, $sdgl_members_location_id_or_ids = null) {
		$start_days = TimestampConvert::daysSinceEpoch(TimezoneHelper::gmstrtotime($from_date));
		$max_days += $start_days;
		
		$events = array();
		
		for ($day = $start_days; $day <= $max_days; $day++) {
			$timestamp = $day * 86400;
			$date = gmdate('Y-m-d', $timestamp);
			$events_on_day = self::getByDay($date, $category_id_or_ids, null, $sdgl_members_location_id_or_ids);
			$events = array_merge($events, $events_on_day);
			if (count($events) >= $number_of_events) {
				return array_slice($events, 0, $number_of_events);
			}
		}
		
		return $events;
	}
	
	public static function getByDayAndEventId($occurrence_start_date, $event_id) {
		$occurrences = self::getByDay($occurrence_start_date, null, $event_id);
		$occurrence_start_ts = TimezoneHelper::gmstrtotime($occurrence_start_date);
		foreach ($occurrences as $occurrence) {
			$this_days = TimestampConvert::daysSinceEpoch($occurrence->getStartTimestamp());
			$match_days = TimestampConvert::daysSinceEpoch($occurrence_start_ts);
			if ($this_days == $match_days) {
				return $occurrence;
			}
		}
		return null;
	}
	
	//final arg ($sdgl_members_location_id_or_ids) is custom for SDGL
	public static function getByDay($date, $category_id_or_ids = null, $event_id = null, $sdgl_members_location_id_or_ids = null) {
		$day_timestamp = TimezoneHelper::gmstrtotime($date);
		
		$years = TimestampConvert::yearsSinceEpoch($day_timestamp);
		$months = TimestampConvert::monthsSinceEpoch($day_timestamp);
		$days = TimestampConvert::daysSinceEpoch($day_timestamp);
		
		$day_of_month = TimestampConvert::dayOfMonth($day_timestamp);
		$day_of_month_from_last = TimestampConvert::dayOfMonthFromLast($day_timestamp, true);
		
		$day_of_week_position_in_month = TimestampConvert::dayOfWeekPositionInMonth($day_timestamp); //1st Tuesday, 2nd Tuesday, etc.
		$day_of_week_position_from_last = TimestampConvert::dayOfWeekPositionFromLast($day_timestamp, true); //last Tuesday, 2nd-last Tuesday, etc.
		
		$day_of_week = TimestampConvert::dayOfWeek($day_timestamp);
		$month_of_year = TimestampConvert::monthOfYear($day_timestamp);
		
		$safe_event_id = (int)$event_id;
		
		$unsafe_category_ids = is_array($category_id_or_ids) ? $category_id_or_ids : array($category_id_or_ids);
		$safe_category_ids = array();
		foreach ($unsafe_category_ids as $category_id) {
			if ((int)$category_id > 0) {
				$safe_category_ids[] = (int)$category_id;
			}
		}
		$safe_category_ids = implode(',', $safe_category_ids);
		
		//CUSTOM FOR SDGL
		$unsafe_sdgl_members_location_id_or_ids = is_array($sdgl_members_location_id_or_ids) ? $sdgl_members_location_id_or_ids : array($sdgl_members_location_id_or_ids);
		$safe_sdgl_members_location_id_or_ids = array();
		foreach ($unsafe_sdgl_members_location_id_or_ids as $sdgl_members_location_id) {
			if ((int)$sdgl_members_location_id > 0) {
				$safe_sdgl_members_location_id_or_ids[] = (int)$sdgl_members_location_id;
			}
		}
		$safe_sdgl_members_location_id_or_ids = implode(',', $safe_sdgl_members_location_id_or_ids);
		//END CUSTOM FOR SDGL
		
		//The magical query that figures everything out!
		//
		//DEV NOTE about the "CAST(___ AS SIGNED)" stuff below...
		//The "CAST" function is because the start_day/start_month/start_year fields 
		// are defined in the db schema as UNSIGNED ints, which means they cannot be used
		// in any math operations that result in a negative number
		// (*even* if it's just "inline" math, not being written back to the field)!
		// For example, if $days is 1 and start_day is 2, an error would occur. Pretty weird, eh?
		$sql = "SELECT *, null AS day_in_sequence"
			. " FROM dec_events ev"
			. " RIGHT JOIN dec_event_recurrences re ON ev.id = re.event_id"
			. " WHERE ("
				. "("
					. "("
						//repeat DAILY or WEEKLY every x days (b/c weekly is same as every 7 days)...
						. "repeat_interval_days IS NOT NULL"
						. " AND "
						. "(({$days} - CAST(start_day AS SIGNED)) % repeat_interval_days = 0)"
					. ") OR ("
						//repeat MONTHLY every x months ON a specific day of the month (e.g "the 12th", or "last day of the month"), or on a day of a week relative to month begin/end (first Tuesday, last Thursday, etc.)...
						. " repeat_interval_months IS NOT NULL"
						. " AND "
						. "(({$months} - CAST(start_month AS SIGNED)) % repeat_interval_months = 0)"
						. " AND "
						. "(day_of_month = {$day_of_month} OR day_of_month = {$day_of_month_from_last} OR (week_of_month = {$day_of_week_position_in_month} AND day_of_week = {$day_of_week}) OR (week_of_month = {$day_of_week_position_from_last} AND day_of_week = {$day_of_week}))"
					. ") OR ("
						//repeat YEARLY every x years ON a specific day of the year (e.g. February 12), or on a day of a week relative to month begin/end (e.g. "first Sunday in April" or "last Tuesday in June")
						. "repeat_interval_years IS NOT NULL"
						. " AND "
						. "(({$years} - CAST(start_year AS SIGNED)) % repeat_interval_years = 0)"
						. " AND "
						. "((month_of_year = {$month_of_year} AND day_of_month = {$day_of_month}) OR (month_of_year = {$month_of_year} AND week_of_month = {$day_of_week_position_in_month} AND day_of_week = {$day_of_week}) OR (month_of_year = {$month_of_year} AND week_of_month = {$day_of_week_position_from_last} AND day_of_week = {$day_of_week}))"
					. ") OR ("
						//NON-REPEATING events
						. "repeat_interval_days IS NULL"
						. " AND "
						. "repeat_interval_months IS NULL"
						. " AND "
						. "repeat_interval_years IS NULL"
					. ")"
				. ")"
				. " AND start_day <= {$days}"
				. " AND (repeat_or_occur_until_day IS NULL OR repeat_or_occur_until_day > {$days} OR (is_final_day_ignored_by_query = 0 && repeat_or_occur_until_day = {$days}))"
			. ") AND ("
				//exclude cancelled dates
				. "NOT EXISTS (SELECT * FROM dec_event_cancellations ca WHERE ca.event_id = ev.id AND ca.start_day = {$days})"
			. ")"
			. ( empty($safe_category_ids) ? '' : " AND EXISTS (SELECT * FROM dec_event_categories cat WHERE cat.event_id = ev.id AND cat.category_id IN ({$safe_category_ids}))" )
/* CUSTOM FOR SDGL: */ . ( empty($safe_sdgl_members_location_id_or_ids) ? '' : " AND EXISTS (SELECT * FROM dec_event_sdgl_members_locations sdglml WHERE sdglml.event_id = ev.id AND sdglml.sdgl_members_location_id IN ({$safe_sdgl_members_location_id_or_ids}))" )
			. ( empty($safe_event_id) ? '' : " AND ev.id = {$safe_event_id}" )
			//2nd+ days of multiday repeating events...
			. " UNION ALL" //<--theoretically faster than just plain "UNION", because MySQL won't bother checking for duplicates between the two sides (we know there shouldn't be any dups so don't bother checking).
			. " SELECT ev.*"
			. " , null AS event_id, null AS start_day, null AS start_month, null AS start_year, null AS week_of_month, null AS day_of_week, null AS day_of_month, null AS month_of_year"
			. " , mrc.day_in_sequence"
			. " FROM dec_events ev"
			. " INNER JOIN dec_event_multiday_recurrence_cache mrc ON ev.id = mrc.event_id"
			. " WHERE mrc.occurs_on_day = {$days}"
			. ( empty($safe_category_ids) ? '' : " AND EXISTS (SELECT * FROM dec_event_categories cat WHERE cat.event_id = ev.id AND cat.category_id IN ({$safe_category_ids}))" )
/* CUSTOM FOR SDGL: */ . ( empty($safe_sdgl_members_location_id_or_ids) ? '' : " AND EXISTS (SELECT * FROM dec_event_sdgl_members_locations sdglml WHERE sdglml.event_id = ev.id AND sdglml.sdgl_members_location_id IN ({$safe_sdgl_members_location_id_or_ids}))" )
			. ( empty($safe_event_id) ? '' : " AND ev.id = {$safe_event_id}" )
			//ORDER BY applies to both parts of the UNION...
			. " ORDER BY start_time, id";
		$records = Loader::db()->GetArray($sql);
		
		$events = array();
		foreach ($records as $record) {
			$event = new EventOccurrence;
			$event->populateFromDayQuery($record, $day_timestamp);
			$events[] = $event;
		}
		
		return $events;
	}
	
	
	
	//////////////////////
	// PUBLIC ACCESSORS //
	///////////////////////////////////////////////////////////////////////////
	
	public function getId() {
		return parent::getId();
	}
	
	public function getTitle($escape_output = true) {
		return parent::getTitle($escape_output);
	}
	
	public function getSubtitle($escape_output = true) {
		return parent::getSubtitle($escape_output);
	}
	public function hasSubtitle() {
		return parent::hasSubtitle();
	}
	
	public function getPublicSubmitterInfo($escape_output = true) {
		return parent::getPublicSubmitterInfo($escape_output);
	}
	public function hasPublicSubmitterInfo() {
		return parent::hasPublicSubmitterInfo();
	}
	
	public function getLocationName($escape_output = true) {
		return parent::getLocationName($escape_output);
	}
	public function getLocationUrl($escape_output = true) {
		return parent::getLocationUrl($escape_output);
	}
	public function getLocationLinkTag() {
		return parent::getLocationLinkTag();
	}
	public function hasLocation() {
		return parent::hasLocation();
	}
	
	public function getDescription($edit_mode = false) {
		return parent::getDescription($edit_mode);
	}
	
	public function isLinkToNothing() {
		return parent::isLinkToNothing();
	}
	
	public function isLinkExternal() {
		return parent::isLinkExternal();
	}
	
	public function isLinkToDescription() {
		return parent::isLinkToDescription();
	}
	
	public function getLinkToDetailsPage() {
		return parent::getLinkToDetailsPage();
	}
	
	//CUSTOM FOR SDGL
	public function getSdglMembersLocationIds() {
		return parent::getSdglMembersLocationIds();
	}
	//END CUSTOM FOR SDGL
	
	public function getCategoryIds() {
		return parent::getCategoryIds();
	}
	
	public function getCategoryNames() {
		return parent::getCategoryNames();
	}
	
	public function getLengthInDays() {
		return parent::getLengthInDays();
	}
	
	public function isAllDay() {
		return parent::isAllDay();
	}
	
	public function isMultiDay() {
		return parent::isMultiDay();
	}
	
	public function isRepeating() {
		return parent::isRepeating();
	}
	
	/**
	 * Returns the start date/time of this occurrence.
	 * See http://php.net/date for format options.
	 */
	public function getStartFormatted($format) {
		return gmdate($format, $this->getStartTimestamp());
	}
	
	/**
	 * Returns the end date/time of this occurrence.
	 * See http://php.net/date for format options.
	 */
	public function getEndFormatted($format) {
		return gmdate($format, $this->getEndTimestamp());
	}
	
	public function getStartTimestamp() {
		$day_in_sequence = $this->getDayInSequence();
		$queried_day = TimestampConvert::daysSinceEpoch($this->day_query_timestamp);
		$start_days = $queried_day - ($day_in_sequence - 1);
		$start_day_seconds = $start_days * 86400;
		$start_time_seconds = is_null($this->start_time) ? 0 : $this->start_time;
		return $start_day_seconds + $start_time_seconds;
	}
	
	public function getEndTimestamp() {
		$day_in_sequence = $this->getDayInSequence();
		$event_length = $this->getLengthInDays();
		$queried_day = TimestampConvert::daysSinceEpoch($this->day_query_timestamp);
		$end_days = $queried_day + ($event_length - $day_in_sequence);
		$end_day_seconds = $end_days * 86400;
		$end_time_seconds = is_null($this->end_time) ? 0 : $this->end_time;
		return $end_day_seconds + $end_time_seconds;
	}
	
	/**
	 * Tells you which day in a multiday event
	 * this occurrence falls on. For example,
	 * if this event occurrence was returned for a "getByDay()"
	 * query for December 2, 2013, and this event is 3 days long
	 * (starting "yesterday" and ending "tomorrow"), then
	 * the occurence's "day in sequence" is 2.
	 * Use this in conjunction with getLengthInDays() to tell
	 * the user things like "this is a continuation of an event that
	 * started 2 days ago" and "this event continues for another 3 days"
	 * (or if displaying a calendar view, this info in conjunction with getLengthInDays()
	 * lets you know if you should display a "bar" that extends all the way to the left/right
	 * edge of a calendar day's box).
	 */
	public function getDayInSequence() {
		//multiday repeating (2nd day or higher)...
		if (!is_null($this->multiday_recurrence_day_in_sequence)) {
			$day_in_sequence = $this->multiday_recurrence_day_in_sequence;
			
		//multiday repeating (1st day)...
		} else if ($this->isRepeating()) {
			//since the multiday_recurrence_cache table
			// contains records for every day AFTER the first day
			// of each recurrence, we can deduce that a date
			// which doesn't have a cache record must be for
			// the first day of that event recurrence.
			$day_in_sequence = 1;
			
		//multiday non-repeating...
		} else if ($this->getLengthInDays() > 1) {
			$start_day = $this->getFirstRecurrence()->start_day; //can't call getTimeStamp(), because that function calls us, thus resulting in an infinite loop
			$queried_day = TimestampConvert::daysSinceEpoch($this->day_query_timestamp);
			$day_in_sequence = $queried_day - $start_day + 1;
			
		//single-day event...	
		} else {
			$day_in_sequence = 1;
		}
		
		return $day_in_sequence;
	}
	
	public function getDayInSequenceFormatted($format) {
		return gmdate($format, $this->day_query_timestamp);		
	}
	
}

class EventRecord extends EventBase {
	
	/////////////////////
	// FACTORY METHODS //
	///////////////////////////////////////////////////////////////////////////
	
	public static function getById($id) {
		$event = new EventRecord;
		$exists = $event->populateById($id);
		return $exists ? $event : null;
	}
	
	/**
	 * Returns null if invalid data is provided.
	 * (So you should always call EventRecord::validatePOST() BEFORE calling this function).
	 */
	public static function loadFromPOST($post) {
		if (self::validatePOST($post)->has()) {
			return null;
		}
		
		$event = new EventRecord;
		$event->populateFromPOST($post);
		return $event;
	}
	
	public static function getByTitleSearch($title, $category_id = null) {
		if (empty($title)) {
			return array();
		}
		
		$sql = 'SELECT id FROM dec_events AS ev WHERE ev.title LIKE ?';
		$vals = array("%{$title}%");
		
		if (!empty($category_id)) {
			$sql .= ' AND EXISTS (SELECT * FROM dec_event_categories cat WHERE cat.event_id = ev.id AND cat.category_id = ?)';
			$vals[] = (int)$category_id;
		}
		
		$ids = Loader::db()->GetCol($sql, $vals);
		$events = array();
		foreach ($ids as $id) {
			$events[] = self::getById($id);
		}
		
		return $events;
	}
	
	
	//////////////////////
	// PUBLIC ACCESSORS //
	///////////////////////////////////////////////////////////////////////////
	
	public function getId() {
		return parent::getId();
	}
	
	public function getTitle($escape_output = true) {
		return parent::getTitle($escape_output);
	}
	
	public function getSubtitle($escape_output = true) {
		return parent::getSubtitle($escape_output);
	}
	public function hasSubtitle() {
		return parent::hasSubtitle();
	}
	
	public function getPublicSubmitterInfo($escape_output = true) {
		return parent::getPublicSubmitterInfo($escape_output);
	}
	public function hasPublicSubmitterInfo() {
		return parent::hasPublicSubmitterInfo();
	}
	
	public function getLocationName($escape_output = true) {
		return parent::getLocationName($escape_output);
	}
	public function getLocationUrl($escape_output = true) {
		return parent::getLocationUrl($escape_output);
	}
	public function getLocationLinkTag() {
		return parent::getLocationLinkTag();
	}
	public function hasLocation() {
		return parent::hasLocation();
	}
	
	public function getDescription($edit_mode = false) {
		return parent::getDescription($edit_mode);
	}
	
	public function getLinkToCID() {
		return parent::getLinkToCID();
	}
	
	public function getLinkToUrl() {
		return parent::getLinkToUrl();
	}
	
	public function isLinkToNothing() {
		return parent::isLinkToNothing();
	}
	
	public function isLinkExternal() {
		return parent::isLinkExternal();
	}
	
	public function isLinkToDescription() {
		return parent::isLinkToDescription();
	}
	
	public function getLinkToDetailsPage() {
		return parent::getLinkToDetailsPage();
	}
	
	//CUSTOM FOR SDGL
	public function getSdglMembersLocationIds() {
		return parent::getSdglMembersLocationIds();
	}
	//END CUSTOM FOR SDGL

	public function getCategoryIds() {
		return parent::getCategoryIds();
	}
	
	public function getCategoryNames() {
		return parent::getCategoryNames();
	}
	
	public function getLengthInDays() {
		return parent::getLengthInDays();
	}
	
	public function isAllDay() {
		return parent::isAllDay();
	}
	
	public function isMultiDay() {
		return parent::isMultiDay();
	}
	
	public function isFinalDayIgnoredByQuery() {
		return parent::isFinalDayIgnoredByQuery();
	}
	
	//CUSTOM FOR SDGL
	public function isEndTimeHidden() {
		return parent::isEndTimeHidden();
	}

	public function isRepeating() {
		return parent::isRepeating();
	}
	
	/**
	 * Returns the start date/time of the event
	 * (or of the first occurrence if this is a repeating event).
	 * See http://php.net/date for format options.
	 */
	public function getStartFormatted($format) {
		return gmdate($format, $this->getStartTimestamp());
	}
	
	/**
	 * Returns the end date/time of the event
	 * (or of the first occurrence if this is a repeating event).
	 * See http://php.net/date for format options.
	 */
	public function getEndFormatted($format) {
		return gmdate($format, $this->getEndTimestamp());
	}
	
	public function getStartTimestamp() {
		$start_days = $this->getFirstRecurrence()->start_day;
		$start_day_seconds = $start_days * 86400;
		$start_time_seconds = is_null($this->start_time) ? 0 : $this->start_time;
		return $start_day_seconds + $start_time_seconds;
	}
	
	public function getEndTimestamp() {
		$end_days = ($this->getFirstRecurrence()->start_day + $this->length_extra_days);
		$end_day_seconds = $end_days * 86400;
		$end_time_seconds = is_null($this->end_time) ? 0 : $this->end_time;
		return $end_day_seconds + $end_time_seconds;
	}
	
	/**
	 * Returns one of four strings:
	 *  'daily'
	 *  'weekly'
	 *  'monthly'
	 *  'yearly'
	 * (or null if this is not a repeating event)
	 *
	 * NOTE that the returned string is NOT localized!
	 * (It is more of a code / constant than a human-readable display value.)
	 */
	public function getRepeatUnit() {
		return parent::getRepeatUnit();
	}
	
	/**
	 * Returns an integer for the repeat frequency
	 *  (e.g. 1 for "repeats every day" or "repeats every week",
	 *  3 for "repeats every 3 days" or "repeats every 3 months", etc.)
	 * Returns null if this is not a repeating event.
	 */
	public function getRepeatFrequency() {
		return parent::getRepeatFrequency();
	}
	
	/**
	 * Returns an array of integers representing the weekdays this event repeats on.
	 *  1=Monday ... 7=Sunday
	 * Returns empty array if this isn't a weekly repeating event
	 */
	public function getRepeatWeeklyOn() {
		$repeat_weekly_on = array();
		if ($this->getRepeatUnit() == 'weekly') {
			foreach ($this->recurrence_data as $recurrence) {
				$repeat_weekly_on[] = TimestampConvert::dayOfWeek(($recurrence->start_day * 86400));
			}
		}
		
		sort($repeat_weekly_on);
		
		return $repeat_weekly_on;
	}
	
	/**
	 * For monthly repeating events, returns one of three strings:
	 *  'day_of_month' (e.g. "repeats on the 15th of every month")
	 *  'day_of_week_from_first' (e.g. "repeats on the 4th Tuesday each month")
	 *  'day_of_week_from_last' (e.g. "repeats on the 2nd-last Wednesday each month")
	 * For events that don't repeat monthly, returns null.
	 *
	 * NOTE that the returned string is NOT localized!
	 * (It is more of a code / constant than a human-readable display value.)
	 */
	public function getRepeatMonthlyBy() {
		return parent::getRepeatMonthlyBy();
	}
	
	/**
	 * For repeating events, returns one of three strings:
	 *  'never' (event repeats forever)
	 *  'after' (event repeats for a number of occurrences)
	 *  'on' (event repeats until a certain date)
	 * For non-repeating events, returns null.
	 *
	 * NOTE that the returned string is NOT localized!
	 * (It is more of a code / constant than a human-readable display value.)
	 */
	public function getRepeatEnds() {
		return parent::getRepeatEnds();
	}
	
	public function getRepeatEndsOnFormatted($format) {
		return parent::getRepeatEndsOnFormatted($format);
	}
	
	public function getRepeatEndsAfterCount() {
		return parent::getRepeatEndsAfterCount();
	}
	
	/**
	 * Returns a human-readable summary of the overall repetition pattern (e.g. "Repeats Weekly on Monday, Wednesday, Friday" or "Repeats monthly, every 2 months, on the last Thursday")
	 */
	public function getRepeatSummary($escape_output = true) {
		
		/* WARNING: This function's logic is duplicated in the front-end javascript code (single_pages/dashboard/event_calendar/events/edit.php) */
		
		$summary = '';
		
		if ($this->isRepeating()) {
			$days_of_week = array(
				1 => t('Monday'),
				2 => t('Tuesday'),
				3 => t('Wednesday'),
				4 => t('Thursday'),
				5 => t('Friday'),
				6 => t('Saturday'),
				7 => t('Sunday'),
			);
			
			$start_ts = $this->getStartTimestamp();
			
			$repeat_unit = $this->getRepeatUnit();
			$repeat_frequency = $this->getRepeatFrequency();
			if ($repeat_unit == 'daily') {
				if ($repeat_frequency == 1) {
					$summary .= t('Daily');
				} else {
					$summary .= t('Every') . ' ' . t($repeat_frequency) . ' ' . t('days');
				}
			} else if ($repeat_unit == 'weekly') {
				if ($repeat_frequency == 1) {
					$summary .= t('Weekly');
				} else {
					$summary .= t('Every') . ' ' . t($repeat_frequency) . ' ' . t('weeks');
				}
				
				$repeat_weekly_on = $this->getRepeatWeeklyOn();
				if (count($repeat_weekly_on) == 7) {
					$summary .= ' ' . t('on all days');
				} else {
					$summary .= ' ' . t('on') . ' ';
					$chosen_week_day_names = array();
					foreach ($repeat_weekly_on as $week_day_number) {
						$chosen_week_day_names[] = $days_of_week[$week_day_number];
					}
					$summary .= implode(', ', $chosen_week_day_names);
				}
			} else if ($repeat_unit == 'monthly') {
				if ($repeat_frequency == 1) {
					$summary .= t('Monthly');
				} else {
					$summary .= t('Every') . ' ' . t($repeat_frequency) + ' ' . t('months');
				}
				
				$repeat_monthly_by = $this->getRepeatMonthlyBy();
				$start_date_day_of_month = TimestampConvert::dayOfMonth($start_ts);
				$start_date_day_of_week = TimestampConvert::dayOfWeek($start_ts);
				
				if ($repeat_monthly_by == 'day_of_week_from_first') {
					$start_date_day_of_week_ordinal = TimestampConvert::dayOfWeekPositionInMonth($start_ts);
					$ordinal_names = array(1 => t('first'), 2 => t('second'), 3 => t('third'), 4 => t('fourth'), 5 => t('fifth'), 6 => t('sixth'));
					$summary .= ' ' . t('on the') . ' ' . $ordinal_names[$start_date_day_of_week_ordinal] . ' ' . $days_of_week[$start_date_day_of_week];
				} else if ($repeat_monthly_by == 'day_of_week_from_last') {
					$start_date_ordinal_from_end_of_month = TimestampConvert::dayOfWeekPositionFromLast($start_ts, false);
					$ordinal_names = array(1 =>t('last'), 2 => t('second-to-last'), 3 => t('third-to-last'), 4 => t('fourth-to-last'), 5 => t('fifth-to-last'), 6 => t('sixth-to-last'));
					$summary .= ' ' . t('on the') . ' ' . $ordinal_names[$start_date_ordinal_from_end_of_month] . ' ' . $days_of_week[$start_date_day_of_week];
				} else {
					$summary .= ' ' . t('on day') . ' ' . $start_date_day_of_month;
				}
			} else if ($repeat_unit == 'yearly') {
				if ($repeat_frequency == 1) {
					$summary .= t('Annually');
				} else {
					$summary .= t('Every') . ' ' . $repeat_frequency . ' ' . t('years');
				}
				$summary .= ' ' . t('on') . ' ' . gmdate('F j', $start_ts);
			}
			
			$repeat_ends = $this->getRepeatEnds();
			if ($repeat_ends == 'after') {
				$repeat_ends_after = $this->getRepeatEndsAfterCount();
				if (!empty($repeat_ends_after)) {
					if ($repeat_ends_after > 1) {
						$summary .= ', ' . $repeat_ends_after . ' ' . t('times');
					} else if ($repeat_ends_after == 1) {
						$summary = t('Once'); //override the entire string (this is the same as not repeating at all)
					}
				}
			} else if ($repeat_ends == 'on') {
				$repeat_ends_on = $this->getRepeatEndsOnTimestamp();
				if (!empty($repeat_ends_on)) {
					$summary .= ', ' . t('until') . ' ' . gmdate('F j, Y', $repeat_ends_on);
				}
			}
		}
		
		return $escape_output ? Loader::helper('text')->entities($summary) : $summary;
	}
	
	
	
	///////////////////////////////////////////
	// PUBLIC METHODS (validate/save/delete) //
	///////////////////////////////////////////////////////////////////////////
	
	/**
	 * Returns a C5 Error object
	 */
	public static function validatePOST($post) {
		$e = Loader::helper('validation/error', 'designer_event_calendar'); //load our custom helper to facilitate unit testing (see comment in the helper override file for details)
		
		if (empty($post['title'])) {
			$e->add(t('Title is required.'));
		} else if (strlen($post['title']) > 255) {
			$e->add(t('Title cannot exceed 255 characters in length.'));
		}
		
		//don't bother validating subtitle (it's not required, and if it's longer than 255 characters it will just get truncated when saved to the db)
		//ditto for location name/url
		
		list($start_ts, $end_ts) = parent::extractStartAndEndTimestampsFromPOST($post);
		$start_days = TimestampConvert::daysSinceEpoch($start_ts);
		$end_days = TimestampConvert::daysSinceEpoch($end_ts);
		
		if (empty($start_ts)) {
			$e->add(t('You must choose a "From" date'));
		} else if (!empty($end_ts) && ($end_ts < $start_ts)) {
			$e->add(t('"From" must start before "To" date'));
		}
		//Note that we don't need to check for empty $end_ts,
		// because the extractStartAndEndTimestampsFromPOST
		// function sets it to the $start_ts when empty.
		//Also note that since we're retrieving full timestamps,
		// the above check also catches if the end time is
		// before the start time on a single-day event.
		
		
		$ignore_final_day = !empty($post['is_final_day_ignored_by_query']);
		$is_all_day = !empty($post['is_all_day']);
		$is_single_day = ($start_days == $end_days);
		if ($ignore_final_day && $is_all_day) {
			$e->add(t('The "hide final day in event list" option is not applicable to all-day events.'));
		} else if ($ignore_final_day && $is_single_day) {
			$e->add(t('The "hide final day in event list" option is not applicable to single-day events.'));
		}
		
		
		if (!empty($post['is_repeating'])) {
			if (empty($post['repeat_unit']) || !in_array($post['repeat_unit'], array('daily', 'weekly', 'monthly', 'yearly'))) {
				$e->add(t('You must choose a "repeat" option (daily/weekly/monthly/yearly)'));
			} else if ($post['repeat_unit'] == 'weekly') {
				if (empty($post['repeat_weekly_on']) || !is_array($post['repeat_weekly_on'])) {
					$e->add(t('You must choose at least one day of the week for this event to repeat on'));
				} else {
					$chosen_week_days = array(); //for dup checking
					foreach ($post['repeat_weekly_on'] as $week_day) {
						if (empty($week_day) || !ctype_digit($week_day) || $week_day < 1 || $week_day > 7 || in_array($week_day, $chosen_week_days)) {
							$e->add(t('Invalid day selection for weekly "repeat on"'));
							break;
						}
						$chosen_week_days[] = $week_day;
					}
				}
			} else if ($post['repeat_unit'] == 'monthly') {
				if (empty($post['repeat_monthly_by']) || !in_array($post['repeat_monthly_by'], array('day_of_month', 'day_of_week_from_first', 'day_of_week_from_last'))) {
					$e->add(t('You must choose a monthly "repeat by" option'));
				}
			}
			
			if (empty($post['repeat_frequency']) || !ctype_digit($post['repeat_frequency']) || $post['repeat_frequency'] < 1) {
				$e->add(t('You must choose a "repeat every" option'));
			} else if ($post['repeat_frequency'] > 30) {
				$e->add(t('The "repeat every" number cannot exceed 30'));
			}
			
			
			if (empty($post['repeat_ends']) || !in_array($post['repeat_ends'], array('never', 'after', 'on'))) {
				$e->add(t('You must choose how long this event repeats for'));
			} else if ($post['repeat_ends'] == 'never') {
				$is_single_day = ($start_days == $end_days);
				$is_two_day = ($start_days == ($end_days - 1));
				if ((!$ignore_final_day && !$is_single_day) || ($ignore_final_day && !$is_two_day)) {
					$e->add(t('Multi-day events cannot repeat forever'));
				}
			} else if ($post['repeat_ends'] == 'after') {
				if (empty($post['repeat_ends_after_count']) || !ctype_digit($post['repeat_ends_after_count']) || $post['repeat_ends_after_count'] < 1) {
					$e->add(t('You must enter a number for "ends after occurrences"'));
				} else if ($post['repeat_ends_after_count'] > 999) {
					$e->add(t('The "ends after occurrences" number cannot exceed 999'));
				}
			} else if ($post['repeat_ends'] == 'on') {
				if (empty($post['repeat_ends_on_date'])) {
					$e->add(t('You must enter a repeat "ends on" date'));
				} else {
					$repeat_ends_on_ts = TimezoneHelper::gmstrtotime($post['repeat_ends_on_date']);
					if (empty($repeat_ends_on_ts)) {
						$e->add(t('Repeat "ends on" date is not valid'));
					} else if ($repeat_ends_on_ts < $start_ts) {
						$e->add(t('Repeat "ends on" date cannot be earlier than the event start date'));
					}
				}

			}
		}
		
		return $e;
	}
	
	
	/**
	 * Returns saved event id
	 */
	public function save() {
		$db = Loader::db();
		
		//EVENT RECORD...
			$event_record = array(
				'title' => $this->title,
				'subtitle' => $this->subtitle,
				'public_submitter_info' => $this->public_submitter_info,
				'location_name' => $this->location_name,
				'location_url' => $this->location_url,
				'description' => $this->description,
				'link_to_cID' => $this->link_to_cID,
				'link_to_url' => $this->link_to_url,
				'link_to_nothing' => $this->link_to_nothing,
				'start_time' => $this->start_time,
				'end_time' => $this->end_time,
				'repeat_interval_days' => $this->repeat_interval_days,
				'repeat_interval_weeks' => $this->repeat_interval_weeks,
				'repeat_interval_months' => $this->repeat_interval_months,
				'repeat_interval_years' => $this->repeat_interval_years,
				'repeat_or_occur_until_day' => $this->repeat_or_occur_until_day,
				'repeat_count' => $this->repeat_count,
				'length_extra_days' => $this->length_extra_days,
				'is_final_day_ignored_by_query' => $this->is_final_day_ignored_by_query,
				'is_end_time_hidden' => !empty($this->is_end_time_hidden), //custom for sdgl
			);
		
			if (empty($this->id)) {
				$db->AutoExecute('dec_events', $event_record, 'INSERT');
				$this->id = $db->Insert_ID();
			} else {
				$this->id = (int)$this->id; //cast to int just to be sure (since it will be injected directly into SQL in the next line)
				$db->AutoExecute('dec_events', $event_record, 'UPDATE', "id={$this->id}");
			}
		
		//RECURRENCE RECORDS...
			$sql = "DELETE FROM dec_event_recurrences WHERE event_id = ?";
			$vals = array((int)$this->id);
			$db->Execute($sql, $vals);
			foreach ($this->recurrence_data as $recurrence) {
				$recurrence_record = array(
					'event_id' => $this->id,
					'start_day' => $recurrence->start_day,
					'start_month' => $recurrence->start_month,
					'start_year' => $recurrence->start_year,
					'week_of_month' => $recurrence->week_of_month,
					'day_of_week' => $recurrence->day_of_week,
					'day_of_month' => $recurrence->day_of_month,
					'month_of_year' => $recurrence->month_of_year,
				);
				$db->AutoExecute('dec_event_recurrences', $recurrence_record, 'INSERT');
			}
		
		//CATEGORY RECORDS...
			$category_ids = $this->getCategoryIds(); //do this before deleting existing records, in case category ids were not populated from POST (because in that case they'd need to be retrieved from the database)
			
			$sql = "DELETE FROM dec_event_categories WHERE event_id = ?";
			$vals = array((int)$this->id);
			$db->Execute($sql, $vals);
			
			$stmt = $db->Prepare("INSERT INTO dec_event_categories (event_id, category_id) VALUES (?, ?)");
			foreach ($category_ids as $category_id) {
				$vals = array((int)$this->id, (int)$category_id);
				$db->Execute($stmt, $vals);
			}
		
		//If a repeating event was changed to a non-repeating event, delete any existing cancellation records
			if (!$this->isRepeating()) {
				$sql = 'DELETE FROM dec_event_cancellations WHERE event_id = ?';
				$vals = array((int)$this->id);
				$db->Execute($sql, $vals);
			}
		
		//MULTIDAY RECURRENCE CACHE RECORDS...
			$this->rebuildMultidayRecurrenceCache();
		
		//CUSTOM FOR SDGL: Member locations...
			$sdgl_members_location_ids = $this->getSdglMembersLocationIds(); //do this before deleting existing records, in case ids were not populated from POST (because in that case they'd need to be retrieved from the database)
			
			$sql = "DELETE FROM dec_event_sdgl_members_locations WHERE event_id = ?";
			$vals = array((int)$this->id);
			$db->Execute($sql, $vals);
			
			$stmt = $db->Prepare("INSERT INTO dec_event_sdgl_members_locations (event_id, sdgl_members_location_id) VALUES (?, ?)");
			foreach ($sdgl_members_location_ids as $sdgl_members_location_id) {
				$vals = array((int)$this->id, (int)$sdgl_members_location_id);
				$db->Execute($stmt, $vals);
			}

		
		return $this->id;
	}
	
	public function delete($occurrence_start_date = null, $also_delete_all_future_occurrences = false) {
		if (empty($this->id)) {
			throw new Exception(t('Designer Event Calender - Developer Error: EventRecord::delete() was called without first loading an event via EventRecord::getById().'));
		}
		
		if (empty($occurrence_start_date) || !$this->isRepeating()) {
			$this->deleteEveryOccurrence();
		} else {
			$occurrence_start_day = TimestampConvert::daysSinceEpoch(TimezoneHelper::gmstrtotime($occurrence_start_date));
			$first_start_day = $this->getFirstRecurrence()->start_day;
			if ($occurrence_start_day <= $first_start_day) {
				//we are being asked to delete the very first occurrence and all future occurrences,
				// which is equivelant to "delete the whole thing"
				$this->deleteEveryOccurrence();
			} else if ($also_delete_all_future_occurrences) {
				$this->deleteThisAndFutureOccurrences($occurrence_start_day);
			} else {
				$this->deleteOneOccurrence($occurrence_start_day);
			}
		}
	}
	
	//Returns array of cancellation dates (deleted event occurrences) for the given event.
	//The returned array key is days-since-unix-epoch, array value is timestamp.
	//Note that for multi-day events, only the start day is returned.
	//As a convenience, you can pass in an empty $event_id and we'll return an empty array.
	public static function getCancellations($event_id) {
		if (empty($event_id)) {
			return array();
		}
		
		$sql = 'SELECT start_day FROM dec_event_cancellations WHERE event_id = ? ORDER BY start_day ASC';
		$vals = array((int)$event_id);
		$days = Loader::db()->GetCol($sql, $vals);
		
		$dates = array();
		foreach ($days as $day) {
			$dates[$day] = ($day * 86400);
		}
		
		return $dates;
	}
	
	public function uncancelOccurrence($occurrence_start_day) {
		$sql = 'DELETE FROM dec_event_cancellations WHERE event_id = ? AND start_day = ?';
		$vals = array((int)$this->id, (int)$occurrence_start_day);
		Loader::db()->Execute($sql, $vals);
		$this->rebuildMultidayRecurrenceCache();
	}
	
}



//Provides an strtotime function that acts as if all times are UTC
// (just like gmdate() is for date()).
class TimezoneHelper {
	public static function gmstrtotime($time, $now = null) {
		$orig_tz = date_default_timezone_get();
		date_default_timezone_set('UTC');
		$ts = is_null($now) ? strtotime($time) : strtotime($time, $now);
		date_default_timezone_set($orig_tz);
		return $ts;
	}
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//DO NOT USE THIS CLASS DIRECTLY!
//ALWAYS USE EITHER EventOccurrence OR EventRecord!
class EventBase {
	
	/////////////////////
	// Private Members //
	///////////////////////////////////////////////////////////////////////////
	protected $id;
	protected $title;
	protected $subtitle;
	protected $public_submitter_info;
	protected $location_name;
	protected $location_url;
	protected $description;
	protected $link_to_cID;
	protected $link_to_url;
	protected $link_to_nothing;
	
	protected $start_time; //number of seconds from midnight on the first (or only) day of event, or null for all-day events
	protected $end_time; //number of seconds from midnight of the last (or only) day of event, or null for all-day events
	
	protected $repeat_interval_days; //Number of days between event repetition. THIS IS USED FOR DAYS AND WEEKS!
	protected $repeat_interval_weeks; //FOR EDITING UI ONLY (not calculations): number of weeks between event repetition
	protected $repeat_interval_months; //Number of months between event repetition (e.g. "every month" or "every 3 months")
	protected $repeat_interval_years; //Number of years between event repetition (e.g. "every year" or "every 2 years")
	//note that non-recurring events will have NULL for all 4 of the above fields
	
	protected $repeat_or_occur_until_day; //For repeating events, this specifies a limiting date after which no recurrences will be generated for this event. Set this column to NULL for no limit (infinitely-recurring events). For non-recurring events, this is the ending date of the event (so for non-recurring single-day events it equals the start_day, and for non-recurring multi-day events it equals start_day plus length_extra_days). Stored as number of days since unix epoch (so unix timestamp at midnight divided by 86,400).
	protected $repeat_count; //FOR EDITING UI ONLY (not calculations): number of times to repeat the event. At time of save, this will be converted to a "repeat_or_occur_until_day" value, as that field is always used for calculations (but we retain this number separately so we can put it into the edit interface for the user when they bring up this event again for editing).
	protected $length_extra_days; //number of days past the first day that an occurrence of a multi-day event lasts. 0 for single-day events, 1 for 2-day events, 2 for 3-day events, etc. Note that this field isn't queried on (because non-repeating multi-day events store their "end date" in the repeat_or_occur_until_day field, and repeating multi-day events store this information in the cache table), but it is used by the editing UI to know what the user chose (and to populate the repeat_or_occur_until_day field or the cache table upon event save), and by the view ui to quickly display an event's "thru" date.
	protected $is_final_day_ignored_by_query; //For multiday non-all-day events, denotes if the last day of the event (or the last day of each recurrence if the event repeats) should be ignored in the getByDay query. This is useful for "late-night" events that go on past midnight, so you want to show the ending time but do not want to list the event as occurring on that last date.
	protected $is_end_time_hidden; //CUSTOM FOR SDGL
	
	protected $multiday_recurrence_day_in_sequence = null; //only applies when object is populated via day query
	protected $day_query_timestamp = null; //only applies when object is populated via day query
	
	protected $recurrence_data = array(); //array of EventRecurrenceRecord objects
	
	protected $category_ids = null; //lazy-loaded (or populated from POST)... will become an array of integers when asked for
	protected $category_names = null; //lazy-loaded... will become an array of category id/names (key is category id, value is category name) when asked for
	
	//CUSTOM FOR SDGL
	protected $sdgl_members_location_ids = null; //lazy-loaded (or populated from POST)... will become an array of integers when asked for
	//END CUSTOM FOR SDGL
	
	
	//////////////////////////////////////////////////////////////
	// PUBLIC ACCESSORS                                         //
	// (utilized internally by EventRecord AND EventOccurrence, //
	//  but not necessarily exposed publicly by both)           //
	///////////////////////////////////////////////////////////////////////////
	
	protected function getId() {
		return $this->id;
	}
	
	protected function getTitle($escape_output = true) {
		return $escape_output ? Loader::helper('text')->entities($this->title) : $this->title;
	}
	
	protected function getSubtitle($escape_output = true) {
		return $escape_output ? Loader::helper('text')->entities($this->subtitle) : $this->subtitle;
	}
	protected function hasSubtitle() {
		return !empty($this->subtitle);
	}
	
	protected function getPublicSubmitterInfo($escape_output = true) {
		return $escape_output ? Loader::helper('text')->entities($this->public_submitter_info) : $this->public_submitter_info;
	}
	protected function hasPublicSubmitterInfo() {
		return !empty($this->public_submitter_info);
	}
	
	public function getLocationName($escape_output = true) {
		return $escape_output ? Loader::helper('text')->entities($this->location_name) : $this->location_name;
	}
	public function getLocationUrl($escape_output = true) {
		return $escape_output ? Loader::helper('text')->entities($this->location_url) : $this->location_url;
	}
	//Dev note about this function name:
	// Within C5 naming conventions there is some inconsistency
	// around the term "Link". Note that the `getLinkToDetailsPage()` function
	// should probably have been named `getUrlToDetailsPage()`
	// (or `getDetailsPageUrl()`), but that ship has sailed...
	//This function, on the other hand, returns the full `<a>` tag (not just the URL).
	public function getLocationLinkTag() {
		if (!$this->hasLocation()) {
			return null;
		}
		
		$link = empty($this->location_name) ? $this->getLocationUrl() : $this->getLocationName();
		if (!empty($this->location_url)) {
			$link = '<a href="' . $this->getLocationUrl() . '" target="_blank" rel="noopener noreferrer">' . $link . '</a>';
		}
		
		return $link;
	}
	public function hasLocation() {
		return (!empty($this->location_name) && !empty($this->location_url));
	}
	
	protected function getDescription($edit_mode = false) {
		$ch = Loader::helper('content', 'designer_event_calendar');
		if ($edit_mode) {
			//if putting into a WYSIWYG editor (TinyMCE) widget
			return $ch->translateFromEditMode($this->description);
		} else {
			//if displaying as html
			return $ch->translateFrom($this->description);
		}
	}
	
	protected function getLinkToCID() {
		return $this->link_to_cID;
	}
	
	protected function getLinkToUrl() {
		return $this->link_to_url;
	}
	
	protected function isLinkToNothing() {
		return $this->link_to_nothing;
	}
	
	protected function isLinkExternal() {
		return (empty($this->link_to_cID) && !empty($this->link_to_url));
	}
	
	protected function isLinkToDescription() {
		return (empty($this->link_to_cID) && empty($this->link_to_url));
	}
	
	protected function getLinkToDetailsPage() {
		if ($this->isLinkToNothing()) {
			return null;
		}
		
		$url = '';
		
		if (!empty($this->link_to_cID)) {
			$page = Page::getByID($this->link_to_cID);
			if ($page->getCollectionID()) {
				$url = Loader::helper('navigation')->getLinkToCollection($page);
			}
		} else if (!empty($this->link_to_url)) {
			$url = $this->link_to_url;
		}
		
		if (empty($url)) {
			$url = View::url('/calendar-event-details', $this->getId());
		}
		
		return $url;
	}
	
	//CUSTOM FOR SDGL
	protected function getSdglMembersLocationIds() {
		if (is_null($this->sdgl_members_location_ids)) {
			$this->populateSdglMembersLocationIds();
		}
		return $this->sdgl_members_location_ids;
	}
	//END CUSTOM FOR SDGL

	protected function getCategoryIds() {
		if (is_null($this->category_ids)) {
			$this->populateCategoryIds();
		}
		return $this->category_ids;
	}
	
	protected function getCategoryNames() {
		if (is_null($this->category_names)) {
			$this->populateCategoryNames();
		}
		return $this->category_names;
	}
	
	protected function getLengthInDays() {
		return $this->length_extra_days + 1;
	}
	
	protected function isAllDay() {
		return (is_null($this->start_time) && is_null($this->end_time));
	}
	
	protected function isMultiDay() {
		return ($this->getLengthInDays() > 1);
	}
	
	protected function isFinalDayIgnoredByQuery() {
		return $this->is_final_day_ignored_by_query;
	}

	//CUSTOM FOR SDGL
	public function isEndTimeHidden() {
		return $this->is_end_time_hidden;
	}

	protected function isRepeating() {
		return !is_null($this->getRepeatUnit());
	}
	
	protected function getRepeatUnit() {
		if (!is_null($this->repeat_interval_years)) {
			return 'yearly';
		} else if (!is_null($this->repeat_interval_months)) {
			return 'monthly';
		} else if (!is_null($this->repeat_interval_weeks)) {
			//It's important that we check 'weekly' before 'daily',
			// because weekly also has non-null repeat_interval_days value!
			return 'weekly';
		} else if (!is_null($this->repeat_interval_days)) {
			return 'daily';
		} else {
			return null;
		}
	}
	
	protected function getRepeatFrequency() {
		$var_map = array(
			'yearly' => $this->repeat_interval_years,
			'monthly' => $this->repeat_interval_months,
			'weekly' => $this->repeat_interval_weeks,
			'daily' => $this->repeat_interval_days,
		);
		
		$repeat_unit = $this->getRepeatUnit();
		
		if (array_key_exists($repeat_unit, $var_map)) {
			return $var_map[$repeat_unit];
		} else {
			return null;
		}
	}
	
	protected function getRepeatMonthlyBy() {
		if ($this->getRepeatUnit() == 'monthly') {
			$week_of_month = (int)$this->getFirstRecurrence()->week_of_month;
			if ($week_of_month > 0) {
				return 'day_of_week_from_first';
			} else if ($week_of_month < 0) {
				return 'day_of_week_from_last';
			} else {
				return 'day_of_month';
			}
		} else {
			return null;
		}
	}
	
	protected function getRepeatEnds() {
		if ($this->isRepeating()) {
			if (is_null($this->repeat_or_occur_until_day)) {
				return 'never';
			} else if (is_null($this->repeat_count)) {
				return 'on';
			} else {
				return 'after';
			}
		} else {
			return null;
		}
	}

	protected function getRepeatEndsOnFormatted($format) {
		if ($this->getRepeatEnds() == 'on') {
			return gmdate($format, $this->getRepeatEndsOnTimestamp());
		} else {
			return null;
		}
	}
	
	protected function getRepeatEndsAfterCount() {
		if ($this->getRepeatEnds() == 'after') {
			return $this->repeat_count;
		} else {
			return null;
		}
	}
	
	
	/////////////////////////////
	// Data Population Methods //
	///////////////////////////////////////////////////////////////////////////
	
	/**
	 * Don't call this directly -- instead use EventRecord::loadFromPOST()
	 *
	 * Returns nothing
	 */
	public function populateFromPOST($post) {
		if (EventRecord::validatePOST($post)->has()) {
			throw new Exception(t('Designer Event Calendar Developer Error: EventRecord::populateFromPOST was called with invalid data. You must always validate POSTed data with EventRecord::validatePOST() before calling EventRecord::loadFromPOST()!'));
		}

		list($start_ts, $end_ts) = self::extractStartAndEndTimestampsFromPOST($post);
		$start_days = TimestampConvert::daysSinceEpoch($start_ts);
		$end_days = TimestampConvert::daysSinceEpoch($end_ts);
		
		//EVENT RECORD...
			$this->id = $post['id'];
			$this->title = $post['title'];
			$this->subtitle = $post['subtitle'];
			$this->public_submitter_info = $post['public_submitter_info'];
			$this->location_name = $post['location_name'];
			$this->location_url = $post['location_url'];
			$this->description = Loader::helper('content', 'designer_event_calendar')->translateTo($post['description']);
			
			$this->link_to_cID = null;
			$this->link_to_url = null;
			$this->link_to_nothing = 0;
			if (!empty($post['link_to_nothing'])) {
				$this->link_to_nothing = 1;
			} else if (!empty($post['link_to_cID'])) {
				$this->link_to_cID = (int)$post['link_to_cID'];
			} else if (!empty($post['link_to_url'])) {
				$this->link_to_url = $this->getNormalizeUrl($post['link_to_url']);
			}
			
			//Start/stop times
			$this->start_time = !empty($post['is_all_day']) ? null : TimestampConvert::secondsSinceMidnight($start_ts);
			$this->end_time = !empty($post['is_all_day']) ? null : TimestampConvert::secondsSinceMidnight($end_ts);
			
			$this->length_extra_days = $end_days - $start_days;
			$this->is_final_day_ignored_by_query = !empty($post['is_final_day_ignored_by_query']);
			$this->is_end_time_hidden = !empty($post['is_end_time_hidden']);
			
			//repeat intervals
			$this->repeat_interval_days = null;
			$this->repeat_interval_weeks = null;
			$this->repeat_interval_months = null;
			$this->repeat_interval_years = null;
			if (!empty($post['is_repeating'])) {
				$repeat_unit = $post['repeat_unit'];
				$repeat_freq = $post['repeat_frequency'];
				if ($repeat_unit == 'daily') {
					$this->repeat_interval_days = $repeat_freq;
				} else if ($repeat_unit == 'weekly') {
					$this->repeat_interval_weeks = $repeat_freq;
					$this->repeat_interval_days = ($repeat_freq * 7);
				} else if ($repeat_unit == 'monthly') {
					$this->repeat_interval_months = $repeat_freq;
				} else if ($repeat_unit == 'yearly') {
					$this->repeat_interval_years = $repeat_freq;
				}
			}
			
			//occurr/repeat until...			
			if (empty($post['is_repeating'])) {
				$this->repeat_or_occur_until_day = $end_days;
				$this->repeat_count = null;
			} else if ($post['repeat_ends'] == 'never') {
				$this->repeat_or_occur_until_day = null;
				$this->repeat_count = null;
			} else if ($post['repeat_ends'] == 'after') {
				$this->repeat_or_occur_until_day = TimestampConvert::daysSinceEpoch($this->getFinalRecurrenceDateForCount($start_days, $post));
				$this->repeat_count = $post['repeat_ends_after_count'];
			} else if ($post['repeat_ends'] == 'on') {
				$this->repeat_or_occur_until_day = TimestampConvert::daysSinceEpoch(TimezoneHelper::gmstrtotime($post['repeat_ends_on_date']));
				$this->repeat_count = null;
			}
		//END EVENT RECORD
		
		//RECURRENCE RECORDS...
			//In most cases, there's only one recurrence record,
			// so create 1 object now with common defaults
			// that can be used by all of the various cases below.
			$this->recurrence_data = array();
			$recurrence = new EventRecurrenceRecord;
			$recurrence->start_day = TimestampConvert::daysSinceEpoch($start_ts);
			$recurrence->start_month = TimestampConvert::monthsSinceEpoch($start_ts);
			$recurrence->start_year = TimestampConvert::yearsSinceEpoch($start_ts);
			$recurrence->week_of_month = null;
			$recurrence->day_of_week = null;
			$recurrence->day_of_month = null;
			$recurrence->month_of_year = null;
			
			$repeat_unit = $post['repeat_unit'];
			if (empty($post['is_repeating']) || ($repeat_unit == 'daily')) {
				$this->recurrence_data[] = $recurrence;
			} else if ($repeat_unit == 'weekly') {
				if (empty($post['repeat_weekly_on'])) {
					//if user didn't check any days-of-week,
					// then default to just the day-of-week of the start_date.
					$this->recurrence_data[] = $recurrence;
				} else {
					//go through 7 days, starting from start_date,
					// checking if each day-of-week is in the checkbox array.
					// Note that the start_date's day-of-week might not actually be checked!
					// (in which case the first start_date is the first checked day-of-week after it).
					for ($i = 0; $i < 7; $i++) {
						$test_ts = $start_ts + ($i * 86400);
						if (in_array(TimestampConvert::dayOfWeek($test_ts), $post['repeat_weekly_on'])) {
							$weekly_recurrence = clone $recurrence;
							$weekly_recurrence->start_day = TimestampConvert::daysSinceEpoch($test_ts);
							$weekly_recurrence->start_month = TimestampConvert::monthsSinceEpoch($test_ts);
							$weekly_recurrence->start_year = TimestampConvert::yearsSinceEpoch($test_ts);
							$this->recurrence_data[] = $weekly_recurrence;
						}
					}
				}
			} else if ($repeat_unit == 'monthly') {
				if ($post['repeat_monthly_by'] == 'day_of_month') {
					$recurrence->day_of_month = TimestampConvert::dayOfMonth($start_ts);
				} else if ($post['repeat_monthly_by'] == 'day_of_week_from_first') {
					$recurrence->week_of_month = TimestampConvert::dayOfWeekPositionInMonth($start_ts);
					$recurrence->day_of_week = TimestampConvert::dayOfWeek($start_ts);
				} else if ($post['repeat_monthly_by'] == 'day_of_week_from_last') {
					$recurrence->week_of_month = TimestampConvert::dayOfWeekPositionFromLast($start_ts, true);
					$recurrence->day_of_week = TimestampConvert::dayOfWeek($start_ts);
				}
				$this->recurrence_data[] = $recurrence;
			} elseif ($repeat_unit == 'yearly') {
				$recurrence->month_of_year = TimestampConvert::monthOfYear($start_ts);
				$this->recurrence_data[] = $recurrence;
			}
		//END RECURRENCE RECORDS
		
		//CATEGORIES...
		$this->category_ids = empty($post['category_ids']) ? array() : $post['category_ids'];
		//END CATEGORIES
		
		//No need to retrieve multiday recurrence cache records
		// (because that only exists to facilitate getByDay() queries -- not when editing records).
		
		//CUSTOM FOR SDGL: member locations
		$this->sdgl_members_location_ids = (empty($post['sdgl_members_location_ids']) || !is_array($post['sdgl_members_location_ids'])) ? array() : $post['sdgl_members_location_ids'];
		//END CUSTOM FOR SDGL
	}
	
	/**
	 * Don't call this directly -- instead use EventRecord::getById()
	 *
	 * Returns true if an event record exists for the given id,
	 * otherwise returns false.
	 */
	public function populateById($id) {
		$db = Loader::db();
		
		//Event record...
		$sql = "SELECT * FROM dec_events WHERE id = ?";
		$vals = array((int)$id);
		$event_record = $db->GetRow($sql, $vals);
		if (empty($event_record)) {
			return false;
		}
		$this->populateEventFieldsFromDBRecord($event_record);
		
		//Recurrence records...
		$sql = "SELECT * FROM dec_event_recurrences WHERE event_id = ? ORDER BY start_day ASC";
		$vals = array((int)$id);
		$recurrence_records = $db->GetArray($sql, $vals);
		foreach ($recurrence_records as $recurrence_record) {
			$this->recurrence_data[] = new EventRecurrenceRecord($recurrence_record);
		}
		
		return true;
	}
	
	/**
	 * Don't call this directly -- instead use EventOccurrence::getByDay()
	 *
	 * Returns nothing
	 */
	public function populateFromDayQuery($record, $day_query_timestamp) {
		$this->populateEventFieldsFromDBRecord($record);
		$this->recurrence_data = array(new EventRecurrenceRecord($record));
		
		$this->multiday_recurrence_day_in_sequence = $record['day_in_sequence'];
		$this->day_query_timestamp = $day_query_timestamp;
		
		return true;
	}
	
	
	
	////////////////////////////////
	// UTILITY & HELPER FUNCTIONS //
	///////////////////////////////////////////////////////////////////////////
	
	protected function populateEventFieldsFromDBRecord($record) {
		$this->id = $record['id'];
		$this->title = $record['title'];
		$this->subtitle = $record['subtitle'];
		$this->public_submitter_info = $record['public_submitter_info'];
		$this->location_name = $record['location_name'];
		$this->location_url = $record['location_url'];
		$this->description = $record['description'];
		$this->link_to_cID = $record['link_to_cID'];
		$this->link_to_url = $record['link_to_url'];
		$this->link_to_nothing = $record['link_to_nothing'];
		$this->start_time = $record['start_time'];
		$this->end_time = $record['end_time'];
		$this->repeat_interval_days = $record['repeat_interval_days'];
		$this->repeat_interval_weeks = $record['repeat_interval_weeks'];
		$this->repeat_interval_months = $record['repeat_interval_months'];
		$this->repeat_interval_years = $record['repeat_interval_years'];
		$this->repeat_or_occur_until_day = $record['repeat_or_occur_until_day'];
		$this->repeat_count = $record['repeat_count'];
		$this->length_extra_days = $record['length_extra_days'];
		$this->is_final_day_ignored_by_query = $record['is_final_day_ignored_by_query'];
		$this->is_end_time_hidden = !empty($record['is_end_time_hidden']);
	}
	
	//CUSTOM FOR SDGL
	protected function populateSdglMembersLocationIds() {
		if (empty($this->id)) {
			$this->sdgl_members_location_ids = array();
		} else {
			$sql = 'SELECT sdgl_members_location_id FROM dec_event_sdgl_members_locations WHERE event_id = ?';
			$vals = array((int)$this->id);
			$this->sdgl_members_location_ids = Loader::db()->GetCol($sql, $vals);
		}
	}
	//END CUSTOM FOR SDGL
	
	protected function populateCategoryIds() {
		if (empty($this->id)) {
			$this->category_ids = array();
		} else {
			$sql = 'SELECT c.id'
			     . ' FROM dec_event_categories ec'
			     . ' INNER JOIN dec_categories c'
			     . ' ON ec.category_id = c.id'
			     . ' WHERE ec.event_id = ?'
			     . ' ORDER BY c.display_order';
			$vals = array((int)$this->id);
			$this->category_ids = Loader::db()->GetCol($sql, $vals);
		}
	}
	
	protected function populateCategoryNames() {
		if (empty($this->id)) {
			//no event id probably means this object was populated via POST data,
			// so retrieve category data from the loaded category id's
			$id_array = $this->getCategoryIds();
			if (empty($id_array)) {
				return array();
			}
			$id_array = array_map('intval', $id_array); //ensure we are only dealing with integers (because we will be directly injecting these values into a SQL string)
			$id_string = implode(',', $id_array);
			$sql = "SELECT id, name FROM dec_categories WHERE id IN ({$id_string}) ORDER BY display_order";
			$vals = array();
		} else {
			//we have an event id, so JOIN against event_categories table
			$sql = 'SELECT c.id, c.name'
			     . ' FROM dec_event_categories ec'
			     . ' INNER JOIN dec_categories c'
			     . ' ON ec.category_id = c.id'
			     . ' WHERE ec.event_id = ?'
			     . ' ORDER BY c.display_order';
			$vals = array((int)$this->id);
		}
		$records = Loader::db()->GetArray($sql, $vals);
		$categories = array();
		$th = Loader::helper('text');
		foreach ($records as $r) {
			$categories[$r['id']] = $th->entities($r['name']);
		}
		$this->category_names = $categories;
	}
	
	//internal helper functions for populateFromPOST()
	protected function getFinalRecurrenceDateForCount($start_days, $post) {
		//DEV NOTE: We are *NOT* taking into account cancelled event occurrences!
		//Our assumption is that this is desired behavior for the user,
		// because cancelled event instance should not push back an end date for a series.
		
		$start_ts = ($start_days * 86400);
		if ($post['repeat_unit'] == 'daily') {
			//optimization: don't call the "getRecurrenceInstances" function
			// because we can very easily do a mathemagical shortcut for this one
			// (and hence save some resources by not having to "walk" all the dates).
			$days_since_start = ($post['repeat_ends_after_count'] - 1) * $post['repeat_frequency'];
			$end_ts = TimezoneHelper::gmstrtotime("+{$days_since_start} days", $start_ts);
		} else {
			//for all the other choices (weekly/monthly/yearly),
			// there is no way to mathematically calculate them -- because there are too many exceptions
			// (e.g. "repeat every month on the 5th week", "repeat on Tuesday AND Thursday", "repeat annually on Feb. 29th").
			// So for those we use brute force and just "walk the days" from the start date to determine the end date.
			$instances = RecurrenceHelpers::getRecurrenceInstances($start_ts, $post['repeat_unit'], $post['repeat_frequency'], $post['repeat_weekly_on'], $post['repeat_monthly_by'], $post['repeat_ends_after_count']);
			$end_ts = end($instances);
		}
		return $end_ts;
	}
	
	protected function getFirstRecurrence() {
		//DEV NOTE: We just look to the first recurrence record for a lot of the calculated data,
		// because that's the earliest one (and hence when the repetitions start).
		// In fact, the only situation where we even need to examine other recurrence records
		// (past the first one) is if the event is repeated weekly (since that's the only
		// repetition type that utilizes multiple recurrence records).
		return $this->recurrence_data[0];
	}
	
	protected function getRepeatEndsOnTimestamp() {
		if ($this->getRepeatEnds() == 'on') {
			return ($this->repeat_or_occur_until_day * 86400);
		} else {
			return null;
		}
	}
	
	/**
	 * returns TWO values (so use list() to put them into variables)...
	 * First is the start datetime, second is the end datetime.
	 * Datetimes are returned as unix timestamps.
	 * If the "end" datetime is empty, we set it to the start_datetime.
	 */
	protected static function extractStartAndEndTimestampsFromPOST($post) {
		if (empty($post['start_date'])) {
			return array(null, null);
		}
		
		$start_datetime = $post['start_date'];
		if (!empty($post['start_time'])) {
			$start_datetime .= ' ' . $post['start_time'];
		}
		
		if (empty($post['end_date'])) {
			$end_datetime = $start_datetime;
		} else {
			$end_datetime = $post['end_date'];
			if (!empty($post['end_time'])) {
				$end_datetime .= ' ' . $post['end_time'];
			}
		}
		
		$start_ts = TimezoneHelper::gmstrtotime($start_datetime);
		$end_ts = TimezoneHelper::gmstrtotime($end_datetime);
		return array($start_ts, $end_ts);
	}
	
	protected function rebuildMultidayRecurrenceCache() {
		$db = Loader::db();
		
		$sql = "DELETE FROM dec_event_multiday_recurrence_cache WHERE event_id = ?";
		$vals = array((int)$this->id);
		$db->Execute($sql, $vals);
		
		$length_extra_days = $this->length_extra_days;
		if ($this->isFinalDayIgnoredByQuery()) {
			$length_extra_days = $length_extra_days - 1;
		}
		
		if ($this->isRepeating() && ($this->getRepeatEnds() != 'never') && ($length_extra_days > 0)) {
			//this is a multi-day event,
			// so "walk" through every recurrence
			// and add a record to the cache table
			// for each day AFTER the first day of
			// each instance.
			$instances = RecurrenceHelpers::getRecurrenceInstances(
				$this->getStartTimestamp(),
				$this->getRepeatUnit(),
				$this->getRepeatFrequency(),
				$this->getRepeatWeeklyOn(),
				$this->getRepeatMonthlyBy(),
				$this->getRepeatEndsAfterCount(),
				$this->getRepeatEndsOnTimestamp()
			);
			
			$cancelled_days = array_keys(EventRecord::getCancellations($this->id));
			
			$stmt = $db->Prepare("INSERT INTO dec_event_multiday_recurrence_cache (event_id, occurs_on_day, day_in_sequence) VALUES (?, ?, ?)");
			
			foreach ($instances as $instance_ts) {
				$instance_days = TimestampConvert::daysSinceEpoch($instance_ts);
				//skip cancelled event occurrences (the getRecurrenceInstances() function does *NOT* exlude them!)
				if (!in_array($instance_days, $cancelled_days)) {
					for ($i = 1; $i <= $length_extra_days; $i++) {
						$occurs_on_day = $instance_days + $i;
						$day_in_sequence = ($i + 1);
						$vals = array((int)$this->id, (int)$occurs_on_day, (int)$day_in_sequence);
						$db->Execute($stmt, $vals);
					}
				}
			}
		}
	}
	
	protected function deleteEveryOccurrence() {
		$db = Loader::db();
		$vals = array((int)$this->id);
		$db->Execute("DELETE FROM dec_event_multiday_recurrence_cache WHERE event_id = ?", $vals);
		$db->Execute("DELETE FROM dec_event_cancellations WHERE event_id = ?", $vals);
		$db->Execute("DELETE FROM dec_event_recurrences WHERE event_id = ?", $vals);
		$db->Execute("DELETE FROM dec_event_categories WHERE event_id = ?", $vals);
		$db->Execute("DELETE FROM dec_events WHERE id = ?", $vals);
	}
	
	protected function deleteThisAndFutureOccurrences($occurrence_start_day) {
		//NOTE: We assume that $occurrence_start_day has already been validated
		// to be on or after the event's first start date!

		$repeat_until_day = ($occurrence_start_day - 1);
		
		//Deleteing all future occurrences basically means "make the repeat-ends-on date sooner"...
		$this->repeat_ends = 'on';
		$this->repeat_or_occur_until_day = $repeat_until_day;
		$this->repeat_count = null;
		$this->save();
	
		//delete existing cancellations that are past the new end date
		$sql = 'DELETE FROM dec_event_cancellations WHERE event_id = ? AND start_day > ?';
		$vals = array((int)$this->id, (int)$repeat_until_day);
		Loader::db()->Execute($sql, $vals);
	
		//note that we do not need to delete existing multiday_reccurrence_cache records that are past the new end date,
		// because the save() method rebuilds the cache anyway
	}
	
	protected function deleteOneOccurrence($occurrence_start_day) {
		$db = Loader::db();
		
		//Add a new record to the cancellations table
		$data = array(
			'event_id' => (int)$this->id,
			'start_day' => (int)$occurrence_start_day,
		);
		$db->AutoExecute('dec_event_cancellations', $data, 'INSERT');
		
		//Delete any multiday_reccurrence_cache records that might exist for that day
		$sql = 'DELETE FROM dec_event_multiday_recurrence_cache WHERE event_id = ? AND (occurs_on_day - day_in_sequence + 1) = ?';
		$vals = array((int)$this->id, (int)$occurrence_start_day);
		$db->Execute($sql, $vals);
	}
	
	//Attempt to create a full and valid url from potentially incomplete data.
	private function getNormalizeUrl($url) {
		$url = trim($url);
		
		if (empty($url)) {
			return '';
		} else if ($this->colonExistsBeforeDot($url)) {
			return $url; //probably already has "http://", "https://", "mailto:", "tel:", etc.
		} else if (strpos($url, '@') !== false) {
			return 'mailto:' . $url;
		} else {
			return 'http://' . $url;
		}
	}
	
	//Check if a colon (:) exists in the string AND it appears before any dots (.)
	//The purpose of this is to guess whether or not a string contains a URI scheme (http://, mailto:, tel:)
	// (since colons can legitemately appear in the path of URL's, we can't just check for their existence).
	private function colonExistsBeforeDot($str) {
		$colonPos = strpos($str, ':');
		$dotPos = strpos($str, '.');
		if ($colonPos === false) {
			return false;
		} else if ($dotPos === false) {
			return true;
		} else {
			return $colonPos < $dotPos;
		}
	}

	
}
//This class is a "dumb" object that only holds data (all functionality is in the EventRecord class)
//This class should *ONLY* be used by the EventRecord class!
class EventRecurrenceRecord {
	public $start_day; //The first (or only) date that an event occurs. Stored as number of days since unix epoch (so unix timestamp at midnight divided by 86,400). FYI, the reason this field is in this table and not the primary "events" table is to facilitate things like "repeats every Tuesday and Thursday" (which would have 2 records in this table, one for the recurrence that starts on Tuesday, and one for the recurrence that starts on Thursday).
	public $start_month; //Number of months since unix epoch. This facilitates queries for monthly-repeating events, but is just derived from the start_day upon save.
	public $start_year; //Number of years since unix epoch. This facilitates queries for yearly-repeating events, but is just derived from the start_day upon save.

	//The following 2 fields are ONLY used for events that repeat monthly on a relative day (e.g. "1st Thursday of the month", or "2nd-last Tuesday of the month")
	//They are *not* used for weekly repetitions (e.g. "weekly on Tuesday and Thursday")... those weekly repetitions are instead based off the "start_day" of each event_recurrences record
	public $week_of_month; //works in conjunction with day_of_week to specify things like "1st Thursday of the month", "4th Thursday of the month", or -1 for "last Thursday of the month", -2 for "2nd-last Thursday in the month"
	public $day_of_week; //1=Monday, 2=Tuesday, ... 7=Sunday. Works in conjunction with week_of_month.

	//The following 1 field is ONLY used for events that repeat monthly on an absolute day (e.g. "on the 1st of the month", or "on the 15th of the month")
	public $day_of_month; //e.g. 13th day of month (aka "every month on the 13th"), or negative to indicate "nth-last-day of month"). NOTE that "nth-last-day of month" is not currently supported in the editing UI, so for now this field should never contain a negative value (but in the future if the UI adds this, all the logic is set up for it to work)

	//The following 1 field is ONLY used for events that repeat yearly (e.g. "annually on June 2nd")
	public $month_of_year; //1=January, 2=February, etc. Only applies to yearly rules.
	
	///////////////////////////////////////////////////////////////////////////
	
	public function __construct($record = null) {
		if (!is_null($record)) {
			$this->populateFromDBRecord($record);
		}
	}
	private function populateFromDBRecord($record) {
		$this->start_day = $record['start_day'];
		$this->start_month = $record['start_month'];
		$this->start_year = $record['start_year'];
		$this->week_of_month = $record['week_of_month'];
		$this->day_of_week = $record['day_of_week'];
		$this->day_of_month = $record['day_of_month'];
		$this->month_of_year = $record['month_of_year'];
	}
}



//This class contains helper functions that convert timestamps (seconds since unix epoch) into other measurements.
class TimestampConvert {
	public static function secondsSinceMidnight($timestamp) {
		$tomorrow_arbitrary_ts = $timestamp + 86400; //this could be any arbitrary time during the following day
		$tomorrow_midnight_date = gmdate('Y-m-d', $tomorrow_arbitrary_ts); //this is always midnight of the following day
		$tomorrow_midnight_ts = TimezoneHelper::gmstrtotime($tomorrow_midnight_date);
		$seconds_until_tomorrow_midnight = $tomorrow_midnight_ts - $timestamp;
		$seconds_since_today_midnight = 86400 - $seconds_until_tomorrow_midnight;
		return $seconds_since_today_midnight;
	}
	
	public static function daysSinceEpoch($timestamp) {
		$midnight_date = gmdate('Y-m-d', $timestamp);
		$seconds_to_midnight_since_epoch = TimezoneHelper::gmstrtotime($midnight_date);
		$days_to_midnight_since_epoch = $seconds_to_midnight_since_epoch / 86400;
		return $days_to_midnight_since_epoch;
	}
	
	public static function monthsSinceEpoch($timestamp) {
		$month = self::monthOfYear($timestamp);
		$years_since_epoch = self::yearsSinceEpoch($timestamp);
		$months_since_epoch = ($years_since_epoch * 12) + $month - 1;
		return $months_since_epoch;
	}
	
	public static function yearsSinceEpoch($timestamp) {
		return self::year($timestamp) - gmdate('Y', 1);
	}
	
	public static function dayOfMonth($timestamp) {
		return (int)gmdate('j', $timestamp);
	}
	
	//Returns an integer between 1 and 31
	// (or -1 and -31, if $return_as_negative_number == true)
	// ...corresponding to the position of the given timestamp's
	// day from the END of the month (e.g. returns 1/-1
	// for a date that is the LAST day of a month, or 5/-5
	// for a date that is the FIFTH-TO-LAST day of a month).
	public static function dayOfMonthFromLast($timestamp, $return_as_negative_number) {
		$day_of_month = self::dayOfMonth($timestamp);
		$last_day_of_month = (int)gmdate('t', $timestamp);
		$day_of_month_from_last = $last_day_of_month - $day_of_month + 1;
		if ($return_as_negative_number) {
			$day_of_month_from_last = (-1 * $day_of_month_from_last);
		}
		return $day_of_month_from_last;
	}
	
	//Returns an integer between 1 and 6... corresponding to
	// the position of the given timestamp's day-of-week in the month
	// (e.g. returns 1 for a date that is the first Tuesday,
	// or 3 for a date that is the third Friday).
	public static function dayOfWeekPositionInMonth($timestamp) {
		$day_of_month = self::dayOfMonth($timestamp);
		$day_of_week_position_in_month = ceil($day_of_month / 7); //1..7 = 1; 8..14 = 2; etc.
		return $day_of_week_position_in_month;
	}
	
	//Returns an integer between 1 and 6
	// (or -1 and -6, if $return_as_negative_number == true)
	// ...corresponding to the position of the given timestamp's
	// day-of-week from the END of the month (e.g. returns 1/-1
	// for a date that is the LAST Thursday of a month, or 3/-3
	// for a date that is the THIRD-TO-LAST Monday of a month).
	public static function dayOfWeekPositionFromLast($timestamp, $return_as_negative_number) {
		$day_of_month_from_last = self::dayOfMonthFromLast($timestamp, $return_as_negative_number);
		if ($return_as_negative_number) {
			return floor($day_of_month_from_last / 7); //-1..-7 should result in -1, -8..-14 should result in -2, etc.
		} else {
			return (floor(($day_of_month_from_last - 1) / 7) + 1); //1-7 should result in 1, 8-14 should result in 2, 15-21 should result in 3, 22-28 should result in 4, 29-31 should result in 5
		}
	}
	
	//returns 1 for Monday, 2 for Tuesday, ... 7 for Sunday
	public static function dayOfWeek($timestamp) {
		return (int)gmdate('N', $timestamp);
	}
	
	public static function monthOfYear($timestamp) {
		return (int)gmdate('n', $timestamp);
	}
	
	public static function year($timestamp) {
		return (int)gmdate('Y', $timestamp);
	}
	
	//Returns array of 2 dates: start date and end date.
	//
	//Start date is the first day of the week
	// that the first day of the month falls on
	// (for example, if the first day of a month is on Wednesday,
	// then the start date is the Sunday or Monday 3 or 2 days prior
	// [depending on the $first_day_of_week_is_sunday_vs_monday arg]).
	//
	//Similarly, end date is the last day of the week
	// that the last day of the month falls on.
	//
	//This function is useful when outputting a "monthly calendar" view
	// where you want to show an entire week (Sun-Sat, or Mon-Sun)
	// even if they contain some days that fall outside the current month.
	public static function firstAndLastDayOfMonthCalendar($month, $year, $first_day_of_week_is_sunday_vs_monday = true) {
		$month = (int)$month;
		$year = (int)$year;
		$first_day_of_month_timestamp = TimezoneHelper::gmstrtotime("{$month}/1/{$year}");
		
		//By default, PHP treats "monday" as the 1st day of week, and "sunday" as the last day of week.
		// So the calculations that treat Sunday as the 1st day of week are a bit more complicated...
		if ($first_day_of_week_is_sunday_vs_monday) {
			$first_day_of_month_is_sunday = (gmdate('N', $first_day_of_month_timestamp) == 7);
			$relative_start_date_format = ($first_day_of_month_is_sunday ? 'today' : 'last sunday');
			$start_date = gmdate('Y-m-d', TimezoneHelper::gmstrtotime($relative_start_date_format, $first_day_of_month_timestamp));

			$last_day_of_month_timestamp = TimezoneHelper::gmstrtotime(gmdate('n/t/Y', $first_day_of_month_timestamp)); //`t` is number-of-days-in-the-month
			$last_day_of_month_is_saturday = (gmdate('N', $last_day_of_month_timestamp) == 6);
			$relative_end_date_format = ($last_day_of_month_is_saturday ? 'today' : 'next saturday'); //'next saturday' means "the next day that is a saturday", not "saturday of next week" (which might be incorrect if the final day of month is not a Sunday, because PHP treats Sunday as the last-day-of-the-week)
			$end_date = gmdate('Y-m-d', TimezoneHelper::gmstrtotime($relative_end_date_format, $last_day_of_month_timestamp));

		} else {
			$first_day_of_month_is_monday = (gmdate('N', $first_day_of_month_timestamp) == 1);
			$relative_start_date_format = ($first_day_of_month_is_monday ? 'today' : 'last monday');
			$start_date = gmdate('Y-m-d', TimezoneHelper::gmstrtotime($relative_start_date_format, $first_day_of_month_timestamp));
			
			$last_day_of_month_timestamp = TimezoneHelper::gmstrtotime(gmdate('n/t/Y', $first_day_of_month_timestamp)); //`t` is number-of-days-in-the-month
			$last_day_of_month_is_sunday = (gmdate('N', $last_day_of_month_timestamp) == 7);
			$relative_end_date_format = ($last_day_of_month_is_sunday ? 'today' : 'next sunday'); //'next sunday' means "the next day that is a sunday", not "sunday of next week"
			$end_date = gmdate('Y-m-d', TimezoneHelper::gmstrtotime($relative_end_date_format, $last_day_of_month_timestamp));
		}
		
		return array($start_date, $end_date);
	}
	
}



//Use this class to get every occurrence (as a timestamp) of a non-infinite repeating event
class RecurrenceHelpers {
	
	//EITHER $repeat_ends_after_count OR $repeat_ends_on_ts MUST be non-null (otherwise an exception is thrown).
	//If both are non-null, we use $repeat_ends_after_count for calculations and ignore $repeat_ends_on_ts.
	public static function getRecurrenceInstances(
		$start_date_ts, //timestamp of the starting day (can be any time during the day)
		$repeat_unit, //'daily', 'weekly', 'monthly', or 'yearly'
		$repeat_frequency, //integer (1 or higher)
		$weekly_repeat_on_days = null, // array of integers (1=Monday...7=Sunday)
		$monthly_repeat_by = null, //'day_of_month', 'day_of_week_from_first', 'day_of_week_from_last'
		$repeat_ends_after_count = null, //integer
		$repeat_ends_on_ts = null //unix timestamp
	) {
		//NOTE: We ignore "cancellations" in these calculations.
		// This is intentional, because a cancelled event does not make an event
		// recur for a longer period of time (this is how google calendar does it).
		
		//convert starting timestamp to the timestamp of MIDNIGHT on that day
		$start_date_ts = (TimestampConvert::daysSinceEpoch($start_date_ts) * 86400);
		
		if ($repeat_unit == 'daily') {
			return self::getDailyRecurrenceInstances($start_date_ts, $repeat_frequency, $repeat_ends_after_count, $repeat_ends_on_ts);
		} else if ($repeat_unit == 'weekly') {
			return self::getWeeklyRecurrenceInstances($start_date_ts, $weekly_repeat_on_days, $repeat_frequency, $repeat_ends_after_count, $repeat_ends_on_ts);
		} else if (($repeat_unit == 'monthly') && ($monthly_repeat_by == 'day_of_month')) {
			return self::getMonthlyByDayOfMonthRecurrenceInstances($start_date_ts, $repeat_frequency, $repeat_ends_after_count, $repeat_ends_on_ts);
		} else if (($repeat_unit == 'monthly') && ($monthly_repeat_by == 'day_of_week_from_first')) {
			return self::getMonthlyByDayOfWeekFromFirstRecurrenceInstances($start_date_ts, $repeat_frequency, $repeat_ends_after_count, $repeat_ends_on_ts);
		} else if (($repeat_unit == 'monthly') && ($monthly_repeat_by == 'day_of_week_from_last')) {
			return self::getMonthlyByDayOfWeekFromLastRecurrenceInstances($start_date_ts, $repeat_frequency, $repeat_ends_after_count, $repeat_ends_on_ts);
		} else if ($repeat_unit == 'yearly') {
			return self::getYearlyRecurrenceInstances($start_date_ts, $repeat_frequency, $repeat_ends_after_count, $repeat_ends_on_ts);
		}
	}
	
	private static function getDailyRecurrenceInstances($start_date_ts, $repeat_frequency, $repeat_ends_after_count = null, $repeat_ends_on_ts = null) {
		$is_counting_occurrences = self::getRecurrenceInstanceMode($repeat_ends_after_count, $repeat_ends_on_ts);
		
		$instances = array();
		$occurrence_tally = 0;
		$days_since_start = 0;
		
		while (true) {
			$occurrence_tally++;
			if ($is_counting_occurrences && ($occurrence_tally > $repeat_ends_after_count)) {
				break;
			}
		
			$current_instance_ts = TimezoneHelper::gmstrtotime("+{$days_since_start} days", $start_date_ts); //equivelant to `($start_date_ts + ($days_since_start * 86400))`
			if (!$is_counting_occurrences && ($current_instance_ts > $repeat_ends_on_ts)) {
				break;
			}
		
			$instances[] = $current_instance_ts;
			
			$days_since_start += $repeat_frequency;
		}
		
		return $instances;
	}
	
	private static function getWeeklyRecurrenceInstances($start_date_ts, $repeat_on_days, $repeat_frequency, $repeat_ends_after_count = null, $repeat_ends_on_ts = null) {
		$is_counting_occurrences = self::getRecurrenceInstanceMode($repeat_ends_after_count, $repeat_ends_on_ts);
		
		if (empty($repeat_on_days)) {
			//Prevent infinite loop if user didn't explicitly choose day(s)-of-week
			// by just using the day-of-week of the event start date.
			$repeat_on_days = array(TimestampConvert::dayOfWeek($start_date_ts));
		}
		
		$instances = array();
		$occurrence_tally = 0;
		$days_since_start = 0;
		$day_of_week = TimestampConvert::dayOfWeek($start_date_ts);
		while (true) {
			if (in_array($day_of_week, $repeat_on_days)) {
				$occurrence_tally++;
				if ($is_counting_occurrences && ($occurrence_tally > $repeat_ends_after_count)) {
					break;
				}
				
				$current_instance_ts = TimezoneHelper::gmstrtotime("+{$days_since_start} days", $start_date_ts); //equivelant to `($start_date_ts + ($days_since_start * 86400))`
				if (!$is_counting_occurrences && ($current_instance_ts > $repeat_ends_on_ts)) {
					break;
				}
				
				$instances[] = $current_instance_ts;
			}
			
			$days_since_start++;
			if ($day_of_week >= 7) {
				$day_of_week = 1;
				//skip weeks if user chose "every 2 weeks", "every 3 weeks", etc.
				//Note that if the start day is Wednesday (for example),
				// and the event repeats every Tuesday and Thursday (for example),
				// we count that first Thursday in the starting week, then skip an entire week,
				// then continue counting with Tuesday of the 3rd week.
				// (As opposed to the Thursday, then the following Tuesday, THEN skipping a week
				// to the Thursday of the 3rd week). Because this is how google calendar does it too.
				$days_since_start += (($repeat_frequency - 1) * 7); //add 0 days for "every week", 7 days for "every 2 weeks", 14 days for "every 3 weeks", etc.
			} else {
				$day_of_week++;
			}
		}
		
		return $instances;
	}
	
	private static function getMonthlyByDayOfMonthRecurrenceInstances($start_date_ts, $repeat_frequency, $repeat_ends_after_count = null, $repeat_ends_on_ts = null) {
		$is_counting_occurrences = self::getRecurrenceInstanceMode($repeat_ends_after_count, $repeat_ends_on_ts);
		
		$instances = array();
		$occurrence_tally = 0;
		$day = TimestampConvert::dayOfMonth($start_date_ts);
		$month = TimestampConvert::monthOfYear($start_date_ts);
		$year = TimestampConvert::year($start_date_ts);
		while (true) {
			//DEV NOTE: We can't use strtotime('+x months') because we might want to skip a month
			// (e.g. if event starts on January 31st, then repeats on the 31st for x number of times...
			// we need to skip February, April, June, September, and November).
			//So instead just keep incrementing the month (rolling over to the next year when needed)
			// and checking if the given day exists in the given month+year.
			//Note that this means if you say "repeat 3 times monthly on the 31st starting in January",
			// you'll get January, March, and May... because the 31st doesn't exist in Feb or Apr!
			// (This is how google calendar does it too).
			if (checkdate($month, $day, $year)) {
				$occurrence_tally++;
				if ($is_counting_occurrences && ($occurrence_tally > $repeat_ends_after_count)) {
					break;
				}
				
				$current_instance_ts = mktime(0, 0, 0, $month, $day, $year);
				if (!$is_counting_occurrences && ($current_instance_ts > $repeat_ends_on_ts)) {
					break;
				}
				
				$instances[] = $current_instance_ts;
			}
			self::incrementMonthAndYear($month, $year, $repeat_frequency); //this function alters the $month and $year args
		}
		
		return $instances;
	}
	
	private static function getMonthlyByDayOfWeekFromFirstRecurrenceInstances($start_date_ts, $repeat_frequency, $repeat_ends_after_count = null, $repeat_ends_on_ts = null) {
		$is_counting_occurrences = self::getRecurrenceInstanceMode($repeat_ends_after_count, $repeat_ends_on_ts);
		
		$day_of_month = TimestampConvert::dayOfMonth($start_date_ts);
		$day_of_week_position_in_month = TimestampConvert::dayOfWeekPositionInMonth($start_date_ts);
		$ordinals = array(1 => 'first', 2 => 'second', 3 => 'third', 4 => 'fourth', 5 => 'fifth', 6 => 'sixth');
		$day_of_week_position_ordinal = $ordinals[$day_of_week_position_in_month];
	
		$day_of_week_name = gmdate('D', $start_date_ts);

		$instances = array();
		$occurrence_tally = 0;
		$month = TimestampConvert::monthOfYear($start_date_ts);
		$year = TimestampConvert::year($start_date_ts);

		while (true) {
			//check if the "nth xday" of the month (e.g. "first tuesday") exists
			//do this by exploiting a wacky feature of strtotime --
			// where you can say "Nov 2013 second Saturday"
			// and it returns the corresponding timestamp, but
			// if you pass in an "impossible date" (e.g. "Nov 2013 sixth Sunday"),
			// it will automatically "rollover" for you (so "Nov 2013 sixth Sunday"
			// would actually return the timestamp for the 2nd Sunday in December 2013).
			// Hence we can compare the month of the returned timestamp with the original month,
			// and if they match then the "nth xday" of that month exists (and hence counts
			// towards the tally), but if they don't match then the month in question
			// doesn't have an "nth xday". Phew!
			// thanks http://stackoverflow.com/a/17038176/477513
			$month_name = gmdate('M', TimezoneHelper::gmstrtotime("{$year}-{$month}-01"));
			$nth_xday = TimezoneHelper::gmstrtotime("{$month_name} {$year} {$day_of_week_position_ordinal} {$day_of_week_name}");
			if (TimestampConvert::monthOfYear($nth_xday) == $month) {
				$occurrence_tally++;
				if ($is_counting_occurrences && ($occurrence_tally > $repeat_ends_after_count)) {
					break;
				}
				
				$current_instance_ts = $nth_xday;
				if (!$is_counting_occurrences && ($current_instance_ts > $repeat_ends_on_ts)) {
					break;
				}
				
				$instances[] = $current_instance_ts;
			}
			self::incrementMonthAndYear($month, $year, $repeat_frequency); //this function alters the $month and $year args
		}
		
		return $instances;
	}
	
	private static function getMonthlyByDayOfWeekFromLastRecurrenceInstances($start_date_ts, $repeat_frequency, $repeat_ends_after_count = null, $repeat_ends_on_ts = null) {
		$is_counting_occurrences = self::getRecurrenceInstanceMode($repeat_ends_after_count, $repeat_ends_on_ts);
		
		$day_of_week = TimestampConvert::dayOfWeek($start_date_ts);
		$day_of_month = TimestampConvert::dayOfMonth($start_date_ts);
		//$day_of_month_from_last = TimestampConvert::dayOfMonthFromLast($start_date_ts, false); //NOT CURRENTLY USED (because there is no ui for editing it... but if such a UI becomes available then this will work and we'll just need to add some code below to utilize it)
		$nth_from_last = TimestampConvert::dayOfWeekPositionFromLast($start_date_ts, false); //1 for "last xday of month", 2 for "2nd-last xday of month", etc.
		
		$instances = array();
		$occurrence_tally = 0;
		$month = TimestampConvert::monthOfYear($start_date_ts);
		$year = TimestampConvert::year($start_date_ts);
	
		while (true) {
			//unfortunately, no built-in php function can save us in this situation...
			// we need to brute-force the check for validity by walking through the days of the month.
			if (self::checkNthFromLastXday($nth_from_last, $day_of_week, $month, $year)) {
				$occurrence_tally++;
				if ($is_counting_occurrences && ($occurrence_tally > $repeat_ends_after_count)) {
					break;
				}
				
				$current_instance_ts = self::getTimestampForNthFromLastXday($nth_from_last, $day_of_week, $month, $year);
				if (!$is_counting_occurrences && ($current_instance_ts > $repeat_ends_on_ts)) {
					break;
				}
				
				$instances[] = $current_instance_ts;
			}
			self::incrementMonthAndYear($month, $year, $repeat_frequency); //this function alters the $month and $year args
		}
		
		return $instances;
	}
	
	private static function getYearlyRecurrenceInstances($start_date_ts, $repeat_frequency, $repeat_ends_after_count = null, $repeat_ends_on_ts = null) {
		$is_counting_occurrences = self::getRecurrenceInstanceMode($repeat_ends_after_count, $repeat_ends_on_ts);
		
		//we can't use straightforward arithmetic (like we do with 'daily') because of Feb. 29th
		// (nor can we use "4 years at a time" arithmetic in the case of Feb. 29th,
		// because leap years are not always every 4 years -- some get skipped!)
		//so instead just walk through the years (like we do with 'monthly')
		
		$day = TimestampConvert::dayOfMonth($start_date_ts);
		$month = TimestampConvert::monthOfYear($start_date_ts);
		$year = TimestampConvert::year($start_date_ts);
		
		$instances = array();
		$occurrence_tally = 0;
		
		while (true) {
			if (checkdate($month, $day, $year)) {
				$occurrence_tally++;
				if ($is_counting_occurrences && ($occurrence_tally > $repeat_ends_after_count)) {
					break;
				}
				
				$current_instance_ts = mktime(0, 0, 0, $month, $day, $year);
				if (!$is_counting_occurrences && ($current_instance_ts > $repeat_ends_on_ts)) {
					break;
				}
				
				$instances[] = $current_instance_ts;
			}
			$year += $repeat_frequency;
		}
		
		return $instances;
	}
	
	//Returns TRUE if you should count number of occurrences, false if you should count until a specific end date.
	//If both args are null, an exception is thrown.
	//If both args are non-null, we go with "count number of occurrences"
	private static function getRecurrenceInstanceMode($repeat_ends_after_count, $repeat_ends_on_ts) {
		if (is_null($repeat_ends_after_count) && is_null($repeat_ends_on_ts)) {
			throw new Exception('Designer Event Calendar Developer Error: RecurrenceHelper function called with null value for both "end" arguments');
		}
		return !is_null($repeat_ends_after_count);
	}
	private static function incrementMonthAndYear(&$month, &$year, $number_of_months) {
		if (($month + $number_of_months) > 12) {
			$year += ceil($number_of_months / 12); //roll over the year as needed
		}
		$month = (((($month + $number_of_months) - 1) % 12) + 1); //get the next month, regardless of how many years need to be "rolled over" to get there
	}
	//Tests if a given "nth-from-last xday of month" exists...
	// for example, "2nd-from-last Tuesday" exists in all months,
	// but "5th-from-last Tuesday" might not exist (depending on the month/year in question).
	//$nth_from_last is a positive number (so `1` for the last xday of the month, `2` for the 2nd-to-last xday of the month, etc.).
	//$xday is a number for day of the week (1 for Monday ... 7 for Sunday).
	private static function checkNthFromLastXday($nth_from_last, $xday, $month, $year) {
		$last_day_of_month = gmdate('t', TimezoneHelper::gmstrtotime("{$year}-{$month}-01"));
		$week_counter = 0;
		for ($i = $last_day_of_month; $i > 0; $i--) {
			$test_date_ts = TimezoneHelper::gmstrtotime("{$year}-{$month}-{$i}");
			$test_day_of_week = TimestampConvert::dayOfWeek($test_date_ts);
			if ($test_day_of_week == $xday) {
				$week_counter++;
				if ($week_counter == $nth_from_last) {
					return true;
				}
			}
		}
		return false;
	}
	//Returns a timestamp for midnight on the "nth-last-xday of month"...
	// for example, the "2nd-last Tuesday of the month".
	//Note that we assume the parameters are valid and hence
	// you must always call checkNthFromLastXday() BEFORE calling this!
	//$nth_from_last is a positive number (so `1` for the last xday of the month, `2` for the 2nd-to-last xday of the month, etc.)
	//$xday is a number for day of the week (1 for Monday ... 7 for Sunday)
	private static function getTimestampForNthFromLastXday($nth_from_last, $xday, $month, $year) {
		$last_day_of_month = gmdate('t', TimezoneHelper::gmstrtotime("{$year}-{$month}-01"));
		$week_counter = 0;
		for ($i = $last_day_of_month; $i > 0; $i--) {
			$test_date_ts = TimezoneHelper::gmstrtotime("{$year}-{$month}-{$i}");
			$test_day_of_week = TimestampConvert::dayOfWeek($test_date_ts);
			if ($test_day_of_week == $xday) {
				$week_counter++;
				if ($week_counter == $nth_from_last) {
					return $test_date_ts;
				}
			}
		}
		return 0; //this should never happen
	}

}
