<?php defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::library('crud_model', 'designer_event_calendar');
class CategoryModel extends DecSortableCRUDModel {
	
	protected $table = 'dec_categories';
	
	public static function factory() {
		return new CategoryModel;
	}
	
	public function getSelectOptions($header_option = array()) {
		return $this->selectOptionsFromArray($this->getAll(), 'id', 'name', $header_option);
	}
	
	public function validate($post) {
		$e = Loader::helper('validation/error');
		
		if (empty($post['name'])) {
			$e->add(t('Name is required'));
		} else if (strlen($post['name']) > 255) {
			$e->add(t('Name cannot exceed 255 characters in length'));
		}
		
		// Don't validate displayOrder -- that's handled separately (not during a normal save() operation).
		
		return $e;
	}
	
	public function hasChildren($id) {
		$sql = "SELECT COUNT(*) FROM dec_event_categories WHERE category_id = ?";
		$vals = array(intval($id));
		return (bool)$this->db->GetOne($sql, $vals);
	}
	
}