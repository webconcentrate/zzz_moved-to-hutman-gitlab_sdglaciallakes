<?php
	defined('C5_EXECUTE') or die("Access Denied.");
	$include_skip_to_main_link = true; // Tells header.php that this template has a main content section to jump to.
	$pageTitle = $event->getTitle(); // Tells header_vars.php/header.php and main_heading.php what the page title should be.
	include($this->getThemeDirectory() . '/elements/header.php');
?>

	<div class="the-page__main">
		<div class="the-page__main-liner">
				
			<div class="page-main-basic">
				<div class="page-main-basic__crumbs">
					<nav class="crumbs">
						<div class="crumbs__heading">
							<h2>You are here</h2>
						</div>
						<ol class="crumbs__list">
							<li class="crumbs__item ">
								<a class="crumbs__link " href="<?=View::url('/');?>">Home</a>
							</li>
							<li class="crumbs__item crumbs__item--current-page">
								<a class="crumbs__link crumbs__link--current-page" href="<?=View::url('/events');?>">Events</a>
							</li>
						</ol>
					</nav>
				</div>
				<div class="page-main-basic__heading">
					<?php include($this->getThemeDirectory() . '/elements/main_heading.php'); ?>
				</div>
				<div class="page-main-basic__content page-main-basic__content--has-sidebar">
					<div class="page-main-basic__main">	
						<a name="main-content" style="display: block;"></a>
						
						<div class="event-detail">
							<div class="event-detail__subtitle">
								<?php if ($event->hasSubtitle()): ?>
									<?php echo $event->getSubtitle(); ?>
								<?php endif; ?>
							</div>						
							<?php if ($event->hasLocation()): ?>
								<div class="event-detail__location">
									<?php echo $event->getLocationLinkTag(); ?>
								</div>
							<?php endif; ?>
							<div class="event-detail__summary">
								<div class="event-detail__summary-line-1">
									<?php
										if ($event->isRepeating()) {
											echo t('Repeats %s', $event->getRepeatSummary());
										} else {
											echo $event->getStartFormatted('F jS, Y');
											
											if ($event->isMultiDay()) {
												echo ' through ' . $event->getEndFormatted('F jS, Y');
											}
										}
									?>
								</div>
								<div class="event-detail__summary-line-2">	
									<?php
										if ($event->isMultiDay() && $event->isAllDay()) {
											echo t('This is an all-day event that lasts for %s days', $event->getLengthInDays());
										} else if ($event->isMultiDay() && !$event->isAllDay()) {
											$start_time = $event->getStartFormatted('g:i a');
											$end_time = $event->getEndFormatted('g:i a');
											$length_in_days = $event->getLengthInDays();
											if ($length_in_days > 2) {
												$day_length_words = t('%s days later', $length_in_days);
											} else {
												$day_length_words = t('the following day');
											}
											echo t('Starts at %s, ends %s', $start_time, $day_length_words);
											if (!$event->isEndTimeHidden()) {
												echo ' at ' . $end_time;
											}
										} else if (!$event->isMultiDay() && $event->isAllDay()) {
											echo t('This is an all-day event');
										} else if (!$event->isMultiDay() && !$event->isAllDay()) {
											$start_time = $event->getStartFormatted('g:i a');
											$end_time = $event->getEndFormatted('g:i a');
											echo 'Starts at ' . $start_time;
											if (!$event->isEndTimeHidden()) {
												echo ', ends at ' . $end_time;
											}
										}
									?>
								</div>
							</div>
							<div class="event-detail__description">
								<?php echo $event->getDescription(); ?>
							</div>
						</div>
						
						<?php if (!empty($sdgl_members_locations)) { ?>
							<div class="spacer spacer--size-10 spacer--style-3"></div>
							
							<div class="member-list">
								<div class="member-list__heading">
									<h2>Featured Members</h2>
								</div>
								<ul class="member-list__list">
									<?php foreach ($sdgl_members_locations as $location) { ?>
										<li class="member-list__item">
											<a class="member-list__item-link" href="<?=$location->details_page_url?>">
												<div class="member-list__item-copy <?=(!empty($location->thumbnail_photo) ? 'member-list__item-copy--has-image' : '')?>">
													<div class="member-list__item-title">
														<?=$location->title?>
													</div>
													<div class="member-list__item-location">
														<?php if ($location->address_city && $location->address_state) { ?>
															<?=$location->address_city?>, <?=$location->address_state?>
														<?php } else if ($location->address_city || $location->address_state) { ?>
															<?=$location->address_city?> <?=$location->address_state?>
														<?php } ?>
													</div>
												</div>
												<?php if (!empty($location->thumbnail_photo)) { ?>
													<div class="member-list__item-image">
														<img src="<?=$location->thumbnail_photo->src(400, 300, true)?>" alt="">
													</div>
												<?php } ?>
											</a>
										</li>
									<?php } ?>
								</ul>
							</div>						
						<?php } ?>
						
					</div>
					<div class="page-main-basic__sidebar">
						<?php include($this->getThemeDirectory() . '/elements/share.php'); ?>
					</div>
				</div>
			</div>
								
		</div>
	</div>

<?php include($this->getThemeDirectory() . '/elements/footer.php'); ?>
