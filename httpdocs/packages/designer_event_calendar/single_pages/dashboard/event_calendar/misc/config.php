<?php defined('C5_EXECUTE') or die(_("Access Denied."));

$dh = Loader::helper('concrete/dashboard');
$ih = Loader::helper('concrete/interface');
$form = Loader::helper('form');
?>

<?php echo $dh->getDashboardPaneHeaderWrapper('Interface Settings', false, 'span12', false); ?>

	<form method="post" action="<?php echo $this->action('config'); ?>">
		<?php echo $token; ?>
		
		<div class="ccm-pane-body">
			<table class="form-table">
				<tr>
					<td class="right"><?php echo $form->label('dec_show_subtitle_field_in_dashboard', t('Show the "Subtitle" field when editing events:')); ?></td>
					<td><?php echo $form->select('dec_show_subtitle_field_in_dashboard', array(1 => 'Yes', 0 => 'No'), ($dec_show_subtitle_field_in_dashboard ? 1 : 0)); ?></td>
				</tr>
				<tr>
					<td class="right"><?php echo $form->label('dec_show_location_fields_in_dashboard', t('Show "Location" fields when editing events:')); ?></td>
					<td><?php echo $form->select('dec_show_location_fields_in_dashboard', array(1 => 'Yes', 0 => 'No'), ($dec_show_location_fields_in_dashboard ? 1 : 0)); ?></td>
				</tr>
				<tr>
					<td class="right"><?php echo $form->label('dec_skip_empty_days_in_dashboard', t('Skip days having no events in dashboard list:')); ?></td>
					<td><?php echo $form->select('dec_skip_empty_days_in_dashboard', array(1 => 'Yes', 0 => 'No'), ($dec_skip_empty_days_in_dashboard ? 1 : 0)); ?></td>
				</tr>
				<tr>
					<td class="right"><?php echo $form->label('dec_list_number_of_days_in_dashboard', t('Number of days per page in dashboard list:')); ?></td>
					<td><?php echo $form->text('dec_list_number_of_days_in_dashboard', $dec_list_number_of_days_in_dashboard); ?></td>
				</tr>
				<tr>
					<td class="right"><?php echo $form->label('dec_event_list_allow_description_hiding', t('Allow the option of showing/hiding descriptions in the Event List block:')); ?></td>
					<td><?php echo $form->select('dec_event_list_allow_description_hiding', array(1 => 'Yes', 0 => 'No'), ($dec_event_list_allow_description_hiding ? 1 : 0)); ?></td>
				</tr>
			</table>
		</div>
		
		<div class="ccm-pane-footer">
			<?php echo $ih->submit(t('Save'), false, 'right', 'primary'); ?>
			<?php echo $ih->button(t('Cancel'), $this->action('config'), 'left'); ?>
		</div>
	</form>
	
<?php echo $dh->getDashboardPaneFooterWrapper(); ?>
