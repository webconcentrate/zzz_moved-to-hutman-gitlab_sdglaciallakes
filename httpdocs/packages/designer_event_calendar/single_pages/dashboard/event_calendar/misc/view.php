<?php defined('C5_EXECUTE') or die(_("Access Denied."));

$dh = Loader::helper('concrete/dashboard');
$ih = Loader::helper('concrete/interface');
?>

<?php echo $dh->getDashboardPaneHeaderWrapper(t('Misc. Settings'), false, 'span6 offset3'); ?>

	<ul class="unstyled">
		<li><a href="<?php echo $this->action('categories_view'); ?>"><?php echo t('Categories'); ?></a></li>
		<li><a href="<?php echo $this->action('config'); ?>"><?php echo t('Interface Settings'); ?></a></li>
	</ul>
	
	<hr>
	<p><?php echo $ih->button(t('&lt; Back to Events'), View::url('/dashboard/event_calendar/events'), false); ?></p>
	
<?php echo $dh->getDashboardPaneFooterWrapper(); ?>