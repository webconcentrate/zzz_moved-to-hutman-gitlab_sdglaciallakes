<?php defined('C5_EXECUTE') or die(_("Access Denied."));

$dh = Loader::helper('concrete/dashboard');
$ih = Loader::helper('concrete/interface');
$th = Loader::helper('text');
$disabled = isset($disabled) && $disabled;
?>

<?php echo $dh->getDashboardPaneHeaderWrapper(t('Delete Category'), false, 'span6 offset3', false); ?>

	<div class="ccm-pane-body">
		<?php if (!$disabled): ?>
		<h3><?php echo t('Are you sure you wish to permanently delete the following category?'); ?></h3>
		<?php endif ?>
	
		<table class="form-table">
			<tr>
				<td class="right">Name:</td>
				<td><?php echo $th->entities($name); ?></td>
			</tr>
		</table>
	</div>
	
	<div class="ccm-pane-footer">
		<form method="post" action="<?php echo $this->action('categories_delete', (int)$id); ?>" style="margin: 0;">
			<?php echo $token; ?>
			<?php echo $disabled ? '' : $ih->submit(t('Delete'), false, 'right', 'error'); ?>
			<?php echo $ih->button(t('Cancel'), $this->action('categories_view'), 'left'); ?>
		</form>
	</div>

<?php echo $dh->getDashboardPaneFooterWrapper(); ?>