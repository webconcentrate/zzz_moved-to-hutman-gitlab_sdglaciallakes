<?php defined('C5_EXECUTE') or die(_("Access Denied."));

$dh = Loader::helper('concrete/dashboard');
$ih = Loader::helper('concrete/interface');

echo $dh->getDashboardPaneHeaderWrapper('Categories', false, 'span6 offset3');
	
	Loader::library('crud_display_table', 'designer_event_calendar');
	$table = new DecCrudDisplayTable($this);
	$table->addColumn('name');
	$table->addAction('categories_sort', 'left', t('Drag To Sort'), 'icon-resize-vertical', true);
	$table->addAction('categories_edit', 'right', t('Edit'), 'icon-pencil');
	$table->addAction('categories_delete', 'right', t('Delete'), 'icon-trash');
	$table->display($categories);
	
	echo '<p>' . $ih->button('Add New...', $this->action('categories_add'), false, 'primary') . '</p>';
	
	echo '<hr>';
	
	echo '<p>' . $ih->button('&lt; Back to Misc. Settings', $this->action('view'), false) . '</p>';
	
echo $dh->getDashboardPaneFooterWrapper();