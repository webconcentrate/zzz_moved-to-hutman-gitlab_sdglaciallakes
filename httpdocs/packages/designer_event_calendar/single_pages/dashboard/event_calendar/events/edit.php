<?php defined('C5_EXECUTE') or die(_("Access Denied."));

$dh = Loader::helper('concrete/dashboard');
$ih = Loader::helper('concrete/interface');
$th = Loader::helper('text');
$form = Loader::helper('form');

Loader::element('editor_config', array('editor_width' => '600')); //for wysiwyg editor

$is_new = empty($id);
$heading = $is_new ? t('Add New Event') : t('Edit Event');
$action = $is_new ? $this->action('add') : $this->action('edit', (int)$id);
$action .= $filter_querystring; //so we can redirect back to the view page with the same date/category filters as before
?>

<?php echo $dh->getDashboardPaneHeaderWrapper($heading, false, 'span12', false); ?>

	<form method="post" action="<?php echo $action; ?>">
		<?php echo $token; ?>
		<?php echo $form->hidden('id', (int)$id); /* redundant, but simplifies processing */ ?>
		
		<div class="ccm-pane-body">

			<table class="form-table">
				
				<tr>
					<td class="right"><?php echo $form->label('title', t('Title:')); ?></td>
					<td>
						<?php /* note that $title is already escaped */ ?>
						<?php echo $form->text('title', $title, array('maxlength' => '255', 'style' => 'width: 95%;')); ?>
					</td>
				</tr>
				
				<tr style="display: <?php echo ($show_subtitle_field ? 'table-row' : 'none'); ?>;">
					<td class="right"><?php echo $form->label('subtitle', t('Venue Info:')); ?></td>
					<td>
						<?php /* note that $subtitle is already escaped */ ?>
						<?php echo $form->text('subtitle', $subtitle, array('maxlength' => '255', 'style' => 'width: 95%;', 'placeholder' => 'e.g. venue name, city+state, contact phone, etc')); ?>
					</td>
				</tr>
				
				<tr style="display: <?php echo ($show_location_fields ? 'table-row' : 'none'); ?>;">
					<td class="right"><?php echo $form->label('location_name', t('Location:')); ?></td>
					<td>
						<?php /* note that $location_name is already escaped */ ?>
						<?php echo $form->text('location_name', $location_name, array('maxlength' => '255', 'style' => 'width: 95%;')); ?>
					</td>
				</tr>
				
				<tr style="display: <?php echo ($show_location_fields ? 'table-row' : 'none'); ?>;">
					<td class="right"><?php echo $form->label('location_url', t('Map URL:')); ?></td>
					<td>
						<?php /* note that $location_url is already escaped */ ?>
						<?php echo $form->textarea('location_url', $location_url, array('style' => 'width: 95%;')); ?>
					</td>
				</tr>
				
			<tr><td colspan="2"><hr></td></tr>
				
				<tr>
					<td class="right"><?php echo $form->label('start_date', t('Date/Time:')); ?></td>
					<td>
						<div class="dec-datepair">
							<?php echo $form->text('start_date', $start_date, array('class' => 'date start input-small')); ?>
							<?php echo $form->text('start_time', $start_time, array('class' => 'time start ui-timepicker-input input-mini', 'autocomplete' => 'off')); ?>
							<?php echo t('to'); ?>
							<?php echo $form->text('end_time', $end_time, array('class' => 'time end ui-timepicker-input input-mini', 'autocomplete' => 'off')); ?>
							<?php echo $form->text('end_date', $end_date, array('class' => 'date end input-small')); ?>
						</div>
						
						<div id="is_final_day_ignored_by_query_container" title="<?php echo t("Check this box for late-night events that end a few hours past midnight, so that they won't show up in lists and calendars on the last day.<br>(For example, if you want an event that starts at 9pm Tuesday and ends at 1am Wednesday to only be listed as a Tuesday event and not a Wednesday event.)"); ?>">
							<?php echo $form->checkbox('is_final_day_ignored_by_query', 1, $is_final_day_ignored_by_query); ?>
							<?php echo $form->label('is_final_day_ignored_by_query', t('Hide Final Day In Event List'), array('style' => 'display: inline;')); ?>
						</div>
						
						<div id="is_end_time_hidden_container">
							<?php echo $form->checkbox('is_end_time_hidden', 1, $is_end_time_hidden); ?>
							<?php echo $form->label('is_end_time_hidden', "Don't show end time", array('style' => 'display: inline;')); ?>
						</div>
					</td>
				</tr>
				<tr>
					<td class="right">&nbsp;</td>
					<td>
						<?php echo $form->checkbox('is_all_day', 1, $is_all_day); ?>
						<?php echo $form->label('is_all_day', t('All Day'), array('style' => 'display: inline;')); ?>
					</td>
				</tr>
				<tr>
					<td class="right">&nbsp;</td>
					<td>
						<?php echo $form->checkbox('is_repeating', 1, $is_repeating); ?>
						<?php echo $form->label('is_repeating', t('Repeats...'), array('style' => 'display: inline;')); ?>
					</td>
				</tr>
				<tr class="repeat_controls">
					<td class="right"><?php echo $form->label('repeat_unit', t('Repeats:')); ?></td>
					<td>
						<?php
						$repeat_unit_options = array(
							'daily' => t('Daily'),
							'weekly' => t('Weekly'),
							'monthly' => t('Monthly'),
							'yearly' => t('Yearly'),
						);
						echo $form->select('repeat_unit', $repeat_unit_options, $repeat_unit, array('style' => 'width: auto;')); ?>
					</td>
				</tr>
				<tr class="repeat_controls">
					<td class="right"><?php echo $form->label('repeat_frequency', t('Repeat Every:')); ?></td>
					<td>
						<?php echo $form->select('repeat_frequency', array_combine(range(1, 30), range(1, 30)), $repeat_frequency, array('style' => 'width: auto;')); ?>
						<span id="repeat_frequency_suffix"></span>
					</td>
				</tr>
				<tr class="repeat_controls">
					<td class="right">
						<?php echo $form->label('repeat_weekly_on', t('Repeat On:')); ?>
						<?php echo $form->label('repeat_monthly_by', t('Repeat By:')); ?>
					</td>
					<td>
						<div id="repeat_weekly_on_container">
							<?php
							$repeat_weekly_on_options = array(
								1 => t('Monday'),
								2 => t('Tuesday'),
								3 => t('Wednesday'),
								4 => t('Thursday'),
								5 => t('Friday'),
								6 => t('Saturday'),
								7 => t('Sunday'),
							);
							foreach ($repeat_weekly_on_options as $index => $label): ?>
								<label style="display: inline;">
									<input type="checkbox" name="repeat_weekly_on[]" value="<?php echo $index; ?>" style="display: inline;" <?php echo (!is_null($repeat_weekly_on) && in_array($index, $repeat_weekly_on)) ? 'checked="checked"' : ''; ?>>
									<?php echo $label; ?>
								</label>
								&nbsp;&nbsp;
							<?php endforeach; ?>
						</div>
						<div id="repeat_monthly_by_container">
							<?php
							$repeat_monthly_by_options = array(
								'day_of_month' => t('day of the month'),
								'day_of_week_from_first' => t('day of the week'),
								'day_of_week_from_last' => t('day of the week (from end of month)'),
							);
							foreach ($repeat_monthly_by_options as $index => $label): ?>
								<label style="display: inline;">
									<input type="radio" name="repeat_monthly_by" value="<?php echo $index; ?>" style="display: inline;" <?php echo ($index == $repeat_monthly_by) ? 'checked="checked"' : ''; ?>>
									<?php echo $label; ?>
								</label>
								&nbsp;&nbsp;
							<?php endforeach; ?>
						</div>
					</td>
				</tr>
				<tr class="repeat_controls">
					<td class="right top"><?php echo $form->label('repeat_ends', t('Ends:')); ?></td>
					<td>
						<div id="repeat_ends_never_wrapper">
							<label style="display: inline;">
								<?php echo $form->radio('repeat_ends', 'never', $repeat_ends, array('style' => 'display: inline;')); ?>
								<?php echo t('Never'); ?>
							</label>
						</div>
						<div>
							<label style="display: inline;">
								<?php echo $form->radio('repeat_ends', 'after', $repeat_ends, array('style' => 'display: inline;')); ?>
								<?php echo t('After'); ?>
							</label>
							&nbsp;
							<label style="display: inline;">
								<?php echo $form->text('repeat_ends_after_count', $repeat_ends_after_count, array('maxlength' => '3', 'class' => 'input-mini', 'style' => 'display: inline; text-align: center;')); ?>
								&nbsp;
								<?php echo t('Occurrences'); ?>
							</label>
						</div>
						<div>
							<label style="display: inline;">
								<?php echo $form->radio('repeat_ends', 'on', $repeat_ends, array('style' => 'display: inline;')); ?>
								<?php echo t('On'); ?>
							</label>
							&nbsp;
							<span class="dec-datepicker">
								<?php echo $form->text('repeat_ends_on_date', $repeat_ends_on_date, array('class' => 'date input-small')); ?>
							</span>
						</div>
					</td>
				</tr>
				<tr class="repeat_controls">
					<td class="right"><?php echo $form->label('repeat_summary', t('Summary:')); ?></td>
					<td><span id="repeat_summary" style="font-weight: bold;"></span></td>
				</tr>
				<?php if (!empty($cancellations)): ?>
				<tr class="repeat_controls">
					<td class="right top"><?php echo t('Cancellations:'); ?></td>
					<td class="edit-cancellations">
						<?php foreach ($cancellations as $cancellation_day => $cancellation_ts): ?>
							<a href="#uncancel" title="<?php echo t('click to un-cancel'); ?>" data-id="<?php echo $id; ?>" data-day="<?php echo $cancellation_day; ?>"><?php
								echo gmdate('F jS, Y', $cancellation_ts);
							?></a>
						<?php endforeach; ?>
					</td>
				</tr>
				<?php endif; ?>
			<tr><td colspan="2"><hr></td></tr>

				<tr>
					<td class="right top">
						<label for="link_type"><?php echo t('Link to:'); ?></label>
					</td>
					<td>
						<?php
						$link_type_options = array(
							'' => t('a page containing rich content...'),
							'page' => t('another page in the site...'),
							'url' => t('an external URL...'),
							'nothing' => t('nothing.'),
						);
						
						if ($link_to_nothing) {
							$link_type_value = 'nothing';
						} else if (!empty($link_to_cID)) {
							$link_type_value = 'page';
						} else if (!empty($link_to_url)) {
							$link_type_value = 'url';
						} else {
							$link_type_value = '';
						}
						echo $form->select('link_type', $link_type_options, $link_type_value);
						?>
						
						<div id="link_to_cID_wrapper" style="display: none;">
							<?php echo Loader::helper('form/page_selector')->selectPage('link_to_cID', $link_to_cID); ?>
						</div>
						
						<div id="link_to_url_wrapper" style="display: none;">
							<?php echo $form->text('link_to_url', $link_to_url, array('placeholder' => 'e.g. http://example.com')); ?>
						</div>
						
						<script>
						$(document).ready(function() {
							$('#link_type').on('change', toggleLinkToFields); //show/hide link controls when the link type dropdown is changed...
							toggleLinkToFields(); //for initial page load
							$('#link_type').closest('form').on('submit', nullifyNonChosenLinkTypes); //explicitly nullify the values of non-chosen link type fields so there is no confusion about which type the user wamts saved
						});

						function toggleLinkToFields() {
							var linkType = $('#link_type').val();
							$('#link_to_description_wrapper').css('display', ((linkType == '') ? 'table-row' : 'none'));
							$('#link_to_cID_wrapper').css('display', ((linkType == 'page') ? 'inline-block' : 'none'));
							$('#link_to_url_wrapper').css('display', ((linkType == 'url') ? 'inline-block' : 'none'));
						}
						
						function nullifyNonChosenLinkTypes() {
							var linkType = $('#link_type').val();
							if (linkType != 'page') {
								$('#link_to_cID_wrapper input[name="link_to_cID"]').val('');
							}
							if (linkType != 'url') {
								$('#link_to_url').val('');
							}
						}
						</script>
					</td>
				</tr>
				
				<tr id="link_to_description_wrapper" style="display: none;">
					<td>&nbsp;</td>
					<td>
						<?php Loader::element('editor_controls'); ?>
						<?php echo $form->textarea('description', $description, array('class' => 'ccm-advanced-editor')); ?>
					</td>
				</tr>
				
			<tr><td colspan="2"><hr></td></tr>

				<tr>
					<td>&nbsp;</td>
					<td><h2>Members</h2></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<div id="sdgl-members-locations">
							
							<table class="table table-striped" style="width: 100%;">
								<tbody>
									<tr v-for="location in chosenLocations" :key="location.id">
										<td>
											<img v-if="!!location.thumbnail_photo_fID" :src="location.thumbnail_photo_c5_lvl1_thumb_src" alt="">
										</td>
										<td>
											{{ location.title }}
											<input type="hidden" name="sdgl_members_location_ids[]" :value="location.id">
										</td>
										<td>
											<button type="button" class="btn" @click="removeLocation(location.id)"><i class="icon-trash"></i> Remove</button>
										</td>
									</tr>
								</tbody>
							</table>
							
							<div class="well">
								<p>
									Add Member from Category:
									<select v-model="subcategoryId">
										<option :value="null">Choose a Category...</option>
										<optgroup v-for="category in allCategoriesAndSubcategories" :label="category.title">
											<option v-for="subcategory in category.subcategories" :value="subcategory.id">{{ subcategory.title }}</option>
										</optgroup>
									</select>
									
									<span v-show="isLoading">
										<img src="<?=ASSETS_URL_IMAGES?>/throbber_white_16.gif" width="16" height="16" alt="">
									</span>
								</p>
								
								<div v-if="subcategoryLocations.length && unchosenLocations.length">
									<table class="table table-striped" style="width: 100%;">
										<tbody>
											<tr v-for="location in unchosenLocations" :key="location.id">
												<td>
													<img v-if="!!location.thumbnail_photo_fID" :src="location.thumbnail_photo_c5_lvl1_thumb_src" alt="">
												</td>
												<td>
													{{ location.title }}
												</td>
												<td>
													<button type="button" class="btn btn-primary" @click="addLocation(location)"><i class="icon-plus icon-white"></i> Add</button>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<div v-else-if="subcategoryId !== null && !isLoading">
									<p style="font-style: italic; font-weight: bold;">
										No available members in this category
									</p>
								</div>
							</div>
						</div>
					</td>
				</tr>
			
			<?php if (count($categories)): ?>
				
				<tr><td colspan="2"><hr></td></tr>
		
				<tr>
					<td>&nbsp;</td>
					<td><h2><?php echo t('Categories'); ?></h2></td>
				</tr>
			
				<tr>
					<td>&nbsp;</td>
					<td>
						<?php foreach ($categories as $category): ?>
						<label class="checkbox">
							<input type="checkbox" name="category_ids[]" value="<?php echo $category['id']; ?>" <?php echo empty($category['has']) ? '' : 'checked="checked"'; ?>>
							<?php echo $th->entities($category['name']); ?>
						</label>
						<?php endforeach; ?>
					</td>
				</tr>
				
			<?php else: ?>
				
				<input type="hidden" name="category_ids[]" value="">
				
			<?php endif; ?>
			
			<?php if (!empty($public_submitter_info)) { ?>
				<tr><td colspan="2"><hr></td></tr>

				<tr>
					<td>&nbsp;</td>
					<td><h3>Public Submitter Info</h3></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td><?php
						echo $form->hidden('public_submitter_info', $public_submitter_info);
						echo nl2br($public_submitter_info);
					?></td>
				</tr>
			<?php } ?>
			
			</table>
		
		</div>
		
		<div class="ccm-pane-footer">
			<?php echo $ih->submit(t('Save'), false, 'right', 'primary'); ?>
			<?php echo $ih->button(t('Cancel'), ($this->action('view') . $filter_querystring), 'left'); ?>
		</div>

	</form>
	
<?php echo $dh->getDashboardPaneFooterWrapper(); ?>

<script type="text/javascript">
	$(document).ready(function() {
		$('#is_all_day').on('change', toggleTimePickers);
		toggleTimePickers();
		
		$('#is_repeating').on('change', toggleRepeatControls);
		toggleRepeatControls();
		
		$('#repeat_unit').on('change', updateRepeatFrequencyLabel);
		updateRepeatFrequencyLabel();
		
		$('#repeat_unit').on('change', toggleRepeatWeeklyMonthlyOptions);
		toggleRepeatWeeklyMonthlyOptions();
		
		$('#end_time, #end_date, #is_final_day_ignored_by_query, #is_all_day, input[name="repeat_ends"]').on('change', function() {
			toggleIsFinalDayIgnored();
			toggleRepeatEndsOptions();
			toggleIsEndTimeHidden();
		});
		$('#start_date, #start_time').on('change', function() { setTimeout(function() {
			toggleIsFinalDayIgnored();
			toggleRepeatEndsOptions();
			toggleIsEndTimeHidden();
		}, 100); }); //give datepair.js a chance to update the "to" date first
		toggleIsFinalDayIgnored();
		toggleRepeatEndsOptions();
		toggleIsEndTimeHidden();
		
		$('.repeat_controls input, .repeat_controls select, #is_repeating, #start_date').on('change', updateRepeatSummary);
		updateRepeatSummary();
		
		$('.edit-cancellations a').on('click', function() {
			var data = {
				'id': $(this).attr('data-id'),
				'day': $(this).attr('data-day'),
				'ccm_token': $('input[name="ccm_token"]').val()
			};
			$.post('<?php echo $this->action('uncancel_event_occurrence'); ?>', data);
			$(this).slideUp(100);
			return false;
		});
		
		$('#is_final_day_ignored_by_query_container, .edit-cancellations a').tooltip()
	});
	
	function toggleTimePickers() {
		var is_all_day = $('#is_all_day').prop('checked');
		$('#start_time, #end_time').toggle(!is_all_day);
	}
	
	function toggleRepeatControls() {
		var is_repeating = $('#is_repeating').prop('checked');
		$('.repeat_controls').toggle(is_repeating);
	}
	
	function updateRepeatFrequencyLabel() {
		var localized_labels = {
			'daily': '<?php echo t('days'); ?>',
			'weekly': '<?php echo t('weeks'); ?>',
			'monthly': '<?php echo t('months'); ?>',
			'yearly': '<?php echo t('years'); ?>'
		};
		var repeat_unit = $('#repeat_unit').val();
		$('#repeat_frequency_suffix').html(localized_labels[repeat_unit]);
	}
	
	function toggleRepeatWeeklyMonthlyOptions() {
		var repeat_unit = $('#repeat_unit').val();
		var is_weekly = (repeat_unit == 'weekly');
		var is_monthly = (repeat_unit == 'monthly');
		$('#repeat_weekly_on_container, label[for="repeat_weekly_on"]').toggle(is_weekly);
		$('#repeat_monthly_by_container, label[for="repeat_monthly_by"]').toggle(is_monthly);
	}
	
	function toggleRepeatEndsOptions() {
		var repeat_ends = $('input[name="repeat_ends"]:checked').val();
	
		if (repeat_ends == 'after') {
			$('#repeat_ends_after_count').removeAttr('disabled');
		} else {
			$('#repeat_ends_after_count').attr('disabled', 'disabled');
		}
	
		if (repeat_ends == 'on') {
			$('#repeat_ends_on_date').removeAttr('disabled');
		} else {
			$('#repeat_ends_on_date').attr('disabled', 'disabled');
		}
		
		//show/hide "never" option depending on if this is a multiday event or not
		var $repeat_ends_never_wrapper = $('#repeat_ends_never_wrapper');
		var $repeat_ends_never_control = $('input[name="repeat_ends"][value="never"]');
		var diff_in_milliseconds = (new Date($('#end_date').val())) - (new Date($('#start_date').val()));
		if ($('input[name="is_final_day_ignored_by_query"]').prop('checked')) {
			diff_in_milliseconds = diff_in_milliseconds - (24*60*60*1000);
		}
		if (diff_in_milliseconds > 0) {
			$repeat_ends_never_wrapper.hide();
			$repeat_ends_never_control.prop('checked', false);
		} else {
			$repeat_ends_never_wrapper.show();
			$repeat_ends_never_control.prop('checked', (typeof(repeat_ends) === 'undefined' || repeat_ends == 'never'));
		}
	}
	
	function toggleIsFinalDayIgnored() {
		var is_all_day = $('#is_all_day').prop('checked');
		
		var diff_in_milliseconds = (new Date($('#end_date').val())) - (new Date($('#start_date').val()));
		var is_multiday = (diff_in_milliseconds > 0);
		
		var start_time_seconds = $('#start_time').timepicker('getSecondsFromMidnight');
		var end_time_seconds = $('#end_time').timepicker('getSecondsFromMidnight');
		var end_time_is_before_start_time = (end_time_seconds < start_time_seconds);
		
		//custom for SDGL: never show this
		//var show = (is_multiday && !is_all_day && end_time_is_before_start_time);
		var show = false;
		$('#is_final_day_ignored_by_query_container').toggle(show);
		if (!show) {
			$('#is_final_day_ignored_by_query').prop('checked', false); //prevent validation failures if user checks the box but then does something else that causes it to be hidden
		}
	}
	
	function toggleIsEndTimeHidden() {
		var is_all_day = $('#is_all_day').prop('checked');
		
		//custom for SDGL: never show this
		var show = !is_all_day;
		$('#is_end_time_hidden_container').toggle(show);
		if (!show) {
			$('#is_end_time_hidden_container').prop('checked', false); //in case user checks the box but then does something else that causes it to be hidden
		}
	}
	
	<?php /* WARNING: THIS LOGIC IS DUPLICATED IN THE BACK-END PHP CODE (models/event.php) */ ?>
	function updateRepeatSummary() {
		var summary = '';
		
		if ($('#is_repeating').prop('checked')) {
			<?php /* Note that Sunday is repeated, to account for the difference between php (7) and JS (0) */ ?>
			var days_of_week = ['<?php echo t('Sunday'); ?>', '<?php echo t('Monday'); ?>', '<?php echo t('Tuesday'); ?>', '<?php echo t('Wednesday'); ?>', '<?php echo t('Thursday'); ?>', '<?php echo t('Friday'); ?>', '<?php echo t('Saturday'); ?>', '<?php echo t('Sunday'); ?>'];
			
			var start_date = new Date($('#start_date').val());
			
			var repeat_unit = $('#repeat_unit').val();
			var repeat_frequency = parseInt($('#repeat_frequency').val());
			if (repeat_unit == 'daily') {
				if (repeat_frequency == 1) {
					summary += '<?php echo t('Daily'); ?>';
				} else {
					summary += '<?php echo t('Every'); ?> ' + repeat_frequency + ' <?php echo t('days'); ?>';
				}
			} else if (repeat_unit == 'weekly') {
				if (repeat_frequency == 1) {
					summary += '<?php echo t('Weekly'); ?>';
				} else {
					summary += '<?php echo t('Every'); ?> ' + repeat_frequency + ' <?php echo t('weeks'); ?>';
				}
				
				var $repeat_weekly_on_selections = $('input[name="repeat_weekly_on[]"]:checked');
				if ($repeat_weekly_on_selections.length == 0) {
					var start_date_day_of_week = start_date.getDay();
					summary += ' <?php echo t('on'); ?> ' + days_of_week[start_date_day_of_week];
				} else if ($repeat_weekly_on_selections.length == 7) {
					summary += ' <?php echo t('on all days'); ?>';
				} else {
					summary += ' <?php echo t('on'); ?> ' + $repeat_weekly_on_selections.map(function() { return days_of_week[$(this).val()] }).get().join(', ');
				}
			} else if (repeat_unit == 'monthly') {
				if (repeat_frequency == 1) {
					summary += '<?php echo t('Monthly'); ?>';
				} else {
					summary += '<?php echo t('Every'); ?> ' + repeat_frequency + ' <?php echo t('months'); ?>';
				}
				
				var repeat_monthly_by = $('input[name="repeat_monthly_by"]:checked').val();
				var start_date_day_of_month = start_date.getDate();
				var start_date_day_of_week = start_date.getDay();
				
				if (repeat_monthly_by == 'day_of_week_from_first') {
					var start_date_day_of_week_ordinal = Math.floor(start_date_day_of_month / 7);
					var ordinal_names = ['<?php echo t('first'); ?>', '<?php echo t('second'); ?>', '<?php echo t('third'); ?>', '<?php echo t('fourth'); ?>', '<?php echo t('fifth'); ?>', '<?php echo t('sixth'); ?>'];
					summary += ' <?php echo t('on the'); ?> ' + ordinal_names[start_date_day_of_week_ordinal] + ' ' + days_of_week[start_date_day_of_week];
				} else if (repeat_monthly_by == 'day_of_week_from_last') {
					var month = start_date.getMonth();
					var year = start_date.getFullYear();
					var last_day_of_month = new Date(year, month + 1, 0).getDate(); //YES, this works even if month + 1 goes into next year!
					var start_date_ordinal_from_end_of_month = Math.floor((last_day_of_month - start_date_day_of_month) / 7);
					var ordinal_names = ['<?php echo t('last'); ?>', '<?php echo t('second-to-last'); ?>', '<?php echo t('third-to-last'); ?>', '<?php echo t('fourth-to-last'); ?>', '<?php echo t('fifth-to-last'); ?>', '<?php echo t('sixth-to-last'); ?>'];
					summary += ' <?php echo t('on the'); ?> ' + ordinal_names[start_date_ordinal_from_end_of_month] + ' ' + days_of_week[start_date_day_of_week];
				} else {
					summary += ' <?php echo t('on day'); ?> ' + start_date_day_of_month;
				}
			} else if (repeat_unit == 'yearly') {
				if (repeat_frequency == 1) {
					summary += '<?php echo t('Annually'); ?>';
				} else {
					summary += '<?php echo t('Every'); ?> ' + repeat_frequency + ' <?php echo t('years'); ?>';
				}
				var month_names = ['<?php echo t('January'); ?>', '<?php echo t('February'); ?>', '<?php echo t('March'); ?>', '<?php echo t('April'); ?>', '<?php echo t('May'); ?>', '<?php echo t('June'); ?>', '<?php echo t('July'); ?>', '<?php echo t('August'); ?>', '<?php echo t('September'); ?>', '<?php echo t('October'); ?>', '<?php echo t('November'); ?>', '<?php echo t('December'); ?>'];
				var month_number = start_date.getMonth();
				var day_number = start_date.getDate();
				summary += ' <?php echo t('on'); ?> ' + month_names[month_number] + ' ' + day_number;
			}
			
			var repeat_ends = $('input[name="repeat_ends"]:checked').val();
			if (repeat_ends == 'after') {
				var repeat_ends_after_count = $('#repeat_ends_after_count').val();
				if (repeat_ends_after_count.length) {
					repeat_ends_after_count = parseInt(repeat_ends_after_count);
					if (repeat_ends_after_count > 1) {
						summary += ', ' + repeat_ends_after_count + ' <?php echo t('times'); ?>';
					} else if (repeat_ends_after_count == 1) {
						summary = '<?php echo t('Once'); ?>'; //override the entire string (this is the same as not repeating at all)
					}
				}
			} else if (repeat_ends == 'on') {
				var repeat_ends_on_date = $('#repeat_ends_on_date').val();
				if (repeat_ends_on_date.length) {
					summary += ', <?php echo t('until'); ?> ' + (new Date(repeat_ends_on_date)).toDateString();
				}
			}
		}
		
		$('#repeat_summary').html(summary);
	}
</script>

<script>
$(function() {
	new Vue({
		el: '#sdgl-members-locations',
		data: {
			subcategoryId: null,
			subcategoryLocations: [],
			chosenLocations: <?=json_encode(is_array($sdgl_members_locations) ? $sdgl_members_locations : array())?>,
			isLoading: false,
		},
		created: function() {
			this.allCategoriesAndSubcategories = <?=json_encode($all_categories_and_subcategories)?>;
		},
		computed: {
			chosenLocationIds: function() {
				if (!this.chosenLocations) {
					return [];
				} else {
					return this.chosenLocations.map(function(location) {
						return location.id;
					});
				}
			},
			
			unchosenLocations: function() {
				if (!this.subcategoryLocations) {
					return [];
				} else {
					return this.subcategoryLocations.filter(function(location) {
						return (this.chosenLocationIds.indexOf(location.id) === -1);
					}, this);
				}
			},
		},
		
		watch: {
			subcategoryId: function(newSubcategoryId) {
				if (newSubcategoryId) {
					this.isLoading = true;
					var self = this;
					var url = '<?=rtrim($this->action('edit_get_sdgl_members_locations'), '/')?>/' + newSubcategoryId;
					$.get(url, function(response) {
						if (response.success) {
							self.subcategoryLocations = response.locations;
							self.isLoading = false;
						} else {
							alert('An unknown error occurred');
						}
					}, 'json');
				} else {
					this.subcategoryLocations = [];
				}
			},
		},
		
		methods: {
			addLocation: function(location) {
				var clone = JSON.parse(JSON.stringify(location));
				this.chosenLocations.push(clone);
			},
			
			removeLocation: function(locationId) {
				var index = this.chosenLocationIds.indexOf(locationId);
				if (index > -1) {
					this.chosenLocations.splice(index, 1);
				}
			},
		},

	});
});
</script>