<?php defined('C5_EXECUTE') or die(_("Access Denied."));

$dh = Loader::helper('concrete/dashboard');
$ih = Loader::helper('concrete/interface');
$form = Loader::helper('form');
?>


<?php echo $dh->getDashboardPaneHeaderWrapper(t('Manage Events'), false, 'span12', false); ?>
	
	<div class="ccm-pane-options">
		
		<form action="<?php echo $this->action('view'); ?>" method="get" class="calendar-events-filter form-inline">
			
			<label>
				<?php echo t('Date:'); ?>
				<span class="dec-datepicker">
					<?php echo $form->text('day', $day, array('class' => 'date input-small')); ?>
				</span>
			</label>
			
			<?php if (count($category_options) > 1): ?>
				<label>
					<?php echo t('Category:'); ?>
					<?php echo $form->select('cat', $category_options, $category_id); ?>
				</label>
			<?php endif; ?>
			
			<noscript>
				<input type="submit" value="<?php echo t('Go'); ?>" />
			</noscript>
			
			<span class="loading-indicator" style="display: none;">
				<img src="<?php echo ASSETS_URL_IMAGES; ?>/throbber_white_16.gif" width="16" height="16" alt="<?php echo t('loading indicator'); ?>" />
			</span>
			
		</form><script>
		$(document).ready(function() {
			//auto-submit form when a filter is changed
			$('.calendar-events-filter').find('input, select').on('change', function() {
				var $form = $(this).closest('form');
				$form.find('.loading-indicator').show();
				$form.submit();
			});
		});
		</script>
		
		<?php echo $ih->button(t('Add New Event...'), ($this->action('add') . $filter_querystring), 'right', 'primary'); ?>
		<?php echo $ih->button(t('Search...'), $this->action('search'), 'right', 'btn-success'); ?>
		<div style="clear: both;"></div>
	</div>
	
	<div class="ccm-pane-body">
		<?php if ($events_per_day) { ?>
			<table class="table table-striped">
				<?php foreach ($events_per_day as $day_ts => $events) { ?>
				<tr>
					<td style="white-space: nowrap;">
						<h3><?php echo gmdate('D, M j', $day_ts) ;?></h3>
					</td>
					<?php
					if (empty($events)) {
						echo '<td colspan="3" style="font-style: italic;">' . t('No events on this day') . '</td>';
					} else {
						$is_first = true;
						foreach ($events as $event) {
							echo $is_first ? '' : '<tr><td>&nbsp;</td>';
							$is_first = false;
							?>
							<td class="last-field">
								<b><?php echo $event->getTitle(); ?></b>
								<br>
								
								<?php if ($event->hasSubtitle()) { ?>
									<i><?php echo $event->getSubtitle(); ?></i>
									<br>
								<?php } ?>

								<?php
								$start_date = $event->getStartFormatted('M jS');
								$start_time = $event->getStartFormatted('g:i a');
								$end_date = $event->getEndFormatted('M jS');
								$end_time = $event->getEndFormatted('g:i a');

								if ($event->isMultiDay() && $event->isAllDay()) {
									echo t('This is an all-day event that starts on %s and ends on %s', $start_date, $end_date);
								} else if ($event->isMultiDay() && !$event->isAllDay()) {
									echo t('%s on %s thru ', $start_time, $start_date);
									if (!$event->isEndTimeHidden()) {
										echo $end_time . ' on ';
									}
									echo $end_date;
								} else if (!$event->isMultiDay() && $event->isAllDay()) {
									echo t('This is an all-day event');
								} else if (!$event->isMultiDay() && !$event->isAllDay()) {
									echo $start_time;
									if (!$event->isEndTimeHidden()) {
										echo ' - ' . $end_time;
									}
								}
								?>
							</td>
							
							<?php
							$edit_action = $this->action('edit', $event->getId()) . $filter_querystring;
							$duplicate_action = $this->action('duplicate', $event->getId()) . $filter_querystring;
							$delete_action = ($event->isRepeating() ? $this->action('delete', $event->getId(), $event->getStartFormatted('Y-m-d')) : $this->action('delete', $event->getId())) . $filter_querystring;
							?>
							<td class="action"><a class="btn btn-info" href="<?php echo $edit_action; ?>"><i class="icon-pencil"></i> <?php echo t('Edit&nbsp;&nbsp;'); ?></a></td>
							<td class="action"><a class="btn" href="<?php echo $duplicate_action; ?>"><i class="icon-share-alt"></i> <?php echo t('Duplicate'); ?></a></td>
							<td class="action"><a class="btn" href="<?php echo $delete_action; ?>"><i class="icon-trash"></i> <?php echo t('Delete'); ?></a></td>
						</tr>
						<?php } ?>
					<?php } ?>
				<?php } ?>
			</table>
		<?php } else { ?>
			<p><i><?php echo $no_events_message ? $no_events_message : 'No events in this date range'; ?></i></p>
		<?php } ?>
	</div>
	
	<div class="ccm-pane-footer">
		<div class="pagination-wrapper">
			<?php
			if ($pagination_prev_link) {
				echo '<a href="' . $pagination_prev_link . '">' . t('&laquo; Previous %d Days', $list_number_of_days) . '</a>';
			}
			
			echo ($pagination_prev_link && $pagination_next_link) ? ' | ' : '';

			if ($pagination_next_link) {
				echo '<a href="' . $pagination_next_link . '">' . t('Next %d Days &raquo;', $list_number_of_days) . '</a>';
			}
			?>
		</div>
	</div>
	
<?php echo $dh->getDashboardPaneFooterWrapper(); ?>
