<?php defined('C5_EXECUTE') or die(_("Access Denied."));

$dh = Loader::helper('concrete/dashboard');
$ih = Loader::helper('concrete/interface');

$action = $occurrence_start_date ? $this->action('delete', $event->getId(), $occurrence_start_date) : $this->action('delete', $event->getId());
$action .= $filter_querystring; //so we can redirect back to the view page with the same date/category filters as before
?>

<?php echo $dh->getDashboardPaneHeaderWrapper(t('Delete Event'), false, 'span9 offset1', false); ?>
	
	<form method="post" action="<?php echo $action; ?>" style="margin: 0;">
		<div class="ccm-pane-body">
		
			<table class="form-table">
				<tr>
					<td class="right top"><?php echo t('Title:'); ?></td>
					<td style="font-weight: bold;">
						<?php echo $event->getTitle(); ?>
						
						<?php if ($event->hasSubtitle()) { ?>
							<br><br><i><?php echo $event->getSubtitle(); ?></i>
						<?php } ?>

					</td>
				</tr>
				<tr>
					<td class="right top"><?php echo t('Date:'); ?></td>
					<td style="font-weight: bold;">
						<?php
						echo $event->getStartFormatted('F jS, Y');
						if (empty($occurrence_start_date) && $event->isRepeating()) {
							echo '<br>';
							echo t('Repeats %s', $event->getRepeatSummary());
						}
						?>
					</td>
				<tr>
				<tr>
					<td class="right top"><?php echo t('Time:'); ?></td>
					<td style="font-weight: bold;">
						<?php
						if ($event->isMultiDay() && $event->isAllDay()) {
							echo t('All-day event (lasting for %s days)', $event->getLengthInDays());
						} else if ($event->isMultiDay() && !$event->isAllDay()) {
							$start_time = $event->getStartFormatted('g:i a');
							$end_time = $event->getEndFormatted('g:i a');
							$length_in_days = $event->getLengthInDays();
							if ($length_in_days > 2) {
								$day_length_words = t('%s days later', $length_in_days);
							} else {
								$day_length_words = t('the following day');
							}
							echo t('%s - %s (%s)', $start_time, $end_time, $day_length_words);
						} else if (!$event->isMultiDay() && $event->isAllDay()) {
							echo t('All-day event');
						} else if (!$event->isMultiDay() && !$event->isAllDay()) {
							$start_time = $event->getStartFormatted('g:i a');
							$end_time = $event->getEndFormatted('g:i a');
							echo t('%s - %s', $start_time, $end_time);
						}
						?>
					</td>
				</tr>
			</table>

			<?php if ($occurrence_start_date): ?>
				<hr>
				<h4 style="margin-bottom: 10px;">
					<?php echo t('This is a repeating event...'); ?>
				</h4>
				<label class="radio">
					<input type="radio" name="occurrences" value="1" checked="checked">
					<?php echo t('Delete this event only'); ?>
				</label>
				<label class="radio">
					<input type="radio" name="occurrences" value="all">
					<?php echo t('Delete all future events'); ?>
				</label>
			<?php endif; ?>
		
		</div>
	
		<div class="ccm-pane-footer">
			<?php echo $token; ?>
			<?php echo $ih->submit(t('Delete'), false, 'right', 'error'); ?>
			<?php echo $ih->button(t('Cancel'), ($this->action('view') . $filter_querystring), 'left'); ?>
		</div>
		
	</form>

<?php echo $dh->getDashboardPaneFooterWrapper(); ?>
