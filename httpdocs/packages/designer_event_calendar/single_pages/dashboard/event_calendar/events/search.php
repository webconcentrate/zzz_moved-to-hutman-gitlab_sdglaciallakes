<?php defined('C5_EXECUTE') or die(_("Access Denied."));

$dh = Loader::helper('concrete/dashboard');
$ih = Loader::helper('concrete/interface');
$form = Loader::helper('form');
?>


<?php echo $dh->getDashboardPaneHeaderWrapper(t('Search Events'), false, 'span12', false); ?>
	
	<div class="ccm-pane-options">
		
		<form action="<?php echo $this->action('search'); ?>" method="get" class="calendar-events-filter form-inline">
			
			<label>
				<?php echo t('Title:'); ?>
				<?php echo $form->text('q', $query, array('class' => 'input-medium')); ?>
			</label>
			
			<?php if (count($category_options) > 1): ?>
				<label>
					<?php echo t('Category:'); ?>
					<?php echo $form->select('cat', $category_options, $category_id); ?>
				</label>
			<?php endif; ?>
			
			<?php echo $ih->submit(t('Search'), false, 'left'); ?>
		</form>
		
		<div style="clear: both;"></div>
	</div>
	
	<div class="ccm-pane-body">
		<?php if (empty($query)) { ?>
			<p><i><?php echo t('Enter an event title (or a portion of a title) above, then click the Search button.'); ?></i></p>
		<?php } else if (empty($events)) { ?>
			<p><i><?php echo t('No events found'); ?></i></p>
		<?php } else { ?>
			<table class="table table-striped">
				<?php foreach ($events as $event) { ?>
					<tr>
						<td class="last-field">
							<b><?php echo $event->getTitle(); ?></b>
							<br>
							
							<?php if ($event->hasSubtitle()) { ?>
								<i><?php echo $event->getSubtitle(); ?></i>
								<br>
							<?php } ?>

							<?php echo $event->getStartFormatted('F jS, Y'); ?>
							<?php echo ($event->isRepeating() ? t(' (Repeats %s)', $event->getRepeatSummary()) : ''); ?>
							<br>
							
							<?php
							if ($event->isMultiDay() && $event->isAllDay()) {
								echo t('All-day event (lasting for %s days)', $event->getLengthInDays());
							} else if ($event->isMultiDay() && !$event->isAllDay()) {
								$start_time = $event->getStartFormatted('g:i a');
								$end_time = $event->getEndFormatted('g:i a');
								$length_in_days = $event->getLengthInDays();
								if ($length_in_days > 2) {
									$day_length_words = t('%s days later', $length_in_days);
								} else {
									$day_length_words = t('the following day');
								}
								echo $start_time;
								if (!$event->isEndTimeHidden()) {
									echo ' - ' . $end_time;
								}
								echo ' (' . $day_length_words . ')';
							} else if (!$event->isMultiDay() && $event->isAllDay()) {
								echo t('All-day event');
							} else if (!$event->isMultiDay() && !$event->isAllDay()) {
								$start_time = $event->getStartFormatted('g:i a');
								$end_time = $event->getEndFormatted('g:i a');
								echo $start_time;
								if (!$event->isEndTimeHidden()) {
									echo ' - ' . $end_time;
								}
							}
							?>
						</td>
				
						<?php
						$edit_action = $this->action('edit', $event->getId());
						$duplicate_action = $this->action('duplicate', $event->getId());
						$delete_action = $this->action('delete', $event->getId());
						?>
						<td class="action"><a class="btn btn-info" href="<?php echo $edit_action; ?>"><i class="icon-pencil"></i> <?php echo t('Edit&nbsp;&nbsp;'); ?></a></td>
						<td class="action"><a class="btn" href="<?php echo $duplicate_action; ?>"><i class="icon-share-alt"></i> <?php echo t('Duplicate'); ?></a></td>
						<td class="action"><a class="btn" href="<?php echo $delete_action; ?>"><i class="icon-trash"></i> <?php echo t('Delete'); ?></a></td>
					</tr>
				<?php } ?>
			</table>
		<?php } ?>
	</div>
	
	<div class="ccm-pane-footer">
		<?php echo $ih->button(t('&laquo; Back to Manage Events'), $this->action('view'), 'left'); ?>
	</div>
	
<?php echo $dh->getDashboardPaneFooterWrapper(); ?>
