<?php 

//override the error helper so it has a reset() method,
// which is called by Loader::helper().
//We do this to support our unit testing...
// otherwise when we attempt to validate the EventRecord model,
// it retains prior errors from prior tests!

defined('C5_EXECUTE') or die("Access Denied.");
class ValidationErrorHelper extends Concrete5_Helper_Validation_Error {
	public function reset() {
		$this->error = array();
	}
}