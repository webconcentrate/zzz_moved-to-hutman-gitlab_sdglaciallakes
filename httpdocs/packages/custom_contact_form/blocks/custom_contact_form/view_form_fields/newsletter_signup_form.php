<?php defined('C5_EXECUTE') or die("Access Denied.");

/**
 * Custom Contact Form version 3.0, by Jordan Lev
 *
 * See https://github.com/jordanlev/c5_custom_contact_form for instructions
 */

$form = Loader::helper('form');

/* DEV NOTES:
 *  ~ You don't need to populate field values upon validation failure re-display,
 *    because C5 form helpers do that automatically for us.
 *  ~ If you're using placeholders in lieu of visible labels,
 *    you should still have a <label> that is "visuallyhidden" for screenreaders,
 *    AND another <label> inside a <noscript> tag for non-JS people on browsers
 *    that lack native support for the placeholder attribute!
 *  ~ Note that we're not using the C5 form helper for the submit button,
 *    because we don't want a "name" attribute on it. If you want to add a name attribute,
 *    be careful not to use the name "submit", as this might cause problems with javascript
 *    (because it trounces the built-in form.submit() method)!
 */
?>

<div class="custom-contact-form__input">
	<div class="custom-contact-form__input-label">
		<?php echo $form->label('first_name', 'First Name *'); ?>
	</div>
	<div class="custom-contact-form__input-field">
		<?php echo $form->text('first_name', null, array('required'=>'required')); ?>
	</div>
</div>

<div class="custom-contact-form__input">
	<div class="custom-contact-form__input-label">
		<?php echo $form->label('last_name', 'Last Name *'); ?>
	</div>
	<div class="custom-contact-form__input-field">
		<?php echo $form->text('last_name', null, array('required'=>'required')); ?>
	</div>
</div>

<div class="custom-contact-form__input">
	<div class="custom-contact-form__input-label">
		<?php echo $form->label('email', 'Email *'); ?>
	</div>
	<div class="custom-contact-form__input-field">
		<?php echo $form->email('email', null, array('required'=>'required')); ?>
	</div>
</div>