<?php defined('C5_EXECUTE') or die("Access Denied.");

/**
 * Custom Contact Form version 3.0, by Jordan Lev
 *
 * See https://github.com/jordanlev/c5_custom_contact_form for instructions
 */

$form = Loader::helper('form');

/* DEV NOTES:
 *  ~ You don't need to populate field values upon validation failure re-display,
 *    because C5 form helpers do that automatically for us.
 *  ~ If you're using placeholders in lieu of visible labels,
 *    you should still have a <label> that is "visuallyhidden" for screenreaders,
 *    AND another <label> inside a <noscript> tag for non-JS people on browsers
 *    that lack native support for the placeholder attribute!
 *  ~ Note that we're not using the C5 form helper for the submit button,
 *    because we don't want a "name" attribute on it. If you want to add a name attribute,
 *    be careful not to use the name "submit", as this might cause problems with javascript
 *    (because it trounces the built-in form.submit() method)!
 */
?>

<div class="custom-contact-form__input">
	<div class="custom-contact-form__input-label">
		<?php echo $form->label('first_name', 'First Name *'); ?>
	</div>
	<div class="custom-contact-form__input-field">
		<?php echo $form->text('first_name', null, array('required'=>'required')); ?>
	</div>
</div>

<div class="custom-contact-form__input">
	<div class="custom-contact-form__input-label">
		<?php echo $form->label('last_name', 'Last Name *'); ?>
	</div>
	<div class="custom-contact-form__input-field">
		<?php echo $form->text('last_name', null, array('required'=>'required')); ?>
	</div>
</div>

<div class="custom-contact-form__input">
	<div class="custom-contact-form__input-label">
		<?php echo $form->label('email', 'Email *'); ?>
	</div>
	<div class="custom-contact-form__input-field">
		<?php echo $form->email('email', null, array('required'=>'required')); ?>
	</div>
</div>

<div class="custom-contact-form__input">
	<div class="custom-contact-form__input-label">
		<?php echo $form->label('phone', 'Phone'); ?>
	</div>
	<div class="custom-contact-form__input-field">
		<?php echo $form->telephone('phone', null, array()); ?>
	</div>
</div>

<div class="custom-contact-form__input">
	<div class="custom-contact-form__input-label">
		<?php echo $form->label('address', 'Address *'); ?>
	</div>
	<div class="custom-contact-form__input-field">
		<?php echo $form->text('address', null, array('required'=>'required')); ?>
	</div>
</div>

<div class="custom-contact-form__input">
	<div class="custom-contact-form__input-label">
		<?php echo $form->label('city', 'City *'); ?>
	</div>
	<div class="custom-contact-form__input-field">
		<?php echo $form->text('city', null, array('required'=>'required')); ?>
	</div>
</div>

<div class="custom-contact-form__input-group custom-contact-form__input-group--medium">	
	<div class="custom-contact-form__input">
		<div class="custom-contact-form__input-label">
			<?php echo $form->label('state', 'State *'); ?>
		</div>
		<div class="custom-contact-form__input-field">
			<?php echo $form->text('state', null, array('required'=>'required')); ?>
		</div>
	</div>
	
	<div class="custom-contact-form__input">
		<div class="custom-contact-form__input-label">
			<?php echo $form->label('zip', 'Zip *'); ?>
		</div>
		<div class="custom-contact-form__input-field">
			<?php echo $form->text('zip', null, array('required'=>'required')); ?>
		</div>
	</div>
</div>

<div class="custom-contact-form__input">
	<div class="custom-contact-form__input-label">
		<?php echo $form->label('born', 'When were you born?'); ?>
	</div>
	<div class="custom-contact-form__input-field">
		<?php
			$born_decades = range(round((date("Y") - 110), -1), date("Y"), 10);
			$born_options = array('' => '-- Choose One --');
			foreach ($born_decades as $decade) {
				$option_string = $decade . ' - ' . ($decade + 9);
				$born_options[$option_string] = $option_string;
			}
			echo $form->select('born', $born_options, null, array());
		?>
	</div>
</div>

<div class="custom-contact-form__input">
	<div class="custom-contact-form__input-label">
		<?php echo $form->label('family', 'Which best describes your family?'); ?>
	</div>
	<div class="custom-contact-form__input-field">
		<?php
			$family_options = array(
				'' => '-- Choose One --',
				'Children at home' => 'Children at home',
				'Children are grown' => 'Children are grown',
				'Grandkids' => 'Grandkids',
				'None of the above' => 'None of the above',
			);
			echo $form->select('family', $family_options, null, array());
		?>
	</div>
</div>

<div class="custom-contact-form__input">
	<div class="custom-contact-form__input-label">
		What are your areas of interest?
	</div>
	<div class="custom-contact-form__input-field">
		<?php
			$interests_options = array(
				1 => array(
					'label' => 'Fishing',
					'value' => 'Fishing',	
				),
				2 => array(
					'label' => 'Hunting',
					'value' => 'Hunting',	
				),
				3 => array(
					'label' => 'Bird Watching',
					'value' => 'Bird Watching',	
				),
				4 => array(
					'label' => 'Events',
					'value' => 'Events',	
				),
				5 => array(
					'label' => 'Attractions',
					'value' => 'Attractions',	
				),
				6 => array(
					'label' => 'Gaming',
					'value' => 'Gaming',	
				),
				7 => array(
					'label' => 'History',
					'value' => 'History',	
				),
				8 => array(
					'label' => 'Winter Sports',
					'value' => 'Winter Sports',	
				),
				9 => array(
					'label' => 'None of the Above',
					'value' => 'None of the Above',	
				),
			);
		?>
		<?php foreach ($interests_options as $id => $item) { ?>
			<div class="custom-contact-form__input-field-pairing">
				<input type="checkbox" name="interests[]" id="interests-<?=$id?>" value="<?=$item['value']?>" <?=(!empty($_POST['interests']) && in_array($item['value'], $_POST['interests']) ? 'checked' : '')?>>
				<label for="interests-<?=$id?>"><?=$item['label']?></label>
			</div>
		<?php } ?>
	</div>
</div>

<div class="custom-contact-form__input">
	<div class="custom-contact-form__input-label">
		<?php echo $form->label('plans', 'When do you plan to travel?'); ?>
	</div>
	<div class="custom-contact-form__input-field">
		<?php
			$plans_options = array(
				'' => '-- Choose One --',
				'0-3 Months' => '0-3 Months',
				'4-6 Months' => '4-6 Months',
				'7-12 Months' => '7-12 Months',
				'Next Year' => 'Next Year',
				'Undecided' => 'Undecided',
			);
			echo $form->select('plans', $plans_options	, null, array());
		?>
	</div>
</div>

<div class="custom-contact-form__input">
	<div class="custom-contact-form__input-label">
		<?php echo $form->label('how_heard', 'Where did you hear about us? *'); ?>
	</div>
	<div class="custom-contact-form__input-field">
		<?php echo $form->text('how_heard', null, array('required'=>'required')); ?>
	</div>
</div>

<div class="custom-contact-form__input">
	<div class="custom-contact-form__input-label">
		<?php echo $form->label('message', 'Questions or Comments?'); ?>
	</div>
	<div class="custom-contact-form__input-field custom-contact-form__input-field--xlarge">
		<?php echo $form->textarea('message', null, array()); ?>
	</div>
</div>


<div class="custom-contact-form__input">
	<div class="custom-contact-form__input-field">
		<input type="hidden" name="subscribe" value="no">
		<div class="custom-contact-form__input-field-pairing">
			<?php echo $form->checkbox('subscribe', 'yes', null, array()); ?>
			<?php echo $form->label('subscribe', 'Join mailing list?'); ?>
		</div>
	</div>
</div>
