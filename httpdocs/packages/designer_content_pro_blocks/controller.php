<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * This package was automatically created by the Designer Content Pro addon.
 * 
 * The custom blocktypes you create with Designer Content Pro are saved here,
 * so that they won't get erased by C5 when the Designer Content Pro addon is upgraded.
 */

class DesignerContentProBlocksPackage extends Package {

	protected $pkgHandle = 'designer_content_pro_blocks';
	protected $pkgName = 'Designer Content Pro - Custom Block Storage';
	protected $pkgDescription = 'Stores custom blocktypes created with Designer Content Pro.';
	protected $appVersionRequired = '5.6.0';
	protected $pkgVersion = '1.0';

}