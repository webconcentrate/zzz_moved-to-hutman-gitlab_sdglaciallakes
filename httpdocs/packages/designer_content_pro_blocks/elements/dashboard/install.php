<?php defined('C5_EXECUTE') or die(_("Access Denied."));

echo '<h1 style="color: red; font-size: 36px; padding-bottom: 20px;">This addon cannot be installed on its own -- you must install the Designer Content Pro addon (which will then cause this addon to be installed).</h1>';

throw new Exception('This addon cannot be installed on its own -- you must install the Designer Content Pro addon (which will then cause this addon to be installed).');