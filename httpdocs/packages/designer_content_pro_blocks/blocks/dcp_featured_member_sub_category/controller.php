<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * WARNING: This is generated code.
 * Anything in this file may get overwritten or deleted without warning.
 * Do NOT edit, rename, move, or delete this file (doing so will cause errors).
 *
 * To customize this controller, first copy it to SITEROOT/blocks/dcp_featured_member_sub_category/controller.php
 *
 * Visit http://theblockery.com/dcp for documentation.
 */

Loader::library('dcp_controller', 'designer_content_pro');

class DcpFeaturedMemberSubCategoryBlockController extends DcpController {

	protected $btHandle = 'dcp_featured_member_sub_category';
	protected $btName = 'Featured Member Sub-Category';
	protected $btDescription = '';
	protected $btTable = 'btDcpFeaturedMemberSubCategory';

	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;

	public $btDCProGeneratorVersion = '2.1'; //Don't change this!

	public $btDCProBlockOptions = array(
		'help_text_block_header' => "",
		'help_text_block_middle' => "",
		'help_text_block_footer' => "",
		'help_text_repeating_item_header' => "",
		'help_text_repeating_item_footer' => "",
	);

	public $btDCProSingleFields = array(
		'sub_category' => array('type' => 'sdglmemberssubcategory', 'label' => 'Sub-Category', 'options' => array('required' => true)),
	);

	public $btDCProRepeatingItemFields = array();

}
