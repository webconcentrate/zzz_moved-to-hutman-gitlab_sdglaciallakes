<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * WARNING: This is generated code.
 * Anything in this file may get overwritten or deleted without warning.
 * Do NOT edit, rename, move, or delete this file (doing so will cause errors).
 *
 * To customize this controller, first copy it to SITEROOT/blocks/dcp_photo_cta/controller.php
 *
 * Visit http://theblockery.com/dcp for documentation.
 */

Loader::library('dcp_controller', 'designer_content_pro');

class DcpPhotoCtaBlockController extends DcpController {

	protected $btHandle = 'dcp_photo_cta';
	protected $btName = 'Photo CTA';
	protected $btDescription = '';
	protected $btTable = 'btDcpPhotoCta';

	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;

	public $btDCProGeneratorVersion = '2.1'; //Don't change this!

	public $btDCProBlockOptions = array(
		'help_text_block_header' => "",
		'help_text_block_middle' => "",
		'help_text_block_footer' => "",
		'help_text_repeating_item_header' => "",
		'help_text_repeating_item_footer' => "",
	);

	public $btDCProSingleFields = array(
		'image_wide' => array('type' => 'image', 'label' => 'Image (wide screen)', 'options' => array('help_text' => 'This image will be automatically downscaled and cropped to 1500x600 pixels. This image should be these dimensions or larger. If possible, sizing to these dimensions and optimizing to a low file size in Photoshop prior to uploading will yield the best results.', 'required' => true)),
		'image_narrow' => array('type' => 'image', 'label' => 'Image (narrow screen)', 'options' => array('help_text' => 'This image will be automatically downscaled and cropped to 700x500 pixels. This image should be these dimensions or larger. If possible, sizing to these dimensions and optimizing to a low file size in Photoshop prior to uploading will yield the best results. If blank, wide image will be used. ')),
		'heading' => array('type' => 'textbox', 'label' => 'Heading', 'options' => array('searchable' => true, 'required' => true)),
		'link' => array('type' => 'link', 'label' => 'Link', 'options' => array('control_type' => 'combo', 'has_editable_text' => true, 'searchable' => true, 'required' => true)),
		'new_window' => array('type' => 'checkbox', 'label' => 'Open in New Window', 'options' => array()),
	);

	public $btDCProRepeatingItemFields = array();

}
