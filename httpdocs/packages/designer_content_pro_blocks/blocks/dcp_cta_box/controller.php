<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * WARNING: This is generated code.
 * Anything in this file may get overwritten or deleted without warning.
 * Do NOT edit, rename, move, or delete this file (doing so will cause errors).
 *
 * To customize this controller, first copy it to SITEROOT/blocks/dcp_cta_box/controller.php
 *
 * Visit http://theblockery.com/dcp for documentation.
 */

Loader::library('dcp_controller', 'designer_content_pro');

class DcpCtaBoxBlockController extends DcpController {

	protected $btHandle = 'dcp_cta_box';
	protected $btName = 'CTA box';
	protected $btDescription = '';
	protected $btTable = 'btDcpCtaBox';

	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;

	public $btDCProGeneratorVersion = '2.1'; //Don't change this!

	public $btDCProBlockOptions = array(
		'help_text_block_header' => "",
		'help_text_block_middle' => "",
		'help_text_block_footer' => "",
		'help_text_repeating_item_header' => "",
		'help_text_repeating_item_footer' => "",
	);

	public $btDCProSingleFields = array(
		'heading' => array('type' => 'textbox', 'label' => 'Heading', 'options' => array('searchable' => true, 'required' => true)),
		'content' => array('type' => 'wysiwyg', 'label' => 'Content', 'options' => array('required' => true)),
		'link' => array('type' => 'link', 'label' => 'Link', 'options' => array('control_type' => 'combo', 'has_editable_text' => true, 'searchable' => true)),
		'new_window' => array('type' => 'checkbox', 'label' => 'Open in New Window', 'options' => array()),
	);

	public $btDCProRepeatingItemFields = array();

}
