<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * WARNING: This is generated code.
 * Anything in this file may get overwritten or deleted without warning.
 * Do NOT edit, rename, move, or delete this file (doing so will cause errors).
 *
 * To customize this controller, first copy it to SITEROOT/blocks/dcp_expandable_content/controller.php
 *
 * Visit http://theblockery.com/dcp for documentation.
 */

Loader::library('dcp_controller', 'designer_content_pro');

class DcpExpandableContentBlockController extends DcpController {

	protected $btHandle = 'dcp_expandable_content';
	protected $btName = 'Expandable Content';
	protected $btDescription = '';
	protected $btTable = 'btDcpExpandableContent';

	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;

	public $btDCProGeneratorVersion = '1.9.10'; //Don't change this!

	public $btDCProBlockOptions = array(
		'help_text_block_header' => "",
		'help_text_block_middle' => "",
		'help_text_block_footer' => "",
		'help_text_repeating_item_header' => "",
		'help_text_repeating_item_footer' => "",
	);

	public $btDCProSingleFields = array();

	public $btDCProRepeatingItemFields = array(
		'heading' => array('type' => 'textbox', 'label' => 'Heading', 'options' => array('required' => true, 'searchable' => true, 'is_item_label' => true)),
		'content' => array('type' => 'wysiwyg', 'label' => 'Content', 'options' => array('required' => true, 'searchable' => true)),
	);

}
