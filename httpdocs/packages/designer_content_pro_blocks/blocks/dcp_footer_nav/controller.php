<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * WARNING: This is generated code.
 * Anything in this file may get overwritten or deleted without warning.
 * Do NOT edit, rename, move, or delete this file (doing so will cause errors).
 *
 * To customize this controller, first copy it to SITEROOT/blocks/dcp_footer_nav/controller.php
 *
 * Visit http://theblockery.com/dcp for documentation.
 */

Loader::library('dcp_controller', 'designer_content_pro');

class DcpFooterNavBlockController extends DcpController {

	protected $btHandle = 'dcp_footer_nav';
	protected $btName = 'Footer Nav';
	protected $btDescription = '';
	protected $btTable = 'btDcpFooterNav';

	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;

	public $btDCProGeneratorVersion = '2.1'; //Don't change this!

	public $btDCProBlockOptions = array(
		'help_text_block_header' => "",
		'help_text_block_middle' => "",
		'help_text_block_footer' => "",
		'help_text_repeating_item_header' => "",
		'help_text_repeating_item_footer' => "",
	);

	public $btDCProSingleFields = array(
		'heading' => array('type' => 'textbox', 'label' => 'Heading', 'options' => array('required' => true, 'searchable' => true)),
		'heading_link' => array('type' => 'link', 'label' => 'Link', 'options' => array('control_type' => 'combo', 'help_text' => '', 'searchable' => true)),
		'heading_link_new_window' => array('type' => 'checkbox', 'label' => 'Open in New Window', 'options' => array()),
	);

	public $btDCProRepeatingItemFields = array(
		'link' => array('type' => 'link', 'label' => 'Link', 'options' => array('control_type' => 'combo', 'required' => true, 'searchable' => true, 'has_editable_text' => true, 'is_item_label' => true)),
		'new_window' => array('type' => 'checkbox', 'label' => 'Open in New Window', 'options' => array()),
		'icon' => array('type' => 'select', 'label' => 'Icon', 'options' => array('control_type' => 'dropdown', 'select_options' => array('' => 'None', 'facebook' => 'Facebook', 'twitter' => 'Twitter', 'linkedin' => 'LinkedIn', 'instagram' => 'Instagram', 'youtube' => 'YouTube', 'email' => 'Email'))),
	);

}
