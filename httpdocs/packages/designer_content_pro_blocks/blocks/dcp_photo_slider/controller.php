<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * WARNING: This is generated code.
 * Anything in this file may get overwritten or deleted without warning.
 * Do NOT edit, rename, move, or delete this file (doing so will cause errors).
 *
 * To customize this controller, first copy it to SITEROOT/blocks/dcp_photo_slider/controller.php
 *
 * Visit http://theblockery.com/dcp for documentation.
 */

Loader::library('dcp_controller', 'designer_content_pro');

class DcpPhotoSliderBlockController extends DcpController {

	protected $btHandle = 'dcp_photo_slider';
	protected $btName = 'Photo Slider';
	protected $btDescription = '';
	protected $btTable = 'btDcpPhotoSlider';

	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;

	public $btDCProGeneratorVersion = '2.1'; //Don't change this!

	public $btDCProBlockOptions = array(
		'help_text_block_header' => "",
		'help_text_block_middle' => "",
		'help_text_block_footer' => "",
		'help_text_repeating_item_header' => "",
		'help_text_repeating_item_footer' => "",
	);

	public $btDCProSingleFields = array(
		'popup' => array('type' => 'select', 'label' => 'Enable photo popup?', 'options' => array('control_type' => 'dropdown', 'help_text' => 'If enabled, slides can be clicked to reveal an enlarged/non-cropped version of the image in a popup.', 'select_options' => array('1' => 'Yes', '2' => 'No'), 'required' => true)),
	);

	public $btDCProRepeatingItemFields = array(
		'image' => array('type' => 'image', 'label' => 'Image', 'options' => array('help_text' => 'This image will be downscaled and cropped to 1200x800 pixels. For best results, this image should be these dimensions or larger. If the popup is enabled, the image be downscaled to fit a max of 1200x1200 pixels (retaining the original width/height ratio).', 'required' => true, 'is_item_thumb' => true, 'is_item_label' => true)),
		'caption' => array('type' => 'textarea', 'label' => 'Caption', 'options' => array('help_text' => 'If popup is enabled, this caption will display under the photo in the popup.', 'searchable' => true)),
	);

}
