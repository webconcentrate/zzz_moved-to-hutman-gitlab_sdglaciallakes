<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * WARNING: This is generated code.
 * Anything in this file may get overwritten or deleted without warning.
 * Do NOT edit, rename, move, or delete this file (doing so will cause errors).
 *
 * To customize this controller, first copy it to SITEROOT/blocks/dcp_tourism_regions/controller.php
 *
 * Visit http://theblockery.com/dcp for documentation.
 */

Loader::library('dcp_controller', 'designer_content_pro');

class DcpTourismRegionsBlockController extends DcpController {

	protected $btHandle = 'dcp_tourism_regions';
	protected $btName = 'Tourism Regions Map';
	protected $btDescription = '';
	protected $btTable = 'btDcpTourismRegions';

	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;

	public $btDCProGeneratorVersion = '2.1'; //Don't change this!

	public $btDCProBlockOptions = array(
		'help_text_block_header' => "",
		'help_text_block_middle' => "",
		'help_text_block_footer' => "",
		'help_text_repeating_item_header' => "",
		'help_text_repeating_item_footer' => "",
	);

	public $btDCProSingleFields = array(
		'heading' => array('type' => 'textbox', 'label' => 'Heading', 'options' => array('required' => true, 'searchable' => true)),
		'region_1_name' => array('type' => 'textbox', 'label' => 'Region 1 Name', 'options' => array('required' => true, 'searchable' => true)),
		'region_1_link' => array('type' => 'link', 'label' => 'Region 1 Link', 'options' => array('control_type' => 'url', 'required' => true)),
		'region_2_name' => array('type' => 'textbox', 'label' => 'Region 2 Name', 'options' => array('required' => true, 'searchable' => true)),
		'region_2_link' => array('type' => 'link', 'label' => 'Region 2 Link', 'options' => array('control_type' => 'url', 'required' => true)),
		'region_3_name' => array('type' => 'textbox', 'label' => 'Region 3 Name', 'options' => array('required' => true, 'searchable' => true)),
		'region_3_link' => array('type' => 'link', 'label' => 'Region 3 Link', 'options' => array('control_type' => 'url', 'required' => true)),
		'region_4_name' => array('type' => 'textbox', 'label' => 'Region 4 Name', 'options' => array('required' => true, 'searchable' => true)),
		'region_4_link' => array('type' => 'link', 'label' => 'Region 4 Link', 'options' => array('control_type' => 'url', 'required' => true)),
	);

	public $btDCProRepeatingItemFields = array();

}
