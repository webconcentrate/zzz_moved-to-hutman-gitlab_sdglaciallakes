<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * WARNING: This is generated code.
 * Anything in this file may get overwritten or deleted without warning.
 * Do NOT edit, rename, move, or delete this file (doing so will cause errors).
 *
 * To customize this controller, first copy it to SITEROOT/blocks/dcp_footer_ctas/controller.php
 *
 * Visit http://theblockery.com/dcp for documentation.
 */

Loader::library('dcp_controller', 'designer_content_pro');

class DcpFooterCtasBlockController extends DcpController {

	protected $btHandle = 'dcp_footer_ctas';
	protected $btName = 'Footer CTAs';
	protected $btDescription = '';
	protected $btTable = 'btDcpFooterCtas';

	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;

	public $btDCProGeneratorVersion = '2.1'; //Don't change this!

	public $btDCProBlockOptions = array(
		'help_text_block_header' => "",
		'help_text_block_middle' => "",
		'help_text_block_footer' => "",
		'help_text_repeating_item_header' => "",
		'help_text_repeating_item_footer' => "",
	);

	public $btDCProSingleFields = array(
		'cta_1_heading' => array('type' => 'textbox', 'label' => 'CTA 1 Heading', 'options' => array('required' => true, 'searchable' => true)),
		'cta_1_image' => array('type' => 'image', 'label' => 'CTA 1 Image', 'options' => array('help_text' => 'This image will be automatically downscaled and cropped to 300x390 pixels. This image should be these dimensions or larger.', 'required' => true)),
		'cta_1_link' => array('type' => 'link', 'label' => 'CTA 1 Link', 'options' => array('control_type' => 'combo', 'required' => true)),
		'cta_2_heading' => array('type' => 'textbox', 'label' => 'CTA 2 Heading', 'options' => array('required' => true, 'searchable' => true)),
	);

	public $btDCProRepeatingItemFields = array();

}
