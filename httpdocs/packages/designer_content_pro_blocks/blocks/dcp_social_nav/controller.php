<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * WARNING: This is generated code.
 * Anything in this file may get overwritten or deleted without warning.
 * Do NOT edit, rename, move, or delete this file (doing so will cause errors).
 *
 * To customize this controller, first copy it to SITEROOT/blocks/dcp_social_nav/controller.php
 *
 * Visit http://theblockery.com/dcp for documentation.
 */

Loader::library('dcp_controller', 'designer_content_pro');

class DcpSocialNavBlockController extends DcpController {

	protected $btHandle = 'dcp_social_nav';
	protected $btName = 'Social Nav';
	protected $btDescription = '';
	protected $btTable = 'btDcpSocialNav';

	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;

	public $btDCProGeneratorVersion = '2.1'; //Don't change this!

	public $btDCProBlockOptions = array(
		'help_text_block_header' => "",
		'help_text_block_middle' => "",
		'help_text_block_footer' => "",
		'help_text_repeating_item_header' => "",
		'help_text_repeating_item_footer' => "",
	);

	public $btDCProSingleFields = array();

	public $btDCProRepeatingItemFields = array(
		'link' => array('type' => 'link', 'label' => 'Link', 'options' => array('control_type' => 'combo', 'required' => true, 'searchable' => true, 'has_editable_text' => true, 'is_item_label' => true)),
		'new_window' => array('type' => 'checkbox', 'label' => 'Open in New Window', 'options' => array()),
		'icon' => array('type' => 'select', 'label' => 'Icon', 'options' => array('control_type' => 'dropdown', 'select_options' => array('facebook' => 'Facebook', 'twitter' => 'Twitter', 'linkedin' => 'LinkedIn', 'instagram' => 'Instagram', 'youtube' => 'YouTube', 'email' => 'Email'), 'required' => true)),
	);

}
