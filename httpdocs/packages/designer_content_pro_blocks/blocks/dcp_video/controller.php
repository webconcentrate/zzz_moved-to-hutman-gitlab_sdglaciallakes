<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * WARNING: This is generated code.
 * Anything in this file may get overwritten or deleted without warning.
 * Do NOT edit, rename, move, or delete this file (doing so will cause errors).
 *
 * To customize this controller, first copy it to SITEROOT/blocks/dcp_video/controller.php
 *
 * Visit http://theblockery.com/dcp for documentation.
 */

Loader::library('dcp_controller', 'designer_content_pro');

class DcpVideoBlockController extends DcpController {

	protected $btHandle = 'dcp_video';
	protected $btName = 'YouTube/Vimeo Video';
	protected $btDescription = '';
	protected $btTable = 'btDcpVideo';

	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;

	public $btDCProGeneratorVersion = '2.0'; //Don't change this!

	public $btDCProBlockOptions = array(
		'help_text_block_header' => "",
		'help_text_block_middle' => "",
		'help_text_block_footer' => "",
		'help_text_repeating_item_header' => "",
		'help_text_repeating_item_footer' => "",
	);

	public $btDCProSingleFields = array(
		'video_url' => array('type' => 'link', 'label' => 'YouTube/Vimeo URL', 'options' => array('control_type' => 'url', 'required' => true)),
		'image' => array('type' => 'image', 'label' => 'Image', 'options' => array('help_text' => 'The play button will be automatically overlaid (no need to add beforehand). This image will be automatically downscaled and cropped to the dimensions you enter below (or 600x400 pixels if not specifically set). The image will not upscale, so make sure the original image is at least as large as the dimensions you want.', 'required' => true)),
		'width' => array('type' => 'textbox', 'label' => 'Width', 'options' => array('help_text' => 'Optional. This is the number of pixels to crop the image to. If left blank, "600" will be used. Make sure you enter a number (no units) otherwise cropping will not work.')),
		'height' => array('type' => 'textbox', 'label' => 'Height', 'options' => array('help_text' => 'Optional. This is the number of pixels to crop the image to. If left blank, "400" will be used. Make sure you enter a number (no units) otherwise cropping will not work.')),
		'alignment' => array('type' => 'select', 'label' => 'Alignment', 'options' => array('control_type' => 'dropdown', 'select_options' => array('1' => 'Left', '2' => 'Center', '3' => 'Right'), 'help_text' => 'If the image doesn\'t fill the screen, how should the video be aligned?', 'required' => true)),
	);

	public $btDCProRepeatingItemFields = array();

}
