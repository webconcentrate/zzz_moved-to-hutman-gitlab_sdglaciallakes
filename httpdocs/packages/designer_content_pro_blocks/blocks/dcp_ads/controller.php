<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * WARNING: This is generated code.
 * Anything in this file may get overwritten or deleted without warning.
 * Do NOT edit, rename, move, or delete this file (doing so will cause errors).
 *
 * To customize this controller, first copy it to SITEROOT/blocks/dcp_ads/controller.php
 *
 * Visit http://theblockery.com/dcp for documentation.
 */

Loader::library('dcp_controller', 'designer_content_pro');

class DcpAdsBlockController extends DcpController {

	protected $btHandle = 'dcp_ads';
	protected $btName = 'Ads';
	protected $btDescription = '';
	protected $btTable = 'btDcpAds';

	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;

	public $btDCProGeneratorVersion = '2.1'; //Don't change this!

	public $btDCProBlockOptions = array(
		'help_text_block_header' => "",
		'help_text_block_middle' => "",
		'help_text_block_footer' => "",
		'help_text_repeating_item_header' => "",
		'help_text_repeating_item_footer' => "",
	);

	public $btDCProSingleFields = array();

	public $btDCProRepeatingItemFields = array(
		'advertiser' => array('type' => 'textbox', 'label' => 'Advertiser Name', 'options' => array('required' => true, 'is_item_label' => true)),
		'size' => array('type' => 'select', 'label' => 'Ad Size', 'options' => array('control_type' => 'dropdown', 'select_options' => array('300_250_crop' => '300x250'), 'help_text' => '', 'required' => true)),
		'image' => array('type' => 'image', 'label' => 'Ad Image', 'options' => array('help_text' => 'This image will be auto downsized and cropped to match the chosen "Ad Size" above. For best results, this image should be at least those dimensions.', 'required' => true, 'is_item_thumb' => true)),
		'link' => array('type' => 'link', 'label' => 'Ad links to', 'options' => array('control_type' => 'combo', 'help_text' => '')),
		'new_window' => array('type' => 'checkbox', 'label' => 'Open link in new window?', 'options' => array()),
	);

}
