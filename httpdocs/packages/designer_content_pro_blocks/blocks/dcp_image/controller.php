<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * WARNING: This is generated code.
 * Anything in this file may get overwritten or deleted without warning.
 * Do NOT edit, rename, move, or delete this file (doing so will cause errors).
 *
 * To customize this controller, first copy it to SITEROOT/blocks/dcp_image/controller.php
 *
 * Visit http://theblockery.com/dcp for documentation.
 */

Loader::library('dcp_controller', 'designer_content_pro');

class DcpImageBlockController extends DcpController {

	protected $btHandle = 'dcp_image';
	protected $btName = 'Image';
	protected $btDescription = '';
	protected $btTable = 'btDcpImage';

	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;

	public $btDCProGeneratorVersion = '2.0.1'; //Don't change this!

	public $btDCProBlockOptions = array(
		'help_text_block_header' => "",
		'help_text_block_middle' => "",
		'help_text_block_footer' => "",
		'help_text_repeating_item_header' => "",
		'help_text_repeating_item_footer' => "",
	);

	public $btDCProSingleFields = array(
		'image' => array('type' => 'image', 'label' => 'Image', 'options' => array('help_text' => '', 'required' => true)),
		'size' => array('type' => 'select', 'label' => 'Preset Size', 'options' => array('control_type' => 'dropdown', 'select_options' => array('' => 'None', '800_500_crop' => 'Blog Post Image (auto sized/cropped to 800x500)'), 'help_text' => 'If chosen, max width/height settings will be ignored.')),
		'width' => array('type' => 'textbox', 'label' => 'Max Width', 'options' => array('help_text' => 'Pixel width to constrain the image to. If blank, the original dimensions will be used. Enter a number only (no unit). Has no effect if "Size Preset" chosen above.')),
		'height' => array('type' => 'textbox', 'label' => 'Max Height', 'options' => array('help_text' => 'Pixel height to constrain the image to. If blank, the original dimensions will be used. Enter a number only (no unit). Has no effect if "Size Preset" chosen above.')),
		'crop' => array('type' => 'checkbox', 'label' => 'Crop to max width/height?', 'options' => array('help_text' => 'Image must be at least as big as the max width/height for cropping to occur.')),
		'alignment' => array('type' => 'select', 'label' => 'Alignment', 'options' => array('control_type' => 'dropdown', 'select_options' => array('1' => 'Left', '2' => 'Center', '3' => 'Right'), 'help_text' => 'If the image doesn\'t fill the space, how should the image be aligned?')),
		'link' => array('type' => 'link', 'label' => 'Link image to', 'options' => array('control_type' => 'combo', 'help_text' => '')),
		'new_window' => array('type' => 'checkbox', 'label' => 'Open link in new window?', 'options' => array()),
	);

	public $btDCProRepeatingItemFields = array();

}
