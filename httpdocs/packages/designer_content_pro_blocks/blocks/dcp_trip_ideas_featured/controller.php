<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * WARNING: This is generated code.
 * Anything in this file may get overwritten or deleted without warning.
 * Do NOT edit, rename, move, or delete this file (doing so will cause errors).
 *
 * To customize this controller, first copy it to SITEROOT/blocks/dcp_trip_ideas_featured/controller.php
 *
 * Visit http://theblockery.com/dcp for documentation.
 */

Loader::library('dcp_controller', 'designer_content_pro');

class DcpTripIdeasFeaturedBlockController extends DcpController {

	protected $btHandle = 'dcp_trip_ideas_featured';
	protected $btName = 'Featured Trip Ideas';
	protected $btDescription = '';
	protected $btTable = 'btDcpTripIdeasFeatured';

	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;

	public $btDCProGeneratorVersion = '2.1'; //Don't change this!

	public $btDCProBlockOptions = array(
		'help_text_block_header' => "",
		'help_text_block_middle' => "",
		'help_text_block_footer' => "",
		'help_text_repeating_item_header' => "",
		'help_text_repeating_item_footer' => "",
	);

	public $btDCProSingleFields = array(
		'heading' => array('type' => 'textbox', 'label' => 'Heading', 'options' => array('searchable' => true, 'required' => true)),
		'link' => array('type' => 'link', 'label' => 'Link', 'options' => array('control_type' => 'combo', 'searchable' => true, 'required' => true, 'has_editable_text' => true)),
	);

	public $btDCProRepeatingItemFields = array(
		'trip_idea' => array('type' => 'page', 'label' => 'Trip Idea Page', 'options' => array('page_type' => '10', 'required' => true, 'is_item_label' => true)),
	);

}
