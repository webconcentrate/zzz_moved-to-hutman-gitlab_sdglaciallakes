<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * WARNING: This is generated code.
 * Anything in this file may get overwritten or deleted without warning.
 * Do NOT edit, rename, move, or delete this file (doing so will cause errors).
 *
 * To customize this controller, first copy it to SITEROOT/blocks/dcp_content/controller.php
 *
 * Visit http://theblockery.com/dcp for documentation.
 */

Loader::library('dcp_controller', 'designer_content_pro');

class DcpContentBlockController extends DcpController {

	protected $btHandle = 'dcp_content';
	protected $btName = 'Content';
	protected $btDescription = '';
	protected $btTable = 'btDcpContent';

	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;

	public $btDCProGeneratorVersion = '2.0.1'; //Don't change this!

	public $btDCProBlockOptions = array(
		'help_text_block_header' => "",
		'help_text_block_middle' => "",
		'help_text_block_footer' => "",
		'help_text_repeating_item_header' => "",
		'help_text_repeating_item_footer' => "",
	);

	public $btDCProSingleFields = array(
		'width' => array('type' => 'select', 'label' => 'Width', 'options' => array('control_type' => 'dropdown', 'select_options' => array('1' => 'Standard (recommended)', '2' => 'Full'), 'help_text' => 'One wide screens, "standard" will keep the content constrained to a narrower, more optimal line-length for readability. "Full" can be used when you need the content to stretch full width (eg: you need a wide table or image).')),
		'content' => array('type' => 'wysiwyg', 'label' => 'Content', 'options' => array('searchable' => true)),
	);

	public $btDCProRepeatingItemFields = array();

}
