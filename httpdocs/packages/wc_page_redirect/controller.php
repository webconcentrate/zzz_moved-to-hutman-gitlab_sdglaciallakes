<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/**
 * version 2015-09-11
 * 
 * A lot of this copied from Michael Krasnow's page_redirect addon <mnkras@gmail.com>
 * 
 * But we added another attribute for entering a url via textbox.
 */
class WcPageRedirectPackage extends Package {

	protected $pkgHandle = 'wc_page_redirect';
	protected $appVersionRequired = '5.4.1';
	protected $pkgVersion = '1.0';
	protected $pkgDescription = 'Specify that a page should redirect elsewhere (via a "choose page" or "external url" field)';
	protected $pkgName = 'WC Page Redirect';
	
	public function install() {
		//Original (mnkras's) functionality: add a page selector attribute
		Loader::model('collection_attributes');		 
		$PageAttrType = AttributeType::getByHandle('page_selector');
		if(!is_object($PageAttrType) || !intval($PageAttrType->getAttributeTypeID())){ 
			throw new exception(t('Please install %s before installing this addon.', '<a href="http://www.concrete5.org/marketplace/addons/page-selector-attribute/">Page Selector Attribute</a>'));
			exit;
		}
		$pkg = parent::install();
		$ak_page = CollectionAttributeKey::add('page_selector', array('akHandle' => 'page_selector_redirect', 'akName' => t('Page Redirect')), $pkg);
		
		//Custom WC functionality: add another attribute that's for "external urls" (because using C5's built-in "external link" thing doesn't play nice with our custom navigation blocks)
		$ak_url = CollectionAttributeKey::add('text', array('akHandle' => 'url_redirect', 'akName' => 'URL Redirect'), $pkg);

		//Custom WC functionality: Add the attributes to the "Navigation and Indexing" set
		if (version_compare(APP_VERSION, '5.5.0', '>=')) {
			$as = AttributeSet::getByHandle('navigation');
			if (!is_null($as)) {
				$as->addKey($ak_page);
				$as->addKey($ak_url);
			}
		}

	}
	
	public function on_start() {
		//Even though we're in an `on_start` function here,
		// it's not actually the 'on_start' event yet
		// (so Page::getCurrentPage() isn't available)...
		if (!defined('ENABLE_APPLICATION_EVENTS')) { define('ENABLE_APPLICATION_EVENTS', true); } //<--REQUIRED IN 5.5+ (despite what c5 docs say!!)
		Events::extend('on_start', __CLASS__, 'handleRedirect', __FILE__);
	}
	
	public static function handleRedirect() {
		self::pageSelectorRedirect();
		self::urlRedirect();
	}
	
	private static function pageSelectorRedirect() {
		$page = Page::getCurrentPage();
		if (is_object($page) && !$page->isError()) {
			$page_cID = $page->getCollectionAttributeValue('page_selector_redirect');
			if($page_cID > 0) {
				Loader::model('page');
				$redirect_to_page = Page::getByID($page_cID);
				if(is_object($redirect_to_page) && !$redirect_to_page->isError()) {
					$url = $redirect_to_page->isExternalLink() ? $redirect_to_page->getCollectionPointerExternalLink() : Loader::Helper('navigation')->getLinkToCollection($redirect_to_page, true);
					header("Location: $url", true, 301);
					exit;
				}
			}
		}
	}
	
	private static function urlRedirect() {
		$page = Page::getCurrentPage();
		if (is_object($page) && !$page->isError()) {
			$url = $page->getCollectionAttributeValue('url_redirect');
			if (!empty($url)) {
				if (substr($url, 0, 1) === '/') {
					$url = BASE_URL . DIR_REL . $url;
				}
				header("Location: {$url}", true, 301);
				exit;
			}
		}
	}
}
