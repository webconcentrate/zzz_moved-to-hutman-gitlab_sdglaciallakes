<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));

class Dashboard301RedirectsCsvImportController extends Controller {

	public function view($msg=false) {
		$html = Loader::helper("html");
		$this->addHeaderItem($html->css("dashboard_2.0.css", "swp_301_redirects"));
		
		if ($msg == "import_complete") {
			$this->set("message", t("Import complete"));
			if (isset($_SESSION["csv_import_errors"]))
				unset($_SESSION["csv_import_errors"]);
		} elseif ($msg == "import_error") {
			$this->set("message", t("Errors detected. Import canceled. Please correct the csv file and retry."));
			$errors = $_SESSION["csv_import_errors"];
			$this->set("import_errors", $errors);
		}
	}
	
	public function do_import() {
		if ($this->isPost()) {
			$import_helper = Loader::helper("csv_import", "swp_301_redirects");
		
			$errors = array();
			$rows = array();
			
			$delimiter = $this->post("csv_delimiter");
			$filepath = $_FILES["csv_file"]["tmp_name"];
			
			$rows = $import_helper->readCSV($filepath, $delimiter);
			
			foreach($rows as $rowIndex =>$row) {
				$err = $import_helper->validateRow($row);
				if (!empty($err))
					$errors[$rowIndex] = $err;
			}
			
			if (empty($errors)) {
				$import_helper->importData($rows);
			} else {
				$_SESSION["csv_import_errors"] = $errors;
				$this->redirect("/dashboard/301_redirects/csv_import", "import_error");
			}
			
			$this->redirect("/dashboard/301_redirects/csv_import", "import_complete");
		}
		
		$this->redirect("/dashboard/301_redirects/csv_import");
	}
	
}