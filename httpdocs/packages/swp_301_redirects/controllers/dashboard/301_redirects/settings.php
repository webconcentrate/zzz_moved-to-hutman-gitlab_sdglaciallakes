<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));

class Dashboard301RedirectsSettingsController extends Controller {

	public function view($msg=false) {
		$html = Loader::helper("html");
		$this->addHeaderItem($html->css("dashboard_2.0.css", "swp_301_redirects"));
	
		if ($msg == "updated") {
			$this->set("message", t("Settings updated"));
		}
		
		$pkg = Package::getByHandle("swp_301_redirects");
		
		$this->set("only_404", $pkg->config("301_REDIRECT_ONLY_404"));
		$this->set("expert_mode", $pkg->config("301_REDIRECT_EXPERT_MODE"));
		$this->set("redirects_per_page", $pkg->config("301_REDIRECT_REDIRECTS_PER_PAGE"));
	}
	
	public function save_settings() {
		if ($this->isPost()){
			$pkg = Package::getByHandle("swp_301_redirects");
		
			// trigger redirects on 404 pages only
			$only_404 = 0;
			if ($this->post("only_404")) {
				$only_404 = 1;
			}
			$pkg->saveConfig("301_REDIRECT_ONLY_404", $only_404);
			
			// expert mode (enables regular expressions support)
			$expert_mode = 0;
			if ($this->post("expert_mode")) {
				$expert_mode = 1;
			}
			$pkg->saveConfig("301_REDIRECT_EXPERT_MODE", $expert_mode);
			
			// number of redirects per page (in dashboard)
			$redirects_per_page = 0;
			if ($this->post("redirects_per_page")) {
				$redirects_per_page = intval($this->post("redirects_per_page"));
			}
			$pkg->saveConfig("301_REDIRECT_REDIRECTS_PER_PAGE", $redirects_per_page);
		
			$this->redirect("/dashboard/301_redirects/settings", "view", "updated");
		}
		$this->redirect("/dashboard/301_redirects/settings");
	}
	
}