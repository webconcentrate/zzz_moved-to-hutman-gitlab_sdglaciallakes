<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));

class Dashboard301RedirectsController extends Controller {

	public function view($msg=false) {
		$this->redirect("/dashboard/301_redirects/redirects");
	}
}