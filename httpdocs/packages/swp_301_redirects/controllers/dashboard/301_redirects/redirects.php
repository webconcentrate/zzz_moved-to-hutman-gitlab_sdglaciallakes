<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));

class Dashboard301RedirectsRedirectsController extends Controller {

	public function view($msg=false) {
		$html = Loader::helper("html");
		$this->addHeaderItem($html->css("dashboard_2.0.css", "swp_301_redirects"));
		$this->addHeaderItem($this->getJavascriptLang());
		$this->addHeaderItem($html->javascript("redirects_2.0.js", "swp_301_redirects"));
	
		if ($msg == "added") {
			$this->set("message", t("Redirect added"));
		}
		
		$pkg = Package::getByHandle("swp_301_redirects");
		$this->set("expert_mode", $pkg->config("301_REDIRECT_EXPERT_MODE"));
		
		$helper = Loader::helper("swp_301_redirects", "swp_301_redirects");
		$current_page = intval($_GET["page"]);
		if ($current_page <= 1)
			$current_page = 1;
		$helper->setPage($current_page);
		$helper->setItemsPerPage(intval($pkg->config("301_REDIRECT_REDIRECTS_PER_PAGE")));
		
		$this->set("current_page", $current_page);
		$this->set("redirects", $helper->getRedirects());
		$this->set("total_pages", $helper->getTotalPages());
	}
	
	function getJavascriptLang() {
		$h = '<script type="text/javascript">' . "\n";
		$h .= "<!--\n";
		$h .= "rLang = {\n";
		$h .= "areYouSure: '";
		$h .= str_replace("'", "\'", t("Are you sure that you want to delete this redirect?"));
		$h .= "'\n";
		$h .= "}\n";
		$h .= '// --></script>';
		return $h;
	}
	
	public function add_redirect($msg=false) {
		$helper = Loader::helper("swp_301_redirects", "swp_301_redirects");
	
		if ($this->isPost()) {
		
			$redirect_type = $this->post("redirect_type");
			if (!in_array($redirect_type, array("P", "U", "F", "M"))) {
				$redirect_type = "U";
			}
			
			if ($redirect_type == "U") {
				$redirect_to = $this->post("redirect_to");
				if (strpos($redirect_to, "http://") !== 0 && strpos($redirect_to, "https://") !== 0 && strpos($redirect_to, "/") !== 0) {
					$redirect_to = "/" . $redirect_to;
				}
			} elseif ($redirect_type == "F") {
				$redirect_to = $this->post("redirectFile");
			} elseif ($redirect_type == "M") {
				$redirect_to = $this->post("redirect_to");
			} else {
				$redirect_to = $this->post("redirectPage");
			}
			
			$is_preg = false;
			if ($this->post("redirect_from_type") == "regexp") {
				$is_preg = true;
			}
		
			$data = array(
				"redirect_from" => $this->post("redirect_from"),
				"redirect_to" => $redirect_to,
				"redirect_type" => $redirect_type,
				"is_preg" => $is_preg,
			);
			
			$id = $helper->addRedirect($data);
		
			$this->redirect("/dashboard/301_redirects/redirects", "view", "added");
		}
	}
	
	public function delete_redirect() {
		// method is called via ajax request
		$helper = Loader::helper("swp_301_redirects", "swp_301_redirects");
		if ($this->isPost()) {
			$validation_token = Loader::helper("validation/token");
			if ($validation_token->validate("delete_redirect")) {
				$redirectID = intval($this->post("redirectID"));
				$helper->deleteRedirect($redirectID);
				
				$json = Loader::helper("json");
				$data = array(
					"result" => "OK",
					"rowID" => $redirectID,
				);
				echo $json->encode($data);
				exit;
			}
		}
	}
	
}