<?php       
defined('C5_EXECUTE') or die(_("Access Denied."));

class Swp301RedirectsHelper {

	public $page = 1;
	public $items_per_page = 0;
	public $total_items = 0;
	public $total_pages = 1;

	function getTable() {
		return 'swp301Redirects';
	}
	
	public function addRedirect($data) {
		$db = Loader::db();
		
		$q = "replace into " . $this->getTable();
		$q .= " set ";
		$q .= " redirect_from=?,";
		$q .= " redirect_to=?,";
		$q .= " redirect_type=?,";
		$q .= " isWildchar=?,";
		$q .= " isRegexp=?";
		
		$isWildchar = "N";
		$isRegexp = "N";
		
		$orig_from = $data["redirect_from"];
		$schema = @parse_url($data["redirect_from"]);
		$data["redirect_from"] = $schema["path"];
		if (strpos($orig_from, "?") !== false) {
			$data["redirect_from"] .= "?" . $schema["query"];
		}
		
		if ($data["is_preg"]) {
			$isRegexp = "Y";
		} else {
			$data["redirect_from"] = ltrim($data["redirect_from"], "/");
		}
		
		if (strpos($data["redirect_from"], "*") !== false && $isRegexp == "N") {
			$isWildchar = "Y";
		}
		
		$i = array(
			"redirect_from" => $data["redirect_from"],
			"redirect_to" => $data["redirect_to"],
			"redirect_type" => $data["redirect_type"],
			"isWildchar" => $isWildchar,
			"isRegexp" => $isRegexp,
		);
		
		$db->Execute($q, $i);
		
		return $db->Insert_ID();
	}
	
	public function deleteRedirect($redirectID) {
		$db = Loader::db();
	
		$redirectID = intval($redirectID);
		
		$q = "delete from " . $this->getTable() . " where ID=?";
		$i = array(
			"ID" => $redirectID,
		);
		
		$db->Execute($q, $i);
		
		return true;
	}

	public function setItemsPerPage($num) {
		$this->items_per_page = intval($num);
	}
	
	public function setPage($num) {
		$this->page = intval($num);
		if ($this->page < 1)
			$this->page = 1;
	}
	
	public function getTotalPages() {
		return $this->total_pages;
	}
	
	public function getRedirects() {
		$db = Loader::db();
		
		$q_count = "select count(1) from ";
		$q = "select * from ";
		
		$q .= $this->getTable();
		$q_count .= $this->getTable();
		
		$q .= ' order by ID asc';
		
		$this->total_items = $db->GetOne($q_count);
		
		if (!empty($this->items_per_page)) {
			$this->total_pages = ceil($this->total_items / $this->items_per_page);
		} else {
			$this->total_pages = 1;
		}
		
		if ($this->total_pages > 1) {
			$limit = " LIMIT ";
			$limit .= (($this->page - 1) * $this->items_per_page);
			$limit .= ", " . $this->items_per_page;
			$q .= $limit;
		}
		
		$redirects = $db->GetAll($q);
		
		return $redirects;
	}
	
	public static function getWildchars() {
		$db = Loader::db();
		
		$q = "select * from ";
		$q .= self::getTable();
		$q .= ' where isWildchar="Y"';

		$redirects = $db->GetAll($q);
		
		return $redirects;
	}
	
	public static function getRegexps() {
		$db = Loader::db();
		
		$q = "select * from ";
		$q .= self::getTable();
		$q .= ' where isRegexp="Y"';

		$redirects = $db->GetAll($q);
		
		return $redirects;
	}
	
	public static function getTrailingSlashAlternative($url) {
		$url2 = rtrim($url, "/");
		if ($url2 !== $url)
			return $url2;
		return $url2 . '/';
	}
	
	public static function getRedirectByURL($url) {
		$url = ltrim($url, "/");
		$url2 = self::getTrailingSlashAlternative($url);
	
		$db = Loader::db();
		
		$q = "select * from ";
		$q .= self::getTable();
		$q .= ' where redirect_from=? or redirect_from=?';
		
		$i = array(
			"redirect_from" => $url,
			"trailing_slash_alternative" => $url2,
		);
		
		$redirect = $db->GetRow($q, $i);
		return $redirect;
	}
	
	public static function doRedirect($redirectRec) {
		$redirect_to = $redirectRec["redirect_to"];

		if ($redirectRec["redirect_type"] == "P") {
			Loader::model("page");
			$nh = Loader::helper("navigation");
			
			$pageID = intval($redirect_to);
			$page = Page::getByID($pageID);
			$redirect_to = $nh->getLinkToCollection($page);
		} elseif ($redirectRec["redirect_type"] == "F") {
			Loader::model("file");
			
			$fileID = intval($redirect_to);
			$redirect_to = View::url('/download_file', $fileID);
		} elseif ($redirectRec["isRegexp"] && $redirectRec["redirect_type"] == "M") {
			$redirect_to = preg_replace($redirectRec["redirect_from"], $redirectRec["redirect_to"], $_SERVER["REQUEST_URI"]);
		}
		
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: $redirect_to");
		exit;
	}
	
	public static function checkPage($url = null) {
		$orig_url = $url;
		
		if ($url === null) {
			$orig_url = $url = $_SERVER["REQUEST_URI"];
		}
		
		if (strpos($url, "/dashboard/") !== false)
			return;
		
		$url = ltrim($url, "/");
		
		// check the url in the way it is
		$r = self::getRedirectByURL($url);
		if (!empty($r)) {
			// perform redirect
			self::doRedirect($r);
			return;
		}
		
		$r = self::getRedirectByURL(urldecode($url));
		if (!empty($r)) {
			// perform redirect
			self::doRedirect($r);
			return;
		}
		
		// if no matches found, check for wildchars		
		$wc = self::getWildchars();
		foreach($wc as $w) {
			if (self::matchWildchar($url, $w["redirect_from"])) {
				self::doRedirect($w);
				return;
			}
		}
		
		// if no wildchars worked, check for regexps
		$wc = self::getRegexps();
		foreach($wc as $w) {
			if (self::matchRegexp($orig_url, $w["redirect_from"])) {
				self::doRedirect($w);
				return;
			}
		}
	}
	
	static public function matchWildchar($url, $pattern) {
		$pattern = preg_quote($pattern, "/");
		$pattern = str_replace('\\*', '.*', $pattern);
		if (preg_match('/^'.$pattern.'$/u', $url)) {
			return true;
		}
		$url = urldecode($url);
		if (preg_match('/^'.$pattern.'$/u', $url)) {
			return true;
		} else {
			return false;
		}
	}
	
	static public function matchRegexp($url, $pattern) {
		$url = urldecode($url);
		
		if (preg_match($pattern, $url)) {
			return true;
		} else {
			return false;
		}
	}
	
	static public function on_page_start($caller) {

		if (get_class($caller) != "Concrete5_Library_View" && get_class($caller) != "View") {
			return;
		}
		
		$enable_all_pages = false;
		$pkg = Package::getByHandle("swp_301_redirects");
		if (intval($pkg->config("301_REDIRECT_ONLY_404")) === 0) {
			$enable_all_pages = true;
		}
		
		if ($enable_all_pages) {
			self::checkPage();
			return;
		}
		
		$db = debug_backtrace();
		if (isset($db[3]) && isset($db[3]["args"]) && ($db[3]["args"][0] == "/page_not_found"))
			self::checkPage();
	}
	
}