<?php       
defined('C5_EXECUTE') or die(_("Access Denied."));
$ih = Loader::helper("concrete/interface");

?>
<?php     
$pageUp = Page::getByPath("/dashboard/301_redirects/redirects");
echo Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('Settings'), t("These are the settings for &quot;SEO: 301 Redirects&quot; package"), 'span12 offset2', false, array(), $pageUp);
?>
<form action="<?php     echo $this->url("/dashboard/301_redirects/settings", "save_settings"); ?>" method="post" id="save-settings-form">
<div class="ccm-pane-body">

<div class="setting-box">
<input id="checkbox-only-404" type="checkbox" name="only_404" value="Y"<?php    
if ($only_404)
	echo ' checked="checked"';
?>	/>
<label for="checkbox-only-404"><?php     echo t("Trigger redirects only for non-existent pages"); ?></label>
<br />
<em><?php     echo t("Disabling this option is not recommended, especially if you have redirects with wildchars. Make sure  all redirects are setup correctly."); ?></em>
</div><!-- setting-box -->

<div class="setting-box">

<strong><?php     echo t("Redirects per page to display in the dashboard"); ?>:</strong> <input type="text" size="4" name="redirects_per_page" value="<?php     echo intval($redirects_per_page); ?>" /><br />
<em><?php     echo t("Zero means no pagination, i.e. all redirects are shown on one page"); ?></em>

</div><!-- setting-box -->

<div class="setting-box">

<input id="checkbox-expert-mode" type="checkbox" name="expert_mode" value="Y"<?php    
if ($expert_mode)
	echo ' checked="checked"';
?>	/>
<label for="checkbox-expert-mode"><?php     echo t("Enable Expert mode"); ?></label>
<br />
<em><?php     echo t("Expert mode allows to use regular expressions (PCRE). If you don't know what is PCRE, hire an expert or just don't use this option :)"); ?></em>

</div><!-- setting-box -->

<br />

</div><!-- ccm-pane-body -->
<div class="ccm-pane-footer">
<?php    
$b = $ih->submit(t("Save"), "save-settings-form", "left", "primary");
echo $b;
?>
</div><!-- ccm-pane-footer -->
</form>
<?php    
echo Loader::helper('concrete/dashboard')->getDashboardPaneFooterWrapper(false);
?>