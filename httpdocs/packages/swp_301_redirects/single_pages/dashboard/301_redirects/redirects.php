<?php       
defined('C5_EXECUTE') or die(_("Access Denied."));
$nh = Loader::helper("navigation");
$validation_token = Loader::helper("validation/token");
Loader::model("file");

echo Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('Redirects'), false, 'span16', true);

if (empty($redirects)) {
	echo t("No redirects yet.");
} else {
	echo '<div class="validation-token-wrapper" style="display: none;">';
	$validation_token->output("delete_redirect");
	echo '<a href="'. $this->url("/dashboard/301_redirects/redirects", "delete_redirect") .'">submit</a>';
	echo '</div>';
	echo '<table class="redirects-list">';
	
	/*
	echo '<tr>';
	echo '<th>'. t("Redirect From") .'</th>';
	echo '<th>'. t("Redirect To") .'</th>';
	echo '<th>'. t("Delete") .'</th>';
	echo '</tr>';
	*/
	
	foreach($redirects as $k => $redirect) {
		echo '<tr id="redirect-row-'. $redirect["ID"] .'"';
		if ($k % 2 == 1) {
			echo ' class="even-row"';
		} else {
			echo ' class="odd-row"';
		}
		echo '>';
		
		echo '<td>';
		echo '<div class="redirect-from-table">';
		
		if ($redirect["isRegexp"] == "N") {
			echo '<a href="';
			echo "/" . $redirect["redirect_from"];
			echo '" target="_blank"  title="/'. $redirect["redirect_from"] .'">';
			echo "/" . $redirect["redirect_from"];
			echo '</a>';
		} else {
			echo t('REGEXP:') ." ". htmlspecialchars($redirect["redirect_from"]);
		}
		
		echo '</div>';
		echo '</td>';
		
		echo '<td>';
		echo '<img src="'.DIR_REL.'/packages/swp_301_redirects/images/arrow.gif" alt="" />';
		echo '</td>';
		echo '</td>';
		
		echo '<td>';
		echo '<div class="redirect-to-table">';
		
		$redirect_to = $redirect["redirect_to"];
		if ($redirect["redirect_type"] == "P") {
			$rpage = Page::getByID($redirect_to);
			$redirect_to = $nh->getLinkToCollection($rpage);
		} elseif ($redirect["redirect_type"] == "F") {
			$rfile = File::getByID($redirect_to);
			$redirect_to = View::url('/download_file', $redirect_to);
			
			echo t('FILE: ');
		} else {
			$redirect_to = $redirect_to;
		}		
		
		if ($redirect["redirect_type"] == "M") {
			echo t("REGEXP REPLACEMENT: ");
			echo htmlspecialchars($redirect_to);
		} else {
			echo '<a href="';
			echo $redirect_to;
			echo '" target="_blank" title="'. $redirect_to .'">';
			if ($redirect["redirect_type"] == "F") {
				echo $rfile->getFilename();
			} else {
				echo $redirect_to;
			}
			echo '</a>';
		}
		
		echo '</div>';
		echo '</td>';
		
		echo '<td>';
		echo '<img class="del-button" src="'.DIR_REL.'/packages/swp_301_redirects/images/delete.png" title="'. t("Delete redirect") .'" alt="'. t("Delete redirect") .'" />';
		echo '</td>';
		
		echo '</tr>';
	}
	echo '</table>';
}

if ($total_pages > 1) {
	$uh = Loader::helper("url");
	echo '<div class="redirects-pagination">';
	echo t("Pages:");
	echo ' ';
	for($i=1;$i<=$total_pages;$i++) {
		echo ' ';
		if ($i == $current_page) {
			echo '<strong>'.$i.'</strong>';
		} else {
			echo '<a href="';
			echo $uh->setVariable("page", $i);
			echo '">';
			echo $i;
			echo '</a>';
		}
		echo ' ';
	}
	echo '</div>';
}


echo '<h3>' . t("Add Redirect") . '</h3>';

Loader::packageElement("dashboard/add_redirect", "swp_301_redirects", array(
	"expert_mode" => $expert_mode,
));

echo Loader::helper('concrete/dashboard')->getDashboardPaneFooterWrapper(true);

?>