<?php       
defined('C5_EXECUTE') or die(_("Access Denied."));
$ih = Loader::helper("concrete/interface");

$pageUp = Page::getByPath("/dashboard/301_redirects/redirects");
echo Loader::helper('concrete/dashboard')->getDashboardPaneHeaderWrapper(t('CSV Import'), false, 'span12 offset2', false, array(), $pageUp);

?>
<div class="ccm-pane-body">

<?php    
if (!empty($import_errors)) {
?>
<div class="csv-import-errors">
<?php    
foreach($import_errors as $line => $err) {
	echo '<div class="error-item">';
	echo '<strong>';
	echo t("Line ");
	echo ($line+1);
	echo ": ";
	echo '</strong>';
	echo '<br />';
	echo implode('<br />', $err);
	echo '</div>'; // error-item
}
?>
</div>
<?php    
}
?>

<div class="csv-import-description">
<p><?php     echo t("This section allows you to import redirects from a csv file. The structure of the csv file should be the following:"); ?></p>
<p><?php     echo t("It must have 3 columns:"); ?>
<ol>
<li><?php     echo t("source link, i.e. the URL you want the redirect to be made from, for example: /products.html or just products.html;"); ?></li>
<li><?php     echo t("destination type: PAGE, FILE or URL; other values are not allowed;"); ?></li>
<li><?php     echo t("destination point:"); ?>
<ul>
<li><?php     echo t("if the destination type is PAGE, you should either specify the page ID number, or the page path, like this: /products/;"); ?></li>
<li><?php     echo t("if the destination type is FILE, you should specify the file ID number;"); ?></li>
<li><?php     echo t("if the destination type is URL, you should specify the destination location as it is, for example: http://www.concrete5.org/"); ?></li>
</ul>
</li></ol>
</p>
</div><!-- csv-import-description -->

<div class="csv-import-example">
<strong><?php     echo t("Example"); ?></strong>
<pre>
&quot;/products.html&quot;;&quot;PAGE&quot;;&quot;/products/&quot;
&quot;/about.html&quot;;&quot;PAGE&quot;;&quot;/about/&quot;
&quot;/products2.html&quot;;&quot;URL&quot;;&quot;/products/?page=2&quot;
</pre>
</div><!-- csv-import-example -->

<form action="<?php     echo $this->url("/dashboard/301_redirects/csv_import", "do_import"); ?>" method="post" id="csv-import-form" enctype="multipart/form-data">

<table cellspacing="0" cellpadding="0" class="import-settings-table">
<tr class="odd-row">
<th align="left"><?php     echo t("CSV File"); ?></th>
<td><input type="file" name="csv_file" class="import-file" /></td>
</tr>
<tr class="even-row">
<th align="left"><?php     echo t("CSV Delimiter: "); ?></th>
<td>
<select name="csv_delimiter">
<option value=","><?php     echo t("Comma"); ?></option>
<option value=";" selected="selected"><?php     echo t("Semicolon"); ?></option>
<option value="tab"><?php     echo t("Tab"); ?></option>
<option value="|"><?php     echo t("Pipe"); ?></option>
</select>
</td>
</tr>
</table>

</div>
<div class="ccm-pane-footer">
<?php    

$submit = $ih->submit(t("Import"), "csv-import-form", "left", "primary");
echo $submit;

?>
</div><!-- ccm-pane-footer -->
</form>
<?php    
echo Loader::helper('concrete/dashboard')->getDashboardPaneFooterWrapper(false);
?>