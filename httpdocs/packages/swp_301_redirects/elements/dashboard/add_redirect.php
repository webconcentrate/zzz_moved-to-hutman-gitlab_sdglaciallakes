<?php       
defined('C5_EXECUTE') or die(_("Access Denied."));
$ih = Loader::helper("concrete/interface");
$ph = Loader::helper('form/page_selector');
$al = Loader::helper("concrete/asset_library");

?>
<form action="<?php     echo $this->url("/dashboard/301_redirects/redirects", "add_redirect"); ?>" method="post" id="add-redirect-form">

<div class="swp-fieldset">
<div class="swp-legend"><?php     echo t("Redirect From"); ?></div>
<?php    
if ($expert_mode) {
?>
<table class="redirect-source-table">
<tr>
<td><input id="redirect-from-type-url" type="radio" name="redirect_from_type" value="url" checked="checked" /></td>
<td><label for="redirect-from-type-url"><?php     echo t("URL"); ?></label></td>
<td class="separator"><input id="redirect-from-type-regexp" type="radio" name="redirect_from_type" value="regexp" /></td>
<td><label for="redirect-from-type-regexp"><?php     echo t("Regular Expression"); ?> (<a href="http://www.php.net/manual/en/book.pcre.php" target="_blank">PCRE</a>)</label></td>
</tr>
</table>
<?php    
} else { // not expert mode
?>
<div class="fieldDescr"><?php     echo t("Enter the link you want to redirect from"); ?></div>
<?php    
} // expert mode switcher
?>
<input id="new-redirect-from" type="text" name="redirect_from" size="60" value="" />
<?php     if ($expert_mode) { ?>
<div class="regexp-note">
<em><?php     echo t("Include the pattern delimiters and necessary modifiers. Example: "); ?></em><?php    
echo '<input type="text" name="t" readonly="readonly" value="'. htmlspecialchars("/\\/(.*)\\.html/i") . '" />'; ?>
<br /><em><?php     echo t("Remember that the pattern is matched against REQUEST_URI, which starts with slash, for example: %s", "/test.html"); ?></em>
</div>
<?php     } else { ?>
<br /><em><?php     echo t("You can use wildchar symbol <b>*</b> which means any sequence of symbols. But keep in mind that having too many redirects with wildchars may negatively affect the website performance"); ?></em>
<?php     } ?>
</div><!-- swp-fieldset -->

<div class="swp-fieldset">
<div class="swp-legend"><?php     echo t("Redirect To"); ?></div><!-- swp-legend -->
<div class="fieldDescr"><?php     echo t("Choose the destination point"); ?></div>
<table class="redirect-type-table">
<tr>
<td><input id="redirect-type-P" class="redirect-type-input" type="radio" name="redirect_type" value="P" checked="checked" /> </td>
<td><label for="redirect-type-P"><?php     echo t("Concrete5 page"); ?></label></td>
<td class="separator"><input  class="redirect-type-input" id="redirect-type-F" type="radio" name="redirect_type" value="F" /> </td>
<td><label for="redirect-type-F"><?php     echo t("Concrete5 file"); ?></label></td>
<td class="separator"><input  class="redirect-type-input" id="redirect-type-U" type="radio" name="redirect_type" value="U" /> </td>
<td><label for="redirect-type-U"><?php     echo t("Static URL"); ?></label></td>
<td class="separator type-M"><input  class="redirect-type-input" id="redirect-type-M" type="radio" name="redirect_type" value="M" /> </td>
<td class="type-M"><label for="redirect-type-M"><?php     echo t("Regular expression replacement"); ?></label></td>
</tr>
</table>
<div class="redirect-type-P-panel">
<?php    
echo $ph->selectPage('redirectPage', 0);
?>
</div>
<div class="redirect-type-U-panel" style="display: none;">
<input id="new-redirect-to" type="text" name="redirect_to" size="60" value="" />
<div class="redirect-matches"><?php    
echo t("Example: ") . '/products/\1/';
?></div>
</div>
<div class="redirect-type-F-panel" style="display: none;"><?php    
echo $al->file('redirect-to-file', "redirectFile", t('Choose File'), null);
?></div>
</div><!-- swp-fieldset -->

<?php    

$b = $ih->submit(t("Add Redirect"), "add-redirect-form", "left", "primary");
echo $b;

?>

</form>