<?php     
defined('C5_EXECUTE') or die(_("Access Denied."));

class Swp301RedirectsPackage extends Package {

	protected $pkgHandle = 'swp_301_redirects';
	protected $appVersionRequired = '5.5';
	protected $pkgVersion = '2.2.3';

	public function on_start() {
		$h = Loader::helper("swp_301_redirects", "swp_301_redirects");
		Events::extend('on_start', 'Swp301RedirectsHelper', 'on_page_start', false, array());
	}
	
	public function getPackageDescription() {
		return t('Allows to setup 301 redirects on your website<br />By <a href="http://www.smartwebprojects.net/">www.smartwebprojects.net</a>');
	}

	public function getPackageName() {
		return t("301 Redirects");
	}

	public function install() {
		$pkg = parent::install();
		
		Loader::model('single_page');
		Loader::model('attribute/categories/collection');
		Loader::model("collection_types");
		Loader::model('collection_attributes');
		
		// dashboard top level page:
		$dsp0 = SinglePage::add('dashboard/301_redirects/', $pkg);
		$dsp0->update(array(
			'cName' => t('301 Redirects'),
			'cDescription' => t('Manage redirects for SEO')
		));
		
		// Redirects page
		$dsp1 = SinglePage::add('dashboard/301_redirects/redirects', $pkg);
		$dsp1->update(array(
			'cName' => t('Redirects'),
			'cDescription' => ""
		));
		
		// csv import page
		$dsp3 = SinglePage::add('dashboard/301_redirects/csv_import', $pkg);
		$dsp3->update(array(
			'cName' => t('CSV Import'),
			'cDescription' => "",
		));
		
		// Settings page
		$dsp2 = SinglePage::add('dashboard/301_redirects/settings', $pkg);
		$dsp2->update(array(
			'cName' => t('Settings'),
			'cDescription' => ""
		));
		
		// config options
		$pkg->saveConfig("301_REDIRECT_ONLY_404", 1);
		$pkg->saveConfig("301_REDIRECT_EXPERT_MODE", 0);
		$pkg->saveConfig("301_REDIRECT_REDIRECTS_PER_PAGE", 20);
	}
	
	public function uninstall() {
		parent::uninstall();
		
		$db = Loader::db();
		$db->Execute("drop table swp301Redirects");
	}
	
	public function upgrade() {
		$old_version = $this->getPackageVersion();
		
		parent::upgrade();
		
		switch ($old_version) {
			case "1.0":
				$this->upgrade_1_0();
			case "1.1":
			case "2.0":
			case "2.1":
			case "2.2":
			case "2.2.1":
			case "2.2.2":
				break;
			default:
				// version not in the list
				break;
		}
	}
	
	private function upgrade_1_0() {
		// put upgrade things here
		$this->saveConfig("301_REDIRECT_EXPERT_MODE", 0);
		$this->saveConfig("301_REDIRECT_REDIRECTS_PER_PAGE", 20);
		
		Loader::model('single_page');
		
		// csv import page
		$dsp3 = SinglePage::add('dashboard/301_redirects/csv_import', $this);
		$dsp3->update(array(
			'cName' => t('CSV Import'),
			'cDescription' => "",
		));
	}
	
}