<?php defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('location/query', 'sdgl_members');
Loader::model('category/query', 'sdgl_members');
Loader::model('subcategory/query', 'sdgl_members');

Loader::library('wc_crud/better_controller', 'wc_utils');
class DirectoryController extends BetterController {
	
	public function view($location_url_slug = null) {
		if (!$location_url_slug) {
			$this->redirectToAttractions();
		}
		
		$location = LocationQuery::activeByUrlSlug($location_url_slug);
		if (!$location) {
			$this->render404AndExit();
		}
		
		Loader::model('event', 'designer_event_calendar');
		$location_events = EventOccurrence::getByCount(date(DEC_DATETIMEPICKER_DATE_FORMAT), 5, null, 365, $location->id);
		
		$this->set('location', $location);
		$this->set('location_events', $location_events);
		
		$this->set('pageCanonicalUrl', BASE_URL . $this->url($location_url_slug));
		$this->set('pageTitle', $location->raw('title')); //pass raw (unescaped) title because it gets escaped in our header_vars.php theme element
		
		if (!empty($location->photos)) {
			$html = Loader::helper('html');
			$this->addHeaderItem($html->css('flickity.min.css'));
			$this->addFooterItem($html->javascript('flickity.pkgd.min.js'));
			$this->addHeaderItem($html->css('magnific-popup.css'));
			$this->addFooterItem($html->javascript('jquery.magnific-popup.min.js'));
		}
		
		$this->render('location');
	}
	
	public function category($category_url_slug = null) {
		if (!$category_url_slug) {
			$this->redirectToAttractions();
		}
		
		$category = CategoryQuery::byUrlSlug($category_url_slug);
		if (!$category) {
			$this->render404AndExit();
		}
		
		$subcategories = SubcategoryQuery::byCategoryId($category->id);
		$nearby_locations = LocationQuery::allNearbyLocations();
		
		$filter_subcategory_id = (!empty($_GET['sc']) && ctype_digit($_GET['sc'])) ? (int)$_GET['sc'] : null;
		$filter_nearby_location_id = (!empty($_GET['nl']) && ctype_digit($_GET['nl'])) ? (int)$_GET['nl'] : null;
		
		$locations = LocationQuery::activeByCategoryIdWithSubcategoryIdsAndNearbyLocationIds($category->id);
		$locations_keyed_by_id = Loader::helper('wc_utils', 'wc_utils')->arrayKeyedByColumn($locations, 'id');
		
		$this->set('category', $category);
		$this->set('subcategories', $subcategories);
		$this->set('nearby_locations', $nearby_locations);
		$this->set('filter_subcategory_id', $filter_subcategory_id);
		$this->set('filter_nearby_location_id', $filter_nearby_location_id);
		$this->set('locations', array_values($locations_keyed_by_id)); //intentionally don't key by id, because the output loop wants an index
		$this->set('category_ads', CategoryQuery::categoryAds($category->id));
		
		$this->set('pageCanonicalUrl', BASE_URL . $this->url('category', $category_url_slug));
		$this->set('pageTitle', $category->raw('title')); //pass raw (unescaped) title because it gets escaped in our header_vars.php theme element
		
		$this->addFooterItem('<script>var sdglMembersDirectoryLocationData = ' . $this->getLocationDataJson($locations_keyed_by_id) . ';</script>');
		$this->addFooterItem('<script src="' . DIR_REL . '/packages/sdgl_members/js/directory.' . WC_ASSET_CACHE_BUST_NUMBER . '.js"></script>'); //don't use C5 html helper because we need to inject the cache bust number into the filename
		$this->addFooterItem('<script src="https://maps.googleapis.com/maps/api/js?key=' . GOOGLE_MAPS_API_KEY_FRONTEND . '"></script>');
		
		$this->render('category');
	}
	
	private function getLocationDataJson($locations) {
		return json_encode(array_map(function ($location) {
			return array(
				'id' => $location->id,
				'title' => $location->title,
				'url_slug' => $location->url_slug,
				'details_page_url' => $location->details_page_url,
				'thumbnail_photo_src' => $location->thumbnail_photo->src(200, 150, true),
				'latitude' => $location->latitude,
				'longitude' => $location->longitude,
				'address_street' => $location->address_street,
				'address_city' => $location->address_city,
				'address_state' => $location->address_state,
				'address_zip' => $location->address_zip,
				'address_full_html' => $location->address_full_html,
				'phone' => $location->phone,
				'email' => $location->email,
				'website_url' => $location->website_url,
				'subcategory_ids' => $location->subcategory_ids,
				'nearby_location_ids' => $location->nearby_location_ids,
			);
		}, $locations));
	}
	
	private function redirectToAttractions() {
		$url = BASE_URL . DIR_REL . '/attractions';
		header("Location: {$url}");
		exit;
	}
}