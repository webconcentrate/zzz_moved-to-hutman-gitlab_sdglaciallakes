<?php defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('category/query', 'sdgl_members');
Loader::model('category/crud', 'sdgl_members');

Loader::library('wc_crud/better_controller', 'wc_utils');
class DashboardSdglMembersAdsController extends BetterController {

	public function on_before_render() {
		//Load css into the <head> and javascript into the footer of all views for this controller
		// (If you want to load js/css only for one action, put the addHeaderItem/addFooterItem call in that action's method instead)
		//DEV NOTE: we use "on_before_render()" instead of "on_page_view()" (on_page_view only works in block controllers [??])
		$hh = Loader::helper('html');
		$this->addHeaderItem($hh->css('dashboard/ads/dashboard.css', 'sdgl_members'));
		$this->addFooterItem($hh->javascript('dashboard/ads/dashboard.js', 'sdgl_members'));
	}
	
	public function view() {
		$this->set('categories', CategoryQuery::all());
	}
	
	public function edit($category_id = null) {
		$category_id = (int)$category_id;
		
		if ($_POST) {
			$data = array_filter($this->post('ads', array()), function ($row) { return !empty($row['image_fID']); }); //discard rows that have no image chosen
			$image_fIDs = array_map(function($a) { return $a['image_fID']; }, $data);
			$ads = new CategoryAdCRUDs($image_fIDs);
			$ads->save($category_id);
			$this->flash('Ads Saved');
			$this->redirect('view');
		} else {
			$category = CategoryQuery::byId($category_id);
			if (!$category) {
				$this->render404AndExit();
			}
			$ads = new CategoryAdCRUDs($category->id);
		}
		
		$this->set('category', $category);
		$this->set('ads', array_map(function($fID) { return array('image_fID' => $fID, 'errors' => array()); }, $ads->getImageFIDs()));
		
		$this->addFooterItem(Loader::helper('html')->javascript('dashboard/ads/underscore-min.js', 'sdgl_members'));
		$vue_js_filename = (Config::get('SITE_DEBUG_LEVEL') == DEBUG_DISPLAY_ERRORS ? 'dashboard/ads/vue1/vue.js' : 'dashboard/ads/vue1/vue.min.js');
		$this->addFooterItem(Loader::helper('html')->javascript($vue_js_filename, 'sdgl_members'));

		$this->render('edit');
	}
	
}