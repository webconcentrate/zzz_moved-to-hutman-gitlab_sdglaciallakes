<?php defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('category/crud', 'sdgl_members');
Loader::model('category/query', 'sdgl_members');
Loader::model('subcategory/crud', 'sdgl_members');
Loader::model('subcategory/query', 'sdgl_members');

Loader::library('wc_crud/better_controller', 'wc_utils');
class DashboardSdglMembersCategoriesController extends BetterController {
	
	public function view() {
		$this->set('categories', CategoryQuery::allAsArraysForJSON(true)); //pass true to get subcategoryCounts per each record
		
		//don't use `$html->javascript()` because we need to insert the cache bust number
		$this->addFooterItem('<script src="' . DIR_REL . '/packages/sdgl_members/js/dashboard/categories/dist/build.' . WC_ASSET_CACHE_BUST_NUMBER . '.js"></script>');
	}
	
	public function get_subcategories($category_id) {
		if (!$this->isAjax()) {
			exit;
		}
		
		if (empty($category_id)) {
			$this->json404AndExit();
		}
		
		$subcategories = SubcategoryQuery::byCategoryIdAsArraysForJSON($category_id);
		
		$this->sendJson(array(
			'success' => true,
			'data' => $subcategories,
		));
	}
	
	public function post_save_category() {
		$this->requireAjaxPost();
		
		$category = new CategoryCRUD($_POST);
		$success = $category->save();
		
		$this->sendJson(array(
			'success' => $success,
			'errors' => ($success ? array() : $category->getErrors()->asKeyedArray()),
			'data' => ($success ? $category->asArrayForJSON() : null),
		));
	}
	
	public function post_save_subcategory() {
		$this->requireAjaxPost();
		
		$subcategory = new SubcategoryCRUD($_POST);
		$success = $subcategory->save();
		
		$this->sendJson(array(
			'success' => $success,
			'errors' => ($success ? array() : $subcategory->getErrors()->asKeyedArray()),
			'data' => ($success ? $subcategory->asArrayForJSON() : null),
		));
	}
	
	public function post_sort_categories() {
		$this->requireAjaxPost();
		
		if (!empty($_POST['ids']) && is_array($_POST['ids'])) {
			$ids = Loader::helper('wc_utils', 'wc_utils')->sanitizeIds($_POST['ids']);
			CategoryCRUD::sort($ids);
		}
		
		//we don't have a failure mode (invalid input is just ignored)
		
		$this->sendJson(array(
			'success' => true,
		));
	}
	
	public function post_sort_subcategories($category_id) {
		if (empty($category_id)) {
			$this->json404AndExit();
		}
		
		$this->requireAjaxPost();
		
		if (!empty($_POST['ids']) && is_array($_POST['ids'])) {
			$ids = Loader::helper('wc_utils', 'wc_utils')->sanitizeIds($_POST['ids']);
			SubcategoryCRUD::sort($ids, $category_id);
		}
		
		//we don't have a failure mode (invalid input is just ignored)
		
		$this->sendJson(array(
			'success' => true,
		));
	}
	
//TODO:
	public function post_sort_locations($subcategory_id) {
		$this->requireAjaxPost();
		
		//not sure if we receive all at once, or one at a time
		// (depends on UI... is there a save/cancel button, or just real-time takes-effect?)
	}
	
	public function post_delete_category($category_id) {
		if (empty($category_id)) {
			$this->json404AndExit();
		}
		
		$this->requireAjaxPost();
		
		$category = new CategoryCRUD($category_id);
		if (!$category) {
			$this->json404AndExit();
		}
		
		$success = $category->delete();
		$error = $success ? null : 'Cannot delete category because it has one or more subcategories. You must delete all subcategories before the main category can be deleted.';
		
		$this->sendJson(array(
			'success' => $success,
			'error' => $error,
		));
	}
	
	public function post_delete_subcategory($subcategory_id) {
		if (empty($subcategory_id)) {
			$this->json404AndExit();
		}
		
		$this->requireAjaxPost();
		
		$subcategory = new SubcategoryCRUD($subcategory_id);
		if (!$subcategory) {
			$this->json404AndExit();
		}
		
		$subcategory->delete();
		
		$this->sendJson(array(
			'success' => true,
		));
	}
	
}