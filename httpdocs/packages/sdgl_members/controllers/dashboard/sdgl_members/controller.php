<?php defined('C5_EXECUTE') or die("Access Denied.");

Loader::library('wc_crud/better_controller', 'wc_utils');
class DashboardSdglMembersController extends BetterController {

	//This page is just a menu for sub-pages,
	//so we just dynamically generate the list from child pages...
	public function view() {
		$pl = new PageList;
		$pl->filterByParentID(Page::getCurrentPage()->getCollectionID());
		$pl->sortByDisplayOrder();
		$pages = $pl->get();
		
		$nh = Loader::helper('navigation');
		$presenters = array_map(function($page) use ($nh) {
			return (object)array(
				'url' => $nh->getLinkToCollection($page),
				'title' => h($page->getCollectionName()),
			);
		}, $pages);
		
		$this->set('pages', $presenters);
	}
}
