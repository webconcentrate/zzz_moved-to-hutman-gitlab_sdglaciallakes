<?php defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('location/crud', 'sdgl_members');
Loader::model('location/query', 'sdgl_members');
Loader::model('subcategory/query', 'sdgl_members');

Loader::library('wc_crud/better_controller', 'wc_utils');
class DashboardSdglMembersLocationsController extends BetterController {
	
	public function view() {
		$this->set('all_categories_and_subcategories', SubcategoryQuery::allGroupedByCategoryAsArraysForJSON());
		$this->set('all_nearby_locations', LocationQuery::allNearbyLocationsAsArraysForJSON());
		
		//don't use `$html->javascript()` because we need to insert the cache bust number
		$this->addFooterItem('<script src="' . DIR_REL . '/packages/sdgl_members/js/dashboard/locations/dist/build.' . WC_ASSET_CACHE_BUST_NUMBER . '.js"></script>');
	}
	
	public function get_locations() {
		if (!$this->isAjax()) {
			exit;
		}
		
		$subcategory_id = empty($_GET['sc']) ? 0 : max((int)$_GET['sc'], 0);
		$page = empty($_GET['p']) ? 1 : max((int)$_GET['p'], 1);
		
		if ($subcategory_id) {
			$locations = LocationQuery::bySubcategoryAsArraysForJSON($subcategory_id);
			$pagination = null;
		} else {
			$items_per_page = 100;
			list($locations, $pagination) = LocationQuery::allWithPaginationAsArraysForJSON($page, $items_per_page);
		}
		
		$this->sendJson(array(
			'success' => true,
			'locations' => $locations,
			'pagination' => $pagination,
		));
	}
	
	public function get_location($location_id) {
		if (!$this->isAjax()) {
			exit;
		}
		
		if (empty($location_id)) {
			$this->json404AndExit();
		}
		
		$location = new LocationCRUD($location_id);
		if (!$location) {
			$this->json404AndExit();
		}
		$data = $location->asArrayForJSON();
		
		$data['description'] = $this->wysiwygDbToEdit($data['description']);
		
		$photos = new LocationPhotoCRUDs($location->id);
		$data['photo_fIDs'] = $photos->getPhotoFIDsForJson();
		
		$subcategories = new LocationSubcategoryCRUDs($location->id);
		$data['subcategory_ids'] = $subcategories->getSubcategoryIdsForJson();
		
		$nearby_locations = new LocationNearbyLocationCRUDs($location->id);
		$data['nearby_location_ids'] = $nearby_locations->getNearbyLocationIdsForJson();
		
		$this->sendJson(array(
			'success' => true,
			'data' => $data,
		));
	}
	
	//note: this route is in the $bypassCsrfTokenValidationPaths above
	// because data is sent as raw JSON in the post body
	public function post_save_location() {
		$this->requireAjaxPost();
		
		if (array_key_exists('description', $_POST)) {
			$_POST['description'] = $this->wysiwygEditToDb($_POST['description']);
		}
		
		$location = new LocationCRUD($_POST);
		
		$photo_fIDs = (!empty($_POST['photo_fIDs']) && is_array($_POST['photo_fIDs']) ? $_POST['photo_fIDs'] : array());
		$photo_fIDs = array_filter($photo_fIDs); //discard rows that have no photo chosen
		$photos = new LocationPhotoCRUDs($photo_fIDs);
		
		$subcategory_ids = (!empty($_POST['subcategory_ids']) && is_array($_POST['subcategory_ids']) ? $_POST['subcategory_ids'] : array());
		$subcategories = new LocationSubcategoryCRUDs($subcategory_ids);
		
		$nearby_location_ids = (!empty($_POST['nearby_location_ids']) && is_array($_POST['nearby_location_ids']) ? $_POST['nearby_location_ids'] : array());
		$nearby_locations = new LocationNearbyLocationCRUDs($nearby_location_ids);
		
		if ($location->validate()) {
			$location_id = $location->save();
			$photos->save($location_id);
			$subcategories->save($location_id);
			$nearby_locations->save($location_id);
		}
		
		$this->sendJson(array(
			'success' => !empty($location_id),
			'errors' => ($location_id ? array() : $location->getErrors()->asKeyedArray()),
		));
	}
	
	public function post_delete_location($location_id) {
		if (empty($location_id)) {
			$this->json404AndExit();
		}
		
		$this->requireAjaxPost();
		
		$location = new LocationCRUD($location_id);
		if (!$location) {
			$this->json404AndExit();
		}
		
		$location->delete();
		
		$this->sendJson(array(
			'success' => true,
		));
	}
	
}