<?php defined('C5_EXECUTE') or die(_("Access Denied."));

class SdglMembersPackage extends Package {
	
	protected $pkgHandle = 'sdgl_members';
	protected $pkgName = 'SDGL Members';
	protected $pkgDescription = 'Manage Members, Locations and Categories';
	protected $appVersionRequired = '5.6.1'; //requires 5.6.1 due to copious use of the "h()" function
	protected $pkgVersion = '0.8';
	
	public function install() {
		if (!is_object(Package::getByHandle('wc_utils'))) {
			throw new exception('The "Web Concentrate Utilities" package must be installed');
			exit;
		}
		
		$pkg = parent::install();
		$this->installOrUpgrade($pkg);
		$this->runAfterSchemaUpdates();
	}
	
	public function upgrade() {
		$this->installOrUpgrade($this);
		parent::upgrade();
		$this->runAfterSchemaUpdates();
	}
	
	private function installOrUpgrade($pkg) {
		
		//Dashboard Pages:
		//Install one page for each *controller* (not each view),
		// plus one at the top-level to serve as a placeholder in the dashboard menu
		$this->getOrAddSinglePage($pkg, '/dashboard/sdgl_members', 'Manage SDGL Members'); //top-level placeholder

		$this->getOrAddSinglePage($pkg, '/dashboard/sdgl_members/locations', 'Members');
		$this->getOrAddSinglePage($pkg, '/dashboard/sdgl_members/categories', 'Categories');
		$this->getOrAddSinglePage($pkg, '/dashboard/sdgl_members/ads', 'Advertisements');
		
		//Front-End Pages:
		$sp = $this->getOrAddSinglePage($pkg, '/directory', 'Member Directory');
		$sp->setAttribute('exclude_nav', 1);
		$sp->setAttribute('exclude_page_list', 1);
		//$sp->setAttribute('exclude_search_index', 1);
		//$sp->setAttribute('exclude_sitemapxml', 1);
		
		//Blocktypes:
		$this->getOrInstallBlockType($pkg, 'sdgl_members_category_nav');
	}
	
	private function runAfterSchemaUpdates() {
		//nothing here yet
	}
		
/*** UTILITY FUNCTIONS ***/
	private function getOrAddSinglePage($pkg, $cPath, $cName = '', $cDescription = '') {
		Loader::model('single_page');
		
		$sp = SinglePage::add($cPath, $pkg);
		
		if (is_null($sp)) {
			//SinglePage::add() returns null if page already exists
			$sp = Page::getByPath($cPath);
		} else {
			//Set page title and/or description...
			$data = array();
			if (!empty($cName)) {
				$data['cName'] = $cName;
			}
			if (!empty($cDescription)) {
				$data['cDescription'] = $cDescription;
			}
			
			if (!empty($data)) {
				$sp->update($data);
			}
		}
		
		return $sp;
	}
	
	private function getOrInstallBlockType($pkg, $btHandle) {
		$bt = BlockType::getByHandle($btHandle);
		if (empty($bt)) {
			BlockType::installBlockTypeFromPackage($btHandle, $pkg);
			$bt = BlockType::getByHandle($btHandle);
		}
		return $bt;
	}
	
	private function getOrAddConfig($pkg, $key, $default_value_if_new = null) {
		$cfg = $pkg->config($key, true); //pass true to retrieve the full object (so we can differentiate between a non-existent config versus an existing config that has value set to null)
		if (is_null($cfg)) {
			$pkg->saveConfig($key, $default_value_if_new);
			return $default_value_if_new;
		} else {
			return $pkg->config($key);
		}
	}
	
}