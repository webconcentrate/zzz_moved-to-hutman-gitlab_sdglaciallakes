<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>

<div class="section-nav">

	<?php if ($single_category) { ?>
	
		<ul class="section-nav__list section-nav__list--level-1">
			<?php foreach ($subcategories as $subcategory) { ?>
				<?php
					// Hard-code remove of "cities" from "counties" category
					if ($single_category-> id == 9 && $subcategory->id == 55) {
						continue;
					}
				?>
				<li class="section-nav__item section-nav__item--level-1">
					<a class="section-nav__item-link section-nav__item-link--level-1" href="<?=$single_category->directory_url($subcategory->id)?>"><?=$subcategory->title?></a>
				</li>
			<?php } ?>
		</ul>
		
	<?php } else { ?>
	
		<ul class="section-nav__list section-nav__list--level-1">
			<?php foreach ($attractions as $category) { ?>
				<li class="section-nav__item section-nav__item--level-1">
					<a class="section-nav__item-link section-nav__item-link--level-1" href="<?=$category->directory_url?>">
						<?=$category->title?>
					</a>
					<ul class="section-nav__list section-nav__list--level-2">
						<?php foreach ($category->subcategories as $subcategory) { ?>
							<li class="section-nav__item section-nav__item--level-2">
								<a class="section-nav__item-link section-nav__item-link--level-2" href="<?=$category->directory_url($subcategory->id)?>"><?=$subcategory->title?></a>
							</li>
						<?php } ?>
					</ul>
				</li>
			<?php } ?>
		</ul>
		
	<?php } ?>
	
</div>
