<?php defined('C5_EXECUTE') or die("Access Denied.");

$wch = Loader::helper('wc_utils', 'wc_utils');
?>

<div class="control-group">
	<label class="control-label" for="category_id">Category:</label>
	<div class="controls">
		<select name="category_id" id="category_id">
			<option value="">All Attractions</option>
			<?=$wch->select_options($category_options, $category_id)?>
		</select>
	</div>
</div>
