<?php defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('category/query', 'sdgl_members');
Loader::model('subcategory/query', 'sdgl_members');

class SdglMembersCategoryNavBlockController extends BlockController {

	protected $btName = 'SDGL Members Category Nav';
	protected $btTable = 'btSdglMembersCategoryNav';
	
	protected $btWrapperClass = 'ccm-ui'; //for twitter bootstrap styles in edit dialog
	protected $btInterfaceWidth = "450";
	protected $btInterfaceHeight = "150";
	
	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = false;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;
	
	public function add() {
		$this->set('category_options', $this->getCategoryOptions());
	}
	
	public function edit() {
		$this->set('category_options', $this->getCategoryOptions());
	}
	
	private function getCategoryOptions() {
		$categories = CategoryQuery::all();
		
		$wch = Loader::helper('wc_utils', 'wc_utils');
		$options = $wch->arrayColumn($categories, 'title', 'id');
		
		return $options;
	}
	
	public function save($args) {
		if (!empty($args['category_id']) && ctype_digit($args['category_id'])) {
			$args['category_id'] = (int)$args['category_id'];
		} else {
			$args['category_id'] = null;
		}
		
		return parent::save($args);
	}
	
	public function view() {
		if ($this->category_id) {
			$this->set('single_category', CategoryQuery::byId($this->category_id));
			$this->set('subcategories', SubcategoryQuery::byCategoryId($this->category_id));
		} else {
			$this->set('attractions', SubcategoryQuery::attractionsGroupedByCategory());
		}
	}
}
