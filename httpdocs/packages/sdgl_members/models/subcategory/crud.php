<?php defined('C5_EXECUTE') or die("Access Denied.");

Loader::library('wc_crud/crud_model', 'wc_utils');

class SubcategoryCRUD extends SortableCRUDModel {
	protected $table = 'sdgl_members_subcategories';
	protected $segment_by = 'category_id';
	
	public function validate() {
		$this->autoValidate(array(
			'category_id' => 'Main Category',
			'title' => 'Title',
		));

		if ($this->shouldValidate('title') && !empty($this->title)) {
			if (!$this->validateUniqueness('title', true)) {
				$this->errors->add('title', 'Title is already in use by another sub-category within the same main category');
			}
		}

		return parent::validate();
	}
	
	public function delete() {
		$this->deleteAssociations();
		
		return parent::delete();
	}
	
	private function deleteAssociations() {
		$sql = 'DELETE FROM sdgl_members_location_subcategories WHERE subcategory_id = ?';
		$vals = array((int)$this->id);
		$this->db->Execute($sql, $vals);
	}
	
	//Explicitly casts non-string values to their proper types
	// so javascript doesn't misinterpret them (e.g. "0" is true but 0 is false)
	public function asArrayForJSON() {
		return self::castArray($this->asArray());
	}
	
	public static function castArray($array) {
		$array['id'] = (int)$array['id'];
		$array['category_id'] = (int)$array['category_id'];
		$array['display_order'] = (int)$array['display_order'];
		return $array;
	}
}
