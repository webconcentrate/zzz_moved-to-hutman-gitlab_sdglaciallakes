<?php defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('subcategory/crud', 'sdgl_members');
Loader::model('subcategory/presenter', 'sdgl_members');

class SubcategoryQuery {
	
	public static function byId($id) {
		$sql = 'SELECT *
		        FROM sdgl_members_subcategories sc
		        WHERE id = ?';
		$vals = array((int)$id);
		return SubcategoryPresenter::one($sql, $vals);
	}
	
	public static function byCategoryId($category_id) {
		$sql = 'SELECT *
		        FROM sdgl_members_subcategories
		        WHERE category_id = ?
		        ORDER BY display_order';
		$vals = array((int)$category_id);
		return SubcategoryPresenter::many($sql, $vals);
	}
	
	//Assumes this is for json output, and hence casts values to their type
	// (because ADODB returns every value as a string, but we want javascript
	// to recognize the difference between "0" vs. 0, or "0" vs. false).
	public static function byCategoryIdAsArraysForJSON($category_id) {
		$sql = 'SELECT *
		        FROM sdgl_members_subcategories sc
		        WHERE category_id = ?
		        ORDER BY display_order';
		$vals = array((int)$category_id);
		$records = Loader::db()->GetArray($sql, $vals);
		
		$arrays = array_map(function ($r) {
			return SubcategoryCRUD::castArray($r);
		}, $records);
		
		return $arrays;
	}
	
	public static function attractionsGroupedByCategory() {
		return self::allGroupedByCategory(true);
	}
	
	public static function allGroupedByCategory($restrict_to_attractions = false) {
		Loader::model('category/query', 'sdgl_members');
		$wch = Loader::helper('wc_utils', 'wc_utils');
		
		$categories = $restrict_to_attractions ? CategoryQuery::attractions() : CategoryQuery::all();
		$subcategories = self::all();
		
		$subcategories_per_category_id = $wch->arrayGroupedByColumn($subcategories, 'category_id');
		foreach ($categories as $category) {
			$category->subcategories = array_key_exists($category->id, $subcategories_per_category_id) ? $subcategories_per_category_id[$category->id] : array();
		}
		
		return $categories;
	}
	
	public static function allGroupedByCategoryAsArraysForJSON() {
		Loader::model('category/query', 'sdgl_members');
		$wch = Loader::helper('wc_utils', 'wc_utils');
		
		$categories = CategoryQuery::allAsArraysForJSON();
		$subcategories = self::allAsArraysForJSON();
		
		$subcategories_per_category_id = $wch->arrayGroupedByColumn($subcategories, 'category_id');
		foreach ($categories as &$category) {
			$category_id = $category['id'];
			$category['subcategories'] = array_key_exists($category_id, $subcategories_per_category_id) ? $subcategories_per_category_id[$category_id] : array();
		}
		
		return $categories;
	}
	
	public static function all() {
		$sql = 'SELECT *
		        FROM sdgl_members_subcategories sc
		        ORDER BY display_order';
		return SubcategoryPresenter::many($sql);
	}
	
	public static function allAsArraysForJSON() {
		$sql = 'SELECT *
		        FROM sdgl_members_subcategories sc
		        ORDER BY display_order';
		$records = Loader::db()->GetArray($sql, $vals);
		
		$arrays = array_map(function ($r) {
			return SubcategoryCRUD::castArray($r);
		}, $records);
		
		return $arrays;
	}
	
}
