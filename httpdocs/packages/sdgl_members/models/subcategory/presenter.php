<?php defined('C5_EXECUTE') or die("Access Denied.");

Loader::library('wc_crud/presenter', 'wc_utils');

class SubcategoryPresenter extends Presenter {
	
	private $category = null;
	public function directory_url() {
		if (!$this->id) {
			return null;
		}
		
		if (is_null($this->category)) {
			Loader::model('category/query', 'sdgl_members');
			$this->category = CategoryQuery::byId($this->id);
		}
		
		return $this->category ? $this->category->directory_url($this->id) : null;
	}
	
}
