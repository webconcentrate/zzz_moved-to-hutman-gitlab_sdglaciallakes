<?php defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('category/crud', 'sdgl_members');
Loader::model('category/presenter', 'sdgl_members');

class CategoryQuery {
	
	public static function all() {
		$sql = 'SELECT * FROM sdgl_members_categories ORDER BY display_order';
		return CategoryPresenter::many($sql);
	}
	
	public static function attractions() {
		$sql = 'SELECT *
		        FROM sdgl_members_categories
		        WHERE is_attraction
		        ORDER BY display_order';
		return CategoryPresenter::many($sql);
	}
	
	//Assumes this is for json output, and hence casts values to their type
	// (because ADODB returns every value as a string, but we want javascript
	// to recognize the difference between "0" vs. 0, or "0" vs. false).
	public static function allAsArraysForJSON($with_subcategory_counts = false) {
		$sql = 'SELECT c.*';
		
		if ($with_subcategory_counts) {
			$subSql = 'SELECT COUNT(*) FROM sdgl_members_subcategories sc WHERE sc.category_id = c.id';
			$sql .= ", ({$subSql}) AS subcategoryCount";
		}
		
		$sql .= ' FROM sdgl_members_categories c ORDER BY c.display_order';
		
		$records = Loader::db()->GetArray($sql);
		
		$arrays = array_map(function ($r) use ($with_subcategory_counts) {
			$r = CategoryCRUD::castArray($r);
			if ($with_subcategory_counts) {
				$r['subcategoryCount'] = (int)$r['subcategoryCount'];
			}
			return $r;
		}, $records);
		
		return $arrays;
	}
	
	public static function byId($id) {
		$sql = 'SELECT * FROM sdgl_members_categories WHERE id = ?';
		$vals = array((int)$id);
		return CategoryPresenter::one($sql, $vals);
	}
	
	public static function byUrlSlug($url_slug) {
		$sql = 'SELECT * FROM sdgl_members_categories WHERE url_slug = ?';
		$vals = array($url_slug);
		return CategoryPresenter::one($sql, $vals);
	}
	
	public static function categoryAds($category_id_or_ids) {
		$category_ids = is_array($category_id_or_ids) ? $category_id_or_ids : array($category_id_or_ids);
		
		$category_ids = Loader::helper('wc_utils', 'wc_utils')->sanitizeIds($category_ids);
		if (empty($category_ids)) {
			return array();
		}
		
		$category_id_string = implode(',', $category_ids);
		
		$sql = "SELECT *
		        FROM sdgl_members_category_ads
		        WHERE category_id IN ({$category_id_string})
		        ORDER BY display_order";
		$ads = CategoryAdPresenter::many($sql);
		
		return $ads;
	}
	
}
