<?php defined('C5_EXECUTE') or die("Access Denied.");

Loader::library('wc_crud/crud_model', 'wc_utils');

class CategoryCRUD extends SortableCRUDModel {
	protected $table = 'sdgl_members_categories';
	
	public function validate() {
		$this->autoValidate(array(
			'title' => 'Title',
			'url_slug' => 'URL Slug',
		));

		if ($this->shouldValidate('title') && !empty($this->title)) {
			if (!$this->validateUniqueness('title')) {
				$this->errors->add('title', 'Title is already in use by another category');
			}
		}

		if ($this->shouldValidate('url_slug') && !empty($this->url_slug)) {
			if (!$this->validateUrlSlugFormat('url_slug')) {
				$this->errors->add('url_slug', 'URL Slug can only contain lowercase letters, numbers, dashes, and underscores');
			} else if (!$this->validateUniqueness('url_slug')) {
				$this->errors->add('url_slug', 'URL Slug is already in use by another category');
			}
		}
		
		return parent::validate();
	}
	
	public function delete() {
		if ($this->hasChildren()) {
			return false;
		}
		
		$sql = 'DELETE FROM sdgl_members_category_ads WHERE category_id = ?';
		$vals = array((int)$this->id);
		$this->db->Execute($sql, $vals);
		
		return parent::delete();
	}
	
	private function hasChildren() {
		$sql = 'SELECT COUNT(*) FROM sdgl_members_subcategories WHERE category_id = ?';
		$vals = array((int)$this->id);
		return (bool)$this->db->GetOne($sql, $vals);
	}
	
	//Explicitly casts non-string values to their proper types
	// so javascript doesn't misinterpret them (e.g. "0" is true but 0 is false)
	public function asArrayForJSON() {
		return self::castArray($this->asArray());
	}
	
	public static function castArray($array) {
		$array['id'] = (int)$array['id'];
		$array['display_order'] = (int)$array['display_order'];
		$array['is_attraction'] = (bool)$array['is_attraction'];
		return $array;
	}
}

class CategoryAdCRUDs {
	private $image_fIDs = array();
	
	public function __construct($category_id_or_image_fIDs = null) {
		if (is_array($category_id_or_image_fIDs)) {
			$this->image_fIDs = $category_id_or_image_fIDs;
		} else if ((int)$category_id_or_image_fIDs) {
			$sql = 'SELECT image_fID FROM sdgl_members_category_ads WHERE category_id = ? ORDER BY display_order';
			$vals = array((int)$category_id_or_image_fIDs);
			$this->image_fIDs = Loader::db()->GetCol($sql, $vals);
		}
	}
	
	public function getImageFIDs() {
		return array_map('intval', $this->image_fIDs);
	}
	
	public function save($category_id) {
		$db = Loader::db();
		
		$sql = 'DELETE FROM sdgl_members_category_ads WHERE category_id = ?';
		$vals = array((int)$category_id);
		$db->Execute($sql, $vals);
	
		$sql = 'INSERT INTO sdgl_members_category_ads (category_id, image_fID, display_order) VALUES (?, ?, ?)';
		$stmt = $db->Prepare($sql);
		$display_order = 0;
		foreach ($this->image_fIDs as $image_fID) {
			$display_order++;
			$vals = array((int)$category_id, (int)$image_fID, $display_order);
			$db->Execute($stmt, $vals);
		}
	}
	
}
