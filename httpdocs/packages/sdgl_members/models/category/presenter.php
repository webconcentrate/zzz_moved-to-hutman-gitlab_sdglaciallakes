<?php defined('C5_EXECUTE') or die("Access Denied.");

Loader::library('wc_crud/presenter', 'wc_utils');

class CategoryPresenter extends Presenter {
	
	public function directory_url($subcategory_id = null) {
		$url = View::url('/directory/category/' . $this->url_slug);
		
		$subcategory_id = max((int)$subcategory_id, 0);
		if ($subcategory_id) {
			$url .= "?sc={$subcategory_id}";
		}
		
		return $url;
	}
	
}

class CategoryAdPresenter extends Presenter {
	protected $image_properties = array(
		'image' => 'image_fID',
	);
	
	//Since there is only 1 image field in this table,
	// and that's the only reason someone would want to use this class,
	// pass thru image functions to that field...
	
	public function img($width = 0, $height = 0, $crop = false) {
		return $this->image->img($width, $height, $crop);
	}
	public function src($width = 0, $height = 0, $crop = false) {
		return $this->image->src($width, $height, $crop);
	}
	public function alt() {
		return $this->image->alt();
	}
	public function adURL() {
		$file = File::getByID($this->image_fID);
		if (is_null($file)) {
			return null;
		}
		$ad_url = $file->getAttribute('ad_url');
		if (!empty($ad_url)) {
			return $ad_url;
		} else {
			return null;	
		}
	}

}
