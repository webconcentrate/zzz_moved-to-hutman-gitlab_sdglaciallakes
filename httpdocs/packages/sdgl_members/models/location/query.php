<?php defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('location/crud', 'sdgl_members');
Loader::model('location/presenter', 'sdgl_members');

class LocationQuery {
	
	public function allAsSelectOptions() {
		$sql = 'SELECT id
		             , CONCAT(title, IF(NOT is_active, " [DISABLED / WILL NOT BE SHOWN]", "")) AS label
		        FROM sdgl_members_locations
		        ORDER BY title';
		$records = Loader::db()->GetArray($sql);
		return Loader::helper('wc_utils', 'wc_utils')->arrayColumn($records, 'label', 'id');
	}
	
	public static function allWithPaginationAsArraysForJSON($get_page = 1, $items_per_page = 100) {
		$db = Loader::db();
		
		$items_per_page = max((int)$items_per_page, 1);
		$offset_page_num = max((int)$get_page, 1);
		$offset_page_index = $offset_page_num - 1;
		$offset_record_index = $offset_page_index * $items_per_page;
		
		$sql = "SELECT *
		        FROM sdgl_members_locations
		        ORDER BY title
		        LIMIT {$items_per_page} OFFSET {$offset_record_index}";
		$records = $db->GetArray($sql);
		
		$wch = Loader::helper('wc_utils', 'wc_utils');
		$fIDs = $wch->arrayColumn($records, 'thumbnail_photo_fID');
		$thumbnail_srcs_per_fID = $wch->c5ThumbnailLvl1SrcsByFIDs($fIDs);
		
		$arrays = array_map(function ($r) use ($thumbnail_srcs_per_fID) {
			$array = LocationCRUD::castArray($r);
			$array['thumbnail_photo_c5_lvl1_thumb_src'] = array_key_exists($r['thumbnail_photo_fID'], $thumbnail_srcs_per_fID) ? $thumbnail_srcs_per_fID[$r['thumbnail_photo_fID']] : null;
			return $array;
		}, $records);
		
		$record_count = (int)$db->GetOne('SELECT COUNT(*) FROM sdgl_members_locations');
		$page_count = ceil($record_count / $items_per_page);
		
		$pagination = array(
			'currentPage' => $offset_page_num,
			'totalPages' => $page_count,
			'nextPage' => ($offset_page_num < $page_count ? $offset_page_num + 1 : null),
			'prevPage' => ($offset_page_num > 1 ? $offset_page_num - 1 : null),
		);
		
		return array($arrays, $pagination);
	}
	
	public static function bySubcategoryAsArraysForJSON($subcategory_id) {
		$subcategory_id = max((int)$subcategory_id, 0);
		if (!$subcategory_id) {
			return array();
		}
		
		$sql = 'SELECT l.*
		        FROM sdgl_members_locations l
		        INNER JOIN sdgl_members_location_subcategories ls
		                ON ls.location_id = l.id
		        WHERE ls.subcategory_id = ?
		        ORDER BY l.title';
		$vals = array($subcategory_id);
		$records = Loader::db()->GetArray($sql, $vals);
		
		$wch = Loader::helper('wc_utils', 'wc_utils');
		$fIDs = $wch->arrayColumn($records, 'thumbnail_photo_fID');
		$thumbnail_srcs_per_fID = $wch->c5ThumbnailLvl1SrcsByFIDs($fIDs);

		$arrays = array_map(function ($r) use ($thumbnail_srcs_per_fID) {
			$array = LocationCRUD::castArray($r);
			$array['thumbnail_photo_c5_lvl1_thumb_src'] = array_key_exists($r['thumbnail_photo_fID'], $thumbnail_srcs_per_fID) ? $thumbnail_srcs_per_fID[$r['thumbnail_photo_fID']] : null;
			return $array;
		}, $records);
		
		return $arrays;
	}
	
	public static function byIdsAsArraysForJSON(array $ids) {
		$wch = Loader::helper('wc_utils', 'wc_utils');
		
		$ids = $wch->sanitizeIds($ids);
		
		if (empty($ids)) {
			return array();
		}
		
		$id_string = implode(',', $ids);
		
		$sql = "SELECT *
		        FROM sdgl_members_locations
		        WHERE id IN ({$id_string})
		        ORDER BY title";
		$records = Loader::db()->GetArray($sql);
		
		$fIDs = $wch->arrayColumn($records, 'thumbnail_photo_fID');
		$thumbnail_srcs_per_fID = $wch->c5ThumbnailLvl1SrcsByFIDs($fIDs);

		$arrays = array_map(function ($r) use ($thumbnail_srcs_per_fID) {
			$array = LocationCRUD::castArray($r);
			$array['thumbnail_photo_c5_lvl1_thumb_src'] = array_key_exists($r['thumbnail_photo_fID'], $thumbnail_srcs_per_fID) ? $thumbnail_srcs_per_fID[$r['thumbnail_photo_fID']] : null;
			return $array;
		}, $records);
		
		return $arrays;
	}
	
	public static function byId($id) {
		$sql = 'SELECT *
		        FROM sdgl_members_locations
		        WHERE id = ?';
		$vals = array((int)$id);
		$location = LocationPresenter::one($sql, $vals);
		
		if ($location) {
			$location->photos = self::locationPhotos($location->id);
		}
		
		return $location;
	}
	
	public static function byIds(array $ids) {
		$id_string = implode(',', $ids);
		
		$sql = "SELECT *
		        FROM sdgl_members_locations
		        WHERE id IN ({$id_string})
		        ORDER BY title";
		$locations = LocationPresenter::many($sql);
		
		foreach ($locations as $index=>$location) {
			$locations[$index]->photos = self::locationPhotos($location->id);
		}

		return $locations;
	}
	
	public static function activeByUrlSlug($url_slug) {
		$sql = 'SELECT *
		        FROM sdgl_members_locations
		        WHERE is_active
		        AND url_slug = ?';
		$vals = array($url_slug);
		$location = LocationPresenter::one($sql, $vals);
		
		if ($location) {
			$location->photos = self::locationPhotos($location->id);
		}
		
		return $location;
	}
	
	public static function locationPhotos($location_id_or_ids) {
		$location_ids = is_array($location_id_or_ids) ? $location_id_or_ids : array($location_id_or_ids);
		
		$location_ids = Loader::helper('wc_utils', 'wc_utils')->sanitizeIds($location_ids);
		if (empty($location_ids)) {
			return array();
		}
		
		$location_id_string = implode(',', $location_ids);
		
		$sql = "SELECT *
		        FROM sdgl_members_location_photos
		        WHERE location_id IN ({$location_id_string})
		        ORDER BY display_order";
		$photos = LocationPhotoPresenter::many($sql);
		
		return $photos;
	}
	
	public static function activeBySubcategoryId($subcategory_id) {
		$sql = 'SELECT l.*
		        FROM sdgl_members_locations l
		        INNER JOIN sdgl_members_location_subcategories ls
		                ON ls.location_id = l.id
		        WHERE l.is_active
		          AND ls.subcategory_id = ?';
		$vals = array((int)$subcategory_id);
		return LocationPresenter::many($sql, $vals);
	}
	
	public static function activeByCategoryIdWithSubcategoryIdsAndNearbyLocationIds($category_id) {
		$sql = 'SELECT l.*
		        FROM sdgl_members_locations l
		        WHERE l.is_active
		        AND l.id IN (SELECT ls.location_id
		                     FROM sdgl_members_location_subcategories ls
		                     INNER JOIN sdgl_members_subcategories sc
		                             ON ls.subcategory_id = sc.id
		                     WHERE ls.location_id = l.id
		                       AND sc.category_id = ?)
		        ORDER BY l.title';
		$vals = array((int)$category_id);
		$locations = LocationPresenter::many($sql, $vals);
		
		if ($locations) {
			$wch = Loader::helper('wc_utils', 'wc_utils');
			$db = Loader::db();
			
			$location_ids = $wch->arrayColumn($locations, 'id');
			$location_id_string = implode(',', $location_ids);
			
			$sql = "SELECT *
			        FROM sdgl_members_location_subcategories
			        WHERE location_id IN ({$location_id_string})";
			$subcategory_ids = $db->GetArray($sql);
			$subcategory_ids_per_location_id = $wch->arrayGroupedByColumn($subcategory_ids, 'location_id');
			foreach ($locations as $location) {
				if (array_key_exists($location->id, $subcategory_ids_per_location_id)) {
					$location->subcategory_ids = $wch->arrayColumn($subcategory_ids_per_location_id[$location->id], 'subcategory_id');
				} else {
					$location->subcategory_ids = array();
				}
			}
			
			$sql = "SELECT *
			        FROM sdgl_members_location_nearby_locations
			        WHERE location_id IN ({$location_id_string})";
			$nearby_location_ids = $db->GetArray($sql);
			$nearby_location_ids_per_location_id = $wch->arrayGroupedByColumn($nearby_location_ids, 'location_id');
			foreach ($locations as $location) {
				if (array_key_exists($location->id, $nearby_location_ids_per_location_id)) {
					$location->nearby_location_ids = $wch->arrayColumn($nearby_location_ids_per_location_id[$location->id], 'nearby_location_id');
				} else {
					$location->nearby_location_ids = array();
				}
			}
		}
		
		return $locations;
	}
	
	public static function activeAttractionsBySearchTerms($search_terms) {
		$sql = 'SELECT l.*
				FROM sdgl_members_locations l
				INNER JOIN sdgl_members_location_subcategories ls
						ON ls.location_id = l.id
				INNER JOIN sdgl_members_subcategories s
						ON ls.subcategory_id = s.id
				INNER JOIN sdgl_members_categories c
						ON s.category_id = c.id
				WHERE c.is_attraction
				  AND l.is_active
				  AND (l.title LIKE ? OR l.description LIKE ?)
				ORDER BY RAND()';
		$vals = array("%{$search_terms}%", "%{$search_terms}%");
		return LocationPresenter::many($sql, $vals);
	}
	
	public static function activeBySearchTerms($search_terms) {
		$sql = 'SELECT DISTINCT l.id, l.*
				FROM sdgl_members_locations l
				INNER JOIN sdgl_members_location_subcategories ls
						ON ls.location_id = l.id
				INNER JOIN sdgl_members_subcategories s
						ON ls.subcategory_id = s.id
				WHERE l.is_active
				  AND (l.title LIKE ? OR l.description LIKE ?)
				GROUP BY l.id
				ORDER BY RAND()';
		$vals = array("%{$search_terms}%", "%{$search_terms}%");
		return LocationPresenter::many($sql, $vals);
	}
	
	public static function allNearbyLocations() {
		$sql = 'SELECT * FROM sdgl_members_nearby_locations ORDER BY display_order';
		return NearbyLocationPresenter::many($sql);
	}
	
	public static function allNearbyLocationsAsArraysForJSON() {
		$sql = 'SELECT * FROM sdgl_members_nearby_locations ORDER BY display_order';
		$records = Loader::db()->GetArray($sql);
		return array_map(function ($r) {
			$r['id'] = (int)$r['id'];
			$r['display_order'] = (int)$r['display_order'];
			return $r;
		}, $records);
	}
}