<?php defined('C5_EXECUTE') or die("Access Denied.");

Loader::library('wc_crud/presenter', 'wc_utils');

class LocationPresenter extends Presenter {
	protected $wysiwyg_html_properties = array(
		'description',
	);
	
	protected $image_properties = array(
		'thumbnail_photo' => 'thumbnail_photo_fID',
	);

	public function details_page_url() {
		return View::url('/directory', $this->url_slug);
	}

	public function address_full_html() {
		$lines = array();
		
		if ($this->address_street) {
			$lines[] = nl2br($this->address_street);
		}
		
		if ($this->address_city || $this->address_state || $this->address_zip) {
			if ($this->address_city && $this->address_state) {
				$line = "{$this->address_city}, {$this->address_state} {$this->address_zip}";
			} else {
				$line = "{$this->address_city} {$this->address_state} {$this->address_zip}";
			}
			$lines[] = trim($line);
		}
		
		return implode('<br>', $lines);
	}

	public function address_street_single_line() {
		$s = $this->address_street;
		$s = str_replace("\r\n", ' ', $s);
		$s = str_replace("\r", ' ', $s);
		$s = str_replace("\n", ' ', $s);
		return $s;
	}
	
	public function address_full_urlencoded() {
		$components = array(
			$this->address_street_single_line,
			$this->address_city,
			$this->address_state,
			$this->address_zip,
		);
		
		$nonempty_components = array_filter($components);
		
		$string = implode(' ', $nonempty_components);
		
		return urlencode($string);
	}
	
	public function website_url() {
		$url = $this->raw('website_url');
		if (empty($url)) {
			return null;
		}
		
		if (substr($url, 0, 4) !== 'http') {
			$url = 'http://' . $url;
		}
		return $url;
	}
	
	// Video stuff
	public function video_url() {
		$video_url = $this->raw('video_url');
		if (empty($video_url)) {
			return null;
		}
		
		$youtube_video_id = $this->extractYoutubeIdFromUrl($video_url);
		$vimeo_video_id = $this->extractVimeoIdFromUrl($video_url);
		$clean_video_url = '';
		if ($youtube_video_id) {
			$clean_video_url = 'https://www.youtube.com/watch?v=' . $youtube_video_id;
		} else if ($vimeo_video_id) {
			$clean_video_url = 'https://vimeo.com/' . $vimeo_video_id;
		}
		return $clean_video_url;
	}
	
	public function video_embed() {
		$video_url = $this->raw('video_url');
		if (empty($video_url)) {
			return null;
		}
		
		$youtube_video_id = $this->extractYoutubeIdFromUrl($video_url);
		$vimeo_video_id = $this->extractVimeoIdFromUrl($video_url);
		
		$video_embed = '';
		if ($youtube_video_id) {
			$video_embed = '<iframe width="560" height="315" src="https://www.youtube.com/embed/' . $youtube_video_id . '?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
		} else if ($vimeo_video_id) {
			$video_embed = '<iframe src="https://player.vimeo.com/video/' . $vimeo_video_id . '" width="560" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
		}
		
		return $video_embed;
	}
	
	private function extractYoutubeIdFromUrl($url) {
		// http://stackoverflow.com/a/6121972/477513
		$regex = "#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#";
		if (preg_match($regex, $url, $matches)) {
			return $matches[0];
		} else {
			return '';
		}
	}
	
	private function extractVimeoIdFromUrl($url) {
		// https://gist.github.com/wwdboer/4943672
		$regex = '~(?:<iframe [^>]*src=")?(?:https?:\/\/(?:[\w]+\.)*vimeo\.com(?:[\/\w]*\/videos?)?\/([0-9]+)[^\s]*)"?(?:[^>]*></iframe>)?(?:<p>.*</p>)?~ix';
		if (preg_match($regex, $url, $matches)) {
			return $matches[1];
		} else {
			return '';
		}
	}
	
	//This function only applies if the presenter object has
	// `->subcategory_ids` and `->nearby_location_ids` arrays
	// (via `LocationQuery::activeByCategoryIdWithSubcategoryIdsAndNearbyLocationIds()`)
	public function is_filtered_out($filter_subcategory_id, $filter_nearby_location_id) {
		if ($filter_subcategory_id && !in_array($filter_subcategory_id, $this->subcategory_ids)) {
			return true;
		} else if ($filter_nearby_location_id && !in_array($filter_nearby_location_id, $this->nearby_location_ids)) {
			return true;
		} else {
			return false;
		}
	}
}

class LocationPhotoPresenter extends Presenter {
	protected $image_properties = array(
		'photo' => 'photo_fID',
	);
	
	//Since there is only 1 image field in this table,
	// and that's the only reason someone would want to use this class,
	// pass thru image functions to that field...
	
	public function img($width = 0, $height = 0, $crop = false) {
		return $this->photo->img($width, $height, $crop);
	}
	public function src($width = 0, $height = 0, $crop = false) {
		return $this->photo->src($width, $height, $crop);
	}
	public function alt() {
		return $this->photo->alt();
	}

}

class NearbyLocationPresenter extends Presenter {
	
}