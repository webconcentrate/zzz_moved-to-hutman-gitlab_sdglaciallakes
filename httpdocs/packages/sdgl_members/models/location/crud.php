<?php defined('C5_EXECUTE') or die("Access Denied.");

Loader::library('wc_crud/crud_model', 'wc_utils');

class LocationCRUD extends CRUDModel {
	protected $table = 'sdgl_members_locations';
	
	public function validate() {
		$this->autoValidate(array(
			'title' => 'Name',
			'url_slug' => 'Page URL Slug',
			'description' => 'Description',
			'thumbnail_photo_fID' => 'Thumbnail Photo',
			'latitude' => 'Latitude',
			'longitude' => 'Longitude',
			'address_street' => 'Street Address',
			'address_city' => 'City',
			'address_state' => 'State',
			'address_zip' => 'ZIP Code',
			'phone' => 'Phone',
			'email' => 'Email',
			'website_url' => 'Website URL',
			'video_url' => 'Video URL',
		));
		
		if ($this->shouldValidate('url_slug')) {
			if (!$this->validateUrlSlugFormat('url_slug')) {
				$this->errors->add('url_slug', 'URL Slug can only contain lowercase letters, numbers, dashes, and underscores');
			} else if (!$this->validateUniqueness('url_slug')) {
				$this->errors->add('url_slug', 'URL Slug is already in use by another member');
			} else if ($this->url_slug == 'category') {
				//prevent this slug because it's where we put the category map pages in the url hierarchy
				$this->errors->add('url_slug', 'URL Slug "category" is not allowed');
			}
		}
		
		return parent::validate();
	}
	
	public function delete() {
		$this->deleteAssociations();
		return parent::delete();
	}
	
	private function deleteAssociations() {
		$vals = array((int)$this->id);
		
		$sql = 'DELETE FROM sdgl_members_location_photos WHERE location_id = ?';
		$this->db->Execute($sql, $vals);
		
		$sql = 'DELETE FROM sdgl_members_location_subcategories WHERE location_id = ?';
		$this->db->Execute($sql, $vals);
		
		$sql = 'DELETE FROM sdgl_members_location_nearby_locations WHERE location_id = ?';
		$this->db->Execute($sql, $vals);
	}
	
	//Explicitly casts non-string values to their proper types
	// so javascript doesn't misinterpret them (e.g. "0" is true but 0 is false)
	public function asArrayForJSON() {
		return self::castArray($this->asArray());
	}
	
	public static function castArray($array) {
		$array['id'] = (int)$array['id'];
		$array['thumbnail_photo_fID'] = (int)$array['thumbnail_photo_fID'];
		$array['is_active'] = (int)$array['is_active']; //yes, make it an int (not a boolean)
		return $array;
	}
}




class LocationPhotoCRUDs {
	private $photo_fIDs = array();
	
	public function __construct($location_id_or_photo_fIDs = null) {
		if (is_array($location_id_or_photo_fIDs)) {
			$this->photo_fIDs = $location_id_or_photo_fIDs;
		} else if ((int)$location_id_or_photo_fIDs) {
			$sql = 'SELECT photo_fID FROM sdgl_members_location_photos WHERE location_id = ? ORDER BY display_order';
			$vals = array((int)$location_id_or_photo_fIDs);
			$this->photo_fIDs = Loader::db()->GetCol($sql, $vals);
		}
	}
	
	public function getPhotoFIDsForJson() {
		return array_map('intval', $this->photo_fIDs);
	}
	
	public function save($location_id) {
		$db = Loader::db();
		
		$sql = 'DELETE FROM sdgl_members_location_photos WHERE location_id = ?';
		$vals = array((int)$location_id);
		$db->Execute($sql, $vals);
	
		$sql = 'INSERT INTO sdgl_members_location_photos (location_id, photo_fID, display_order) VALUES (?, ?, ?)';
		$stmt = $db->Prepare($sql);
		$display_order = 0;
		foreach ($this->photo_fIDs as $photo_fID) {
			$display_order++;
			$vals = array((int)$location_id, (int)$photo_fID, $display_order);
			$db->Execute($stmt, $vals);
		}
	}
}





class LocationSubcategoryCRUDs {
	private $subcategory_ids = array();
	
	public function __construct($location_id_or_subcategory_ids = null) {
		if (is_array($location_id_or_subcategory_ids)) {
			$this->subcategory_ids = $location_id_or_subcategory_ids;
		} else if ((int)$location_id_or_subcategory_ids) {
			$sql = 'SELECT subcategory_id FROM sdgl_members_location_subcategories WHERE location_id = ?';
			$vals = array((int)$location_id_or_subcategory_ids);
			$this->subcategory_ids = Loader::db()->GetCol($sql, $vals);
		}
	}
	
	public function getSubcategoryIdsForJson() {
		return array_map('intval', $this->subcategory_ids);
	}
	
	public function save($location_id) {
		$db = Loader::db();
		
		$sql = 'DELETE FROM sdgl_members_location_subcategories WHERE location_id = ?';
		$vals = array((int)$location_id);
		$db->Execute($sql, $vals);
	
		$sql = 'INSERT INTO sdgl_members_location_subcategories (location_id, subcategory_id) VALUES (?, ?)';
		$stmt = $db->Prepare($sql);
		foreach ($this->subcategory_ids as $subcategory_id) {
			$vals = array((int)$location_id, (int)$subcategory_id);
			$db->Execute($stmt, $vals);
		}
	}
}



class LocationNearbyLocationCRUDs {
	private $nearby_location_ids = array();
	
	public function __construct($location_id_or_nearby_location_ids = null) {
		if (is_array($location_id_or_nearby_location_ids)) {
			$this->nearby_location_ids = $location_id_or_nearby_location_ids;
		} else if ((int)$location_id_or_nearby_location_ids) {
			$sql = 'SELECT nearby_location_id FROM sdgl_members_location_nearby_locations WHERE location_id = ?';
			$vals = array((int)$location_id_or_nearby_location_ids);
			$this->nearby_location_ids = Loader::db()->GetCol($sql, $vals);
		}
	}
	
	public function getNearbyLocationIdsForJson() {
		return array_map('intval', $this->nearby_location_ids);
	}
	
	public function save($location_id) {
		$db = Loader::db();
		
		$sql = 'DELETE FROM sdgl_members_location_nearby_locations WHERE location_id = ?';
		$vals = array((int)$location_id);
		$db->Execute($sql, $vals);
	
		$sql = 'INSERT INTO sdgl_members_location_nearby_locations (location_id, nearby_location_id) VALUES (?, ?)';
		$stmt = $db->Prepare($sql);
		foreach ($this->nearby_location_ids as $nearby_location_id) {
			$vals = array((int)$location_id, (int)$nearby_location_id);
			$db->Execute($stmt, $vals);
		}
	}
}
