<?php defined('C5_EXECUTE') or die("Access Denied.");

//This page is intentionally blank. The controller redirects requests
// for the top-level /directory route elsewhere, but we still must have
// a view.php file here otherwise C5 fails to install the controller.