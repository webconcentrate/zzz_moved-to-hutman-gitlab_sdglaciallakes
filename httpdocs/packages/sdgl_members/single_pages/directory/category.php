<?php
	defined('C5_EXECUTE') or die("Access Denied.");
	$wch = Loader::helper('wc_utils', 'wc_utils');
	$include_skip_to_main_link = false; // Tells header.php that this template has a main content section to jump to.
	$pageTitle = $category->title; // Tells header_vars.php/header.php and main_heading.php what the page title should be.
	$hide_header = true; // Tells header.php to hide the site header
	$hide_footer = true; // Tells footer.php to hide the site footer
	include($this->getThemeDirectory() . '/elements/header.php');
	
	if ($category->is_attraction) {
		$back_to_url = View::url('/attractions');
		$back_to_text = 'Attractions';
	} else if ($category->url_slug == 'where-to-stay') {
		$back_to_url = View::url('/where-to-stay');
		$back_to_text = 'Where to Stay';
	} else if ($category->url_slug == 'travel-services') {
		$back_to_url = View::url('/plan-your-trip/travel-services');
		$back_to_text = 'Travel Services';
	} else if ($category->url_slug == 'counties') {
		$back_to_url = View::url('/plan-your-trip/counties');
		$back_to_text = 'Counties';
	} else {
		$back_to_url = View::url('');
		$back_to_text = 'Home';
	}
	
	//$one_random_category_ad = $category_ads ? $category_ads[array_rand($category_ads, 1)] : null;
	shuffle($category_ads);
	$category_ads = array_slice($category_ads, 0, 3);
?>
	<div class="the-page__main the-page__main--no-footer">
		<div class="the-page__main-liner">
				
			<div class="directory">
				<div class="directory__header">
					<div class="directory__back-link-and-heading">
						<div class="directory__back-link">
							<a href="<?=$back_to_url?>">Back to <?=$back_to_text?></a>
						</div>
						<div class="directory__heading">
							<h1><?=$category->title?></h1>
						</div>
					</div>
					<div class="directory__filters">
						<form id="directory-filters" class="directory__filters-form" method="get" action="<?=View::url('/directory/category', $category->url_slug)?>">
							<div class="directory__filters-input">
								<div class="directory__filters-input-label">
									<label for="directory-filter-subcategory">Category:</label>
								</div>
								<div class="directory__filters-input-field">
									<select name="sc" id="directory-filter-subcategory">
										<option value="">ALL</option>
										<?=$wch->select_options($wch->arrayColumn($subcategories, 'title', 'id'), $filter_subcategory_id)?>
									</select>
								</div>
							</div>
							<?php
								// Hard-code remove "counties" category
								if ($category-> id != 9) {
							?>
								<div class="directory__filters-input">
									<div class="directory__filters-input-label">
										<label for="directory-filter-nearby-location">Near:</label>
									</div>
									<div class="directory__filters-input-field">
										<select name="nl" id="directory-filter-nearby-location">
											<option value="">ALL</option>
											<?=$wch->select_options($wch->arrayColumn($nearby_locations, 'title', 'id'), $filter_nearby_location_id)?>
										</select>
									</div>
								</div>
							<?php } ?>
							<div class="directory__filters-submit">						
								<button>Go</button>
							</div>
						</form>
					</div>
				</div>
				<div class="directory__map">
					<div id="directory-map" class="directory__map-embed"></div>
				</div>
				<div id="directory-results" class="directory__results">
					<ul class="directory__results-list">
						<?php $has_any_visible_results = false; ?>
						<?php foreach ($locations as $index => $location) { ?>
							<?php if ($index > 1 && ($index % 3 == 0) && $category_ads) { ?>
								<?php $one_random_category_ad = array_shift($category_ads); ?>
								<li class="directory__results-item directory__results-item--ad">
									<?php if (!empty($one_random_category_ad->adURL())) { ?>
										<a href="<?=$one_random_category_ad->adURL()?>" target="_blank" rel="noopener">
									<?php }	?>
										<img src="<?=$one_random_category_ad->src(300, 250, true)?>" alt="<?=$one_random_category_ad->alt?>">
									<?php if (!empty($one_random_category_ad->adURL())) { ?>
										</a>
									<?php }	?>
								</li>
							<?php } ?>
							<?php 
								$is_hidden = $location->is_filtered_out($filter_subcategory_id, $filter_nearby_location_id);
								if (!$is_hidden) {
									$has_any_visible_results = true;
								}
							?>
							<li class="directory__results-item <?=($is_hidden ? 'directory__results-item--hidden' : '')?>" data-location-id="<?=$location->id?>">
								<div class="directory__results-item-copy <?=(!empty($location->thumbnail_photo) ? 'directory__results-item-copy--has-image' : '')?>">
									<div class="directory__results-item-title">
										<?=$location->title?>
									</div>
									<div class="directory__results-item-location">
										<?php if ($location->address_city && $location->address_state) { ?>
											<?=$location->address_city?>, <?=$location->address_state?>
										<?php } else if ($location->address_city || $location->address_state) { ?>
											<?=$location->address_city?> <?=$location->address_state?>
										<?php } ?>
									</div>
									<div class="directory__results-item-link">
										<a href="<?=$location->details_page_url?>">View Details</a>
									</div>
								</div>
								<?php if (!empty($location->thumbnail_photo)) { ?>
									<div class="directory__results-item-image">
										<img src="<?=$location->thumbnail_photo->src(200,150,true)?>" alt="">
									</div>
								<?php } ?>
							</li>
						<?php } ?>
						<li id="directory-no-results" class="directory__results-item directory__results-item--no-results <?=($has_any_visible_results ? 'directory__results-item--hidden' : '')?>">
							<div class="directory__results-item-copy">
								<i>No matching results</i>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

<?php include($this->getThemeDirectory() . '/elements/footer.php'); ?>