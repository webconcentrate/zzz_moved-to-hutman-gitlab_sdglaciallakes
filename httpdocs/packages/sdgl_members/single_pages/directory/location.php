<?php
	defined('C5_EXECUTE') or die("Access Denied.");
	$include_skip_to_main_link = true; // Tells header.php that this template has a main content section to jump to.
	$pageTitle = $location->title; // Tells header_vars.php/header.php and main_heading.php what the page title should be.
	include($this->getThemeDirectory() . '/elements/header.php');
?>

	<div class="the-page__main">
		<div class="the-page__main-liner">
				
			<div class="page-main-basic">
				<div class="page-main-basic__crumbs">
					<?php include($this->getThemeDirectory() . '/elements/global_area_crumbs.php'); ?>
				</div>
				<div class="page-main-basic__heading">
					<?php include($this->getThemeDirectory() . '/elements/main_heading.php'); ?>
				</div>
				<div class="page-main-basic__content page-main-basic__content--has-sidebar">
					<div class="page-main-basic__main">	
						<a name="main-content" style="display: block;"></a>
						
						<div clas="content content--width-1">
							<?=$location->description?>
						</div>
						
						<?php if ($location->video_embed) { ?>
							<div class="spacer spacer--size-10 spacer--style-3"></div>
							<div class="video-embed">
								<div class="video-embed__liner">
									<?=$location->video_embed?>
								</div>
							</div>
						<?php } ?>
						
						<?php if (!empty($location->photos)) { ?>
							<div class="spacer spacer--size-10 spacer--style-3"></div>
							
							<div class="js-photo-slider photo-slider" data-popup-enabled="1">
								<div class="photo-slider__liner">
									<div class="photo-slider__image-list-and-controls">
										<div class="js-photo-slider-items photo-slider__image-list">
											<?php foreach ($location->photos as $index=>$photo) { ?>
												<div id="photo-slider-item-<?=$index?>" class="js-photo-slider-item photo-slider__image-item <?=($index < 1 ? 'is-selected' : '')?>">
													<a class="js-photo-slider-image-link photo-slider__image-link" href="<?=$photo->src(1200, 1200, false)?>" data-caption="">
														<?php if ($index < 1) { ?>
															<img src="<?=$photo->src(1200, 800, true)?>" alt="<?=$photo->alt?>">
														<?php } else { ?>
															<img src="<?=DIR_REL?>/images/placeholder.svg" data-flickity-lazyload="<?=$photo->src(1200, 800, true)?>" alt="<?=$photo->alt?>">
														<?php } ?>
													</a>
												</div>
											<?php } ?>
										</div>
										<?php if (count($location->photos) > 1) { ?>
											<div class="photo-slider__controls">
												<button class="js-photo-slider-previous photo-slider__controls-previous">Previous</button>	
												<button class="js-photo-slider-next photo-slider__controls-next">Next</button>
											</div>
										<?php } ?>
									</div>
									<?php if (count($location->photos) > 1) { ?>
										<div class="photo-slider__pagination">
											<ol class="js-photo-slider-pagination-items photo-slider__pagination-list">
												<?php foreach ($location->photos as $index=>$item) { ?>
													<li class="js-photo-slider-pagination-item photo-slider__pagination-item">
														<a class="photo-slider__pagination-link" href="#photo-slider-item-<?=$index?>">
															<span>
																<?=$index?>
															</span>
														</a>
													</li>
												<?php } ?>
											</ol>
										</div>
									<?php } ?>
								</div>
							</div>						
						<?php } ?>
						
						<?php if ($location_events) { ?>
							<div class="spacer spacer--size-10 spacer--style-3"></div>
							<div class="event-list">
								<div class="event-list__heading">
									<h2>Featured Events</h2>
								</div>
								<ul class="event-list__list">
									<?php foreach ($location_events as $event) { ?>
										<?php if ($event->getDayInSequence() == 1) { ?>
											<li class="event-list__item">
												<div class="event-list__item-summary-date">
													<div class="event-list__item-summary-month">
														<?=$event->getStartFormatted('M')?>
													</div>
													<div class="event-list__item-summary-day">
														<?=$event->getStartFormatted('j')?>
													</div>
												</div>
												<div class="event-list__item-details">
													<div class="event-list__item-title">
														<?php if ($event->isLinkToNothing()) { ?>
															<span><?=$event->getTitle()?></span>
														<?php } else { ?>
															<a href="<?=$event->getLinkToDetailsPage()?>">
																<?=$event->getTitle()?>
															</a>
														<?php } ?>
													</div>
													<div class="event-list__item-date">
														<?php
															$start_date = $event->getStartFormatted('l, F d, Y');
															$start_time = $event->getStartFormatted('g:ia');
															$end_date = $event->getEndFormatted('l, F d, Y');
															$end_time = $event->getEndFormatted('g:ia');
															
															if ($event->isAllDay()) {
																echo 'All day';
															} else {
																echo $start_time;
																if (!$event->isEndTimeHidden()) {
																	echo ' - ' . $end_time;
																}
															}
															
															if ($event->isMultiDay()) {
																echo ' (thru ' . $end_date . ')';
															}
														?>
													</div>
													
													<?php if ($event->hasSubtitle()) { ?>
														<div class="event-list__item-location">
															<?=$event->getSubtitle()?>
														</div>
													<?php } ?>
													
												</div>
											</li>
										<?php } ?>
									<?php } ?>
								</ul>
							</div>
						<?php } ?>
					</div>
					<div class="page-main-basic__sidebar">
						<div class="cta-box">
							<?php if (!empty($location->thumbnail_photo)) { ?>
								<div class="cta-box__image">
									<img src="<?=$location->thumbnail_photo->src(400,300,true)?>" alt="">
								</div>
							<?php } ?>
							<div class="cta-box__heading-and-content">
								<div class="cta-box__heading">
									<h2><?=$location->title?></h2>
								</div>
								<div class="cta-box__content">
									<?=$location->address_full_html?><br>
									<a href="https://maps.google.com/?q=<?=$location->address_full_urlencoded?>" target="_blank" rel="noopener">Get Directions</a>
									<br>
									<?php if ($location->phone) { ?>
										<br>
										<?=$location->phone?>
									<?php } ?>
									<?php if ($location->email) { ?>
										<br>
										<a href="mailto:<?=$location->email?>">Email</a>
									<?php } ?>
								</div>
							</div>
							<?php if ($location->website_url) { ?>
								<div class="cta-box__cta">
									<a href="<?=$location->website_url?>" target="_blank" rel="noopener">Website</a>	
								</div>
							<?php } ?>
						</div>
						<?php include($this->getThemeDirectory() . '/elements/share.php'); ?>
					</div>
				</div>
			</div>
								
		</div>
	</div>

<?php include($this->getThemeDirectory() . '/elements/footer.php'); ?>