<?php defined('C5_EXECUTE') or die(_("Access Denied."));
$cdh = Loader::helper('concrete/dashboard');
$cih = Loader::helper('concrete/interface');
$wch = Loader::helper('wc_utils', 'wc_utils');
?>


<?=$cdh->getDashboardPaneHeaderWrapper('Ads')?>

	<?php
	Loader::library('wc_crud/listing_table', 'wc_utils');
	$table = new ListingTable($this);
	
	$table->addColumn('title', 'Category');
	
	$table->addAction('edit', 'right', 'Manage Ads', 'icon-pencil');
	
	$table->display($categories);
	?>

<?=$cdh->getDashboardPaneFooterWrapper()?>
