<?php defined('C5_EXECUTE') or die(_("Access Denied."));

$dh = Loader::helper('concrete/dashboard');
$ih = Loader::helper('concrete/interface');

?>

<?=$dh->getDashboardPaneHeaderWrapper('SDGL Member Categories', false, 'span12', false)?>
	
	<script>
		window.sdglMembersBaseUrl = '<?=DIR_REL?>/dashboard/sdgl_members/categories';
		window.sdglMembersCsrfToken = '<?=Loader::helper('validation/token')->generate()?>';
		window.sdglMembersData = <?=json_encode($categories)?>;
	</script>
	<div id="sdgl-members-categories-app">
		<div class="ccm-pane-body ccm-pane-body-footer">
			<img src="<?=ASSETS_URL_IMAGES?>/throbber_white_16.gif" alt="">
			<span style="vertical-align: top;">Loading...</span>
		</div>
	</div>
	
<?=$dh->getDashboardPaneFooterWrapper()?>
