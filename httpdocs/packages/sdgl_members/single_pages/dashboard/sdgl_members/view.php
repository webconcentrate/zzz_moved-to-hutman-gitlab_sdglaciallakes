<?php defined('C5_EXECUTE') or die(_("Access Denied."));
$dh = Loader::helper('concrete/dashboard');

echo $dh->getDashboardPaneHeaderWrapper('Manage SDGL Members');
?>

<ul class="nav nav-tabs nav-stacked">
	<?php foreach ($pages as $page) { ?>
		<li><a href="<?=$page->url?>"><?=$page->title?></a></li>
	<?php } ?>
</ul>

<?=$dh->getDashboardPaneFooterWrapper()?>