<?php defined('C5_EXECUTE') or die(_("Access Denied."));

$dh = Loader::helper('concrete/dashboard');
$ih = Loader::helper('concrete/interface');
?>

<?=$dh->getDashboardPaneHeaderWrapper('SDGL Members', false, 'span12', false)?>
	
	<script>
		window.sdglMembersBaseUrl = '<?=DIR_REL?>/dashboard/sdgl_members/locations';
		window.sdglMembersCsrfToken = '<?=Loader::helper('validation/token')->generate()?>';
		window.sdglMembersGoogleMapsApiKey = '<?=GOOGLE_MAPS_API_KEY_BACKEND?>';
		window.sdglMembersAllCategoriesAndSubcategories = <?=json_encode($all_categories_and_subcategories)?>;
		window.sdglMembersAllNearbyLocations = <?=json_encode($all_nearby_locations)?>;
		window.sdglMembersRichTextEditorOptions = <?=json_encode(Loader::helper('tinymce')->getOptions(array('height' => 250, 'width' => '100%', 'content_css' => PageTheme::getSiteTheme()->getThemeEditorCSS())))?>;
	</script>
	
	<div id="sdgl-members-locations-app">
		<div class="ccm-pane-body ccm-pane-body-footer">
			<img src="<?=ASSETS_URL_IMAGES?>/throbber_white_16.gif" alt="">
			<span style="vertical-align: top;">Loading...</span>
		</div>
	</div>
	
<?=$dh->getDashboardPaneFooterWrapper()?>
