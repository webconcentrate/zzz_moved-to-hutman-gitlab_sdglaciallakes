/*** drag-n-drop sorting ***/
$(document).ready(function() {
	$('.sortable-container table tbody').each(function() { //explicitly looop through each one so we can reference $(this) when setting the container option (in case there is more than one sortable list per page)
		$(this).sortable({
			handle: '.sortable-handle',
			axis: 'y',
			containment: $(this).closest('.sortable-container'), //contain to a wrapper div (not the <tbody> itself), so there's room for dropping at top and bottom of list
			helper: sortableHelper, //prevent cell widths from collapsing while dragging (see http://www.foliotek.com/devblog/make-table-rows-sortable-using-jquery-ui-sortable/ )
			stop: sortableStop, //save data back to server
			cursor: 'move'
		});
	});
	
	function sortableHelper(event, ui) {
		ui.children().each(function() {
			$(this).width($(this).width());
		});
		return ui;
	}

	function sortableStop(event, ui) {
		var $container = ui.item.closest('.sortable-container');
		var url = $container.attr('data-sortable-save-url');
		if (url) {
			var ids = [];
			ui.item.closest('table').find('tbody tr').each(function() {
				ids.push($(this).attr('data-sortable-id'));
			});


			var token = $container.attr('data-sortable-save-token');
			data = {
				'ids': ids.join(),
				'ccm_token': token
			};
			$.post(url, data);
		}
	}
});

if (typeof Vue !== 'undefined') {
	//Same thing as above, but implemented as a Vue directive.
	//To use this, add 'v-sortable' to the tbody element,
	// make sure a <div class="js-sortable-container"> surrounds the table,
	// and have your view model respond to the 'sortableStop' event
	// by looping through all of the items in the list and modifying the list's data array
	// to match the new order of DOM elements.
	//If you want a different name than 'sortableStop' for the event handler
	// (e.g. if you have a vue component with 2 sortable lists in it),
	// set the sortable-stop-handler attribute on the tbody element.
	Vue.directive('sortable', {
		params: ['sortable-stop-handler'],
		bind: function() {
			var vm  = this.vm;
			var params = this.params;
			var $el = $(this.el);
			
			var options = {
				handle: '.js-sortable-handle',
				axis: 'y',
				cursor: 'move',
				containment: $el.closest('.js-sortable-container'), //contain to a wrapper div (not the <tbody> itself), so there's room for dropping at top and bottom of list
				start: function(event, ui) {
					ui.item.startIndex = ui.item.index();
				},
				stop: function(event, ui) {
					var handler = params.sortableStopHandler || 'sortableStop';
					vm.$emit(handler, ui.item.startIndex, ui.item.index());
				}
			};
			
			if ($el.is('tbody')) {
				 //prevent cell widths from collapsing while dragging (see http://www.foliotek.com/devblog/make-table-rows-sortable-using-jquery-ui-sortable/ )
				options.helper = function(event, ui) {
					ui.children().each(function() {
						$(this).width($(this).width());
					});
					return ui;
				};
			}
			
			$el.sortable(options);
		}
	});
}

/*** segment dropdown filter ***/
$(document).ready(function() {
	$('.segment-filter select').on('change', function() {
		var $form = $(this).closest('form');
		$form.find('.loading-indicator').show();
		$form.trigger('submit');
	});
});


/*** url slugification ***/
function initUrlSlugify($nameField, $slugField) { //<--call this from within $(document).ready(function() { ... });
	var urlSlugWasManuallyChanged = false;
	
	$nameField.on('change keyup paste', function() { //http://stackoverflow.com/a/17317620/477513
		if (!urlSlugWasManuallyChanged) {
			var name = $(this).val();
			var slug = name.toLowerCase().replace(/-+/g, '').replace(/\s+/g, '-').replace(/[^a-z0-9-]/g, ''); //https://gist.github.com/bentruyman/1211400
			$slugField.val(slug);
		}
	});

	$slugField.on('change', function() {
		urlSlugWasManuallyChanged = true;
	});
}