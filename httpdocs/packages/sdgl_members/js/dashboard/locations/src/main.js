import Vue from 'vue'
import VueRouter from 'vue-router'
import draggable from 'vuedraggable'

import App from './App.vue'
import ListPage from './pages/ListPage.vue'
import EditPage from './pages/EditPage.vue'
import PageNotFound from './pages/PageNotFound.vue'

Vue.use(VueRouter)
Vue.component('draggable', draggable)

Vue.mixin({
  methods: {
    castValuesForPOST (data) {
      //sigh...
      // C5 doesn't like it when we send raw JSON (sometimes it works but sometimes it doesn't),
      // so instead we need to send the usual url-encoded form values...
      // BUT jquery is not very nice and it sends js nulls as string "null"
      // (and js true/false as string "true"/"false"), so we need to convert
      // nulls to empty strings, and booleans to 0/1.
      Object.keys(data).forEach(key => {
        if (data[key] === null) {
          data[key] = ''
        } else if (data[key] === false) {
          data[key] = 0
        } else if (data[key] === true) {
          data[key] = 1
        } else if (data[key] instanceof Array) {
          data[key] = this.castValuesForPOST(data[key])
        } else if (typeof data[key] === 'object') {
          data[key] = Object.assign({}, data[key]) //convert Vue-observed objects to plain JS objects
          data[key] = this.castValuesForPOST(data[key])
        }
      })
      
      return data
    },
  },
})

import _uniqueId from 'lodash.uniqueid';
window._uniqueId = _uniqueId

let router = new VueRouter({
  mode: 'hash',
  routes: [
    { path: '/', redirect: { name: 'list' } },
    { path: '/list',     name: 'list',   component: ListPage },
    { path: '/create',   name: 'create', component: EditPage },
    { path: '/edit/:id', name: 'edit',   component: EditPage },
    { path: '*', component: PageNotFound },
  ],
  
  //scroll to the top of the page whenever the route changes
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },
})

router.afterEach((to, from) => {
  if (to.name == 'list') {
    //When navigating back to the list from an edit page,
    // the C5 popup menu (e.g. when user clicks on a file chooser widget)
    // is invisible but exists at a place on the page farther down
    // than the list page has content for... which makes the page load
    // with just a blue screen.
    //SO whenever navigating back to the list page, look for those lingering popups
    // and if they have visibility:hidden then set their "top" to 0.
    $('.ccm-menu.ccm-ui').each(function() {
      if ($(this).css('visibility') == 'hidden') {
        $(this).css('top', 0)
      }
    })
  }
})

new Vue({
  el: '#sdgl-members-locations-app',
  router,
  render: h => h(App)
})
