To get set up, run `npm install` from this directory.

During development, run `npm run dev` to watch for changed files and auto-recompile.

When done making changes, run `npm run build` to compile minified js, and commit that to the repo (so it can be deployed).

Note that there is an .htaccess file in this directory to prevent public access to the source files,
but then we have *another* .htaccess file that gets put into the /dist directory which undoes those restrictions
within that subdirectory (so the /dist files will be publicly accessible).
