import Vue from 'vue'
import VueRouter from 'vue-router'
import draggable from 'vuedraggable'

import App from './App.vue'
import ListPage from './pages/ListPage.vue'
import PageNotFound from './pages/PageNotFound.vue'

Vue.use(VueRouter)
Vue.component('draggable', draggable)

Vue.mixin({
  methods: {
    castValuesForPOST (data) {
      //sigh...
      // C5 doesn't like it when we send raw JSON (sometimes it works but sometimes it doesn't),
      // so instead we need to send the usual url-encoded form values...
      // BUT jquery is not very nice and it sends js nulls as string "null"
      // (and js true/false as string "true"/"false"), so we need to convert
      // nulls to empty strings, and booleans to 0/1.
      Object.keys(data).forEach(key => {
        if (data[key] === null) {
          data[key] = ''
        } else if (data[key] === false) {
          data[key] = 0
        } else if (data[key] === true) {
          data[key] = 1
        } else if (data[key] instanceof Array) {
          data[key] = this.castValuesForPOST(data[key])
        } else if (typeof data[key] === 'object') {
          data[key] = Object.assign({}, data[key]) //convert Vue-observed objects to plain JS objects
          data[key] = this.castValuesForPOST(data[key])
        }
      })
      
      return data
    },
  },
})

let router = new VueRouter({
  mode: 'hash',
  routes: [
    { path: '/', redirect: { name: 'list' } },
    { path: '/list',     name: 'list',   component: ListPage },
    { path: '*', component: PageNotFound },
  ],
})

new Vue({
  el: '#sdgl-members-categories-app',
  router,
  render: h => h(App)
})
