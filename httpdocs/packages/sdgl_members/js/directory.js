$(function() {
	
	var gmap;
	var markers;
	var infoWindow;
	
	$('#directory-filter-subcategory').on('change', updateDirectory);
	$('#directory-filter-nearby-location').on('change', updateDirectory);
	$('#directory-filters').on('submit', function(event) {
		event.preventDefault();
		updateDirectory();
	});
	updateDirectory();
	
	$('[data-location-id]').on('click', selectLocation);
	$('[data-location-id] a').on('click', function(event) {
		event.stopPropagation();
	});
	
	function updateDirectory() {
		var filterSubcategoryId = $('#directory-filter-subcategory').val();
		var filterNearbyLocationId = $('#directory-filter-nearby-location').val();
		var showLocationIds = [];
		var hideLocationIds = [];
		$.each(sdglMembersDirectoryLocationData, function(locationId, location) {
			//var doShow = true;
			if (filterSubcategoryId && (-1 == $.inArray(filterSubcategoryId, location.subcategory_ids))) {
				hideLocationIds.push(locationId);
			} else if (filterNearbyLocationId && (-1 == $.inArray(filterNearbyLocationId, location.nearby_location_ids))) {
				hideLocationIds.push(locationId);
			} else {
				showLocationIds.push(locationId);
			}
		});
		
		selectListItem(null);
		filterList(showLocationIds, hideLocationIds);
		updateMapMarkers(showLocationIds, hideLocationIds);
		
		if (window.history) {
			var newQSArgs = [];
			if (filterSubcategoryId) {
				newQSArgs.push('sc=' + filterSubcategoryId);
			}
			if (filterNearbyLocationId) {
				newQSArgs.push('nl=' + filterNearbyLocationId);
			}
			var newUrl = '?' + newQSArgs.join('&');
			window.history.replaceState({}, '', newUrl);
		}
	}
	
	function filterList(showLocationIds, hideLocationIds) {
		showLocationIds.forEach(function(locationId) {
			var $item = $('[data-location-id="' + locationId + '"]');
			$item.removeClass('directory__results-item--hidden');
		});
		
		hideLocationIds.forEach(function(locationId) {
			var $item = $('[data-location-id="' + locationId + '"]');
			$item.addClass('directory__results-item--hidden');
		});
		
		$('#directory-no-results').toggleClass('directory__results-item--hidden', !!showLocationIds.length);
	}
	
	function updateMapMarkers(showLocationIds, hideLocationIds) {
		//destroy existing markers and map
		if (markers) {
			for (var key in markers) {
				markers[key].setMap(null);
			}
		}
		markers = {};
		gmap = null;
		infoWindow = null;
		
		//create new map
		gmap = new google.maps.Map($('#directory-map').get(0), {
			disableDefaultUI: true,
			zoomControl: true,
			zoom: 11,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
		});
		
		// create an info window. just creating one and changing the content based on click (avoids multi-window overlap)
		infoWindow = new google.maps.InfoWindow();

		// as we add markers, we'll re-adjust the map for best fit
		var bounds = new google.maps.LatLngBounds();

		// simulated "on complete" event (because gmap doesn't have a built-in one)
		var listener = google.maps.event.addListener(gmap, "idle", function() {
			//if zoomed in too far, bring it out to reasonable level
			if (gmap.getZoom() > 14) {
				gmap.setZoom(14);
			}
		
			google.maps.event.removeListener(listener);
		});

		// add markers for each location
		showLocationIds.forEach(function(locationId) {
			var location = sdglMembersDirectoryLocationData[locationId];
			
			// check that this location has lat/lng coordinates
			if (location.latitude && location.longitude && !isNaN(location.latitude) && !isNaN(location.longitude)) {
				var latlng = new google.maps.LatLng(location.latitude, location.longitude);
				
				var marker = new google.maps.Marker({
					position: latlng,
					map: gmap,
					title: location.title
				});

				google.maps.event.addListener(marker, 'click', function() {
					openMapInfoWindow(location, marker);
					selectListItem(locationId, true);
				});
				
				// adjust map to fit markers
				bounds.extend(latlng);
				gmap.fitBounds(bounds);
				
				//store a reference to this marker
				markers[locationId] = marker;
			}
		});
		
		google.maps.event.addListener(infoWindow, 'closeclick', function() {
		   selectListItem(null);
		});
	}
	
	function selectListItem(locationId, scrollTheList) {
		$('[data-location-id]').removeClass('directory__results-item--selected-item');
		if (locationId) {
			var $selectItem = $('[data-location-id="' + locationId + '"]');
			$selectItem.addClass('directory__results-item--selected-item');			
			if (scrollTheList) {
				$('#directory-results').animate({scrollTop : $('#directory-results').scrollTop() + $selectItem.position().top}, 100);
			}
		}
	}
	
	function selectLocation() {
		var locationId = $(this).attr('data-location-id');
		selectListItem(locationId);
		
		if (locationId in markers) {
			var marker = markers[locationId];
			var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
			gmap.panTo(latlng);
			
			var location = sdglMembersDirectoryLocationData[locationId];
			openMapInfoWindow(location, marker);
		} else {
			infoWindow.close();
		}
	}
	
	function openMapInfoWindow(location, marker) {
		//open info bubble on marker click
		var infoWindowContent = '<div class="directory__map-info-window">'
							  + '<div class="directory__map-info-window-copy">'
							  + '<div class="directory__map-info-window-title">' + location.title + '</div>'
		                      + '<div class="directory__map-info-window-address">' + location.address_full_html + '</div>'
		                      + (location.phone ? '<div class="directory__map-info-window-phone">' + location.phone + '</div>' : '')
		                      + (location.email ? '<div class="directory__map-info-window-email"><a href="mailto:' + location.email + '">' + location.email + '</a></div>' : '')
		                      + (location.website_url ? '<div class="directory__map-info-window-website"><a href="' + location.website_url + '" target="_blank" rel="noopener">Website</a></div>' : '')
		                      + '<div class="directory__map-info-window-link"><a href="' + location.details_page_url + '">View Details</a></div>'
		                      + '</div>';
		
		if (location.thumbnail_photo_src) {
			infoWindowContent += '<div class="directory__map-info-window-image"><img src="' + location.thumbnail_photo_src + '" alt=""></div>';
		}
		
		infoWindowContent += '</div>';
		infoWindow.setContent(infoWindowContent);
		infoWindow.open(gmap, marker);
	}
});