<?php defined('C5_EXECUTE') or die(_("Access Denied."));

class HidePageDescriptionsPackage extends Package {

	protected $pkgHandle = 'hide_page_descriptions';
	protected $appVersionRequired = '5.6';
	protected $pkgVersion = '1.1';
	
	public function getPackageName() {
		return t('Hide Page Descriptions');
	}	
	
	public function getPackageDescription() {
		return t('Hides "Description" field when adding/editing pages.');
	}
	
	public function on_start() {
		Events::extend('on_before_render', __CLASS__, 'inject_css', __FILE__);
		Events::extend('on_page_view', __CLASS__, 'inject_css', __FILE__); //on_before_render doesn't fire early enough if user is viewing a single_page
	}
	
	public function inject_css() {
		global $cp;
		$canViewToolbar = (isset($cp) && is_object($cp) && $cp->canViewToolbar()); //5.6+ only!
		if ($canViewToolbar) {
			$css = <<<EOT
<style>
#ccm-properties-standard-tab #cDescription,
#ccm-properties-standard-tab label[for="cDescription"],
#ccm-add-page-information textarea[name="cDescription"],
#ccm-add-page-information label[for="cDescription"],
#ccm-dashboard-composer-form #cDescription,
#ccm-dashboard-composer-form label[for="cDescription"] {
	display: none;
}
</style>
EOT;
			View::getInstance()->addHeaderItem($css);
		}
		
		
		//DEV NOTE:
		// In the interest of efficiency, we don't bother checking if user is in the dashboard area,
		// because we assume if user is in dashboard area then they also can view the toolbar.
		// But if it turns out that this is not the case, you can use the following code
		// to determine if user is in the dashboard area:
		//
		//    global $c;
		//    if ($c->isAdminArea()) { ... }
		
		//DEV NOTE #2:
		// There is intentionally no ampersand (&) in front of the $view function paramater,
		// because PHP 5.3.1 has a bug that generates a warning when you do that
		// (due to it not being able to pass by reference via dynamic __call-like methods,
		// which is what C5 uses to call event handlers). See https://bugs.php.net/bug.php?id=50394
		//
		// Fortunately, it's effectively the same thing to not have the ampersand (&)
		// because only an "alias" to the object is passed in by value (not the whole object itself).
		// See http://php.net/manual/en/language.oop5.references.php for full explanation.
	}
}