<?php defined('C5_EXECUTE') or die("Access Denied.");

/**
 * version: 2015-02-11
 * 
 * This class contains helper functions for inserting dynamic content
 * into the html <head> of a page from a block controller.
 * 
 * The problem this solves is that sometimes you have some CSS that you want to insert PHP variables into
 * (for example if you have dynamic background images that can be chosen by the user,
 * so you don't know ahead of time which `url` to use)...
 * 
 * In theory, the easiest way to achieve this would be to call "addHeaderItem" from a block' view.php file,
 * but in practice this does not work because by the time the block template is called,
 * C5 has already outputted the <head>!
 * 
 * The only way C5 provides to add items to the <head> from a block is by calling
 * `$this->addHeaderItem()` from the `on_page_view()` method, which is great and all,
 * except that as with many things related to the C5 API it is quite verbose and unintuitive.
 * 
 * So this function serve as a wrapper around all the boilerplate code needed to achieve this.
 * 
 * To use it, do something like this in your block controller:
 *   
 *   public function on_page_view() {
 *     Loader::library('wc_add_header_item', 'wc_utils');
 *     WcAddHeaderItem::fromBlock($this, 'the_name_of_the_file_in_your_block_directory.php');
 *   }
 */

class WcAddHeaderItem {

	public static function fromBlock($block_controller, $file_name, $additional_args = array()) {
		$bv = new BlockView();
		$bv->setBlockObject($block_controller->getBlockObject());
		$dir = $bv->getBlockPath($file_name);
		$path = $dir . '/' . trim($file_name, '/');

		$args = array_merge($additional_args, $block_controller->getSets(), $block_controller->getHelperObjects());

		$header_item = self::getRenderedTemplate($path, $args);
		
		$block_controller->addHeaderItem($header_item);
	}
	
	private static function getRenderedTemplate($path, $args = array()) {
		ob_start();
		extract($args);
		include($path);
		return ob_get_clean();
	}
}