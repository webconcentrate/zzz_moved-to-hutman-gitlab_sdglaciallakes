<?php defined('C5_EXECUTE') or die("Access Denied.");

/* version 2017-06-02 */

abstract class Presenter {
	protected $data;
	protected $wch; //general helper functions
	
	protected $raw_html_properties = array(); //names of fields containing strings that should NOT have their output html-escaped (e.g. embed codes or js tracking codes)
	protected $wysiwyg_html_properties = array(); //names of fields containing strings that have WYSIWYG editor controls -- their output will not be escaped AND CMS link replacements will be performed.
	protected $image_properties = array(); //names of fields containing images that we will do auto-thumbnail-caching for (key is accessor property name, value is db field name) -- see comments on the `getCachedImageThumbnail` function below for more details
	
	//Factory method for loading and returning 1 Presenter object
	// (returns null if no record found).
	public static function one($sql, $vals = array()) {
		$record = Loader::db()->GetRow($sql, $vals);
		$class = get_called_class();
		return empty($record) ? null : new $class($record);
	}
	
	//Factory method for loading and returning an array of Presenter objects
	// (returns empty array if no records found)
	public static function many($sql, $vals = array()) {
		$records = Loader::db()->GetArray($sql, $vals);
		return self::fromArrays($records);
	}
	
	public static function fromArrays(array $records) {
		$objects = array();
		
		$class = get_called_class();
		foreach ($records as $record) {
			$objects[] = new $class($record);
		}
		
		return $objects;
	}
	
	public function __construct($data) {
		if (is_array($data)) {
			$this->data = $data;
		} else if ($data instanceof CRUDModel) {
			$this->data = $data->asArray();
		} else if (is_object($data)) {
			$this->data = get_object_vars($data);
		} else {
			$this->data = array();
		}
			
		$this->wch = Loader::helper('wc_utils', 'wc_utils');
	}

	public function __get($name) {
		if (method_exists($this, $name)) {
			return $this->{$name}();
		}
	
		if (array_key_exists($name, $this->data)) {
			$value = $this->data[$name];
			
			if (in_array($name, $this->wysiwyg_html_properties)) {
				$value = Loader::helper('content')->translateFrom($value);
			} else if (is_string($value) && !in_array($name, $this->raw_html_properties)) {
				$value = h($value);
			}
			
			return $value;
		}
		
		if (array_key_exists($name, $this->image_properties)) {
			return $this->getCachedImageThumbnail($name);
		}
	
		throw new Exception('Unknown property "' . $name . '"');
	}
	
	public function __isset($name) {
		if (array_key_exists($name, $this->data)) {
			return isset($this->data[$name]);
		} else if (array_key_exists($name, $this->image_properties)) {
			$image_id_field_name = $this->image_properties[$name];
			return !empty($this->data[$image_id_field_name]);
		} else if (method_exists($this, $name)) {
			$tmp = $this->$name();
			return isset($tmp);
		} else {
			return false;
		}
	}
	
	//Use this if you want to add a new property (or update an existing value)
	// after instantiation has occurred. Most common use case would be if you add
	// calculated or additional data after db query, and you want output of that
	// to benefit from the `__get` method's html-escaping.
	public function set($name, $value) {
		$this->data[$name] = $value;
	}
	
	//returns the "raw" field value without any kind of escaping or link replacement,
	// or if the field doesn't exist then returns the given default value
	public function raw($name, $default = null) {
		return array_key_exists($name, $this->data) ? $this->data[$name] : $default;
	}
	//deprecated (use `raw()` instead):
	public function get($name, $default = null) {
		return $this->raw($name, $default);
	}
	
	//returns all field values as an associative array (useful for json_encode()'ing).
	public function asArray() {
		$array = array();
		foreach (array_keys($this->data) as $field) {
			$array[$field] = $this->$field; //run through our magic method so values get escaped and/or accessor methods get called
		}
		return $array;
	}
	
	//Automatically cache image thumbnails
	// so if a template calls for the same image
	// multiple times, we don't need to keep re-querying the db
	// (Most commonly this is for optimizing calls for the ->src
	// and then the ->alt text of an image).
	//To use this, the child presenter class should declare the image field
	// in its $image_properties array (key is accessor field name,
	// value is the db field name containing the file id).
	// E.g. `protected $image_properties = array('my_photo' => 'my_photo_fID');
	// Then in templates, use the accessor name followed by either ->src() or ->alt
	/* e.g. `<img src="<?=$thing->my_photo->src(200, 100, true)?>" alt="<?=$thing->my_photo->alt?>">` */
	private $cached_image_thumbnails = array();
	private function getCachedImageThumbnail($name) {
		if (!array_key_exists($name, $this->image_properties)) {
			throw new Exception('Unknown image field "' . $name . '"');
		}

		if (!array_key_exists($name, $this->cached_image_thumbnails)) {
			$fID_field = $this->image_properties[$name];
			$fID_value = $this->data[$fID_field];
			$this->cached_image_thumbnails[$name] = new CachedImageThumbnail($fID_value, $this->wch);
		}
		
		return $this->cached_image_thumbnails[$name];
	}
}

class CachedImageThumbnail {
	private $fID;
	private $cached_thumbs = array();
	private $wch;
	
	public function __construct($fID, $wch = null) {
		$this->fID = $fID;
		$this->wch = $wch ?: Loader::helper('wc_utils', 'wc_utils');
	}
	
	public function img($width = 0, $height = 0, $crop = false) {
		$key = "{$width}x{$height}" . ($crop ? '_c' : '');
		if (!array_key_exists($key, $this->cached_thumbs)) {
			$this->cached_thumbs[$key] = $this->wch->img($this->fID, $width, $height, $crop);
		}
		return $this->cached_thumbs[$key];
	}
	
	public function src($width = 0, $height = 0, $crop = false) {
		$img = $this->img($width, $height, $crop);
		return $img ? $img->src : null;
	}
	
	public function alt() {
		//If we have any thumbnails generated already,
		// arbitrarily return the first one's alt text
		// (since the alt text is the same for all thumbs).
		//Otherwise, generate an un-resized image object
		// and use that one's alt text.
		$first_thumb = reset($this->cached_thumbs);
		$img = $first_thumb ?: $this->img();
		return $img ? $img->alt : null;
	}
	
	
	public function __get($name) {
		if (method_exists($this, $name)) {
			return $this->{$name}();
		}
		
		throw new Exception('Unknown property "' . $name . '"');
	}
	
	public function __isset($name) {
		return method_exists($this, $name);
	}
}
