<?php defined('C5_EXECUTE') or die("Access Denied.");

/* version 2017-07-18 */

/**
 * Data structure for storing form errors,
 * allowing form markup to access them succinctly and flexibly
 * (to facilitate both "display all errors at the top of the page"
 * and "display errors next to the field they pertain to" styles).
 * 
 * Just "foreach" over an ErrorsCollection object to get a list of all errors,
 * or call the ->get('fieldname') method to retrieve errors for just 1 field.
 * Note that a field can have more than 1 error, so you can loop through the
 * result of ->get('fieldname') as well.
 * 
 * Or if you want, you can just "echo" the entire collection
 * (which outputs all errors with a '<br>' separator by default),
 * or "echo" the errors of just 1 field (which outputs all of that field's
 * errors with a ', ' separator by default).
 * 
 * If you prefer a more function programming style of looping through errors,
 * you can pass a callback to the ->each() method (optionally with a 2nd arg
 * for fieldname if you only want to loop through 1 field's errors).
 * 
 * Call ->has() to determine if the collection has any errors.
 * (Unfortunately you can't just do `if ($errors)`, because an object
 * is always true in PHP regardless of its internal state).
 * Or you can pass in a field name (->has('fieldname')) to check a specific field.
 * 
 * Call ->asArray() to get a plain old php array of all error messages
 * ("flat" -- not keyed to individual fields).
 */

class ErrorsCollection implements Iterator, Countable
{
	private $errors_flat = array(); //array of all error messages, regardless of field
	private $errors_grouped = array(); //array of FieldErrors objects, one each per field

	private $form_error_separator;
	private $field_error_separator;

	public function __construct($form_error_separator = '<br>', $field_error_separator = ', ')
	{
		$this->form_error_separator = $form_error_separator;
		$this->field_error_separator = $field_error_separator;
	}

	public function __set($name, $value)
	{
		$this->add($name, $value);
	}

	public function add($field, $error)
	{
		if (is_array($error) || $error instanceof Traversable) {
			foreach ($error as $e) {
				$this->add($field, $e);
			}
			return;
		}
		
		$this->errors_flat[] = $error;

		if (!array_key_exists($field, $this->errors_grouped)) {
			$this->errors_grouped[$field] = new FieldErrors($this->field_error_separator);
		}
		$this->errors_grouped[$field]->add($error);
	}

	public function __toString()
	{
		return implode($this->form_error_separator, $this->errors_flat);
	}

	public function get($field)
	{
		return array_key_exists($field, $this->errors_grouped) ? $this->errors_grouped[$field] : null;
	}

	public function has($field = null)
	{
		if ($field) {
			$errors = $this->get($field);
			return ($errors && (bool)count($errors));
		} else {
			return (bool)count($this->errors_flat);
		}
	}

	public function each($callback, $field = null)
	{
		if ($field) {
			$errors = $this->get($field);
			if ($errors) {
				foreach ($errors as $error) {
					call_user_func($callback, $error);
				}
			}			
		} else {
			foreach ($this->errors_flat as $error) {
				call_user_func($callback, $error);
			}
		}
	}

	//If a $field is provided, we return an array of error messages for that field.
	//Otherwise, we return an array of all error messages.
	//The returned array is numerically indexed, so error messages are NOT keyed to a field!
	public function asArray($field = null)
	{
		$messages = array();

		$errors = is_null($field) ? $this : $this->get($field);

		if ($errors) {
			foreach ($errors as $error) {
				$messages[] = $error;
			}
		}

		return $messages;
	}

	//Returns flat array of all error messages WITH their keys.
	//Each value of the array is a concatenated string of all of that field's errors
	// (but the array only contains keys for fields that have an error,
	// so an empty array is returned if no errors).
	public function asKeyedArray() {
		$messages = array();
		
		foreach ($this->errors_grouped as $field => $errors) {
			if (count($errors)) {
				$messages[$field] = (string)$errors;
			}
		}
		
		return $messages;
	}


	/*** Iterator Implementation ***/
	function rewind() { reset($this->errors_flat); }
	function current() { return current($this->errors_flat); }
	function key() { return key($this->errors_flat); }
	function next() { next($this->errors_flat); }
	function valid() { return (key($this->errors_flat) !== null); }

	/*** Countable Implementation ***/
	function count() { return count($this->errors_flat); }
}

//Basically an array, but we wanted the __toString functionality
// (so you can just echo a form's error if you know it can only have 1).
class FieldErrors implements Iterator, Countable {
	private $errors = array();
	private $separator;

	public function __construct($separator)
	{
		$this->separator = $separator;
	}

	public function __toString()
	{
		return implode($this->separator, $this->errors);
	}

	public function add($error)
	{
		$this->errors[] = $error;
	}

	/*** Iterator Implementation ***/
	function rewind() { reset($this->errors); }
	function current() { return current($this->errors); }
	function key() { return key($this->errors); }
	function next() { next($this->errors); }
	function valid() { return (key($this->errors) !== null); }

	/*** Countable Implementation ***/
	function count() { return count($this->errors); }
}