<?php

/* version 2017-07-03 */

/**
 * A simple wrapper for form field values and errors
 * that allows the form markup to access them succinctly
 * (without having to check for empty(), nor having to care
 * about where they came from, etc).
 * 
 * Inspired by https://github.com/jordanlev/php-form-helper
 * (but with the validation stuff removed, because our models handle that).
 */

class FormVals
{
	public $fields = array();
	public $values = array();
	public $errors = array();

	public function __construct(array $values, array $fields, ErrorsCollection $errors = null)
	{
		$this->fields = $fields;
		$this->values = array_intersect_key($values, array_flip($fields));
		$this->errors = (is_null($errors) || !$errors->has()) ? array() : $errors; //if no errors, set empty array so `if($form->errors)` works as expected
	}

	public function __get($name)
	{
		$value = isset($this->values[$name]) ? $this->values[$name] : null;
		return is_string($value) ? h($value) : $value;
	}

	public function __isset($name)
	{
		return isset($this->values[$name]);
	}

	public function raw($name)
	{
		return isset($this->values[$name]) ? $this->values[$name] : null;
	}

	//If there are any errors for the given field,
	// returns the given $message if provided,
	// or the error(s) (FieldErrors object) if no $message is provided.
	//Returns null if the field has no errors.
	public function error($field, $message = null)
	{
		$error = ($this->errors && $this->errors->has()) ? $this->errors->get($field) : null;
		
		if (!$error) {
			return null;
		} else if ($message) {
			return h($message);
		} else {
			return h($error);
		}
	}
}
