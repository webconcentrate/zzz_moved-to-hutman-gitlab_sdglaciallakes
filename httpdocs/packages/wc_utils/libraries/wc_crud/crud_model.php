<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/* version 2017-11-03 */

Loader::library('wc_crud/form_vals', 'wc_utils');
Loader::library('wc_crud/errors_collection', 'wc_utils');

/**
 * Simple ActiveRecord-esque base class.
 * 
 * Use this ONLY for operations on a single record.
 * For bulk data operations and multi-record querying
 * of read-only data, use something else better-suited
 * to those purposes (like custom SQL queries).
 * 
 * To define a class for a specific table, make a new class
 * that extends CRUDModel (or SortableCRUDModel)
 * and set its $table property to the table name.
 * We assume an integer primary key id field named 'id'
 * (but you can override this via the $idname property).
 * We will automatically determine field names by examining
 * the table schema (but you can override this by explicitly
 * setting the $fields array in your sub-class).
 * 
 * When you instantiate a table's model class,
 * you can pass various things into the constructor
 * to determine how data is loaded:
 *  -Pass in nothing or null to get an empty object (no data).
 *  -Pass in an integer to load an existing record from the db having that id.
 *  -Pass in an array or object of data to explicitly set values (no db query) -- e.g. POSTed forms.
 *  
 *   NOTES ABOUT PASSING IN AN ARRAY/OBJECT OF DATA:
 *   1) We whitelist based on the defined fields,
 *      so only items having keys/properties that exist in our $fields list
 *      will be taken from the passed-in array/object.
 *   2) We do NOT set fields that aren't in the given array/object,
 *      which means you could do a partial update of a record
 *      by only passing in partial data (e.g. you only want to update
 *      certain fields of an existing db record but keep other fields
 *      as their current values, without having to load them from the db first).
 * 
 * After you have instantiated the class and loaded data,
 * you can do the following things:
 *  -get and set specific fields (e.g. `$object->title = 'Whatevs'` or `$x = $object->title`)
 *  -validate() loaded data (after which you can call ->getErrors() or ->hasErrors())
 *  -save() to persist loaded data to the database
 *   (either INSERT or UPDATE, depending on existence of 'id' in the loaded data).
 *  -delete() to delete the loaded record from the database (if 'id' exists)
 *  
 * If you sub-classes "SortableCRUDModel", it is assumed that a `display_order`
 * integer field exists in the table (you can override that field name
 * via the $order_by property). Optionally, you can define the $segment_by
 * property to be the name of an integer foreign key id field that sorting
 * is restricted to (e.g. child records are only sorted within their parent).
 * After you have instantiated a SortableCRUDModel, it will automatically
 * set the display order on newly-inserted records to be 1 higher than the max
 * existing record. You can also bulk-set sort orders via the `sort` static method.
 * 
 * This file also contains three additional classes that encapsulate logic for working with
 * many records of a table at once:
 *  -MultiCRUDContainer (for "repeating item lists" where user edits all table records in one fell swoop)
 *  -ChildrenCRUDContainer (for "repeating item lists" where user can add as many or as few children as they want to the parent)
 *  -AssociationCRUDContainer (for "checkbox lists" where user is shown all available options and checks which ones to associate with)
 *  -SortedAssociationCRUDContainer (same as above, but saves non-destructively to maintain existing display orders, and generates new display orders for insertions)
 * These are useful when you have an edit form that interacts with a parent entity
 * as well as all of its child entities and/or associations (one-to-many relationships
 * and many-to-many relationships).
 * The benefit to using these is that your controller can just interact with one class
 * for an entire set of records without having to do its own looping, cascading save logic, etc.
 * See notes below (on the classes themselves) for more details and usage instructions.
 */

abstract class CRUDModel {
	
	protected $table = '';
	protected $fields = array();
	protected $idname = 'id'; //primary key id field name
	
	protected $values = array();
	protected $errors;
	
	protected $db = null;
	
/*** Initialization / Data Population ***/

	public function __construct($id_or_data = null) {
		if (empty($this->table)) {
			throw new Exception('CRUDModel class error: table not set!');
		}
		
		$this->db = Loader::db();
		
		if (empty($this->fields)) {
			$this->populateFieldsFromSchema();
		}
		
		if (is_array($id_or_data)) {
			$this->populateValuesFromArray($id_or_data);
		} else if ($id_or_data instanceof Presenter) {
			$this->populateValuesFromArray($id_or_data->asArray());
		} else if (is_object($id_or_data)) {
			$this->populateValuesFromArray(get_object_vars($id_or_data));
		} else if ((int)$id_or_data) {
			$this->populateValuesById($id_or_data);
		}
		
		$this->resetErrors();
	}
	
	protected function populateFieldsFromSchema() {
		$cols = $this->db->MetaColumns($this->table);
		$this->fields = array_values(array_map(function($col) { return $col->name; }, $cols));
		if (!in_array($this->idname, $this->fields)) {
			$this->fields[] = $this->idname;
		}
	}
	
	protected function populateValuesById($id) {
		$sql = "SELECT * FROM {$this->table} WHERE {$this->idname} = ? LIMIT 1";
		$vals = array((int)$id);
		$record = $this->db->GetRow($sql, $vals);
		$this->populateValuesFromArray($record, false); //don't trim() values from db... if surrounding whitespace made it in there we assume it was intentional
	}
	
	protected function populateValuesFromArray($data, $trim_values = true) {
		if (empty($data)) {
			$this->values = array();
		} else {
			//only grab data for defined fields
			$this->values = array_intersect_key($data, array_flip($this->fields));
			
			//convert empty strings to null
			foreach ($this->values as $field => $value) {
				$value = ($trim_values && is_string($value)) ? trim($value) : $value; //avoid accidental conversions of false-y (zero) numeric/bool values to empty strings!
				$this->values[$field] = ($value === '') ? null : $value; //don't use empty() because we don't want to nullify 0's
			}
			
			//explicitly cast "id" to int (if it exists) to avoid potential security issues
			$this->values[$this->idname] = array_key_exists($this->idname, $data) ? (int)$data[$this->idname] : null;
			
			//NOTE that we intentionally do *NOT* set empty values for $fields that are missing from $data!
		}
	}
	
	
/*** Data Access ***/

	public function __get($name) {
		return array_key_exists($name, $this->values) ? $this->values[$name] : null;
	}
	public function __isset($name) {
		return array_key_exists($name, $this->values);
	}
	public function __set($name, $value) {
		if (in_array($name, $this->fields)) {
			$this->values[$name] = $value;
		}
	}

	//Returns a FormVals object with the fields, values, and errors.
	//Use this when outputting form markup (that's the purpose of the FormVals class).
	public function asFormVals() {
		return new FormVals($this->values, $this->fields, $this->errors);
	}
	
	//Returns a plain old php array of this object's fields and values
	// (array keys are field names, array values are field values).
	public function asArray() {
		return $this->values;
	}
	
	public function exists() {
		return array_key_exists($this->idname, $this->values) ? (bool)$this->values[$this->idname] : false;
	}
	
	public function getTable() {
		return $this->table;
	}

	public function getIdName() {
		return $this->idname;
	}

/*** Validation ***/
	
	public function validate() {
		//This should be overridden in subclasses,
		// calling $this->errors->add('FIELDNAME', 'message') for each error
		// (then `return parent::validate()`).
		//
		//Note that in order to support "partial updates"
		// (where only some fields are provided in the UPDATE statement),
		// you'll want to call `if ($this->shouldValidate('the_field_name'))`
		// on each field before you validate it).
		
		return $this->isValid();
	}
	
	protected function shouldValidate($field) {
 		return (!$this->exists() || array_key_exists($field, $this->values));
	}
	
	//Utility function that can be called in child classes' validation functions.
	//Pass in an array of fields (key is field name, value is label for error messages),
	// and we'll validate them for things we can glean from the table schema:
	// * "not null" fields will be required
	// * varchars will have their character length checked
	// * floats must be numeric
	// * ints must be numeric and whole numbers
	// * unsigned ints must be 0 or higher
	// * not-null unsigned ints must be 1 or higher (assumption is that not-null unsigned ints are foreign keys)
	//
	//This function adds errors to $this->error (nothing is returned by the function).
	protected function autoValidate($fields_and_labels) {
		$cols = $this->db->MetaColumns($this->table);
		foreach ($fields_and_labels as $field => $label) {
			if ($this->shouldValidate($field)) {
				$col_key = strtoupper($field); //ADODB capitalizes field names in the MetaColumns() array for some reason
				if (array_key_exists($col_key, $cols)) {
					$col = $cols[$col_key];
					$value = $this->$field;
					$type = $col->type;

					//Be careful -- can't just check empty() because that would erroneously catch legitemate "0" values!
					$is_empty = ((is_array($value) && empty($value)) || $value === '' || $value === null || $value === false);

					if ($is_empty && $col->not_null) {
						$this->errors->add($field, "{$label} is required.");
					}
					
					//Perform additional validations *if* a value was provided...
					if (!$is_empty) {
						if ($type == 'varchar' && (strlen($value) > $col->max_length)) {
							$this->errors->add($field, "{$label} cannot exceed {$col->max_length} characters in length.");
						}
						
						if (($type == 'float' || $type == 'decimal' ) && !is_numeric($value)) {
							$this->errors->add($field, "{$label} must be a number.");
						}
						
						if ($type == 'int') {
							//Don't use ctype_digit because it fails if $value is an actual integer (not a string), and also fails on negative numbers.
							$int_value = filter_var($value, FILTER_VALIDATE_INT); //filter_var returns integer version of string, or FALSE if non-integer
							if ($int_value === false) {
								$this->errors->add($field, "{$label} must be a whole number.");
							} else if ($col->unsigned) {
								if ($col->not_null && ($int_value < 1)) {
									//Assume required unsigned ints are foreign key id's, and hence have a dropdown list for selections
									// (and since our convention is to use 0 as "empty" value, this means the user made no selection)
									$article = in_array(strtolower(substr($label, 0, 1)), array('a', 'e', 'i', 'o', 'u')) ? 'an' : 'a';
									$this->errors->add($field, "You must choose {$article} {$label}.");
								} else if ($int_value < 0) {
									$this->errors->add($field, "{$label} must be a positive number.");
								}
							}
						}
					}
				}
			}
		}
	}
	
	//Utility function that can be called by child classes (in their validation functions).
	//Pass in a field name and we will check the object's value against the existing db records
	// (excluding the existing record itself if it already exists, so it doesn't come up as its own dup).
	//If 2nd arg is true AND this object has a `$segment_by` property defined (e.g. it's a SortableCRUDModel class)
	// AND there is a non-empty value set for the segment_by field, then we will restirct
	// the lookup to only records sharing the same segment id value as this object.
	//Returns true if the field value is empty or unique
	//Returns false if the field value is not empty AND exists in another db record.
	protected function validateUniqueness($field, $limit_to_segment_id = false) {
		if (empty($this->$field)) {
			return true;
		}
		
		$sql = "SELECT COUNT(*) FROM {$this->table} WHERE {$field} = ?";
		$vals = array($this->$field);
		
		if ($limit_to_segment_id && property_exists($this, 'segment_by')) {
			$segment_id = (int)$this->values[$this->segment_by];
			if ($segment_id) {
				$sql .= " AND {$this->segment_by} = ?";
				$vals[] = $segment_id;
			}
		}
		
		//if this is an UPDATE (as opposed to an INSERT), ignore this record's existing value
		if ($this->exists()) {
			$sql .= " AND {$this->idname} <> ?";
			$vals[] = (int)$this->values[$this->idname];
		}
	
		$count = $this->db->GetOne($sql, $vals);
		return !$count;
	}
	
	//Utility function that can be called by child classes (in their validation functions).
	//Returns true if field value is empty or if it's a valid url slug (letters, numbers, underscore and dash only).
	//Returns false if field value is not empty AND is not a valid url slug.
	protected function validateUrlSlugFormat($field, $allow_uppercase_letters = false) {
		if (empty($this->$field)) {
			return true;
		}
		
		$regex = $allow_uppercase_letters ? '/[^a-zA-Z0-9_-]/' : '/[^a-z0-9_-]/';
		return !preg_match($regex, $this->$field);
	}
	
	//Utility function that can be called by child classes (in their validation functions).
	//Returns true if field value is empty or if it's a valid email address format.
	//Returns false if field value is not empty AND is not a valid email address format.
	protected function validateEmailFormat($field) {
		if (empty($this->$field)) {
			return true;
		}
		
		return filter_var($this->$field, FILTER_VALIDATE_EMAIL); //returns the email address if it's valid, which will always be truthy
	}

	//Returns the ErrorsCollection object
	// (which is populated when validate() is called,
	// so there is no point in calling this before validation). 
	public function getErrors() {
		return $this->errors;
	}
	
	//In case something external to the object wants to set an error on it
	// (this isn't normally used, but is here if you need it).
	public function setError($field, $error) {
		$this->errors->add($field, $error);
	}
	
	//Returns true/false if any errors exist
	// (only makes sense after validate() has been called).
	public function hasErrors() {
		return $this->errors->has();
	}
	public function isValid() {
		return !$this->hasErrors();
	}
	
	//Call this if you want to re-validate after already validating
	// (to avoid doubled-up error messages).
	public function resetErrors() {
		$this->errors = new ErrorsCollection;
	}

/*** DB Operations (Save/Delete) ***/

	//Attempts to save the object's data to the db.
	//If the object has a non-empty id, then we UPDATE existing record,
	// otherwise we INSERT a new record.
	//
	//Returns false if validation fails.
	//Otherwise returns inserted/updated record id.
	public function save() {
		$this->resetErrors(); //in case validate() was called earlier... prevents doubling-up of error messages

		if (!$this->validate()) {
			return false;
		} else if ($this->exists()) {
			$id = (int)$this->values[$this->idname];
			$this->db->AutoExecute($this->table, $this->values, 'UPDATE', "{$this->idname}={$id}");
			return $id;
		} else {
			$this->values[$this->idname] = null; //in case id was set to 0 at some point
			$this->db->AutoExecute($this->table, $this->values, 'INSERT');
			$id = (int)$this->db->Insert_ID();
			$this->values[$this->idname] = $id;
			return $id;
		}
	}

	//Deletes the current record from the database.
	//Note that we don't clear any data already loaded into the object
	// (so you can still access this object's field values after calling delete()).
	//Returns true if we ran the deletion (although we don't know if the record existed in the first place or not),
	// false if we didn't run the deletion command.
	public function delete() {
		if (!$this->exists()) {
			return false;
		}
		
		$sql = "DELETE FROM {$this->table} WHERE {$this->idname} = ?";
		$vals = array((int)$this->values[$this->idname]);
		$this->db->Execute($sql, $vals);
		return true;
	}

}

//Extends the basic crud model with functionality for managing a "display_order" field.
abstract class SortableCRUDModel extends CRUDModel {
	
	protected $order_by = 'display_order'; //display order field name (must be an INT)
	protected $segment_by = ''; //optional field name of a foreign key that we'll segment display orders by
	
	public function save() {
		if (!$this->exists()) {
			//Add new records at the end of the display order
			$has_segment = ($this->segment_by && !empty($this->values[$this->segment_by]));
			$segment_id = $has_segment ? $this->values[$this->segment_by] : null;
			$this->values[$this->order_by] = $this->maxDisplayOrder($segment_id) + 1;
		}
		
		return parent::save();
	}
	
	private function maxDisplayOrder($segment_id = null) {
		$sql = "SELECT MAX({$this->order_by}) FROM {$this->table}";
		$sql .= $segment_id ? " WHERE {$this->segment_by} = " . intval($segment_id) : '';
		$max = $this->db->GetOne($sql);
		return intval($max);
	}
	
	//convenience method for calling setDisplayOrders without instantiating the class
	public static function sort($ids, $segment_id = null) {
		$class = get_called_class();
		$model = new $class;
		$model->setDisplayOrders($ids, $segment_id);
	}
	
	//Pass in an array of id's, in the order you want those records to be.
	//Optionally pass in the "segment id" (if sorting only a subset of the table).
	//The given $ids array should contain ALL id's for the table (or segment)
	// -- records whose id's are not in the array will be moved to the end
	// of the display order for that table (or segment).
	public function setDisplayOrders($ids, $segment_id = null) {
		$sql = "UPDATE {$this->table} SET {$this->order_by} = 0";
		$sql .= $segment_id ? " WHERE {$this->segment_by} = " . intval($segment_id) : '';
		$this->db->Execute($sql);
		
		$next_display_order = $this->setPartialDisplayOrder($ids, 1);
		
		//Now move all the ones we didn't have an id for to the end
		$sql = "SELECT {$this->idname} FROM {$this->table} WHERE {$this->order_by} = 0";
		$sql .= $segment_id ? " AND {$this->segment_by} = " . intval($segment_id) : '';
		$sql .= " ORDER BY {$this->idname}";
		$ids = $this->db->GetCol($sql);
		$this->setPartialDisplayOrder($ids, $next_display_order);
	}
	private function setPartialDisplayOrder($ids, $starting_display_order) {
		$current_display_order = $starting_display_order;
		$sql = "UPDATE {$this->table} SET {$this->order_by} = ? WHERE {$this->idname} = ?";
		$stmt = $this->db->Prepare($sql);
		foreach ($ids as $id) {
			$vals = array($current_display_order, (int)$id);
			$this->db->Execute($stmt, $vals);
			$current_display_order++;
		}
		return $current_display_order;
	}
}

/**
 * Encapsulates an entire set of CRUDModel objects so they can all be loaded/validated/saved at once.
 * This is useful when you have an edit form that deals with all records at once.
 * IMPORTANT NOTE: You should *only* use this class when you have a form that submits
 *                 data for *ALL* records of a table!
 *                 Because when save() is called, we delete existing records whose ids
 *                 are not included in the set of data (because we assume that means the user
 *                 explicitly chose to delete those records)!
 *
 * To set up this class, define a sub-class with the following properties set:
 *  -$crud_model_class_name: name of the CRUDModel class that represents each record
 *  -$display_order_field_name (OPTIONAL): If the records have an explicit display order that is set by the user,
 *                                         put the display_order field name here and we will populate it
 *                                         with sequential integers whenever saving occurs
 *                                         NOTE: The display order will be whatever order the records were provided in
 *                                         (probably the order they were POST'ed by the form, which is what we want
 *                                         for user drag-and-drop'able repeating item lists).
 *                                         NOTE: When using this, the record's CRUD Model class should be a plain ol' CRUDModel class
 *                                         (NOT a SortableCRUDModel class!)... because the SortableCRUDModel class
 *                                         runs a query upon each save to determine max sort order for new records
 *                                         which is very inefficient since we're saving a bunch of records at once
 *                                         and we know what order we want them to be in!
 *                                         
 * In addition to settings those properties, you must also define the following method:
 *  -populateAllObjects(): This function must set $this->crud_objects to an array of CRUDModel objects
 *                         representing all records in the table.
 * 
 * To use this, instantiate your sub-class and optionally pass in an array of data arrays (e.g. from POST)
 * (if null or a non-array is passed in, then we load all records from the database).
 * Then call validate() to validate all loaded records, call save() to save all loaded records,
 * call hasErrors() / isValid() to check if there is at least one validation error (or not) in any of the objects,
 * and call asCRUDObjects(), asArrays(), and asArraysWithErrors() to retrieve the objects themselves in various formats.
 */
abstract class MultiCRUDContainer {
	
	protected $crud_model_class_name;
	protected $primary_key_id_field_name = 'id';
	protected $display_order_field_name = null;
	
	protected $crud_objects = array();
	
	private $has_errors = false;
	
	public function __construct($records = null) {
		if (empty($this->crud_model_class_name)) {
			throw new Exception('MultiCRUDContainer class error: crud_model_class_name not set!');
		}
		
		if (is_array($records)) {
			foreach ($records as $record) {
				$this->crud_objects[] = new $this->crud_model_class_name($record);
			}
		} else {
			$this->populateAllObjects();
		}
	}
	
	abstract protected function populateAllObjects();
	
	public function validate() {
		foreach ($this->crud_objects as $crud_object) {
			$crud_object->validate();
			if ($crud_object->hasErrors()) {
				$this->has_errors = true;
				//continue looping through all objects so they each have their errors available for display if needed
			}
		}

		return !$this->has_errors;
	}
	
	//Inserts/Updates/Deletes all recordsas needed per our loaded data
	// (presuming that the loaded data comprises the full set of records you would like to exist
	// in the database table, so we'll delete existing db records that aren't in our loaded data,
	// insert new records for loaded data that doesn't yet exist in the db, and update records
	// that exist in both the db and our loaded data).
	public function save() {
		if (!$this->validate()) {
			return false;
		}
		
		$this->deleteMissingRecords();
		
		foreach ($this->crud_objects as $i => $crud_object) {
			if (!empty($this->display_order_field_name)) {
				$crud_object->{$this->display_order_field_name} = $i + 1;
			}
			$crud_object->save();
		}
		
		return true;
	}
	private function deleteMissingRecords() {
		list($table_name, $id_name) = $this->getTableAndIdName();
		
		//DELETE existing records that aren't represented in our loaded data
		$sql = "DELETE FROM {$table_name}";
		
		$existing_record_ids = array_filter(array_map(function ($crud_object) use ($id_name) { return (int)$crud_object->{$id_name}; }, $this->crud_objects)); //grab all non-empty ids (calling array_filter without a callback removes false-y items by default)
		if ($existing_record_ids) {
			$ids = implode(',', $existing_record_ids);
			$sql .= " WHERE {$id_name} NOT IN ({$ids})";
		}
		
		Loader::db()->Execute($sql, $vals);
	}
	private function getTableAndIdName() {
		//Can't rely on loaded data because there might not be any!
		$temp_obj = new $this->crud_model_class_name;
		$table_name = $temp_obj->getTable();
		$id_name = $temp_obj->getIdName();
		unset($temp_obj);
		return array($table_name, $id_name);
	}
	
	public function hasErrors() {
		return $this->has_errors;
	}
	
	public function isValid() {
		return !$this->has_errors;
	}
	
	public function asCRUDObjects() {
		return $this->crud_objects;
	}
	
	public function asArrays() {
		return array_map(function ($object) {
			return $object->asArray();
		}, $this->crud_objects);
	}

	//Converts the array of CRUD objects to an array of arrays,
	// each containing the object's data AND its error messages
	// (errors are assigned to the array key 'errors').
	//This is useful for controllers that need to send all record data
	// to the view as JSON (along with any validation errors).
	public function asArraysWithErrors() {
		return array_map(function ($object) {
			return array_merge($object->asArray(), array('errors' => $object->getErrors()->asArray()));
		}, $this->crud_objects);
	}
	
}

/**
 * Encapsulates an entire set of CRUDModel objects -- all of the children of one parent
 * (that is, the "many" of a one-to-many or many-to-many relationship).
 * This is useful when you have an edit form that deals with both a parent record
 * and all children records (e.g. a "repeating item" list).
 * IMPORTANT NOTE: You should *only* use this class when you have a form that submits
 *                 *ALL* data for both the parent and all children!
 *                 Because when save() is called, we delete existing child records whose ids
 *                 are not included in the set of children (because we assume that means the user
 *                 explicitly chose to delete those children)!
 *
 * To set up this class, define a sub-class with the following properties set:
 *  -$crud_model_class_name: name of the CRUDModel class that represents each child record
 *  -$parent_id_field_name: field name that is a foreign key reference to the parent in the child table
 *  -$primary_key_id_field_name (OPTIONAL): If the child records have primary key id's (as opposed to compound keys
 *                                          comprised of several foreign keys), set this property to that field name.
 *                                          Doing so allows us to maintain referential integrity by not deleting
 *                                          and then re-inserting existing child records whenever the parent is saved.
 *  -$display_order_field_name (OPTIONAL): If the children have an explicit display order that is set by the user,
 *                                         and that display order is segmented within the parent records,
 *                                         put the display_order field name here and we will populate it
 *                                         with sequential integers whenever saving occurs
 *                                         NOTE: The display order will be whatever order the records were provided in
 *                                         (probably the order they were POST'ed by the form, which is what we want
 *                                         for user drag-and-drop'able repeating item lists).
 *                                         NOTE: When using this, the child class should be a plain ol' CRUDModel class
 *                                         (NOT a SortableCRUDModel class!)... because the SortableCRUDModel class
 *                                         runs a query upon each save to determine max sort order for new records
 *                                         which is very inefficient since we're saving a bunch of records at once
 *                                         and we know what order we want them to be in!
 *                                         
 * In addition to settings those properties, you must also define the following method:
 *  -populateObjectsByParentId($parent_id): This function must set $this->crud_objects to an array of CRUDModel objects
 *                                          representing all children of the given parent.
 * 
 * Last but not least, the `validate()` method of your CRUDModel class should accept a boolean argument
 *  `$ignore_parent_id = false`. If true is passed in, then your function should NOT validate the existence
 *  of a parent id on the record! The reason we must do this is to avoid a catch-22 when saving new parent records
 *  (because we don't want to save parent without first validating children, but we can't validate children without
 *  the parent's id, which might not exist yet if we are inserting a new parent record).
 * 
 * To use this, instantiate your sub-class and pass in a parent id (in which case we load children from the db)
 * or array of data arrays (e.g. from POST). Then call validate() to validate all loaded child records,
 * call save() (passing in the $parent_id as a function arg) to save all loaded child records,
 * call hasErrors() / isValid() to check if there is at least one validation error (or not) in any of the child objects,
 * and call asCRUDObjects(), asArrays(), and asArraysWithErrors() to retrieve the child objects themselves in various formats.
 */
abstract class ChildrenCRUDContainer {
	
	protected $crud_model_class_name;
	protected $parent_id_field_name;
	protected $primary_key_id_field_name = null;
	protected $display_order_field_name = null;
	
	protected $crud_objects = array();
	
	private $has_errors = false;
	
	//$is_duplicate indicates if the user is duplicating an existing parent record,
	// in which case we will nullify the child record id's
	// (so when the duplicate of the parent is saved, new child records will be created
	// for the new parent instead of the existing parent's children being UPDATEd).
	// Note that $is_duplicate only matters when a $primary_key_id_field_name is defined.
	public function __construct($parent_id_or_child_records = null, $is_duplicate = false) {
		if (empty($this->crud_model_class_name)) {
			throw new Exception('ChildrenCRUDContainer class error: crud_model_class_name not set!');
		}
		if (empty($this->parent_id_field_name)) {
			throw new Exception('ChildrenCRUDContainer class error: parent_id_field_name not set!');
		}
		
		if (is_array($parent_id_or_child_records)) {
			foreach ($parent_id_or_child_records as $data) {
				$this->crud_objects[] = new $this->crud_model_class_name($data);
			}
		} else if ((int)$parent_id_or_child_records) {
			$this->populateObjectsByParentId($parent_id_or_child_records);
			if ($is_duplicate && $this->primary_key_id_field_name) {
				foreach ($this->crud_objects as $crud_object) {
					$crud_object->{$this->primary_key_id_field_name} = null;
				}
			}
		}
	}
	
	abstract protected function populateObjectsByParentId($parent_id);
	
	public function validate() {
		foreach ($this->crud_objects as $crud_object) {
			$crud_object->validate(true); //pass true to ignore the parent id during validation -- this avoids a catch-22 when saving new parent records (we don't want to save parent without first validating children, but we can't validate children without the parent's id)
			if ($crud_object->hasErrors()) {
				$this->has_errors = true;
				//continue looping through all objects so they each have their errors available for display if needed
			}
		}

		return !$this->has_errors;
	}
	
	//Inserts/Updates/Deletes child records of the parent as needed per our loaded data
	// (presuming that the loaded data comprises the full set of child records you would like to exist
	// in the database, so we'll delete existing db records that aren't in our loaded data,
	// insert new records for loaded data that doesn't yet exist in the db, and update records
	// that exist in both the db and our loaded data).
	//IMPORTANT: We don't validate! We assume you've already validated as a separate step
	//           (because you need to validate all children before saving the parent,
	//           and you need to save the parent before saving the children [because we need its id]).
	public function save($parent_id) {
		$this->deleteChildren($parent_id);
		
		foreach ($this->crud_objects as $i => $crud_object) {
			$crud_object->{$this->parent_id_field_name} = $parent_id;
			if (!empty($this->display_order_field_name)) {
				$crud_object->{$this->display_order_field_name} = $i + 1;
			}
			$crud_object->save();
		}
	}
	private function deleteChildren($parent_id) {
		//DELETE existing records that aren't represented in our loaded data
		// (or delete all records if we have no primary key id field)...
		$sql = 'DELETE FROM ' . $this->getTableName() . ' WHERE ' . $this->parent_id_field_name . ' = ?';
		$vals = array((int)$parent_id);
		
		$idname = $this->primary_key_id_field_name;
		if ($idname) {
			$existing_record_ids = array_filter(array_map(function ($crud_object) use ($idname) { return (int)$crud_object->{$idname}; }, $this->crud_objects)); //grab all non-empty ids (calling array_filter without a callback removes false-y items by default)
			if ($existing_record_ids) {
				$sql .= ' AND ' . $idname . ' NOT IN (' . implode(',', $existing_record_ids) . ')';
			}
		}
		
		Loader::db()->Execute($sql, $vals);
	}
	private function getTableName() {
		//Can't rely on loaded data because there might not be any!
		$temp_obj = new $this->crud_model_class_name;
		$table_name = $temp_obj->getTable();
		unset($temp_obj);
		return $table_name;
	}
	
	public function hasErrors() {
		return $this->has_errors;
	}
	
	public function isValid() {
		return !$this->has_errors;
	}
	
	public function asCRUDObjects() {
		return $this->crud_objects;
	}
	
	public function asArrays() {
		return array_map(function ($object) {
			return $object->asArray();
		}, $this->crud_objects);
	}

	//Converts the array of CRUD objects to an array of arrays,
	// each containing the object's data AND its error messages
	// (errors are assigned to the array key 'errors').
	//This is useful for controllers that need to send all child record data
	// to the view as JSON (along with any validation errors).
	public function asArraysWithErrors() {
		return array_map(function ($object) {
			return array_merge($object->asArray(), array('errors' => $object->getErrors()->asArray()));
		}, $this->crud_objects);
	}
	
}

/**
 * Encapsulates a many-to-many association where the entire list of available associations
 * is shown to the user as a checkbox list and the user chooses which ones they want
 * to associate the parent with.
 * 
 * Assumes a table with only 2 fields (the parent id and the associated id)... all other field data will be deleted upon save!
 * (If you also have a 'display_order' field, use the SortedAssociationCRUDContainer class instead.)
 * 
 * In your sub-class, declare the $table, $parent_id_field_name, and $associated_id_field_name
 * for the junction table, then define this method:
 *  -populateAllAvailable(): Set $this->all_available to an associative array of all available choices
 *                           The array key should be the id, and the array value should be a label
 *                           for the checkbox list (could be one field or the concatenation of several fields).
 * 
 * To use the class, instantiate it and pass in a parent id (to load associations from the db),
 * or pass in an array of chosen associated id's, or pass in nothing.
 * Call save() and pass in the parent id to delete all existing associations for the parent
 * and insert new associations from our loaded data.
 * Call asObjects() to retrieve an array of StdClass objects representing the currently-loaded data.
 * Each object in the array contains the following 3 properties:
 *  -id: associated record id
 *  -label: display label for the record (as per populateAllAvailable())
 *  -has: boolean, denotes if the record is currently associated with the parent or not
 */
abstract class AssociationCRUDContainer {
	protected $table;
	protected $parent_id_field_name;
	protected $associated_id_field_name;
	
	protected $all_available = array(); //list of all possible associations. key is associated record id, value is a label for the associated record (as per populateAllAvailable())
	protected $associated_ids = array(); //ids of records that are associated with the parent (i.e. those that were checked by the user, or those that have existing association records in the db)
	
	public function __construct($parent_id_or_associated_ids = null) {
		if (empty($this->table)) {
			throw new Exception('AssociationCRUDContainer class error: table not set!');
		}
		if (empty($this->parent_id_field_name)) {
			throw new Exception('AssociationCRUDContainer class error: parent_id_field_name not set!');
		}
		if (empty($this->associated_id_field_name)) {
			throw new Exception('AssociationCRUDContainer class error: associated_id_field_name not set!');
		}
		
		$this->populateAllAvailable();
		
		if (is_array($parent_id_or_associated_ids)) {
			$this->associated_ids = $parent_id_or_associated_ids;
		} else if ((int)$parent_id_or_associated_ids) {
			$this->populateAssociatedIdsByParentId($parent_id_or_associated_ids);
		}
	}
	
	abstract protected function populateAllAvailable();
	
	public function populateAssociatedIdsByParentId($parent_id) {
		$sql = "SELECT {$this->associated_id_field_name} FROM {$this->table} WHERE {$this->parent_id_field_name} = ?";
		$vals = array((int)$parent_id);
		$this->associated_ids = Loader::db()->GetCol($sql, $vals);
	}
	
	public function save($parent_id) {
		$db = Loader::db();
		
		$sql = "DELETE FROM {$this->table} WHERE {$this->parent_id_field_name} = ?";
		$vals = array((int)$parent_id);
		$db->Execute($sql, $vals);
	
		$stmt = $db->Prepare("INSERT INTO {$this->table} ({$this->parent_id_field_name}, {$this->associated_id_field_name}) VALUES (?, ?)");
		foreach ($this->associated_ids as $associated_id) {
			$vals = array((int)$parent_id, (int)$associated_id);
			$db->Execute($stmt, $vals);
		}
	}
	
	public function asObjects() {
		$objects = array();
		foreach ($this->all_available as $id => $label) {
			$objects[] = (object)array(
				'id' => $id,
				'label' => $label,
				'has' => in_array($id, $this->associated_ids),
			);
		}
		return $objects;
	}
	
	public function hasAny() {
		return (bool)count($this->associated_ids);
	}
}

//Same functionality and use situation as AssociationCRUDContainer,
// but assumes a 'display_order' field exists in the junction table
// so we take a more careful (and less efficient) approach to inserting/updating records
// in the junction table (instead of deleting all and then inserting new,
// we leave existing records in place, and when adding new records we assign a new display_order
// based on the max existing order for the parent entity within the associated entity
abstract class SortedAssociationCRUDContainer extends AssociationCRUDContainer {
	
	protected $display_order_field_name = 'display_order';
	
	public function save($parent_id) {
		$db = Loader::db();
		
		//get the current max display order for each association
		// (so we know how to insert new associations at the end of the display order)
		$sql = "SELECT {$this->associated_id_field_name}, MAX({$this->display_order_field_name}) AS max_display_order FROM {$this->table} GROUP BY {$this->associated_id_field_name}";
		$records = $db->GetArray($sql);
		$max_display_orders_per_associated_id = array();
		foreach ($records as $r) {
			$max_display_orders_per_associated_id[$r[$this->associated_id_field_name]] = (int)$r['max_display_order'];
		}
		
		//get the associated ids that this parent is currently assigned to
		// (so we know not to delete the association if it's still chosen by user
		//  -- because we want to maintain its existing display_order)
		$sql = "SELECT {$this->associated_id_field_name} FROM {$this->table} WHERE {$this->parent_id_field_name} = ?";
		$vals = array((int)$parent_id);
		$existing_associated_ids = $db->GetCol($sql, $vals);
		
		//delete associations that are no longer chosen...
		$delete_associated_ids = array_diff($existing_associated_ids, $this->associated_ids);
		if ($delete_associated_ids) {
			$delete_associated_ids = array_map(function($id) { return (int)$id; }, $delete_associated_ids); //we will be directly inserting these ids into a SQL string so ensure they aren't going to mess anything up
			$sql = "DELETE FROM {$this->table}"
			     . " WHERE {$this->parent_id_field_name} = ?"
			     . " AND {$this->associated_id_field_name} IN (" . implode(',', $delete_associated_ids) . ")";
			$vals = array((int)$parent_id);
			$db->Execute($sql, $vals);
		}
		
		//insert new associations that didn't already exist...
		$insert_associated_ids = array_diff($this->associated_ids, $existing_associated_ids);
		$stmt = $db->Prepare("INSERT INTO {$this->table} ({$this->parent_id_field_name}, {$this->associated_id_field_name}, {$this->display_order_field_name}) VALUES (?, ?, ?)");
		foreach ($insert_associated_ids as $associated_id) {
			$max_display_order = array_key_exists($associated_id, $max_display_orders_per_associated_id) ? $max_display_orders_per_associated_id[$associated_id] : 0;
			$new_display_order = $max_display_order + 1;
			$vals = array((int)$parent_id, (int)$associated_id, (int)$new_display_order);
			$db->Execute($stmt, $vals);
		}
	}
	
}