<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/* version 2017-12-12 */

/**
 * A wrapper around C5's single_page Controller class
 * that improves some built-in functionality
 * and provides some additional convenience methods.
 */

class BetterController extends Controller {
	
	public function __construct() {
		parent::__construct();
		$this->initCSRFToken();
		$this->initFlash();
		$this->initIsAjax();
	}
	
	private function initCSRFToken() {
		$token = Loader::helper('validation/token');
		if ($this->isPost() && !$token->validate()) {
			die($token->getErrorMessage());
		}
		$this->set('token', $token->output('', true));
	}
	
	private function initFlash() {
		$types = array('message', 'success', 'error');
		foreach ($types as $type) {
			$key = "flash_{$type}";
			if (!empty($_SESSION[$key])) {
				$this->set($type, $_SESSION[$key]); //C5 automagically displays 'message', 'success', and 'error' for us in dashboard views
				unset($_SESSION[$key]);
			}
		}
	}
	
	private function initIsAjax() {
		$this->set('is_ajax', $this->isAjax());
	}
	
	//Uses session to set 'message', 'success', or 'error' variable next time the page is loaded
	protected function flash($text, $type = 'message') {
		$key = "flash_{$type}";
		$_SESSION[$key] = $text;
	}
	
	protected function isAjax() {
		return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
	}

	protected function wysiwygDbToEdit($html) {
		return Loader::helper('content')->translateFromEditMode($html);
	}
	protected function wysiwygDbToView($html) {
		return Loader::helper('content')->translateFrom($html);
	}
	protected function wysiwygEditToDb($html) {
		return Loader::helper('content')->translateTo($html);
	}
	
	protected function cookieCheck() {
		//Historical note: functionality used to be here in this class,
		// but on 2016-07-30 we moved it into the helper.
		// Keeping function definition here for backwards compatibility.
		return Loader::helper('wc_utils', 'wc_utils')->cookieCheck();
	}
	
	//Redirect to an action in this controller
	public function redirect($action) { //must be public because that's how it is in the base C5 Controller class we're extending
		//Do some fancy php stuff so we can accept and pass along
		// a variable number of args (anything after the $action arg).
		$args = func_get_args();
		array_unshift($args, $this->path());
		call_user_func_array(array('parent', 'redirect'), $args);
	}
	
	//Render a view file with the given name that exists in the single_pages
	// directory that corresponds with this controller's location / class name.
	//NOTE: Requires Concrete 5.5+ (or for 5.4 compatibility you could hack the core file concrete/libraries/view.php, as per https://github.com/concrete5/concrete5/pull/147/files)
	public function render($view) { //must be public because that's how it is in the base C5 Controller class we're extending
		$path = $this->path($view);
		parent::render($path);
	}
	
	//Allows you to bypass the extra stuff we do in the render function
	// (if for some reason you need to render something outside of this controller).
	protected function renderBypass($view) {
		parent::render($view);
	}
	
	//Return this controller's page path, with optional other things appended to it.
	//Note that a controller's path is wherever the single_page lives
	// in the sitemap -- not a specific action or view that is being displayed.
	protected function path($append = '') {
		$path = $this->getCollectionObject()->getCollectionPath();
		if (!empty($append)) {
			$path .= '/' . $append;
		}
		return $path;
	}
	
	//Wrapper around View::url that always passes the controller's path as the url path
	// so you can call url('task', etc.) instead of url('path/to/controller', 'tasks', etc.).
	protected function url($task = null) {
		//Do some fancy php stuff so we can accept and pass along
		// a variable number of args (anything after the $task arg).
		$args = func_get_args();
		array_unshift($args, $this->path());
		return call_user_func_array(array('View', 'url'), $args);
	}
	
	//Sets controller variables from an associative array (keys become variable names)
	//Optionally pass an array of key names that we should use (we'll ignore everything else).
	protected function setArray($arr, $restrict_to_keys = array()) {
		$restrict_to_keys = empty($restrict_to_keys) ? array_keys($arr) : $restrict_to_keys;
		foreach ($restrict_to_keys as $key) {
			$this->set($key, $arr[$key]);
		}
	}
	
	//Renders the 404 page (and send appropriate http header).
	//Useful when the user hits an actual controller method,
	// but passes in the wrong id number or some other parameter makes the request invalid.
	protected function render404() {
		header('HTTP/1.0 404 Not Found');
		parent::render('/page_not_found');
	}
	
	protected function render404AndExit() {
		$this->render404();
		exit;
	}
	
	protected function requireAjaxPost() {
		if (!$this->isAjax() || !$this->isPost()) {
			exit;
		}
	}
	
	//If explicit null is provided for $data (or nothing is passed at all),
	// then we don't output anything (that is, we don't output a JSON-encoded `null`).
	protected function sendJson($data = null) {
		header('Content-Type: application/json');
		if (!is_null($data)) {
			echo json_encode($data);
		}
		exit;
	}
	
	protected function json404AndExit($data = null) {
		header('HTTP/1.0 404 Not Found');
		$this->json($data);
		exit;
	}
}