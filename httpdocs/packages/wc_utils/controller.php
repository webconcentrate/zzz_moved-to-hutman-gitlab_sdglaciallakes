<?php defined('C5_EXECUTE') or die(_("Access Denied."));

class WcUtilsPackage extends Package {

	protected $pkgHandle = 'wc_utils';
	protected $pkgName = 'Web Concentrate Utilities';
	protected $pkgDescription = 'Code libraries and helpers used by various themes and addons by Web Concentrate';
	protected $appVersionRequired = '5.6.1'; //requires 5.6.1 due to use of the "h()" function
	protected $pkgVersion = '1.0'; //note that we will never change this version number (but we do put individual version numbers [last changed datestamps] in the comments at the top of most of the library files)
	
	//This package doesn't do anything during installation...
	// it's just a container for our custom libraries/helpers so they all live in one place.
	
	//If you write another package that relies on any of these utilities,
	// you can check that this is installed by putting the following
	// into the other package's `install()` method:
	/*
		if (!is_object(Package::getByHandle('wc_utils'))) {
			throw new exception('The "Web Concentrate Utilities" package must be installed');
			exit;
		}
	*/

}
