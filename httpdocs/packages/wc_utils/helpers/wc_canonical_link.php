<?php defined('C5_EXECUTE') or die("Access Denied.");

/**
 * Makes a best-guess at "canonical url" for a given page.
 *
 * Note that there is no way we can automatically predict with 100% certainty
 * what the canonical url should be for all situations (especially for single_pages).
 * So you might need to have your controller manually provide a canonical url
 * which would override this in the html <head>... but for most cases these defaults should suffice.
 *
 * version 2015-03-26
 */

class WcCanonicalLinkHelper {
	
	public function getForPage($page, $escape_for_html = true) {
		if ($page->isGeneratedCollection()) {
			//C5 doesn't necessarily know the url to this page (because it might be a sub-route of a single_page controller),
			// so grab the requested url and attempt to fix common issues...
			$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
			
			//ensure all pages have trailing slash (because that's what the navigation helper's getLinkToCollection() function returns)
			$path = '/' . trim($path, '/') . '/';
			
			//prepend base url (technically not required, as long as url starts with a slash,
			// but let's just be cautious, esp. if site is SSL)
			$canonical_url = BASE_URL . $path;
			
			//NOTE that we neither encode or decode the url...
			// there are too many possibilities to handle with one catch-all rule
			// (e.g. some code outputs links urlencoded, others do not [e.g. designer_blog classification archive links]
			// and then some browsers appear to automatically encode [Chrome] or decode [Firefox] the url).
			//So instead we just leave the url as-is and hope that google treats url encodings
			// as equivelant to their unencoded counterparts. (Couldn't find any definitive documentation on this,
			// but this StackExchange page seems to lean towards "yes ideally they should know it's equivalent": http://webmasters.stackexchange.com/q/31499)
			
		} else {
			//a normal C5 page
			$canonical_url = Loader::helper('navigation')->getCollectionURL($page); //yes, this prepends BASE_URL
		}
		
		//tack on certain querystring args
		$canonical_url .= $this->getCanonicalQuerystring();
		
		if ($escape_for_html) {
			$canonical_url = Loader::helper('text')->entities($canonical_url);
		}
		
		return $canonical_url;
	}
	
	//Returns a full querystring (include preceding "?")
	// for args that are considered part of the canonical url.
	//For now, the only querystring arg we consider "canonical" is the paging string
	// (IF value is greater than 1)... but in the future we could add a config setting
	// that allows single_page functionality to add their own whitelist criteria.
	//Returns empty string if no applicable args.
	private function getCanonicalQuerystring() {
		$parts = explode('&', $_SERVER['QUERY_STRING']);
		$keep_parts = array();
		
		foreach ($parts as $part) {
			$pieces = explode('=', $part, 2);
			if (count($pieces) == 2) {
				list($key, $val) = $pieces;
				if ($key == PAGING_STRING && (int)$val > 1) {
					$keep_parts[] = $part;
				}
			}
		}
		
		$new_qs = implode('&', $keep_parts);
		return (empty($new_qs) ? '' : "?{$new_qs}");
	}
	
}