<?php defined('C5_EXECUTE') or die("Access Denied.");

/* version 2017-12-13 */

class WcUtilsHelper {
	
	//Helper for outputting select options.
	// $options is an array of value => label pairs
	// $value is the currently-selected option (maps to a key in the $options array)
	// $escape_html tells us whether or not we should escape the value and label
	// $line_separator is just for the html output
	public function select_options($options, $value = null, $escape_html = true, $line_separator = "\n") {
		$lines = array();
		foreach ($options as $key => $text) {
			$selected = ($value == $key ? ' selected="selected"' : '');
			$key = $escape_html ? h($key) : $key;
			$text = $escape_html ? h($text) : $text;
			$lines[] = "<option value=\"{$key}\"{$selected}>{$text}</option>";
		}

		return implode($line_separator, $lines);
	}
	
	/**
	 * Slightly improved version of the `getThumbnail()` function from C5's built-in image helper.
	 * 
	 * Differences between this and the built-in image helper `getThumbnail()` function are:
	 *  ~Accepts a file id instead of a full file object (if file id is empty or doesn't correspond to a record, we return null).
	 *  ~Works for full-size original images as well (if no width/height is specified,
	 *   or if original image is already equal to or smaller than the specified width/height).
	 *  ~Returned object is the same for both thumbnails and original images.
	 *  ~Adds the image file's description to the return object.
	 */
	public function img($fID, $width = 0, $height = 0, $crop = false) {
		if (empty($fID)) {
			return null;
		}
		
		$file = File::getByID($fID);
		if (is_null($file)) {
			return null;
		}
		
		$oWidth = $file->getAttribute('width');
		$oHeight = $file->getAttribute('height');
		
		$generate_thumbnail = true;
		if (empty($width) && empty($height)) {
			$generate_thumbnail = false;
		} else if ($width >= $oWidth && $height >= $oHeight) {
			$generate_thumbnail = false;
		}
		
		if ($generate_thumbnail) {
			$width = empty($width) ? 9999 : $width;
			$height = empty($height) ? 9999 : $height;
			$image = Loader::helper('image')->getThumbnail($file, $width, $height, $crop);
		} else {
			$image = new stdClass;
			$image->src = $file->getRelativePath();
			$image->width = $oWidth;
			$image->height = $oHeight;
		}

		if ($image) { //in case getThumbnail failed above (if image isn't on filesystem)
			$image->alt = h($file->getDescription());
		}
		
		return $image;
	}
	
	//Thin wrapper around `number_format` that defaults to 2 decimal places,
	// prepends a currency symbol, and doesn't choke on non-numeric values.
	public function price_format($value, $return_if_non_numeric = null, $prepend_symbol = '$', $decimal_places = 2) {
		if (!is_numeric($value)) {
			return $return_if_non_numeric;
		}
		
		if ($value < 0) {
			//move the negative sign in front of the dollar sign
			$prepend_symbol = '-' . $prepend_symbol;
			$value = abs($value);
		}
		
		return $prepend_symbol . number_format($value, $decimal_places);
	}
	
	//Takes a newline-separated string and converts it into an array of lines.
	//Linefeeds will be normalized (\r -> \n), every line will be trimmed,
	// and empty lines will be excluded from the returned array.
	public function lines2array($text) {
		$text = trim($text);
		$text = str_replace("\r\n", "\n", $text);
		$text = str_replace("\r", "\n", $text);
		$array = explode("\n", $text);
		$array = array_map('trim', $array);
		$array = array_filter($array); //remove empty items
		return $array;
	}
	
	//Sends a CSV file to the browser and then halts execution.
	// $headings is just an array of labels that will be outputted to the first row of the file.
	// $data is an array of arrays (row/column data). Note that we don't validate that the number of "columns" matches from one row to another, nor that they match the $headings -- it's up to the caller to ensure that stuff.
	public function csv($filename, $headings, $data) {
		header('Content-type: text/csv');
		header('Content-Disposition: attachment; filename="' . $filename . '"');

		echo '"' . implode('","', $headings) . '"' . "\r\n";
		
		foreach ($data as $row) {
			$quoted_values = array();
			
			foreach ($row as $value) {
				//convert all CR and LF to CRLF for excel
				$value = str_replace("\r\n", "\n", $value);
				$value = str_replace("\r", "\n", $value);
				$value = str_replace("\n", "\r\n", $value);
				
				//escape quotes
				$value = str_replace('"', '""', $value);
				
				//surround value in quotes
				$value = '"' . $value . '"';
				
				$quoted_values[] = $value;
			}
			
			echo implode(',', $quoted_values) . "\r\n";
		}
		
		exit;
	}
	
	//Basic implementation of php 5.5's array_column function (so it can be used in lesser versions),
	// but also works with an array of objects in addition to an array of arrays (assuming the given keys are publicly-accessible properties of the objects).
	public function arrayColumn($input = null, $column_key, $index_key = null) {
		$column = array();
		foreach ($input as $row) {
			$value = is_object($row) ? $row->$column_key : $row[$column_key];
			if (is_null($index_key)) {
				$column[] = $value;
			} else {
				$key = is_object($row) ? $row->$index_key : $row[$index_key];
				$column[$key] = $value;
			}
		}
		return $column;
	}
	
	//Returns a copy of the given "array of arrays" (or array of objects)
	// that is keyed by a value/property of each array/object.
	// (So you should only call this if you know the given column_key contains a unique value per item.)
	public function arrayKeyedByColumn($input, $column_key) {
		//note: in php versions less than 5.4, array_combine outputs a warning
		// and returns false if either of the arrays are empty.
		if (empty($input)) {
			return array();
		}
		
		$keys = $this->arrayColumn($input, $column_key);
		return array_combine($keys, $input);
	}
	
	//Takes an "array of arrays" (or array of objects)
	// and returns a new array of sub-arrays, keyed by a given column of the given items,
	// each sub-array being a portion of the original array that has a certain value for the column.
	//
	//For example, given this:
	//  array(
	//      array('id' => 1, 'color' => 'green', 'name' => 'test one'),
	//      array('id' => 2, 'color' => 'green', 'name' => 'test two'),
	//      array('id' => 3, 'color' => 'red', 'name' => 'test three'),
	//      array('id' => 4, 'color' => 'red', 'name' => 'test four'),
	//  );
	//...if called with column key "color", would return this:
	//  array(
	//      'green' => array(
	//          array('id' => 1, 'color' => 'green', 'name' => 'test one'),
	//          array('id' => 2, 'color' => 'green', 'name' => 'test two'),
	//      ),
	//      'red' => array(
	//          array('id' => 3, 'color' => 'red', 'name' => 'test three'),
	//          array('id' => 4, 'color' => 'red', 'name' => 'test four'),
	//      ),
	//  );
	//
	//Use this when the column_key is *not* unique amongst all items.
	public function arrayGroupedByColumn($input, $column_key, $preserve_keys = false) {
		$grouped_items = array();
		foreach ($input as $input_item_key => $item) {
			$key = is_object($item) ? $item->$column_key : $item[$column_key];
			if ($preserve_keys) {
				$grouped_items[$key][$input_item_key] = $item;
			} else {
				$grouped_items[$key][] = $item;
			}
		}
		return $grouped_items;
	}
	
	//Returns the first item in the given array that the given callback function returns true for
	public function arrayFind(array $array, $callback) {
		foreach ($array as $item) {
			if ($callback($item)) {
				return $item;
			}
		}
	}
	
	//Like array_map(), but the callback function sets both key and value
	// (by returning an array whose 1st element will be the key and 2nd element
	// will be the value).
	//Note that we switch the argument order from array_map (1st arg is the array,
	// 2nd arg is the callback function).
	//The callback function gets passed the input array value as 1st arg,
	// and the array key as 2nd arg (you can just accept 1st arg
	// if you don't need the key).
	public function arrayMapAssoc(array $input, $callback) {
		$return = array();
		foreach ($input as $item_key => $item_value) {
			list($return_key, $return_value) = $callback($item_value, $item_key);
			$return[$return_key] = $return_value;
		}
		return $return;
	}
	
	//Like array_intersect_key(),
	// except filters the first arg's keys on the 2nd arg's values.
	// For example:
	//  $arr1 = ['a' => 1, 'b' => 3, 'c' => 7, 'd' => 99];
	//  $arr2 = ['b', 'c', 'e'];
	//  $filtered_array = arrayFilterByArray($arr1, $arr2);
	//  ^^^result: $filtered_array is now `['b' => 3, 'c' => 7]`.
	function arrayFilterByArray($array, $keys) {
		return array_intersect_key($array, array_flip($keys));
	}
	
	//Casts every element of the given array to INT,
	// and removes all values that are 0 or less
	public function sanitizeIds(array $ids) {
		/* this is more succinct, but anonymous functions are slower than a foreach loop:
		return array_filter(array_map(function ($id) { return max((int)$id, 0); }, $ids));
		*/
		
		$sanitized_ids = array();
		foreach ($ids as $id) {
			$id = (int)$id;
			if ($id > 0) {
				$sanitized_ids[] = $id;
			}
		}
		return $sanitized_ids;
	}
	
	//An efficient way to retrieve many C5-generated "level 1" (60x60)
	// thumbnail img src's in 1 db query (as opposed to calling File::byID()
	// over and over again, or using FileList which inexplicably calls File::byID()
	// over and over again itself).
	//
	//Note that these are the thumbnails automatically generated by C5
	// when an image is first uploaded/imported into the File Manager.
	//These should really only be used for dashboard interfaces, not front-end.
	//
	//Returns an array whose key is the fID and value is the 60x60 thumbnail img src.
	//Note that the returned array *only* contains keys for files that existed
	// and had "level 1" thumbnails! Any invalid id's, or id's that didn't correspond
	// to a file record, or whose file record didn't have an active version,
	// or whose active version didn't have a "level 1" thumbnail [e.g. not an image]
	// will NOT be included in the returned array. So don't assume the returned array
	// contains all of the fID's you passed into this function!
	public function c5ThumbnailLvl1SrcsByFIDs(array $fIDs) {
		$fIDs = $this->sanitizeIds($fIDs);
		if (!$fIDs) {
			return array();
		}
		
		$fID_string = implode(',', $fIDs);
		$sql = "SELECT fID
		             , fvPrefix
		             , fvFilename
		        FROM FileVersions
		        WHERE fvIsApproved = 1
		          AND fvHasThumbnail1 = 1
		          AND fID IN ({$fID_string})";
		$records = Loader::db()->GetArray($sql);
		
		$cfh = Loader::helper('concrete/file');
		$srcs_per_fID = array();
		foreach ($records as $r) {
			$src = $cfh->getThumbnailRelativePath($r['fvPrefix'], $r['fvFilename'], 1);
			$srcs_per_fID[$r['fID']] = $src;
		}
		
		return $srcs_per_fID;
	}
	
	//Returns true/false
	// (with a page redirect possibly occurring during the process)
	public function cookieCheck() {
		//Check SESSION for previously-set flag.
		//If session flag exists, we know they have cookies.
		//If session flag doesn't exist, it is either because 
		// this is the first time they're hitting this page,
		// or because they don't have cookies enabled...
		// In which case we redirect them back to the same page
		//  with a querystring flag...
		// IF they have the querystring flag but still not the session flag,
		// we can deduce it's because they don't have cookies enabled.
		
		$has_session_flag = !empty($_SESSION['has_cookies']);
		$has_qs_flag = !empty($_GET['cookiecheck']);
		
		$cookies_have_already_been_checked = ($has_session_flag && !$has_qs_flag);
		$cookies_have_not_yet_been_checked = (!$has_session_flag && !$has_qs_flag);
		$cookie_check_success = ($has_session_flag && $has_qs_flag);
		$cookie_check_fail = (!$has_session_flag && $has_qs_flag);
		
		if ($cookies_have_already_been_checked) {
			return true;
		}
		
		$redirect_url = BASE_URL . $_SERVER['REQUEST_URI'];
		//
		if ($cookies_have_not_yet_been_checked) {
			$_SESSION['has_cookies'] = 1;
			$redirect_url .= (strpos($redirect_url, '?') === false) ? '?' : '&';
			$redirect_url .= 'cookiecheck=1';
			header("Location: {$redirect_url}");
			exit;
		}
		//
		if ($cookie_check_success) {
			$redirect_url = str_replace('?cookiecheck=1', '', $redirect_url);
			$redirect_url = str_replace('&cookiecheck=1', '', $redirect_url);
			header("Location: {$redirect_url}");
			exit;
		}
		//
		if ($cookie_check_fail) {
			return false;
		}
	}
}