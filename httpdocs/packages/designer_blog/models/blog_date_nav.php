<?php defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('blog', 'designer_blog');

class BlogDateNavIterator implements Iterator {
	protected $children = array();
	
	function rewind() {
		reset($this->children);
	}
	function current() {
		return current($this->children);
	}
	function key() {
		return key($this->children);
	}
	function next() {
		next($this->children);
	}
	function valid() {
		return (key($this->children) !== null);
	}
}

class BlogDateNav extends BlogDateNavIterator {
	public $count = 0;
	
	public function __construct($active_year = null, $active_month = null) {
		$counts_per_month = $this->getCountsPerMonth();
		foreach ($counts_per_month as $cpm) {
			$year = date('Y', strtotime($cpm['ymd']));
			$month = date('m', strtotime($cpm['ymd']));
			$count = $cpm['cnt'];
			$year_is_active = ($active_year == $year);
			$month_is_active = ($year_is_active && ($active_month == $month));
			$this->addMonth($year, $month, $count, $year_is_active, $month_is_active);
		}
	}
	
	private function getCountsPerMonth() {
		$db = Loader::db();
		
		//distilled version of PageList query...
		$sql = 'SELECT COUNT(*) AS cnt, DATE_FORMAT(cv.cvDatePublic, "%Y-%m-01") AS ymd'
		     . ' FROM Pages p1'
		     . '  LEFT JOIN Pages p2'
		     . '   ON (p1.cPointerID = p2.cID)'
		     . '  INNER JOIN CollectionVersions cv'
		     . '   ON (cv.cID = IF(p2.cID IS NULL, p1.cID, p2.cID) AND cvID = (SELECT cvID FROM CollectionVersions WHERE cvIsApproved = 1 AND cID = cv.cID))'
		     . '  INNER JOIN Collections c'
		     . '   ON (c.cID = IF(p2.cID IS NULL, p1.cID, p2.cID))'
		     . ' WHERE cvIsApproved = 1'
		     . ' AND (p1.cIsTemplate = 0 OR p2.cIsTemplate = 0)'
		     . ' AND p1.cIsActive = 1'
		     . ' AND p1.cParentID = ?'
		     . ' GROUP BY ymd'
		     . ' ORDER BY ymd DESC';
		$vals = array((int)BlogConfig::getIndexCollectionID());
		
		return $db->GetArray($sql, $vals);
	}

	private function addMonth($year, $month, $count, $year_is_active, $month_is_active) {
		if (!array_key_exists($year, $this->children)) {
			$this->children[$year] = new BlogDateNavYear($year);
		}
		
		$this->children[$year]->addMonth($month, $count, $year_is_active, $month_is_active);
		
		$this->count += $count;
	}
}

class BlogDateNavYear extends BlogDateNavIterator {
	public $count = 0;
	public $active = false;
	
	private $year = '';
	public function __toString() {
		return $this->year;
	}
	
	public function __construct($year) {
		$this->year = $year;
	}
	
	public function addMonth($month, $count, $year_is_active, $month_is_active) {
		if (!array_key_exists($month, $this->children)) {
			$this->children[$month] = new BlogDateNavMonth($this->year, $month, $count, $month_is_active);
		}
		
		if ($year_is_active) {
			$this->active = true;
		}
		
		$this->count += $count;
	}
}

class BlogDateNavMonth {
	public $count = 0;
	public $active = false;
	public $url = '';
	public $name = '';
	
	private $month = '';
	public function __toString() {
		return $this->month;
	}
	
	public function __construct($year, $month, $count, $month_is_active) {
		$this->month = $month;
		$this->count = $count;
		$this->is_active = $month_is_active;
		$this->url = BlogHelper::getMonthUrl($year, $month);
		$this->name = date('F', mktime(0, 0, 0, intval($month), 1));
	}
}
