<?php defined('C5_EXECUTE') or die("Access Denied.");

class BlogHelper {
	
	public static function getIndexUrl() {
		return Loader::helper('navigation')->getLinkToCollection(Page::getByID(BlogConfig::getIndexCollectionID()));
	}
	
	public static function getRssUrl() {
		return self::getIndexUrl() . 'rss/';
	}
	
	public static function getAuthorUrl($userinfo_or_username) {
		$user_name = is_object($userinfo_or_username) ? $userinfo_or_username->getUserName() : $username;
		return self::getIndexUrl() . "authors/{$user_name}/";
	}
	
	public static function getAuthorRssUrl($userinfo_or_username) {
		$user_name = is_object($userinfo_or_username) ? $userinfo_or_username->getUserName() : $username;
		return self::getRssUrl() . "authors/{$user_name}/";
	}
	
	public static function getMonthUrl($year, $month) {
		return self::getIndexUrl() . "{$year}/{$month}/";
	}
	
	public static function getClassificationUrl($classification, $value) {
		return self::getIndexUrl() . "{$classification}/{$value}/";
	}
	
	public static function getClassificationRssUrl($classification, $value) {
		return self::getRssUrl() . "{$classification}/{$value}/";
	}
	
	public static function getAuthors() {
		$authors_group_id = BlogConfig::getAuthorsGroupID();
		if (empty($authors_group_id)) {
			return array();
		}
		
		$members = Group::getByID($authors_group_id)->getGroupMembers();
		$authors = array();
		foreach ($members as $user_info) {
			$author = new StdClass;
			$author->user_info = $user_info;
			$author->user_name = $user_info->getUserName();
			$author->display_name = self::getAuthorDisplayName($user_info);
			$author->url = self::getAuthorUrl($user_info);
			$authors[$author->user_name] = $author;
		}
		
		return $authors;
	}
	
	public static function getAuthorDisplayName($userinfo_or_username) {
		$author_user_info = is_object($userinfo_or_username) ? $userinfo_or_username : UserInfo::getByUserName($userinfo_or_username);
		$author_display_name_ak_handle = BlogConfig::getAuthorDisplayNameAkHandle();
		if (!empty($author_display_name_ak_handle)) {
			$display_name = $author_user_info->getAttribute($author_display_name_ak_handle);
		} else {
			$display_name = $author_user_info->getUserName();
		}
		return Loader::helper('text')->entities($display_name);
	}
	
	public static function getAllClassifications() {
		$asID = BlogConfig::getClassificationsAttributeSetID();
		if (empty($asID)) {
			return array();
		}
		
		$th = Loader::helper('text');
		$classifications = array();
		$attribute_keys = AttributeSet::getByID($asID)->getAttributeKeys();
		foreach ($attribute_keys as $ak) {
			if ($ak->getAttributeType()->getAttributeTypeHandle() == 'select') {
				$classifications[$ak->getAttributeKeyHandle()] = $th->entities($ak->getAttributeKeyName());
			}
		}
		return $classifications;
	}
	
	public static function getClassificationOptions($classification) {
		$ak = CollectionAttributeKey::getByHandle($classification);
		if (empty($ak)) {
			return array();
		}
		
		//Ensure this classification exists in the chosen attribute set
		// (otherwise it could get displayed but then when a user tries to visit
		// this classification's archive index, they'll get a 404).
		if (!array_key_exists($ak->getAttributeKeyHandle(), self::getAllClassifications())) {
			return array();
		}
		
		$satc = new SelectAttributeTypeController(AttributeType::getByHandle('select'));
		$satc->setAttributeKey($ak);
		
		$th = Loader::helper('text');
		$option_objects = array();
		$raw_options = $satc->getOptions();
		foreach ($raw_options as $raw_option) {
			$value = $raw_option->value;
			
			$opt = new StdClass;
			$opt->value = $value;
			$opt->display_name = $th->entities($value);
			$opt->url = self::getClassificationUrl($classification, $value);
			$option_objects[$value] = $opt;
		}
		return $option_objects;
	}
	
	public static function getClassificationDisplayName($classification) {
		$ak = CollectionAttributeKey::getByHandle($classification);
		$display_name = empty($ak) ? '' : $ak->getAttributeKeyName();
		return Loader::helper('text')->entities($display_name);
	}
}

class BlogConfig {
	public static function setIndexCollectionID($cID) { self::setConfig('index_collection_id', $cID);	}
	public static function getIndexCollectionID() { return self::getConfig('index_collection_id'); }
	
	public static function setDefaultPostCount($post_count) { self::setConfig('default_post_count', $post_count); }
	public static function getDefaultPostCount() { return self::getConfig('default_post_count'); }
	
	public static function setAuthorsGroupID($gID) { self::setConfig('authors_group_id', $gID);	}
	public static function getAuthorsGroupID() { return self::getConfig('authors_group_id'); }
	
	public static function setClassificationsAttributeSetID($asID) { self::setConfig('classifications_attribute_set_id', $asID); }
	public static function getClassificationsAttributeSetID() { return self::getConfig('classifications_attribute_set_id'); }
	
	public static function setContentAreaHandle($handle) { self::setConfig('content_area_handle', $handle); }
	public static function getContentAreaHandle() { return self::getConfig('content_area_handle'); }
	
	public static function setTeaserCharLength($length) { self::setConfig('teaser_char_length', $length); }
	public static function getTeaserCharLength() { return self::getConfig('teaser_char_length'); }
	
	public static function setTeaserOverrideAkHandle($akHandle) { self::setConfig('teaser_override_ak_handle', $akHandle); }
	public static function getTeaserOverrideAkHandle() { return self::getConfig('teaser_override_ak_handle'); }
	
	public static function setThumbnailAkHandle($akHandle) { self::setConfig('teaser_thumbnail_ak_handle', $akHandle); }
	public static function getThumbnailAkHandle() { return self::getConfig('teaser_thumbnail_ak_handle'); }
	
	public static function setAuthorDisplayNameAkHandle($akHandle) { self::setConfig('author_display_name_ak_handle', $akHandle); }
	public static function getAuthorDisplayNameAkHandle() { return self::getConfig('author_display_name_ak_handle'); }
	
	public static function setRssFeedTitle($title) { self::setConfig('rss_feed_title', $title); }
	public static function getRssFeedTitle() { return self::getConfig('rss_feed_title'); }
	
	public static function setIndexCollectionHandle($handle) { self::setConfig('index_collection_handle', $handle); } //<--this shoudl ONLY be called by the package controller during installation!
	public static function getIndexCollectionHandle() { return self::getConfig('index_collection_handle'); }
	
	private static function setConfig($key, $val) { Package::getByHandle('designer_blog')->saveConfig($key, $val); }
	private static function getConfig($key) { return Package::getByHandle('designer_blog')->config($key); }
}

class BlogArgs {
	
	public static function isValidMonthRequest($classification_or_year, $value_or_month) {
		return (self::isMonthRequest($classification_or_year) && self::validateMonth($classification_or_year, $value_or_month));
	}
	
	public static function isValidAuthorRequest($classification_or_year, $value_or_month) {
		return (self::isAuthorRequest($classification_or_year) && self::validateAuthor($value_or_month));
	}
	
	public static function isValidClassificationRequest($classification_or_year, $value_or_month) {
		return (self::isClassificationRequest($classification_or_year) && self::validateClassification($classification_or_year, $value_or_month));
	}
	
	public static function isValidGetAllRequest($classification_or_year, $value_or_month) {
		return (self::isGetAllRequest($classification_or_year) && self::validateGetAll($classification_or_year, $value_or_month));
	}
	
	public static function validateMonth($year, $month) {
		if (!empty($year) && !empty($month)) {
			if (ctype_digit($year) && (strlen($year) == 4)) {
				if (ctype_digit($month) && (strlen($month) == 2)) {
					return true;
				}
			}
		}
		return false;
	}
	
	public static function validateAuthor($author_username) {
		if (!empty($author_username)) {
			if (in_array($author_username, array_keys(BlogHelper::getAuthors()))) {
				return true;
			}
		}
		return false;
	}
	
	public static function validateClassification($classification, $value) {
		if (!empty($classification) && !empty($value)) {
			if (in_array($classification, array_keys(BlogHelper::getAllClassifications()))) {
				if (in_array($value, array_keys(BlogHelper::getClassificationOptions($classification)))) {
					return true;
				}
			}
		}
		return false;
	}
	
	public static function validateGetAll($classification_or_year, $value_or_month) {
		return (empty($classification_or_year) && empty($value_or_month));
	}
	
	//returns true if the given args are valid:
	// * both args empty: valid
	// * one non-empty arg: invalid
	// * both args non-empty: valid if appropriate for the desired year+month, author username, or classification+value
	public static function validateIndexPathArgs($classification_or_year, $value_or_month) {
		if (self::isGetAllRequest($classification_or_year)) {
			return self::validateGetAll($classification_or_year, $value_or_month);
		} else if (self::isMonthRequest($classification_or_year)) {
			return self::validateMonth($classification_or_year, $value_or_month);
		} else if (self::isAuthorRequest($classification_or_year)) {
			return self::validateAuthor($value_or_month);
		} else if (self::isClassificationRequest($classification_or_year)) {
			return self::validateClassification($classification_or_year, $value_or_month);
		} else {
			return false;
		}
	}
	
	private static function isMonthRequest($classification_or_year) {
		return (ctype_digit($classification_or_year) && (strlen($classification_or_year) == 4));
	}
	private static function isAuthorRequest($classification_or_year) {
		return ($classification_or_year == 'authors');
	}
	private static function isClassificationRequest($classification_or_year) {
		return !empty($classification_or_year) && !self::isMonthRequest($classification_or_year) && !self::isAuthorRequest($classification_or_year);
	}
	private static function isGetAllRequest($classification_or_year) {
		return empty($classification_or_year);
	}
	
}

class BlogList {
	
	//Defaults to package config setting if no post_count is provided.
	//Pass in 0 to retrieve ALL blog posts.
	//Returns an object containing a page array (->posts),
	// and pagination info (->paginator and ->pagination).
	public static function getAll($post_count = null) {
		$pl = self::getBasePageList($post_count);
		return self::returnObjectFromPageList($pl);
	}
	
	//Returns an object containing a page array (->posts),
	// and pagination info (->paginator and ->pagination).
	public static function getByMonth($year, $month, $post_count = null) {
		$pl = null;
		
		if (BlogArgs::validateMonth($year, $month)) {
			$start_of_first_day_of_month = date('Y-m-d H:i:s', mktime(0, 0, 0, $month, 1, $year));
			$end_of_last_day_of_month =  date('Y-m-t 23:59:59', strtotime($start_of_first_day_of_month));

			$pl = self::getBasePageList($post_count);
			$pl->filterByPublicDate($start_of_first_day_of_month, '>=');
			$pl->filterByPublicDate($end_of_last_day_of_month, '<=');
		}
		
		return self::returnObjectFromPageList($pl);
	}
	
	//Returns an object containing a page array (->posts),
	// and pagination info (->paginator and ->pagination).
	public static function getByAuthor($username, $post_count = null) {
		$pl = null;
		
		if (BlogArgs::validateAuthor($username)) {
			$pl = self::getBasePageList($post_count);
			$pl->filterByUserID(UserInfo::getByUserName($username)->getUserID());
		}
		
		return self::returnObjectFromPageList($pl);
	}
	
	//Returns an object containing a page array (->posts),
	// and pagination info (->paginator and ->pagination).
	public static function getByClassification($classification, $value, $post_count = null) {
		$pl = null;
		
		if (BlogArgs::validateClassification($classification, $value)) {
			$pl = self::getBasePageList($post_count);
			if (version_compare(APP_VERSION, '5.6.2', '<')) {
				$pl->filterByAttribute($classification, "%\n{$value}\n%", 'LIKE');
			} else {
				//use this one if available... it has an optimization for single-value selects
				$pl->filterBySelectAttribute($classification, $value);
			}
		}
		
		return self::returnObjectFromPageList($pl);
	}
	
	//Returns an object containing a page array (->posts),
	// and pagination info (->paginator and ->pagination).
	public static function getByIndexPathArgs($classification_or_year, $value_or_month, $post_count = null) {
		if (BlogArgs::isValidGetAllRequest($classification_or_year, $value_or_month)) {
			return self::getAll($post_count);
		} else if (BlogArgs::isValidMonthRequest($classification_or_year, $value_or_month)) {
			return self::getByMonth($classification_or_year, $value_or_month, $post_count);
		} else if (BlogArgs::isValidAuthorRequest($classification_or_year, $value_or_month)) {
			return self::getByAuthor($value_or_month, $post_count);
		} else if (BlogArgs::isValidClassificationRequest($classification_or_year, $value_or_month, $post_count)) {
			return self::getByClassification($classification_or_year, $value_or_month, $post_count);
		} else {
			return self::returnObjectFromPageList(null);
		}
	}
	
	private static function getBasePageList($post_count = null) {
		if ($post_count !== 0) {
			$post_count = intval($post_count);
			$post_count = empty($post_count) ? BlogConfig::getDefaultPostCount() : $post_count;
		}
		
		Loader::model('page_list'); //for < 5.6
		$pl = new PageList;
		
		$pl->filterByParentID(BlogConfig::getIndexCollectionID());
		
		$pl->sortByPublicDateDescending();
		
		if (!empty($post_count)) {
			$pl->setItemsPerPage($post_count);
		}
		
		return $pl;
	}
	
	//or pass null to get an object with empty values for each property
	private static function returnObjectFromPageList($pl) {
		if (is_null($pl)) {
			$posts = array();
			$paginator = Loader::helper('pagination');
			$pagination = '';
		} else {
			$posts = array();
			$pages = $pl->getPage();
			foreach ($pages as $page) {
				$posts[] = new BlogPost($page);
			}
		
			$paginator = $pl->getPagination();
			$pagination = $pl->displayPagingV2(false, true);
		}
		
		$obj = new StdClass;
		$obj->posts = $posts;
		$obj->paginator = $paginator;
		$obj->pagination = $pagination;
		
		return $obj;
	}
}

class BlogSibling {

	public static function getNewer($c) { return self::get($c, true); }
	public static function getOlder($c) { return self::get($c, false); }
	public static function get($c, $newer_vs_older) {
		$pl = self::getBasePageList($c, $newer_vs_older);
		return self::returnBlogPostFromPageList($pl);
	}

	public static function getNewerByClassification($c, $classification, $value) { return self::getByClassification($c, true, $classification, $value); }
	public static function getOlderByClassification($c, $classification, $value) { return self::getByClassification($c, false, $classification, $value); }
	public static function getByClassification($c, $newer_vs_older, $classification, $value) {
		if (!BlogArgs::validateClassification($classification, $value)) {
			return null;
		}

		$pl = self::getBasePageList($c, $newer_vs_older);

		if (version_compare(APP_VERSION, '5.6.2', '<')) {
			$pl->filterByAttribute($classification, "%\n{$value}\n%", 'LIKE');
		} else {
			//use this one if available... it has an optimization for single-value selects
			$pl->filterBySelectAttribute($classification, $value);
		}

		return self::returnBlogPostFromPageList($pl);
	}

	public static function getNewerByAuthor($c, $username) { return self::getByAuthor($c, true, $username); }
	public static function getOlderByAuthor($c, $username) { return self::getByAuthor($c, true, $username); }
	public static function getByAuthor($c, $newer_vs_older, $username) {
		if (!BlogArgs::validateAuthor($username)) {
			return null;
		}

		$pl = self::getBasePageList($c, $newer_vs_older);
		$pl->filterByUserID(UserInfo::getByUserName($username)->getUserID());

		return self::returnBlogPostFromPageList($pl);
	}

	private static function getBasePageList($c, $newer_vs_older) {
		Loader::model('page_list'); //for < 5.6
		$pl = new PageList;
		
		$pl->filterByParentID(BlogConfig::getIndexCollectionID());
		
		$c_public_date = $c->getCollectionDatePublic('Y-m-d H:i:s');
		if ($newer_vs_older) {
			$pl->filterByPublicDate($c_public_date, '>');
			$pl->sortByPublicDate();
		} else {
			$pl->filterByPublicDate($c_public_date, '<');
			$pl->sortByPublicDateDescending();
		}

		return $pl;
	}

	private static function returnBlogPostFromPageList($pl) {
		$pages = $pl->get(1);
		$post = count($pages) ? new BlogPost($pages[0]) : null;
		return $post;
	}
}

class BlogPost {
	private $c; //collection object of the blog post page
	private $th; //c5 text helper
	
	public function __construct($c) {
		$this->c = $c;
		$this->th = Loader::helper('text');
	}
	
	public function getPage() {
		return $this->c;
	}
	
	public function getID() {
		return $this->c->getCollectionID();
	}
	
	public function getUrl() {
		return Loader::helper('navigation')->getLinkToCollection($this->c);
	}
	
	public function getTitle($escape = true) {
		$title = $this->c->getCollectionName();
		if ($escape) {
			$title = $this->th->entities($title);
		}
		return $title;
	}
	
	//For RSS feeds (html entities will break RSS feeds!)
	public function getTitleXmlEscaped() {
		$title = $this->getTitle(false);
		$title = str_replace('&', '&amp;', $title);
		$title = str_replace('<', '&lt;', $title);
		$title = str_replace('>', '&gt;', $title);
		return $title;
	}
	
	public function getDate($format = null) {
		return $this->c->getCollectionDatePublic($format);
	}
	
	public function getAuthor() {
		$author_user_info = $this->getAuthorUserInfo();
		return BlogHelper::getAuthorDisplayName($author_user_info);
	}
	
	public function getAuthorUserInfo() {
		$uID = $this->c->getCollectionUserID();
		return UserInfo::getByID($uID);
	}
	
	public function getThumbnail($width = 0, $height = 0, $crop = false) {
		$thumbnail_ak_handle = BlogConfig::getThumbnailAkHandle();
		if (empty($thumbnail_ak_handle)) {
			return null;
		}
		
		$file = $this->c->getAttribute($thumbnail_ak_handle);
		if (empty($file)) {
			return null;
		}
		
		if (empty($width) && empty($height)) {
			//Show image at full size (do not generate a thumbnail)
			$thumbnail = new stdClass;
			$thumbnail->src = $file->getRelativePath();
			$thumbnail->width = $file->getAttribute('width');
			$thumbnail->height = $file->getAttribute('height');
		} else {
			//Generate a thumbnail
			$width = empty($width) ? 9999 : $width;
			$height = empty($height) ? 9999 : $height;
			$thumbnail = Loader::helper('image')->getThumbnail($file, $width, $height, $crop);
		}
		
		$description = trim($file->getDescription());
		if (empty($description)) {
			$thumbnail->alt = t('thumbnail image for blog post: %s', $this->getTitle());
		} else {
			$thumbnail->alt = $this->th->entities($description);
		}
		
		return $thumbnail;
	}
	
	public function displayThumbnail($width = 0, $height = 0, $crop = false, $wrapper_open = '', $wrapper_close = '') {
		$thumbnail = $this->getThumbnail($width, $height, $crop);
		if ($thumbnail) {
			$src = $thumbnail->src;
			$w = $thumbnail->width;
			$h = $thumbnail->height;
			$alt = $thumbnail->alt;
			
			echo $wrapper_open;
			echo "<img src=\"{$src}\" width=\"{$w}\" height=\"{$h}\" alt=\"{$alt}\" />";
			echo $wrapper_close;
		}
	}
	
	public function getTeaser() {
		$override_ak_handle = BlogConfig::getTeaserOverrideAkHandle();
		$teaser = empty($override_ak_handle) ? '' : $this->c->getAttribute($override_ak_handle);
		
		if (empty($teaser)) {
			$content = $this->getContentsOfFirstNonEmptyContentBlock();
			$length = BlogConfig::getTeaserCharLength();
			$teaser = $this->th->shortenTextWord($content, $length); //shortenTextWord also does strip_tags
		}
		
		$escaped_teaser = $this->th->entities($teaser);
		return $escaped_teaser;
	}
	private function getContentsOfFirstNonEmptyContentBlock() {
		$arHandle = BlogConfig::getContentAreaHandle();
		
		//Look in area layouts first (since they are always "on top of" the normal area).
		//Unfortunately we must examine each area layout separately,
		// since technically they are each their own mini-area with different handles.
		$area = new Area($arHandle);
		$layouts = $area->getAreaLayouts($this->c); //returns empty array if no layouts
		foreach ($layouts as $layout) {
			$max_cells = $layout->getMaxCellNumber();
			for ($cell=1; $cell<=$max_cells; $cell++) {
				$cell_handle = $layout->getCellAreaHandle($cell);
				$blocks = $this->c->getBlocks($cell_handle);
				foreach ($blocks as $block) {
					$btHandle = $block->getBlockTypeHandle();
//CUSTOM FOR SDGL: WE REPLACED THE BUILT-IN CONTENT BLOCK WITH A DCP BLOCK
					if ($btHandle == 'content' || $btHandle == 'dcp_content') {
						ob_start();
						$block->display();
						$content = ob_get_clean(); //combines ob_get_contents() and ob_end_clean()
						if (!empty($content)) {
							return $content;
						}
					}
				}
			}
		}
		
		//If there were no area layouts, look in the "normal" area blocks
		$blocks = $this->c->getBlocks($arHandle); //Returns blocks in the order they're on the page
		foreach ($blocks as $block) {
			$btHandle = $block->getBlockTypeHandle();
//CUSTOM FOR SDGL: WE REPLACED THE BUILT-IN CONTENT BLOCK WITH A DCP BLOCK
			if ($btHandle == 'content' || $btHandle == 'dcp_content') {
				ob_start();
				$block->display();
				$content = ob_get_clean(); //combines ob_get_contents() and ob_end_clean()
				if (!empty($content)) {
					return $content;
				}
			}
		}

		//Nothing was found
		return '';
	}
	
	public function getSingleClassification($classification) {
		//In C5, 'select' type attributes are stored the same way
		// regardless of whether or not they have the "allow multiple" option
		// (it's just the editing UI for the attribute that differs).
		$values = $this->getMultiClassification($classification);
		return count($values) ? array_shift($values) : '';
	}
	
	public function getMultiClassification($classification) {
		$values = array();
		$selected_options = $this->c->getAttribute($classification);
		if (!empty($selected_options)) {
			foreach ($selected_options as $opt) {
				$values[$opt->ID] = $this->th->entities($opt->value);
			}
		}
		return $values;
	}
	
	public function getClassifications($classification = '') {
		if (empty($classification)) {
			$classification_handles = array_keys(BlogHelper::getAllClassifications());
			$classifications = array();
			foreach ($classification_handles as $handle) {
				$classifications[$handle] = $this->getClassifications($handle);
			}
			return $classifications;
		} else {
			$values = array();
			$selected_options = $this->c->getAttribute($classification);
			foreach ($selected_options as $opt) {
				$values[$opt->ID] = $this->th->entities($opt->value);
			}
			return $values;
		}
	}
	
	public function getCommentSummary($singular_format = '%d comment', $plural_format = '%d comments', $disabled_message = 'Comments disabled') {
		if ($this->getCommentsAreEnabled()) {
			$count = $this->getCommentCount();
			$format = ($count == 1 ? $singular_format : $plural_format);
			return sprintf($format, $count);
		} else {
			return $disabled_message;
		}
	}

	public function getCommentsAreEnabled() {
		//Determine if a guestbook block exists on the given page
		$db = Loader::db();
		$sql = 'SELECT COUNT(*)'
			 . ' FROM CollectionVersionBlocks INNER JOIN Blocks ON (CollectionVersionBlocks.bID = Blocks.bID)'
			 . ' INNER JOIN BlockTypes ON (Blocks.btID = BlockTypes.btID)'
			 . ' WHERE CollectionVersionBlocks.cID = ?'
			 . ' AND (CollectionVersionBlocks.cvID = ? OR CollectionVersionBlocks.cbIncludeAll=1)'
			 . ' AND BlockTypes.btHandle = "guestbook"';
		$args = array($this->c->getCollectionID(), $this->c->getVersionID());
		$count = $db->GetOne($sql, $args);
		return ($count > 0);
	}
	
	public function getCommentCount() {
		//Query on the collection id as opposed to the block id in case guestbook is an alias of Page Defaults
		// (so we don't get the total count of all instances of that block on all pages).
		$db = Loader::db();
		$sql = 'SELECT COUNT(*) as count FROM btGuestBookEntries WHERE approved=1 AND cID=?';
		$args = Array($this->c->getCollectionID());
		return $db->GetOne($sql, $args);
	}
	
	public function getContent() {
		ob_start();
		$this->displayContent();
		return ob_get_clean();
	}
	
	public function displayContent() {
		$a = new Area(BlogConfig::getContentAreaHandle());
		$a->disableControls();
		$a->display($this->c);
	}

	public function getNewerPost() { return BlogSibling::getNewer($this->c); }
	public function getOlderPost() { return BlogSibling::getOlder($this->c); }
	public function getNewerPostByAuthor() { return BlogSibling::getNewerByAuthor($this->c, $self->getAuthorUserInfo()->getUserName()); }
	public function getOlderPostByAuthor() { return BlogSibling::getOlderByAuthor($this->c, $self->getAuthorUserInfo()->getUserName()); }
	public function getNewerPostByClassification($classification, $value) { return BlogSibling::getNewerByClassification($this->c, $classification, $value); }
	public function getOlderPostByClassification($classification, $value) { return BlogSibling::getOlderByClassification($this->c, $classification, $value); }
	//For the siblings "by classification", we need to know both the classification AND the value,
	// in case it's a "MultiClassification" (e.g. if post has 2 tags, we need to know which of those to filter on)
}
