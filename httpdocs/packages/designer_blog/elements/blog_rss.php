<?php defined('C5_EXECUTE') or die("Access Denied.");

header('Content-Type: application/rss+xml; charset=' . APP_CHARSET);

echo "<?xml version=\"1.0\" encoding=\"" . APP_CHARSET . "\"?>\n";
echo "\n";
echo "<rss version=\"2.0\">\n";
echo "\t<channel>\n";
echo "\t\t<title>{$escaped_title}</title>\n";
echo "\t\t<link>" . BASE_URL . $blog_url . "</link>\n";
echo "\t\t<description>{$escaped_title}</description>\n";
echo "\t\t<lastBuildDate>" . date('D, d M Y H:i:s O') . "</lastBuildDate>\n";
foreach ($posts as $post) {
	echo "\t\t<item>\n";
	echo "\t\t\t<title>" . $post->getTitleXmlEscaped() . "</title>\n";
	echo "\t\t\t<link>" . BASE_URL . $post->getUrl() . "</link>\n";
	echo "\t\t\t<description><![CDATA[";
	$content = $post->getContent();
	$content_with_absolute_urls = preg_replace('#(href|src)="([^:"]*)(?:")#','$1="' . BASE_URL . DIR_REL . '$2"', $content);
	echo $content_with_absolute_urls;
	echo "]]></description>\n";
	echo "\t\t\t<pubDate>" . $post->getDate('D, d M Y H:i:s O') . "</pubDate>\n";
	echo "\t\t\t<guid>" . BASE_URL . $post->getUrl() . "</guid>\n";
	echo "\t\t</item>\n";
}
echo "\t</channel>\n";
echo "</rss>\n";
