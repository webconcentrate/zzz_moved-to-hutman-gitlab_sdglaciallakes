<?php defined('C5_EXECUTE') or die("Access Denied.");

$options = BlogHelper::getClassificationOptions($classification);
?>

<aside class="blog-classification-nav">
	<h2><?php echo $title; ?></h2>

	<ul>
	<?php foreach ($options as $option): ?>
		<li class="<?php echo ($option->value == $active_value) ? 'active' : ''; ?>">
			<a href="<?php echo $option->url; ?>"><?php echo $option->display_name; ?></a>
		</li>
	<?php endforeach; ?>
	</ul>
</aside>