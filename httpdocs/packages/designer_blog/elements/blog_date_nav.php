<?php defined('C5_EXECUTE') or die("Access Denied.");

//Determine the "active" month that should be expanded/hilighted by default in the nav structure...
	//No "active_month" arg was passed in, so use the current page's public date:
	if (!isset($active_month)) {
		$c = Page::getCurrentPage();
		$active_month = $c->getCollectionDatePublic('m');
		$active_year = $c->getCollectionDatePublic('Y');

	//An "active_month" arg of 0 was passed in, so use today's date:
	} else if (empty($active_month)) {
		$active_month = date('m');
		$active_year = date('Y');

	//An "active_month" arg higher than 0 was passed in, so use that as a timestamp:
	} else {
		$active_month_timestamp = $active_month; //store this in another variable so it doesn't get overwritten in the next line
		$active_month = date('m', $active_month_timestamp);
		$active_year = date('Y', $active_month_timestamp);
	}

Loader::model('blog_date_nav', 'designer_blog');
$nav = new BlogDateNav($active_year, $active_month);
?>

<aside class="blog-date-nav">
	<h2>Blog Archive</h2>

	<ul><?php foreach ($nav as $year): ?>
		<li class="<?php echo $year->active ? 'active' : ''; ?>">
			<?php echo "{$year} ({$year->count} posts)"; ?>
			<ul><?php foreach ($year as $month): ?>
				<li class="<?php echo $month->active ? 'active' : ''; ?>">
					<a href="<?php echo $month->url; ?>"><?php echo "{$month->name} ({$month->count} posts)"; ?></a>
				</li>
			<?php endforeach; ?></ul>
		</li>
	<?php endforeach ?></ul>
</aside>

<script>
$(document).ready(function() {
	//collapse all years
	$('.blog-date-nav li ul').hide();
	
	//expand the "active" year
	$('.blog-date-nav li.active ul').show();
	
	//expand/collapse a year when it is clicked on
	$('.blog-date-nav > ul > li').css('cursor', 'pointer').on('click', function() {
		$(this).find('ul').slideToggle('fast');
	});
});
</script>
