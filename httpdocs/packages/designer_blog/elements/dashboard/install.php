<?php defined('C5_EXECUTE') or die("Access Denied.");

$form = Loader::helper('form');

echo $form->label('designer_blog_index_collection_handle', t('Enter the "URL Slug" for the top-level blog index page:'));

echo $form->text('designer_blog_index_collection_handle', 'blog');
