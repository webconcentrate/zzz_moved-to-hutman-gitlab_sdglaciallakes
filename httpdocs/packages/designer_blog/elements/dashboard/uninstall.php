<?php defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('blog', 'designer_blog');
$post_count = BlogList::getAll(1)->paginator->result_count;

if ($post_count): ?>

	<hr>
	
	<h1 style="color: red;"><?php echo t('WARNING:<br>Uninstalling this addon will cause %d blog posts<br>to be PERMANENTLY DELETED!', $post_count); ?></h1>

	<br>

	<h3>
		<?php echo t('To avoid permanent deletion of these blog posts, you must first move them to a different location in your sitemap (so they are no longer beneath the blog index page).'); ?>
		<br>
		<i><?php echo t('Pro Tip: You can use the dashboard <a href="%s">Page Search</a> to move multiple pages at once!', View::url('/dashboard/sitemap/search')); ?></i>
	</h3>

<?php endif; ?>