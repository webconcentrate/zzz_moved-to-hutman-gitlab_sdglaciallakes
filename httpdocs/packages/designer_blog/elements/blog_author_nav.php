<?php defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('blog', 'designer_blog');
$authors = BlogHelper::getAuthors();
?>

<aside class="blog-author-nav">
	<h2>Contributors</h2>

	<ul>
	<?php foreach ($authors as $author): ?>
		<li><a href="<?php echo $author->url; ?>"><?php echo $author->display_name; ?></a></li>
	<?php endforeach; ?>
	</ul>
</aside>