<?php defined('C5_EXECUTE') or die("Access Denied.");

$bootstrap_v1 = version_compare(APP_VERSION, '5.6.2', '<'); //Concrete5.5 uses Twitter Bootstrap v1 (instead of v2), which requires some different CSS classes
?>

<?php if ($bootstrap_v1): ?>
<div class="ccm-ui">
	<div class="form-horizontal">
	<?php endif; ?>
	
		<div class="control-group <?php echo $bootstrap_v1 ? 'clearfix' : ''; ?>">
			<label class="control-label" for="filter_type"><?php echo t('Filter Posts'); ?></label>
			<div class="controls <?php echo $bootstrap_v1 ? 'input' : ''; ?>">
				<?php echo $form->select('filter_type', $filter_type_options, $filter_type); ?>
				
				<?php foreach ($filter_value_options as $filter_type_handle => $filter_options): ?>
					<?php if (!empty($filter_type_handle)): ?>
						<?php echo $form->select("filter_value_{$filter_type_handle}", $filter_options, ($filter_type == $filter_type_handle ? $filter_value : null), array('style' => 'margin-top: 5px;')); ?>
					<?php endif; ?>
				<?php endforeach; ?>
			</div>
		</div>
		
		<div class="control-group <?php echo $bootstrap_v1 ? 'clearfix' : ''; ?>">
			<label class="control-label" for="post_count"><?php echo t('# of posts'); ?></label>
			<div class="controls <?php echo $bootstrap_v1 ? 'input' : ''; ?>">
				<?php echo $form->text('post_count', $post_count, array('style' => 'width: 30px; text-align: center;', 'maxlength' => '3')); ?>
			</div>
		</div>
		
	<?php if ($bootstrap_v1): ?>
	</div>
</div>
<?php endif; ?>

<script>
$(document).ready(function() {
	$('select#filter_type').on('change', toggleFilterOptions);
	toggleFilterOptions();
});

function toggleFilterOptions() {
	$('select[id^="filter_value_"]').hide();
	
	var filter_type = $('select#filter_type').val();
	if (filter_type.length) {
		$('select[id="filter_value_' + filter_type + '"]').show();
	}
}
</script>