<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<section class="blog-recent-posts">
	<h2>Recent Blog Posts</h2>

	<ul>
		<?php foreach ($posts as $post): ?>
			<li>
				<a href="<?php echo $post->getUrl(); ?>"><?php echo $post->getTitle(); ?></a>
			</li>
		<?php endforeach; ?>
	</ul>

	<p><a href="<?php echo $blog_url; ?>">Read More &raquo;</a></p>
</section>