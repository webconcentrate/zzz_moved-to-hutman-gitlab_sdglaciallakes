<?php defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::library('crud_controller', 'designer_blog');
class DashboardSystemDesignerBlogSettingsController extends CrudController {
	
	public function view() {
		if ($_POST) {
			$error = $this->validate($_POST);
			if ($error->has()) {
				$this->set('error', $error); //c5 will display these for us
				//No need to re-load data because c5 form helpers
				// will automatically populate fields from $_POST
			} else {
				$this->save($_POST);
				$this->flash(t('Blog Settings Saved'));
				$this->redirect(''); //redirect back to this same page
			}
		} else {
			$this->set('default_post_count', BlogConfig::getDefaultPostCount());
			$this->set('authors_group_id', BlogConfig::getAuthorsGroupID());
			$this->set('classifications_attribute_set_id', BlogConfig::getClassificationsAttributeSetID());
			$this->set('content_area_handle', BlogConfig::getContentAreaHandle());
			$this->set('teaser_char_length', BlogConfig::getTeaserCharLength());
			$this->set('teaser_override_ak_handle', BlogConfig::getTeaserOverrideAkHandle());
			$this->set('teaser_thumbnail_ak_handle', BlogConfig::getThumbnailAkHandle());
			$this->set('author_display_name_ak_handle', BlogConfig::getAuthorDisplayNameAkHandle());
			$this->set('rss_feed_title', BlogConfig::getRssFeedTitle());
		}
		
		//populate lookup data
		$index = Page::getByID(BlogConfig::getIndexCollectionID());
		$this->set('index_path', $index->getCollectionPath());
		$original_index_path = '/' . BlogConfig::getIndexCollectionHandle();
		$this->set('index_was_moved', ($original_index_path != $index->getCollectionPath()));
		$this->set('original_index_path', $original_index_path);
		$this->set('index_was_refreshed', ($index->getCollectionFilename() === '0'));
		
		$this->set('group_options', $this->getGroupOptions());
		$this->set('attr_set_options', $this->getAttributeSetOptions());
		$this->set('textarea_attr_options', $this->getAttributeKeyOptions('collection', 'textarea'));
		$this->set('image_attr_options', $this->getAttributeKeyOptions('collection', 'image_file'));
		$this->set('user_text_attr_options', $this->getAttributeKeyOptions('user', 'text'));
		
		$this->addHeaderItem(Loader::helper('html')->css('dashboard.css', 'designer_blog'));
	}
	
	private function validate($post) {
		$error = Loader::helper('validation/error');
		
		if (empty($post['default_post_count']) || !intval($post['default_post_count'])) {
			$error->add(t('Default Post Count is required.'));
		}
		
		if (empty($post['content_area_handle'])) {
			$error->add(t('Content Area Handle is required.'));
		}
		
		if (empty($post['teaser_char_length']) || !intval($post['teaser_char_length'])) {
			$error->add(t('Teaser Character Length is required.'));
		}
		
		return $error;
	}
	
	private function save($post) {
		BlogConfig::setDefaultPostCount((int)$post['default_post_count']);
		BlogConfig::setAuthorsGroupID((int)$post['authors_group_id']);
		BlogConfig::setClassificationsAttributeSetID((int)$post['classifications_attribute_set_id']);
		BlogConfig::setContentAreaHandle($post['content_area_handle']);
		BlogConfig::setTeaserCharLength((int)$post['teaser_char_length']);
		BlogConfig::setTeaserOverrideAkHandle($post['teaser_override_ak_handle']);
		BlogConfig::setThumbnailAkHandle($post['teaser_thumbnail_ak_handle']);
		BlogConfig::setAuthorDisplayNameAkHandle($post['author_display_name_ak_handle']);
		BlogConfig::setRssFeedTitle($post['rss_feed_title']);
	}
	
	private function getGroupOptions() {
		Loader::model('search/group');
		$gs = new GroupSearch();
		$records = $gs->get();
		
		$th = Loader::helper('text');
		$options = array(0 => t('None (Disable Authors)'));
		foreach ($records as $record) {
			$options[$record['gID']] = $th->entities($record['gName']);
		}
		
		return $options;
	}
	
	private function getAttributeSetOptions() {
		$akc = AttributeKeyCategory::getByHandle('collection');
		$sets = $akc->getAttributeSets();
		
		$th = Loader::helper('text');
		$options = array(0 => t('None (Disable Classifications)'));
		foreach ($sets as $set) {
			$options[$set->getAttributeSetID()] = $th->entities($set->getAttributeSetName());
		}
		
		return $options;
	}
	
	private function getAttributeKeyOptions($category, $type) {
		$aks = AttributeKey::getList($category);
		
		$th = Loader::helper('text');
		$options = array('' => t('None'));
		foreach ($aks as $ak) {
			if ($ak->getAttributeType()->getAttributeTypeHandle() == $type) {
				$options[$ak->getAttributeKeyHandle()] = $th->entities($ak->getAttributeKeyName());
			}
		}
		
		return $options;
	}
		
}