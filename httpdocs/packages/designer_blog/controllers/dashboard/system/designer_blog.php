<?php defined('C5_EXECUTE') or die(_("Access Denied."));

//This controller serves as a placeholder for the top-level item in the C5 dashboard.

class DashboardSystemDesignerBlogController extends Controller {

	public function view() {
		$this->redirect('/dashboard/system/designer_blog/settings');
	}

}