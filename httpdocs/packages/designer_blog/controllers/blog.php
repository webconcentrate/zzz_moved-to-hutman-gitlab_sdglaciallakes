<?php defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('blog', 'designer_blog');

class BlogController extends Controller {
	
	public function view($classification_or_year = null, $value_or_month = null) {
		if (!BlogArgs::validateIndexPathArgs($classification_or_year, $value_or_month)) {
			header("HTTP/1.0 404 Not Found");
			$this->render('/page_not_found');
			exit;
		}
		
		//Posts & Pagination
		$bl = BlogList::getByIndexPathArgs($classification_or_year, $value_or_month);
		$this->set('posts', $bl->posts);
		$this->set('paginator', $bl->paginator);
		$this->set('pagination', $bl->pagination);
		
		//Headings
		$archive_month = $this->getRequestedMonthTimestamp($classification_or_year, $value_or_month);
		list($author_user_name, $author_display_name) = $this->getRequestedAuthorName($classification_or_year, $value_or_month);
		list($cfcn_handle, $cfcn_name, $cfcn_val) = $this->getRequestedClassificationDisplayNameAndValue($classification_or_year, $value_or_month);
		$this->set('archive_month', $archive_month);
		$this->set('archive_author', $author_user_name);
		$this->set('archive_author_name', $author_display_name);
		$this->set('archive_classification', $cfcn_handle);
		$this->set('archive_classification_name', $cfcn_name);
		$this->set('archive_classification_value', $cfcn_val);
		
		//RSS
		if ($archive_author) {
			$rss_url = BlogHelper::getAuthorRssUrl($classification_or_year);
		} else if ($cfcn_name) {
			$rss_url = BlogHelper::getClassificationRssUrl($classification_or_year, $value_or_month);
		} else {
			//this applies to the month archive too (because there is no month-specific rss feed)
			$rss_url = BlogHelper::getRssUrl();
		}
		$this->set('rss_url', $rss_url);
		$rss_title = $this->getRssTitleHtmlEscaped($classification_or_year, $value_or_month);
		$rss_link = '<link rel="alternate" type="application/rss+xml" href="' . BASE_URL . $rss_url . '" title="' . $rss_title . '" />';
		$this->addHeaderItem($rss_link);
	}
	
	public function rss($classification = null, $value = null) {
		$is_valid_args = BlogArgs::validateIndexPathArgs($classification, $value);
		$is_month_request = BlogArgs::isValidMonthRequest($classification, $value);
		if (!$is_valid_args || $is_month_request) { //no rss feed for month archives
			header("HTTP/1.0 404 Not Found");
			$this->render('/page_not_found');
			exit;
		}
		
		Loader::packageElement('blog_rss', 'designer_blog', array(
			'posts' => BlogList::getByIndexPathArgs($classification, $value)->posts,
			'escaped_title' => $this->getRssTitleXmlEscaped($classification, $value),
			'blog_url' => BlogHelper::getIndexUrl(),
		));
		
		exit;
	}
	
	private function getRssTitle($classification, $value) {
		$title = BlogConfig::getRssFeedTitle();
		if (empty($title)) {
			$title = SITE;
		}
		if (BlogArgs::isValidAuthorRequest($classification, $value)) {
			$title = BlogHelper::getAuthorDisplayName($value) . ' :: ' . $title;
		} else if (BlogArgs::isValidClassificationRequest($classification, $value)) {
			$title = $value . ' :: ' . $title;
		}
		return $title;
	}
	private function getRssTitleHtmlEscaped($classification, $value) {
		$title = $this->getRssTitle($classification, $value);
		$title = Loader::helper('text')->entities($title);
		return $title;
	}
	private function getRssTitleXmlEscaped($classification, $value) {
		$title = $this->getRssTitle($classification, $value);
		$title = str_replace('&', '&amp;', $title);
		$title = str_replace('<', '&lt;', $title);
		$title = str_replace('>', '&gt;', $title);
		return $title;
	}
	
	private function getRequestedMonthTimestamp($classification_or_year, $value_or_month) {
		if (BlogArgs::isValidMonthRequest($classification_or_year, $value_or_month)) {
			return mktime(0, 0, 0, $value_or_month, 1, $classification_or_year); //first day of month
		} else {
			return 0;
		}
	}
	
	private function getRequestedAuthorName($classification_or_year, $value_or_month) {
		if (BlogArgs::isValidAuthorRequest($classification_or_year, $value_or_month)) {
			$user_name = $value_or_month;
			$display_name = BlogHelper::getAuthorDisplayName($value_or_month);
		} else {
			$user_name = '';
			$display_name = '';
		}
		return array($user_name, $display_name);
	}
	
	private function getRequestedClassificationDisplayNameAndValue($classification_or_year, $value_or_month) {
		if (BlogArgs::isValidClassificationRequest($classification_or_year, $value_or_month)) {
			$handle = $classification_or_year;
			$display_name = BlogHelper::getClassificationDisplayName($classification_or_year);
			$value = $value_or_month;
		} else {
			$handle = '';
			$display_name = '';
			$value = '';
		}
		return array($handle, $display_name, $value);
	}
}
