<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<section class="blog-index">
	<h2><?php
	if ($archive_month) {
		echo 'Archives for ' . date('F, Y', $archive_month);
	} else if ($archive_author) {
		echo 'Posts by ' . $archive_author_name;
	} else if ($archive_classification) {
		echo 'Posts with ' . $archive_classification_name . ': ' . $archive_classification_value;
	} else {
		echo 'Recent Blog Posts';
	} ?></h2>
	
	<?php if ($posts): ?>
		<?php foreach ($posts as $post): ?>
			<section>
				<h3><a href="<?php echo $post->getUrl(); ?>"><?php echo $post->getTitle(); ?></a></h3>
				<p>posted <?php echo $post->getDate('F jS, Y'); ?></p>
				<p>by <?php echo $post->getAuthor(); ?></p>
				<p><?php echo $post->getCommentSummary(); ?></p>
				<?php $post->displayThumbnail(200, 100, true, '<p>', '</p>'); ?>
				<p><?php echo $post->getTeaser(); ?></p>
				<?php /* <p>Category: <?php echo $post->getSingleClassification('category'); ?></p> */ ?>
				<p>Tags: <?php echo implode(', ', $post->getMultiClassification('tags')); ?></p>
			</section>
		<?php endforeach; ?>
		
		<?php echo $pagination; ?>
		
		<?php /* or, for more fine-grained control of the pagination output, use $paginator...
		<?php if ($paginator->getTotalPages() > 1): ?>
			<div class="pagination ccm-pagination"><ul>
				<li class="prev"><?php echo $paginator->getPrevious(); ?></li>
				<?php echo $paginator->getPages('li'); ?>
				<li class="next"><?php echo $paginator->getNext(); ?></li>
			</ul></div>
		<?php endif; ?>
		*/ ?>
	
	<?php else: ?>
	
		<p><i>No posts to display</i></p>
	
	<?php endif; ?>
	
	<footer>
		<a href="<?php echo $rss_url; ?>">RSS FEED</a>
	</footer>
	
</section>

<div class="blog-secondary">
	<?php Loader::packageElement('blog_classification_nav', 'designer_blog', array('classification' => 'tags', 'title' => 'Tags', 'active_value' => ($archive_classification == 'tags' ? $archive_classification_value : ''))); ?>
	<?php Loader::packageElement('blog_classification_nav', 'designer_blog', array('classification' => 'category', 'title' => 'Categories', 'active_value' => ($archive_classification == 'category' ? $archive_classification_value : ''))); ?>
	<?php Loader::packageElement('blog_author_nav', 'designer_blog'); ?>
	<?php Loader::packageElement('blog_date_nav', 'designer_blog', array('active_month' => $archive_month)); ?>
</div>
