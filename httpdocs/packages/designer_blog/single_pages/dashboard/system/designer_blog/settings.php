<?php defined('C5_EXECUTE') or die(_("Access Denied."));

$dh = Loader::helper('concrete/dashboard');
$ih = Loader::helper('concrete/interface');
$form = Loader::helper('form');
?>


<?php echo $dh->getDashboardPaneHeaderWrapper(t('Blog Settings'), false, '', false); ?>

	<form method="post" action="<?php echo $this->action(''); ?>">
		<?php echo $token; ?>

		<div class="ccm-pane-body">
			<table class="form-table">
				<tr>
					<td class="label-cell" style="vertical-align: top;"><?php echo t('Blog Index Path'); ?></td>
					<td>
						<p><code><?php echo $index_path; ?></code></p>
						<?php if ($index_was_refreshed): ?>
							<p style="font-weight: bold; color: red;"><?php echo t('Error: The blog index page was moved and its "Refresh" button was clicked in the "Single Pages" dashboard page.'); ?></p>
							<p style="font-weight: bold; color: red;"><?php echo t('The blog index page (including month/author/classification archives and RSS feeds) will not function properly!'); ?></p>
							<p style="font-weight: bold; color: red;"><?php echo t('To fix this, move the blog index page back to its original location (%s), then click its "Refresh" button again in the "Single Pages" dashboard page.', "<code>{$original_index_path}</code>"); ?></p>
						<?php elseif ($index_was_moved): ?>
							<p style="font-style: italic;"><?php echo t('The blog index page has been moved from its original location (%s).', "<code>{$original_index_path}</code>"); ?></p>
							<p style="font-style: italic;"><?php echo t('Do <b>NOT</b> click its "Refresh" button in the "Single Pages" dashboard page!'); ?></p>
						<?php else: ?>
							<p style="font-style: italic; margin-top: 10px;">
								<?php echo t('You can move the blog index page to a new location via the dashboard sitemap.'); ?>
								<br>
								<?php echo t('Unfortunately due to limitations of Concrete5, the "URL Slug" cannot be changed after the index page has been installed -- to change it, you must uninstall the Designer Blog addon and then re-install it with a different URL Slug.'); ?>
							</p>
						<?php endif; ?>
					</td>
				</tr>
				<tr><td colspan="2"><hr></td>
				<tr>
					<td class="label-cell"><?php echo $form->label('authors_group_id', t('Authors Group')); ?></td>
					<td><?php echo $form->select('authors_group_id', $group_options, $authors_group_id); ?></td>
				</tr>
				<tr>
					<td class="label-cell"><?php echo $form->label('author_display_name_ak_handle', t('Author Display Name Attribute')); ?></td>
					<td><?php echo $form->select('author_display_name_ak_handle', $user_text_attr_options, $author_display_name_ak_handle); ?></td>
				</tr>
				<tr>
					<td class="label-cell"><?php echo $form->label('classifications_attribute_set_id', t('Classifications Attribute Set')); ?></td>
					<td><?php echo $form->select('classifications_attribute_set_id', $attr_set_options, $classifications_attribute_set_id); ?></td>
				</tr>
				<tr><td colspan="2"><hr></td>
				<tr>
					<td class="label-cell"><?php echo $form->label('default_post_count', t('Default Post Count')); ?></td>
					<td><?php echo $form->text('default_post_count', $default_post_count, array('maxlength' => '4', 'class' => 'input-mini')); ?></td>
				</tr>
				<tr>
					<td class="label-cell"><?php echo $form->label('content_area_handle', t('Content Area Handle')); ?></td>
					<td><?php echo $form->text('content_area_handle', $content_area_handle, array('class' => 'input-medium')); ?></td>
				</tr>
				<tr>
					<td class="label-cell"><?php echo $form->label('teaser_char_length', t('Teaser Character Count')); ?></td>
					<td><?php echo $form->text('teaser_char_length', $teaser_char_length, array('maxlength' => '4', 'class' => 'input-mini')); ?></td>
				</tr>
				<tr>
					<td class="label-cell"><?php echo $form->label('teaser_override_ak_handle', t('Teaser Override Attribute')); ?></td>
					<td><?php echo $form->select('teaser_override_ak_handle', $textarea_attr_options, $teaser_override_ak_handle); ?></td>
				</tr>
				<tr>
					<td class="label-cell"><?php echo $form->label('teaser_thumbnail_ak_handle', t('Teaser Thumbnail Attribute')); ?></td>
					<td><?php echo $form->select('teaser_thumbnail_ak_handle', $image_attr_options, $teaser_thumbnail_ak_handle); ?></td>
				</tr>
				<tr><td colspan="2"><hr></td>
				<tr>
					<td class="label-cell"><?php echo $form->label('rss_feed_title', t('RSS Feed Title')); ?></td>
					<td><?php echo $form->text('rss_feed_title', $rss_feed_title, array('class' => 'input-xlarge')); ?></td>
				</tr>
			</table>
		</div>
		
		<div class="ccm-pane-footer">
			<?php echo $ih->submit(t('Save'), false, 'right', 'primary'); ?>
		</div>
	</form>
	
<?php echo $dh->getDashboardPaneFooterWrapper(); ?>