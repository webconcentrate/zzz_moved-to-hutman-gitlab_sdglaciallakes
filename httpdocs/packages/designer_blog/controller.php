<?php defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('blog', 'designer_blog');

class DesignerBlogPackage extends Package {
	
	protected $pkgHandle = 'designer_blog';
	public function getPackageName() { return t('Designer Blog'); }
	public function getPackageDescription() { return t("A blog section that's easy for web designers to style."); }
	protected $appVersionRequired = '5.5.2.1';
	protected $pkgVersion = '0.6.4';
	
	private $default_index_collection_handle = 'blog'; //don't change this!
	
	public function install($args) {
		$this->validateInstallArgs($args);
		
		$index_collection_handle = $args['designer_blog_index_collection_handle'];
		$this->rewriteIndexFiles($this->default_index_collection_handle, $index_collection_handle);
		
		$pkg = parent::install();
		
		//save the installed url handle so we can restore it when the addon is updated
		BlogConfig::setIndexCollectionHandle($index_collection_handle);
		
		//Frontend Page:
		$blog_index_c = $this->addSinglePage($pkg, "/{$index_collection_handle}", t('Blog'));
		BlogConfig::setIndexCollectionID($blog_index_c->getCollectionID());
		
		//Frontend Blocktype:
		$this->getOrInstallBlockType($pkg, 'designer_blog_recent_posts');
		
		//Dashboard Page:
		$this->getOrAddSinglePage($pkg, '/dashboard/system/designer_blog', t('Designer Blog')); //top-level pleaceholder
		$this->getOrAddSinglePage($pkg, '/dashboard/system/designer_blog/settings', t('Blog Settings'));
		
		//More config settings:
		BlogConfig::setDefaultPostCount(10);
		BlogConfig::setAuthorsGroupID(0);
		BlogConfig::setClassificationsAttributeSetID(0);
		BlogConfig::setContentAreaHandle(t('Main'));
		BlogConfig::setTeaserCharLength(250);
		BlogConfig::setTeaserOverrideAkHandle('');
		BlogConfig::setThumbnailAkHandle('');
		BlogConfig::setAuthorDisplayNameAkHandle('');
		BlogConfig::setRssFeedTitle(SITE . ' ' . t('Blog'));
	}
	
	public function uninstall() {
		//Change the index page filenames and class declaration back to default
		// (so re-installing the addon in the future doesn't break).
		$this->rewriteIndexFiles(BlogConfig::getIndexCollectionHandle(), $this->default_index_collection_handle);
		parent::uninstall();
	}
	
	///////////////////////////////////////////////////////////////////////////
	
	private function validateInstallArgs($args) {
		$error_msg = '';
		$handle = empty($args['designer_blog_index_collection_handle']) ? '' : $args['designer_blog_index_collection_handle'];
		if (empty($handle)) {
			$error_msg = t('URL Slug is required.');
		} else if (!preg_match('/^[a-z][a-z0-9_-]*$/', $handle)) {
			$error_msg = t('URL Slug must start with a lowercase letter, and may only contain lowercase letters, numbers, dashes and underscores.');
		} else if (Page::getByPath("/{$handle}")->getCollectionID()) {
			$error_msg = t('A page already exists at the top-level of your site with that URL Slug.<br>You must either delete that page or enter a different URL Slug.');
		} else if (($handle != $this->default_index_collection_handle) && !$this->canWriteIndexFiles()) {
			$error_msg = t('The package directory is not writable on your server, *OR* the single_page controller/view files have already been renamed from a prior install.<br><br>In order to have a custom URL Slug, you must modify permissions on your server so that the "/packages/designer_blog/single_pages" and "/packages/designer_blog/controllers" directories are writable.<br><br>If the permissions are set correctly, ensure that the file names have not already been changed from the default (and if they have, change them back).<br><br>If you are unable to change these permissions, then you must use the default URL Slug ("%s").', $this->default_index_collection_handle);
		}

		if ($error_msg) {
			throw new Exception($error_msg);
		}
	}
	
	private function canWriteIndexFiles() {
		$pkg_dir = DIR_PACKAGES . '/designer_blog';
		$single_pages_dir = "{$pkg_dir}/single_pages";
		$single_page_file = "{$single_pages_dir}/{$this->default_index_collection_handle}.php";
		$controllers_dir = "{$pkg_dir}/controllers";
		$controller_file = "{$controllers_dir}/{$this->default_index_collection_handle}.php";
		
		return (is_writable($single_pages_dir) && is_writable($single_page_file) && is_writable($controllers_dir) && is_writable($controller_file));
	}
	
	private function rewriteIndexFiles($from_handle, $to_handle) {
		if ($from_handle == $to_handle) {
			return;
		}
		
		$pkg_dir = DIR_PACKAGES . '/designer_blog';
		$single_page_from_path = "{$pkg_dir}/single_pages/{$from_handle}.php";
		$single_page_to_path = "{$pkg_dir}/single_pages/{$to_handle}.php";
		$controller_from_path = "{$pkg_dir}/controllers/{$from_handle}.php";
		$controller_to_path = "{$pkg_dir}/controllers/{$to_handle}.php";
		$camelcase_from_handle = Object::camelcase($from_handle);
		$camelcase_to_handle = Object::camelcase($to_handle);
		$controller_from_class_declaration = "class {$camelcase_from_handle}Controller extends Controller";
		$controller_to_class_declaration = "class {$camelcase_to_handle}Controller extends Controller";
		
		$result = rename($single_page_from_path, $single_page_to_path);
		if ($result === false) {
			throw new Exception(t('Designer Blog Installation/Upgrade Error: Could not rename single_page file "%" to "%s".', $single_page_from_path, $single_page_to_path));
		}
		
		$result = rename($controller_from_path, $controller_to_path);
		if ($result === false) {
			throw new Exception(t('Designer Blog Installation/Upgrade Error: Could not rename controller file "%" to "%s".', $controller_from_path, $controller_to_path));
		}
		
		$result = file_put_contents($controller_to_path, str_replace($controller_from_class_declaration, $controller_to_class_declaration, file_get_contents($controller_to_path)));
		if ($result === false) {
			throw new Exception(t('Designer Blog Installation/Upgrade Error: Could not change class declaration in file "%s".', $controller_to_path));
		}
	}
	
	
/*** EVENT HANDLERS ***/
	public static function on_start() {
		if (!defined('ENABLE_APPLICATION_EVENTS')) { define('ENABLE_APPLICATION_EVENTS', true); } //<--REQUIRED IN 5.5+ (despite what c5 docs say!!)
		Events::extend('on_page_add', 'DesignerBlogEventHandlers', 'force_single_sublevel', 'packages/designer_blog/libraries/event_handlers.php');
	}
	
/*** UTILITY FUNCTIONS ***/
	private function pageExistsAtPath($path) {
		$page = Page::getByPath($path);
		if ($page->isError() && ($page->getError() == COLLECTION_NOT_FOUND)) {
			return false;
		} else {
			return true;
		}
	}
	
	private function addSinglePage($pkg, $cPath, $cName = '', $cDescription = '') {
		Loader::model('single_page');
		
		$sp = SinglePage::add($cPath, $pkg);
		
		if (!is_null($sp)) { //SinglePage::add() returns null if page already exists
			//Set page title and/or description...
			$data = array();
			if (!empty($cName)) {
				$data['cName'] = $cName;
			}
			if (!empty($cDescription)) {
				$data['cDescription'] = $cDescription;
			}
			
			if (!empty($data)) {
				$sp->update($data);
			}
		}
		
		return $sp;
	}
	
	private function getOrAddSinglePage($pkg, $cPath, $cName = '', $cDescription = '') {
		Loader::model('single_page');

		$sp = SinglePage::add($cPath, $pkg);

		if (is_null($sp)) {
			//SinglePage::add() returns null if page already exists
			$sp = Page::getByPath($cPath);
		} else {
			//Set page title and/or description...
			$data = array();
			if (!empty($cName)) {
				$data['cName'] = $cName;
			}
			if (!empty($cDescription)) {
				$data['cDescription'] = $cDescription;
			}

			if (!empty($data)) {
				$sp->update($data);
			}
		}

		return $sp;
	}
	
	private function getOrInstallBlockType($pkg, $btHandle) {
		$bt = BlockType::getByHandle($btHandle);
		if (empty($bt)) {
			BlockType::installBlockTypeFromPackage($btHandle, $pkg);
			$bt = BlockType::getByHandle($btHandle);
		}
		return $bt;
	}
	
}