<?php defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('blog', 'designer_blog');

class DesignerBlogEventHandlers {
	
	//"Force Single Sublevel" (from http://www.concrete5.org/marketplace/addons/force-single-sublevel)...
	// If grandparent page has 'force_single_sublevel' attribute, move this page so it's a child of the grandparent (sibling of its current parent).
	//
	// Our algorithm is "prevent new grandchildren of pages having 'force_single_sublevel'=true",
	//  so ignore the attribute on parents, great-grandparents, etc.
	//  (e.g. if someone added a sub-sub-page before setting the attribute, it remains in place, and can have children of its own)
	public function force_single_sublevel($page) {
		$parentPage = Page::getByID($page->getCollectionParentID());
		//Bail if parent is home page (because we have no grandparent to check)
		if ($parentPage->getCollectionID() == HOME_CID) {
			return;
		}

		$grandparentPage = Page::getByID($parentPage->getCollectionParentID());
		if ($grandparentPage->getCollectionID() == BlogConfig::getIndexCollectionID()) {
			$page->move($grandparentPage);
			
			//Move page to bottom of new parent's display order (the above call to ->move() doesn't change its display order #)
			$db = Loader::db();
			$sql = "SELECT MAX(cDisplayOrder) FROM Pages WHERE cParentID = ?";
			$args = array($grandparentPage->getCollectionID());
			$max = $db->GetOne($sql, $args);
			$max++;
			$page->updateDisplayOrder($max, $page->getCollectionID());
		}
	}

}