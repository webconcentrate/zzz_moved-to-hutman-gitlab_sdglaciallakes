<?php defined('C5_EXECUTE') or die(_("Access Denied."));

$dh = Loader::helper('concrete/dashboard');
$ih = Loader::helper('concrete/interface');
$th = Loader::helper('text');

echo $dh->getDashboardPaneHeaderWrapper(t('Search Terms Report'), false, 'span6 offset3'); ?>
	
	<form action="<?=$this->action('view')?>" method="get" class="period-filter form-inline">
		<label for="period"><?php echo t('Show:'); ?></label>
		<?php echo Loader::helper('form')->select('period', $period_options, $period_selection); ?>
		<noscript><input type="submit" class="btn ccm-input-submit" value="Go"></noscript>
		<span class="loading-indicator" style="display:none; vertical-align: middle; padding-left: 5px;"><img src="<?php echo ASSETS_URL_IMAGES; ?>/throbber_white_16.gif" width="16" height="16" alt="loading..." /></span>
	</form>
	<script>/* auto-submit form on dropdown change: */ $(document).ready(function() { $('.period-filter select').on('change', function() { $(this).closest('form').trigger('submit').find('.loading-indicator').show(); }); });</script>
	
	<hr>
	
	<table class="table table-striped">
		<thead>
			<tr>
				<th><?php echo t('Search Terms'); ?></th>
				<th><?php echo t('Count'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($report as $item): ?>
			<tr>
				<td><?php echo $th->entities($item['terms']); ?></td>
				<td><?php echo $item['count']; ?></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	
<?php echo $dh->getDashboardPaneFooterWrapper(); ?>
