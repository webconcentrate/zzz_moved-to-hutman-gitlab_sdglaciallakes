<?php defined('C5_EXECUTE') or die(_("Access Denied."));

class WcSearchTermsReportPackage extends Package {

	protected $pkgHandle = 'wc_search_terms_report';
	public function getPackageName() { return t('Search Terms Report'); }
	public function getPackageDescription() { return t('Logs search terms and displays them in a dashboard report.'); }
	protected $appVersionRequired = '5.6';
	protected $pkgVersion = '1.0';
	
	public function on_start() {
		Environment::get()->overrideCoreByPackage('blocks/search/controller.php', $this);
	}
	
	public function install() {
		$pkg = parent::install();
		$this->installOrUpgrade($pkg);
	}
	
	public function upgrade() {
		$this->installOrUpgrade($this);
		parent::upgrade();
	}
	
	private function installOrUpgrade($pkg) {
		$c = $this->getOrAddSinglePage($pkg, '/dashboard/reports/wc_search_terms', t('Search Terms'));
		$this->setPageDashboardIcon($c, 'icon-search');
	}
	
/*** Utility Functions ***/
	private function getOrAddSinglePage($pkg, $cPath, $cName = '', $cDescription = '') {
		Loader::model('single_page');

		$sp = SinglePage::add($cPath, $pkg);

		if (is_null($sp)) {
			//SinglePage::add() returns null if page already exists
			$sp = Page::getByPath($cPath);
		} else {
			//Set page title and/or description...
			$data = array();
			if (!empty($cName)) {
				$data['cName'] = $cName;
			}
			if (!empty($cDescription)) {
				$data['cDescription'] = $cDescription;
			}

			if (!empty($data)) {
				$sp->update($data);
			}
		}

		return $sp;
	}
	
	public function setPageDashboardIcon($c, $icon_glyph_name) {
		$cak = CollectionAttributeKey::getByHandle('icon_dashboard');
		if (is_object($cak)) {
			$c->setAttribute('icon_dashboard', $icon_glyph_name);
		}
	}
}
