<?php defined('C5_EXECUTE') or die(_("Access Denied."));

class DashboardReportsWcSearchTermsController extends Controller {
	
	public function view() {
		
		$period_options = array(
			1 => t('Past 30 days'),
			2 => t('Past 60 days'),
			3 => t('Past 90 days'),
			6 => t('Past 6 months'),
			12 => t('Past year'),
			0 => t('All-Time'),
		);
		$this->set('period_options', $period_options);
		
		$period_selection = isset($_GET['period']) ? (int)$_GET['period'] : 1;
		if (!array_key_exists($period_selection, $period_options)) {
			$period_selection = 1;
		}
		$this->set('period_selection', $period_selection);
		
		Loader::model('search_terms', 'wc_search_terms_report');
		$report = SearchTerms::getReport($period_selection);
		$this->set('report', $report);
	}
	
}