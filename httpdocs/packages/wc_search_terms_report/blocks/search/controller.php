<?php defined('C5_EXECUTE') or die("Access Denied.");

class SearchBlockController extends Concrete5_Controller_Block_Search {
	function do_search() {
		parent::do_search();
		
		if (!empty($_REQUEST['query'])) {
			Loader::model('search_terms', 'wc_search_terms_report');
			SearchTerms::log($_REQUEST['query']);
		}
	}
}