<?php defined('C5_EXECUTE') or die("Access Denied.");

class SearchTerms {
	
	public static function log($search_terms) {
		$submitted_at = date('Y-m-d H:i:s');
		$ip_address = Loader::helper('validation/ip')->getRequestIP();
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		$u = new User;
		$uID = $u->getUserID();
		
		$data = array(
			'search_terms' => $search_terms,
			'submitted_at' => $submitted_at,
			'ip_address' => $ip_address,
			'user_agent' => $user_agent,
			'uID' => $uID,
		);
		Loader::db()->AutoExecute('wc_search_terms_log', $data, 'INSERT');
	}
	
	//Pass in 1 to get search terms for the past 30 days,
	// 2 for past 60 days, 3 for past 90 days, etc.
	//Or pass in NULL/empty/0 to get an "all-time" count
	public static function getReport($past_periods = 1) {
		$sql = 'SELECT COUNT(*) as count, TRIM(LOWER(search_terms)) as terms'
		     . ' FROM wc_search_terms_log';
		$vals = array();
		
		if (!empty($past_periods)) {
			$sql .= ' WHERE FLOOR(DATEDIFF(CURDATE(), submitted_at) / 30) < ?';
			$vals[] = (int)$past_periods;
		}
		
		$sql .= ' GROUP BY terms ORDER BY count DESC';
		
		return Loader::db()->GetArray($sql, $vals);
	}
}
