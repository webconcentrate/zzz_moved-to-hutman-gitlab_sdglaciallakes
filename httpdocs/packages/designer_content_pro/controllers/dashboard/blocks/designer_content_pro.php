<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/***************************************************************************
 * Copyright (C) Web Concentrate - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Written by Jordan Lev <jordan@webconcentrate.com>, January-August 2013
 ***************************************************************************/


Loader::model('dcp_block_type', 'designer_content_pro');

Loader::library('crud_controller', 'designer_content_pro');
class DashboardBlocksDesignerContentProController extends CrudController {
	
	public function on_before_render() {
		$this->addHeaderItem(Loader::helper('html')->css('dashboard.css', 'designer_content_pro'));
	}
	
	public function view() {
		try {
			$bts = DcpBlockTypeModel::getAllBlockTypes();
			$container_package_exists = true;
		} catch (Exception $e) {
			$bts = array();
			$container_package_exists = false;
		}
		
		$this->set('bts', $bts);
		$this->set('container_package_exists', $container_package_exists);
	}
	
	public function add() {
		$this->edit(null);
	}
	
	public function edit($original_btHandle = null) {
		$is_new = empty($original_btHandle);
		
		if ($is_new) {
			$btHandle = null;
			$btName = null;
			$btDescription = null;
			$single_fields = array();
			$repeating_item_fields = array();
			$block_options = array(
				'help_text_block_header' => '',
				'help_text_block_middle' => '',
				'help_text_block_footer' => '',
				'help_text_repeating_item_header' => '',
				'help_text_repeating_item_footer' => '',
			);
		} else {
			//load existing record data from database...
			$btInfo = DcpBlockTypeModel::getBlockTypeInfoByHandle($original_btHandle);
			if (is_null($btInfo)) {
				$this->render404AndExit(); //btHandle not found
			} else if ($btInfo->hasCustomController) {
				$this->flash(t('This blocktype cannot be edited in the dashboard because it has a controller override.'), 'error');
				$this->redirect('view');
			}
			$btHandle = $btInfo->btHandle;
			$btName = $btInfo->btName;
			$btDescription = $btInfo->btDescription;
			$single_fields = DcpBlockTypeModel::getSingleFields($btHandle);
			$single_fields = $this->convertSelectListOptionsToText($single_fields); //hack to work around annoying javascript problems
			$repeating_item_fields = DcpBlockTypeModel::getRepeatingItemFields($btHandle);
			$repeating_item_fields = $this->convertSelectListOptionsToText($repeating_item_fields); //hack to work around annoying javascript problems
			$block_options = $btInfo->btDCProBlockOptions;
		}
		
		$this->set('is_new', $is_new);
		$this->set('is_installed', DcpBlockTypeModel::isBlockTypeInstalled($original_btHandle));
		
		$this->set('can_write_site_blocks', DcpBlockTypeModel::isCustomViewsDirWritable());
		$this->set('can_write_pkg_blocks', DcpBlockTypeModel::isPkgBlocksDirWritable());
		
		list($field_type_labels, $field_type_options) = DcpBlockTypeModel::getAvailableFieldTypeLabelsAndOptions();
		$this->set('field_type_labels', $field_type_labels);
		$this->set('field_type_options', $field_type_options);
		
		$this->set('btHandle', $btHandle);
		$this->set('original_btHandle', $original_btHandle);
		$this->set('btName', $btName);
		$this->set('btDescription', $btDescription);
		$this->set('single_fields', $single_fields);
		$this->set('repeating_item_fields', $repeating_item_fields);
		$this->set('block_options', $block_options);
		
		$hh = Loader::helper('html');
		$this->addFooterItem($hh->javascript('jquery.tmpl.min.js', 'designer_content_pro'));
		$this->addFooterItem($hh->javascript('jquery.blockUI.js', 'designer_content_pro'));
		$this->addFooterItem($hh->javascript('dashboard_edit_model.js', 'designer_content_pro'));
		$this->addFooterItem($hh->javascript('dashboard_edit_ui.js', 'designer_content_pro'));
		$this->addHeaderItem($hh->css('tipsy.css', 'designer_content_pro'));
		$this->addFooterItem($hh->javascript('jquery.tipsy.js', 'designer_content_pro'));
		
		$this->render('edit');
	}
	
	private function convertSelectListOptionsToText($fields) {
		//Browsers do not loop through object key/val pairs in the order they were declared
		// when the keys are combinations of empty string, numeric, and alphanumeric
		// (as is the case with "select options" list choice handles).
		//So convert "select options" list choices to their textual representation here in php
		// instead of in the javascript template (where it would ideally belong)...
		foreach ($fields as $field_handle => $field) {
			foreach ($field['options'] as $option_handle => $option) {
				if ($option_handle == 'select_options') {
					$list_choices = array();
					foreach ($option as $list_choice_handle => $list_choice_label) {
						$list_choices[] = "{$list_choice_handle}: {$list_choice_label}";
					}
					$fields[$field_handle]['options'][$option_handle] = implode("\n", $list_choices);
				}
			}
		}
		return $fields;
	}
	
	/** DEV NOTE about comparing new data to "original" data:
	 *
	 * Some validation rules and most confirm messages require that we compare
	 * new data to the "original" data for a particular field.
	 *
	 * Note that we do *not* retrieve the existing field definitions from the block controller class
	 * to compare it to new POSTed data -- instead we rely on front-end javascript
	 * to store the original data when the page is first loaded and then POST
	 * that back to the ajax_validate() and ajax_confirm() methods.
	 *
	 * The reason we do it this way is because the field handle could be changed due to a user edit,
	 * so we need to be able to differentiate between a field handle change versus the deletion
	 * of a field and then an addition of a different field that happens to have the same handle
	 * (and other edge cases).
	 */
	public function ajax_validate() {
		$post = $this->post();
		if (empty($post)) {
			exit;
		}
		
		$errors = array();
		$th = Loader::helper('text');
		$e = DcpBlockTypeModel::validate($post);
		foreach ($e->getList() as $key => $value) {
			$errors[$key] = $th->entities($value);
		}
		
		$response = array(
			'success' => !$e->has(),
			'errors' => $errors,
		);
		
		echo Loader::helper('json')->encode($response);
		exit;
	}
	
	public function ajax_confirm() {
		$post = $this->post();
		if (empty($post)) {
			exit;
		}
		
		$is_new = empty($post['orig_btHandle']);
		
		$response = array( 'isNew' => $is_new );
		if (!$is_new) {
			$response['installed'] = DcpBlockTypeModel::getBlockTypeInfoByHandle($post['orig_btHandle'])->installed;
			$response['usage'] = DcpBlockTypeModel::getPageUsage($post['orig_btHandle']);
			$response['changes'] = DcpBlockTypeChangeReport::getList($post);
		}
		
		echo Loader::helper('json')->encode($response);
		exit;
	}
	
	public function ajax_save() {
		//Upon success, we set a flash message (assuming that the front-end
		// will redirect back to the top-level 'view' action for us) and output nothing.
		//Upon failure, we output the error message.
		//(So you can check for an empty response to determine if the save was successful)
		
		$post = $this->post();
		if (empty($post)) {
			echo t('ERROR: Could not save because no data was sent');
			exit;
		}
		
		//run validation again just to be sure
		$error = DcpBlockTypeModel::validate($post);
		if ($error->has()) {
			echo t('ERROR: Could not save because data was invalid');
			exit;
		}
		
		$btInfo = DcpBlockTypeModel::save($post);
		if (empty($post['orig_btHandle'])) {
			DcpBlockTypeModel::install($btInfo->btHandle);
			$msg = t('New Block Type created and installed.');
		} else {
			$msg = t('Block Type changes have been saved.');
		}
		$this->flash($msg, 'success');
		exit;
	}
	
	public function ajax_show_sample_code($btHandle) {
		$single_fields = DcpBlockTypeModel::getSingleFields($btHandle);
		$repeating_item_fields = DcpBlockTypeModel::getRepeatingItemFields($btHandle);

		$lines = array();
		$th = Loader::helper('text');
		$php_tag = '<' . '?' . 'p' . 'h' . 'p' . ' '; //outsmart c5 marketplace process that inserts extra spaces after every php opening tag
		
		if (!empty($single_fields)) {
			$line = '<!-- ' . t('single fields') . ' -->';
			$lines[] = $th->entities($line);
		}
		
		foreach ($single_fields as $handle => $field) {
			$line = $php_tag;
			if ($field['type'] == 'checkbox') {
				$line .= 'if ($' . $handle . '->isChecked()) { /* ' . t('do something...') . ' */ } ?>';
			} else {
				$line .= 'echo $' . $handle . '; ?>';
			}
			$lines[] = $th->entities($line);
		}
		
		if (!empty($repeating_item_fields)) {
			if (!empty($single_fields)) {
				$lines[] = '';
			}
			
			$line = '<!-- ' . t('repeating item fields') . ' -->';
			$lines[] = $th->entities($line);
			
			$line = $php_tag . 'foreach ($controller->getRepeatingItems() as $item): ?>';
			$lines[] = $th->entities($line);
			
			foreach ($repeating_item_fields as $handle => $field) {
				$line = $php_tag;
				if ($field['type'] == 'checkbox') {
					$line .= 'if ($item->' . $handle . '->isChecked()) { /* ' . t('do something...') . ' */ } ?>';
				} else {
					$line .= 'echo $item->' . $handle . '; ?>';
				}
				$lines[] = '&nbsp;&nbsp;&nbsp;&nbsp;' . $th->entities($line);
			}

			$line = $php_tag . 'endforeach; ?>';
			$lines[] = $th->entities($line);
		}
		
		echo '<p>' . t("Paste this code into <code>SITEROOT/blocks/%s/view.php</code>", $btHandle) . '</p>';
		echo '<textarea style="-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; width:100%; height: 70%; font-family: monospace; white-space: nowrap; resize: none;">';
		echo implode("\n", $lines);
		echo '</textarea>';
		echo '<p>' . t('Visit %s for complete documentation.', '<a href="http://theblockery.com/dcp" target="_blank">http://theblockery.com/dcp</a>') . '</p>';
		exit;
	}
	
	public function refresh($btHandle) {
		//refresh block so C5 recognizes update btHandle/btDescription
		$btInfo = $this->validateAndGetBtInfo($btHandle);
		BlockType::getByID($btInfo->btID)->refresh();
		
		//Set flash message (one was probably set by the ajax_save method,
		// but now that we're redirecting it will get cleared...
		// but since refresh() is only called for edits of existing
		// blocktypes, we know which message it should be).
		$this->flash(t('Block Type changes have been saved.'), 'success');
		$this->redirect('view');
	}
	
	public function install($btHandle) {
		$btInfo = $this->validateAndGetBtInfo($btHandle);
		
		DcpBlockTypeModel::install($btHandle);
		
		$msg = t("Block type '%s' successfully installed.", $btInfo->btName);
		$this->flash($msg, 'success');
		$this->redirect('view');
	}
	
	public function uninstall($btHandle) {
		$btInfo = $this->validateAndGetBtInfo($btHandle);
		
		if ($this->post()) {
			DcpBlockTypeModel::uninstall($btHandle);
			$msg = t("Block type '%s' has been uninstalled.", $btInfo->btName);
			$this->flash($msg);
			$this->redirect('view');
		}
		
		$this->set('btInfo', $btInfo);
		$this->set('usage', DcpBlockTypeModel::getPageUsage($btHandle));
		$this->render('uninstall');
	}
		
	public function delete($btHandle) {
		$btInfo = $this->validateAndGetBtInfo($btHandle);
		
		if ($this->post()) {
			DcpBlockTypeModel::delete($btHandle);
			$msg = t("Block type '%s' has been permanently deleted.", $btInfo->btName);
			$this->flash($msg);
			$this->redirect('view');
		}
		
		$this->set('btInfo', $btInfo);
		$this->render('delete');
	}
	
	private function validateAndGetBtInfo($btHandle) {
		$btInfo = DcpBlockTypeModel::getBlockTypeInfoByHandle($btHandle);
		if (is_null($btInfo)) {
			$msg = t('ERROR: Cannot identify block handle');
			$this->flash($msg, 'error');
			$this->redirect('view');
		}
		return $btInfo;
	}
	
	public function generate_container_package() {
		if ($this->post()) {
			Loader::library('container_package_generator', 'designer_content_pro');
			$generator = new DesignerContentProContainerPackageGenerator;
			$generator->createAndInstallContainerPkg();
		}
		$this->redirect('view');
	}
	
}