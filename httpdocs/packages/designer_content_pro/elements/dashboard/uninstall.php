<?php defined('C5_EXECUTE') or die("Access Denied.");

$th = Loader::helper('text');

Loader::model('dcp_block_type', 'designer_content_pro');
$installed_blocktypes = DcpBlockTypeModel::getAllBlockTypes();

if (count($installed_blocktypes)) { ?>

	<h2><?php echo t('ALL of the custom blocktypes you created with Designer Content Pro will be uninstalled and their content will be removed from the pages of this site:'); ?></h2>

	<ul>
		<?php
		Loader::model('dcp_block_type', 'designer_content_pro');
		$installed_blocktypes = DcpBlockTypeModel::getAllBlockTypes();
		$th = Loader::helper('text');
		foreach ($installed_blocktypes as $bt):	?>
			<li><?php echo $th->entities($bt->btName); ?></li>
		<?php endforeach; ?>
	</ul>

<?php }