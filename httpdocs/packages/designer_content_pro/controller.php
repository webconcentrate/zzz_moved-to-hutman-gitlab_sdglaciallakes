<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/***************************************************************************
 * Copyright (C) Web Concentrate - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Written by Jordan Lev <jordan@webconcentrate.com>, January-August 2013
 ***************************************************************************/


class DesignerContentProPackage extends Package {

	protected $pkgHandle = 'designer_content_pro';
	public function getPackageName() { return t('Designer Content Pro'); }
	public function getPackageDescription() { return t('Quickly create and manage custom content blocktypes.'); }
	protected $appVersionRequired = '5.6.0';
	protected $pkgVersion = '2.1';
	
	public function install() {
		$pkg = parent::install();
		$this->installOrUpgrade($pkg);
	}
	
	public function upgrade() {
		$this->installOrUpgrade($this);
		parent::upgrade();
		
		//v2.1 (upgrades from earlier versions only... no need to run this on fresh installs of the addon)
		$db = Loader::db();
		$container_pkg_id = (int)$db->getOne('SELECT pkgID FROM Packages WHERE pkgHandle = "designer_content_pro_blocks"');
		if ($container_pkg_id) {
			$btHandles = $db->GetCol('SELECT btHandle FROM BlockTypes WHERE pkgID = ?', array($container_pkg_id));
			if ($btHandles) {
				Loader::library('block_generator', 'designer_content_pro');
				$generator = new DesignerContentProBlockGenerator;
				foreach ($btHandles as $btHandle) {
					$generator->createPkgToolsLabelLookupIfNotExists($btHandle);
				}
			}
		}
	}
	
	private function installOrUpgrade($pkg) {
		//v1.0
		$c = $this->getOrAddSinglePage($pkg, '/dashboard/blocks/designer_content_pro', t('Designer Content Pro'));
		$this->setPageDashboardIcon($c, 'icon-plus-sign');
		
		//v1.1
		Loader::library('container_package_generator', 'designer_content_pro');
		$generator = new DesignerContentProContainerPackageGenerator;
		$generator->createAndInstallContainerPkg();
	}
	
	public function uninstall() {
		Loader::library('container_package_generator', 'designer_content_pro');
		$generator = new DesignerContentProContainerPackageGenerator;
		$generator->uninstallContainerPkg();
		
		parent::uninstall();
	}
	
	
/*** Utility Functions ***/
	private function getOrAddSinglePage($pkg, $cPath, $cName = '', $cDescription = '') {
		Loader::model('single_page');

		$sp = SinglePage::add($cPath, $pkg);

		if (is_null($sp)) {
			//SinglePage::add() returns null if page already exists
			$sp = Page::getByPath($cPath);
		} else {
			//Set page title and/or description...
			$data = array();
			if (!empty($cName)) {
				$data['cName'] = $cName;
			}
			if (!empty($cDescription)) {
				$data['cDescription'] = $cDescription;
			}

			if (!empty($data)) {
				$sp->update($data);
			}
		}

		return $sp;
	}
	
	private function setPageDashboardIcon($c, $icon_glyph_name) {
		$cak = CollectionAttributeKey::getByHandle('icon_dashboard');
		if (is_object($cak)) {
			$c->setAttribute('icon_dashboard', $icon_glyph_name);
		}
	}
}