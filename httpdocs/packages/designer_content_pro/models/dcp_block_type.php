<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/***************************************************************************
 * Copyright (C) Web Concentrate - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Written by Jordan Lev <jordan@webconcentrate.com>, January-August 2013
 ***************************************************************************/

define('DCP_CONTAINER_PACKAGE_HANDLE', 'designer_content_pro_blocks');

class DcpBlockTypeModel {
	
	public static $available_field_type_handles = array(
		'textbox',
		'textarea',
		'wysiwyg',
		'image',
		'file',
		'fileset',
		'link',
		'page',
		'select',
		'checkbox',
		'date',
		'sdglmemberssubcategory',
		'sdglmemberslocation',
	);
	
	//Returns TWO arrays, so call it like this:
	// list($labels, $options) = getAvailableFieldTypeLabelsAndOptions()
	public static function getAvailableFieldTypeLabelsAndOptions() {
		$labels = array();
		$options = array();
		
		Loader::library('dcp_field_types', 'designer_content_pro');
		foreach (self::$available_field_type_handles as $type_handle) {
			$class = 'DcpFieldType_' . ucfirst(strtolower($type_handle));
			if (!class_exists($class)) {
				throw new Exception(t('DESIGNER CONTENT PRO ERROR: Cannot find class definition "%s" for field type "%s"', $class, $type_handle));
			}
			$labels[$type_handle] = call_user_func(array($class, 'getGeneratorLabel')); //I think this is the only way to call a static method on a variable class
			$options[$type_handle] = call_user_func(array($class, 'getGeneratorOptions')); //ditto
		}
		
		return array($labels, $options);
	}
	
	//Returns array of all blocktypes created with
	// Designer Content Pro (both installed AND uninstalled)
	public static function getAllBlockTypes() {
		$bts = array();
		$pkg_blocks_dir = self::getPkgBlocksDir();
		
		if (is_dir($pkg_blocks_dir)) {
			$file_handle = opendir($pkg_blocks_dir);
			while(($btHandle = readdir($file_handle)) !== false) { //block's folder name *is* the handle
				if (strpos($btHandle, '.') === false) {
					$bt = self::getBlockTypeInfoByHandle($btHandle);
					if (!is_null($bt)) {
						$bts[$btHandle] = $bt;
					}
				}				
			}
		}
		
		return $bts;
	}
	
	public static function getBlockTypeInfoByHandle($btHandle) {
		//Check that block files actually exist
		//(Note that we must manually check for block overrides
		// otherwise we could get a fatal error due to class re-declarations!)
		$override_controller_path = self::getCustomViewControllerPath($btHandle);
		$package_controller_path = self::getPkgBlockControllerPath($btHandle);
		$hasCustomController = false;
		if (file_exists($override_controller_path)) {
			$controller_path = $override_controller_path;
			$hasCustomController = true;
		} else if (file_exists($package_controller_path)) {
			$controller_path = $package_controller_path;
		} else {
			return null;
		}
		
		//DEV NOTE: much of the following logic was copied from
		// Concrete5_Model_BlockTypeList::getAvailableList()
		// (from core version 5.6.0.2).
		$bt = new BlockType;
		$bt->btHandle = $btHandle;
		$bt->pkgID = Package::getByHandle(DCP_CONTAINER_PACKAGE_HANDLE)->getPackageID(); //<--we must do this otherwise the next line fails (because it looks in the wrong place)
		$class = $bt->getBlockTypeClass();
		
		require_once($controller_path);
		if (!class_exists($class)) {
			return null;
		}
		$bta = new $class;
		$bt->btName = $bta->getBlockTypeName();
		$bt->btDescription = $bta->getBlockTypeDescription();
		$bt->hasCustomViewTemplate = file_exists(DIR_FILES_BLOCK_TYPES . '/' . $btHandle . '/' . FILENAME_BLOCK_VIEW);
		$bt->hasCustomEditTemplate = file_exists(DIR_FILES_BLOCK_TYPES . '/' . $btHandle . '/' . FILENAME_BLOCK_EDIT);
		$bt->hasCustomAddTemplate = file_exists(DIR_FILES_BLOCK_TYPES . '/' . $btHandle . '/' . FILENAME_BLOCK_ADD);
		$bt->hasCustomController = $hasCustomController; //<--this line is specific to DCP (not copied from core logic)
		
		$btID = Loader::db()->GetOne("select btID from BlockTypes where btHandle = ?", array($btHandle));
		$bt->installed = ($btID > 0);
		$bt->btID = $btID;

		$bt->btDCProBlockOptions = self::getBlockOptions($bta);
		
		return $bt;
	}
	private static function getBlockOptions($btObj) {
		//versions of DCP prior to 2.0 did not have this property, so check for its existence
		$options = (property_exists($btObj, 'btDCProBlockOptions') ? $btObj->btDCProBlockOptions : array());
		
		//since more options might be added in future versions of DCP, set default values for all missing options
		if (!array_key_exists('help_text_block_header', $options)) {
			$options[] = 'help_text_block_header';
		}
		if (!array_key_exists('help_text_block_middle', $options)) {
			$options[] = 'help_text_block_middle';
		}
		if (!array_key_exists('help_text_block_footer', $options)) {
			$options[] = 'help_text_block_footer';
		}
		if (!array_key_exists('help_text_repeating_item_header', $options)) {
			$options[] = 'help_text_repeating_item_header';
		}
		if (!array_key_exists('help_text_repeating_item_footer', $options)) {
			$options[] = 'help_text_repeating_item_footer';
		}
		
		return $options;
	}
	
	public static function isBlockTypeInstalled($btHandle) {
		return !empty($btHandle) && self::getBlockTypeInfoByHandle($btHandle)->installed;
	}
	
	public static function isPkgBlocksDirWritable() {
		return is_writable(self::getPkgBlocksDir());
	}
	
	public static function getPkgBlocksDir() {
		$pkg = Package::getByHandle(DCP_CONTAINER_PACKAGE_HANDLE);
		if (is_null($pkg) || !is_object($pkg)) {
			$pkg = Loader::package(DCP_CONTAINER_PACKAGE_HANDLE);
			if (is_null($pkg) || !is_object($pkg)) {
				throw new Exception(t('Designer Content Pro Error: The storage package does not exist on the server.'));
			} else {
				throw new Exception(t('Designer Content Pro Error: The storage package is not installed.'));
			}
		}
		
		$dir = $pkg->getPackagePath() . '/' . DIRNAME_BLOCKS;
		return $dir;
	}
	
	public static function getPkgBlockDir($btHandle) {
		return self::getPkgBlocksDir() . '/' . $btHandle;
	}
	
	public static function getPkgBlockControllerPath($btHandle) {
		return self::getPkgBlockDir($btHandle) . '/' . FILENAME_BLOCK_CONTROLLER;
	}
	
	public static function isCustomViewsDirWritable() {
		return is_writable(DIR_FILES_BLOCK_TYPES);
	}
	
	public static function getCustomViewDir($btHandle) {
		return DIR_FILES_BLOCK_TYPES . '/' . $btHandle;
	}
	public static function getCustomViewTemplatePath($btHandle) {
		return self::getCustomViewDir($btHandle) . '/' . FILENAME_BLOCK_VIEW;
	}
	
	public static function getCustomViewControllerPath($btHandle) {
		return self::getCustomViewDir($btHandle) . '/' . FILENAME_BLOCK_CONTROLLER;
	}
	
	public static function getBlockTypeDatabaseTableName($btHandle) {
		return 'bt' . Loader::helper('text')->camelcase($btHandle);
	}
	
	public static function getBlockTypeControllerName($btHandle) {
		return Loader::helper('text')->camelcase($btHandle) . 'BlockController';
	}
	
	public static function getSingleFields($btHandle) {
		return self::getFields(true, $btHandle);
	}
	public static function getRepeatingItemFields($btHandle) {
		return self::getFields(false, $btHandle);
	}
	private static function getFields($single_vs_repeating, $btHandle) {
		//Dev note:
		// If the blocktype isn't installed,
		// an error occurs when trying to instantiate the controller class below.
		// (This happens when generating "sample code" in the dashboard list.)
		//Ideally we'd solve this by calling Loader::block($btHandle) first,
		// but that causes an error as well.
		//The solution is to make a call to getBlockTypeInfoByHandle($btHandle)
		// -- we don't actually use the info it returns, but just calling it
		// has the happy side-effect of loading the controller file
		// (as Loader::blocK() should do, but doesn't in this case)
		// which then allows us to successfully instantiate the blocktype controller.
		self::getBlockTypeInfoByHandle($btHandle);
		
		$btClass = self::getBlockTypeControllerName($btHandle);
		$btInstance = new $btClass;
		return $single_vs_repeating ? $btInstance->btDCProSingleFields : $btInstance->btDCProRepeatingItemFields;
	}
	
	public static function validate($data) {
		$e = Loader::helper('validation/error');
		
		//pkg and blocks dirs must be writable
		if (!self::isPkgBlocksDirWritable()) {
			$e->add(t("Warning: The blocks directory in the storage package ('%s') is not writeable. Blocks types cannot be created or edited until permissions are changed on your server.", self::getPkgBlocksDir()));
		}
		if (!self::isCustomViewsDirWritable()) {
			$e->add(t("Your site's top-level /blocks/ directory is not writeable. Blocks types cannot be created or edited until permissions are changed on your server."));
		}
		
		$is_installed = empty($data['orig_btHandle']) ? false : self::isBlockTypeInstalled($data['orig_btHandle']);
		
		//handle is required
		if (empty($data['btHandle'])) {
			$e->add(t('BlockType Handle is required.'));
		//handle maxlen
		} else if (strlen($data['btHandle']) > 32) {
			$e->add(t('BlockType Handle cannot exceed 32 characters in length.'));
		//handle must be handle-like
		} else if (!preg_match('/^[a-z_]+$/', $data['btHandle'])) {
			$e->add(t('BlockType Handle can only contain lowercase letters and underscores'));
		//handle cannot be changed on installed blocktypes
		} else if ($is_installed && ($data['btHandle'] !== $data['orig_btHandle'])) {
			$e->add(t('BlockType Handle cannot be changed while the BlockType is installed.'));
		//new/changed handle must not already be in use (in wider c5 system, or in our blocks dir?)
		} else if (!self::validateBtHandleUniqueness($data['btHandle'], $data['orig_btHandle'])) {
			$e->add(t('BlockType Handle is already in use by another block type. Note that the other block type might be installed or uninstalled, and it might be a Designer Content Pro block or it might be a "normal" C5 block.'));
		//no changes are allowed if an override controller.php file exists
		} else if (file_exists(self::getCustomViewControllerPath($data['btHandle']))) {
			$e->add(t("No changes can be made because a custom controller override exists in '%s'", self::getCustomViewControllerPath($data['btHandle'])));
		}
		
		//name is required
		if (empty($data['btName'])) {
			$e->add(t('BlockType Name is required.'));
		//name maxlen
		} else if (strlen($data['btName']) > 128) {
			$e->add(t('BlockType Name cannot exceed 128 characters in length.'));
		//name cannot have quotation marks (due to C5 js bug -- it's okay to have them but then they don't get displayed properly in the "add blocks" dialog)
		} else if (strpos($data['btName'], '"') !== false) {
			//bug was fixed in 5.6.2.1
			if (version_compare(APP_VERSION, '5.6.2.1', '<')) {
				$e->add(t('BlockType Name cannot contain quotation marks (").'));
			}
		}
		
		//FIELDS...
		//initialize empty field arrays if they don't exist (because jquery $.ajax() doesn't send empty arrays)
		$data['single_fields'] = (empty($data['single_fields']) ? array() : $data['single_fields']);
		$data['orig_single_fields'] = (empty($data['orig_single_fields']) ? array() : $data['orig_single_fields']);
		$data['repeating_item_fields'] = (empty($data['repeating_item_fields']) ? array() : $data['repeating_item_fields']);
		$data['orig_repeating_item_fields'] = (empty($data['orig_repeating_item_fields']) ? array() : $data['orig_repeating_item_fields']);
		
		$combined_fields = array_merge($data['single_fields'], $data['repeating_item_fields']);
		
		if (!self::validateAtLeastOneField($combined_fields)) {
			$e->add(t('BlockType must have at least one field.'));
		}
		
		//field handles must be unique across both groups
		// (theoretically we could allow a single field handle to be the same as a repeating item field handle,
		// but that causes complications in the block edit form due to name collisions...
		// so instead of a complicated fix there we are just preventing it here).
		//Ignore empty fields in this check -- they will be caught in the per-field checks,
		//  and if we catch them here too it will be a confusing error message to the user.
		self::validateNonEmptyFieldHandleUniqueness($combined_fields, $e);
		
		//per-field validations
		self::validateFields(true, $data['single_fields'], $data['orig_single_fields'], $e);
		self::validateFields(false, $data['repeating_item_fields'], $data['orig_repeating_item_fields'], $e);
		
		return $e;
	}
	
	//Returns true (valid) if the given "new handle" isn't already in use by another blocktype.
	//Returns false (INvalid) if the given "new handle" IS already in use.
	//Note that we check both installed and not-installed blocktypes
	// in the normal C5 system and DC Pro (and we also check in the top-level /blocks/ directory).
	// The only place we don't check is not-installed blocktypes in other packages (which shouldn't cause problems).
	private static function validateBtHandleUniqueness($new_btHandle, $original_btHandle) {
		if (!is_null($original_btHandle) && ($new_btHandle === $original_btHandle)) {
			//handle wasn't changed, so this validation doesn't apply
			return true;
		}
		
		$bts_c5_not_installed = BlockTypeList::getAvailableList();
		$bts_c5_installed = BlockTypeList::getInstalledList();
		$bts_dcp_all = self::getAllBlockTypes();
		$bts_combined = array_merge($bts_c5_not_installed, $bts_c5_installed, $bts_dcp_all);
		
		$existing_btHandles = array();
		foreach ($bts_combined as $bt) {
			$existing_btHandles[] = $bt->btHandle;
		}
		
		//now check the top-level /blocks/ directory (just in case user has custom override code in there)
  		$toplevel_blocks_dir = @glob(DIR_FILES_BLOCK_TYPES . '/*');
		if ($toplevel_blocks_dir === false) { //glob should return an empty array if nothing is in the directory, but sometimes it returns FALSE for no apparent reason (has something to do with the php installation, or some combination of the php version with certain OS's???)
			$toplevel_blocks_dir = array();
		}
		foreach ($toplevel_blocks_dir as $dir) {
			$existing_btHandles[] = basename($dir);
		}
		
		return !in_array($new_btHandle, $existing_btHandles);
	}
	
	private static function validateAtLeastOneField($fields) {
		$is_at_least_one_field = false;
		foreach ($fields as $field) {
			if (!$field['isDeleted']) {
				$is_at_least_one_field = true;
				break;
			}
		}
		return $is_at_least_one_field;
	}
	
	private static function validateNonEmptyFieldHandleUniqueness($fields, &$e) {
		$handles = array();
		$dups = array();
		foreach ($fields as $field) {
			if (!empty($field['handle'])) {
				if (!($field['isNew'] && $field['isDeleted'])) { //ignore fields that were added and then deleted (so user doesn't get "trapped" if they add a new field with a dup handle, sees the validation message that they cannot have dup handles, then delete that field to fix the validation problem)
					$handle = $field['handle'];
					if (in_array($handle, $handles)) {
						if (!in_array($handle, $dups)) {
							$dups[] = $handle;
						}
					} else {
						$handles[] = $handle;
					}
				}
			}
		}
		
		foreach ($dups as $dup) {
			$e->add(t("Field handle '%s' is used more than once. Each field must have a unique handle.", $dup));
		}
	}

	private static function validateFields($single_vs_repeating, $fields, $orig_fields, &$e) {
		//error message prefix, so user knows which group of fields the error is in
		$group_label = ($single_vs_repeating ? t('single') : t('repeating item'));
		
		//make orig_fields easily fetchable via id so we can compare data between current field and its original data
		$unkeyed_orig_fields = $orig_fields;
		$orig_fields = array();
		foreach ($unkeyed_orig_fields as $field) {
			$orig_fields[$field['id']] = $field;
		}
		
		$row_num = 1; //for error messages (so we can tell user which row has a problem)
		foreach ($fields as $field) {
			if ($field['isDeleted']) {
				continue; //ignore this field -- it has been deleted by the user
			}
			
			$ordinal = ($row_num == 1) ? t('1st') : (($row_num == 2) ? t('2nd') : (($row_num == 3) ? t('3rd') : $row_num . t('th')));
			$row_num++;
			
			$orig_field = empty($orig_fields[$field['id']]) ? null : $orig_fields[$field['id']];
			$is_new_field = is_null($orig_field);
			
			//field handle required
			if (empty($field['handle'])) {
				$e->add(t('%s %s field is missing a handle.', $ordinal, $group_label));
			//field handle handle-ness (a bit more lenient than btHandle, because this "handle" becomes an array key or object member name -- not a class name like btHandle)
			} else if (!preg_match('/^[a-zA-Z][a-zA-Z0-9_]*$/', $field['handle'])) {
				$e->add(t('%s %s field\'s handle contains invalid character(s) (it must start with a letter, and can only contain letters, numbers, and underscores)', $ordinal, $group_label));
			//field handle reserved names (only applies to single fields because we set those as variables in the view and hence need to avoid collisions with existing variables that are already set by C5)
			} else if ($single_vs_repeating && !self::validateSingleFieldHandleIsNotC5BlockViewVarName($field['handle'])) {
				$e->add(t('%s %s field\'s handle cannot be used because it conflicts with a C5 system variable name.', $ordinal, $group_label));
			//field handles cannot be changed on installed blocktypes
			} else if ($is_installed && !$is_new_field && ($field['handle'] != $orig_field['handle'])) {
				$e->add(t('%s %s field\'s handle cannot be changed because the block type is currently installed.', $ordinal, $group_label));
			}
		
			//field type is required
			if (empty($field['type'])) {
				$e->add(t('%s %s field is missing a type.', $ordinal, $group_label));
			//field type must be valid (in the list of available types)
			} else if (!in_array($field['type'], self::$available_field_type_handles)) {
				$e->add(t('%s %s field\'s type is invalid -- you must choose from the dropdown list.', $ordinal, $group_label)); //not sure how this would happen, but just in case
			//field type cannot be changed on installed blocktypes
			} else if ($is_installed && !$is_new_field && ($field['type'] != $orig_field['type'])) {
				$e->add(t('%s %s field\'s type cannot be changed because the block type is currently installed.', $ordinal, $group_label));
			}
		
			//field label is required
			if (empty($field['label'])) {
				$e->add(t('%s %s field is missing a label.', $ordinal, $group_label));
			}
			
			//select list options validation
			if (!empty($field['type']) && ($field['type'] == 'select')) {
				$select_option_errors = self::validateSelectFieldOptionsText($field['options']['select_options']);
				foreach ($select_option_errors as $select_option_error) {
					$e->add(t('%s %s field: %s', $ordinal, $group_label, $select_option_error));
				}
			}
		}
	}
	
	private static function validateSelectFieldOptionsText($text) {
		$errors = array();
		
		$text = trim($text);
		$text = str_replace("\r\n", "\n", $text);
		$text = str_replace("\r", "\n", $text);
		$lines = explode("\n", $text);
		if (empty($lines) || (count($lines) == 1 && empty($lines[0]))) {
			$errors[] = t('You must enter at least one select list option.');
		} else {
			$unique_handles = array(); //will be filled with valid handles as we loop through the list, so we can identify duplicates
			foreach ($lines as $i => $line) {
				$line_num = ($i + 1);
				$parts = explode(':', $line, 2);
				if (count($parts) < 2) {
					$errors[] = t('Missing a colon in line #%d of select list options.', $line_num);
				} else {
					$handle = trim($parts[0]);
					if (!empty($handle) && !preg_match('/^[a-zA-Z0-9_]+$/', $handle)) {
						$errors[] = t('Invalid handle "%s" in line #%d of select list options. (Only letters, numbers, and underscores are allowed.)', $handle, $line_num);
					} else if (in_array($handle, $unique_handles)) {
						$errors[] = t('Duplicate handle "%s" in line #%d of select list options.', $handle, $line_num);
					} else {
						$unique_handles[] = $handle;
					}
				}
			}
		}

		return $errors;
	}
	private static function validateSingleFieldHandleIsNotC5BlockViewVarName($field_handle) {
		/* Paste this code into a block view to see what variables C5 sets in block views:
			<div style="border: 2px solid black;"><pre><code>
				<p>Concrete5 version <?php echo APP_VERSION; ?></p>
				<?php print_r(array_keys(get_defined_vars())); ?>
			</code></pre></div>
		*/
		$c5_block_view_var_names = array(
			'obj',
			'view',
			'args',
			'customAreaTemplates',
			'base',
			'bFilename',
			'b',
			'btHandle',
			'_action',
			'u',
			'outputContent',
			'useCache',
			'page',
			'_dbat',
			'_table',
			'_tableat',
			'_where',
			'_saved',
			'_lasterr',
			'_original',
			'foreignName',
			'lockMode', /* <-- this one was introduced in 5.6.2 */
			'bID',
			'controller',
			'form',
			'headerItems',
			'_filename',
			'bvt',
			'template',
			'header',
			'footer',
			'bc',
			'blockStyle',
			'singleFieldData', /* <-- this one is defined in the db.xml file of all DCP blocks (and hence automatically sent to the view by C5) */
			'repeatingItemData', /* <-- this one is defined in the db.xml file of all DCP blocks (and hence automatically sent to the view by C5) */
		);
		
		return !in_array($field_handle, $c5_block_view_var_names);
	}
	//It is assumed you have already validated the text before calling this function!
	private static function convertSelectFieldOptionsFromTextToArray($validated_text) {
		$text = trim($validated_text);
		$text = str_replace("\r\n", "\n", $text);
		$text = str_replace("\r", "\n", $text);
		$lines = explode("\n", $text);
		$options = array();
		foreach ($lines as $line) {
			$parts = explode(':', $line, 2);
			$handle = trim($parts[0]);
			$label = trim($parts[1]);
			$options[$handle] = $label;
		}
		return $options;
	}
	public static function convertSelectFieldOptionsFromArrayToText($options) {
		$lines = array();
		$th = Loader::helper('text');
		foreach ($options as $handle => $label) {
			$lines[] = $handle . ': ' . $th->entities($label);
		}
		return implode("\n", $lines);
	}
	
	
	public static function getPageUsage($btHandle) {
		$db = Loader::db();
		$sql = "SELECT cv.cID, cv.cvName, cv.cvIsApproved, p.cIsTemplate, p.cIsSystemPage, p2.cFilename AS parent_cFilename, COUNT(*) as block_count, COUNT(*) AS count"
		     . " FROM BlockTypes bt"
		     . " INNER JOIN Blocks b on bt.btID = b.btID"
		     . " INNER JOIN CollectionVersionBlocks cvb ON b.bID = cvb.bID"
		     . " INNER JOIN CollectionVersions cv ON cv.cvID = cvb.cvID AND cv.cID = cvb.cID"
		     . " INNER JOIN Pages p ON cv.cID = p.cID"
		     . " LEFT JOIN Pages p2 ON p.cParentID = p2.cID"
		     . " WHERE bt.btHandle = ?"
		     . " GROUP BY cv.cID, cv.cvName, cv.cvIsApproved, p.cIsTemplate,  p.cIsSystemPage"
		     . " ORDER BY cv.cID ASC";
		$vals = array($btHandle);
		$records = $db->GetArray($sql, $vals);
		
		$usage = array();
		$th = Loader::helper('text');
		foreach ($records as $record) {
			$cID = $record['cID'];
			
			if (!array_key_exists($cID, $usage)) {
				if ($record['parent_cFilename'] == '/!stacks/view.php') {
					$special = t('Stack');
				} else if ($record['parent_cFilename'] == '/!trash/view.php') {
					$special = t('Trash');
				} else if ($record['parent_cFilename'] == '/dashboard/view.php') {
					$special = t('News Flow');
				} else if (!empty($record['cIsTemplate'])) {
					$special = t('Page Type Defaults Template');
				} else if (!empty($record['cIsSystemPage'])) {
					$special = t('System Page');
				} else {
					$special = '';
				}
			
				$usage[$cID] = array(
					'name' => $th->entities($record['cvName']),
					'special' => $th->entities($special),
					'approved_count' => 0,
					'unapproved_count' => 0,
				);
			}
			
			$count_key = $record['cvIsApproved'] ? 'approved_count' : 'unapproved_count';
			$usage[$cID][$count_key] += $record['block_count'];
		}
		
		return $usage;
	}
		
	public static function save($data) {
		Loader::library('block_generator', 'designer_content_pro');
		$generator = new DesignerContentProBlockGenerator;
		
		$is_new = empty($data['orig_btHandle']);
		$is_installed = $is_new ? false : self::isBlockTypeInstalled($data['orig_btHandle']);
		
		//Gather blocktype data
		$btHandle = $is_installed ? $data['orig_btHandle'] : $data['btHandle']; //this is probably redundant (because validation should prevent btHandle changes on installed blocktypes), but we want to be extra safe.
		$btName = $data['btName'];
		$btDescription = $data['btDescription'];
		$btTable = self::getBlockTypeDatabaseTableName($btHandle);
		$block_options = $data['block_options'];
		
		//initialize empty field arrays if they don't exist (because jquery $.ajax() doesn't send empty arrays)
		$data['single_fields'] = (empty($data['single_fields']) ? array() : $data['single_fields']);
		$data['repeating_item_fields'] = (empty($data['repeating_item_fields']) ? array() : $data['repeating_item_fields']);
		
		//Gather repeating item field data
		list($field_type_labels, $field_type_options) = self::getAvailableFieldTypeLabelsAndOptions(); //we only care about $field_type_options
		$single_fields = self::collateFieldsForSave($data['single_fields'], $field_type_options);
		$repeating_item_fields = self::collateFieldsForSave($data['repeating_item_fields'], $field_type_options);
		
		//Write files
		if ($is_installed) {
			//This is an edit to an installed blocktype.
			//We want to minimize destruction on this, so only overwrite the files that need to be changed
			
			//If the table doesn't have a `singleFieldData` field
			// (which indicates this block was originally made with DCP v1.x),
			// update db.xml to include it and then tell C5 to refresh the schema.
			//BUT WATCH OUT: If the table's existing `repeatingItemData` field
			// is of type LONGTEXT (as was the case prior to DCP v1.2.3)
			// as opposed to LONGBLOB (which v1.2.3+), then make sure it stays that way
			// because converting existing TEXT data to BLOB will mess it up!
			$has_single_field_data = false;
			$repeating_item_data_is_text = false;
			$cols = Loader::db()->MetaColumns($btTable);
			foreach ($cols as $col) {
				if ($col->name == 'singleFieldData') {
					$has_single_field_data = true;
					continue;
				} else if ($col->name == 'repeatingItemData') {
					$repeating_item_data_is_text = (strtolower(substr($col->type, -4)) === 'text');
				}
			}
			if (!$has_single_field_data) {
				$generator->updatePkgBlockTypeDbXml($btHandle, $btTable, $repeating_item_data_is_text);
				BlockType::getByHandle($btHandle)->refresh();
			}
			
			//now that database has been taken care of, update the controller...
			$generator->updatePkgBlockTypeController($btHandle, $btName, $btDescription, $btTable, $single_fields, $repeating_item_fields, $block_options);
		} else {
			//This is either a brand new blocktype, or an edit to an existing uninstalled one.
			//For existing uninstalled blocktypes, just delete everything and recreate it
			// with the new info (since uninstalling a blocktype deletes all of its page placement
			// and saved content data, there is no harm in wiping everything out and recreating it).
			//The only exception to this is for the custom block view dir (in the site's top-level /blocks/
			// directory) -- this we want to keep (or rename if btHandle was changed).

			if (!$is_new) {
				//delete everything (except the custom block view dir) -- it will all be recreated in subsequent steps
				self::delete($data['orig_btHandle'], false);
			}
			
			//create blocktype files in package /blocks/ directory
			$generator->createPkgBlockType($btHandle, $btName, $btDescription, $btTable, $single_fields, $repeating_item_fields, $block_options);
			
			//create "view override" file in site's top-level /blocks/ directory
			if ($is_new) {
				$generator->createOverrideView($btHandle);
			//...or move the existing one if this was an existing blocktype that had its handle changed...
			} else if ($data['btHandle'] != $data['orig_btHandle']) {
				$old_dir = self::getCustomViewDir($data['orig_btHandle']);
				$new_dir = self::getCustomViewDir($data['btHandle']);
				
				//just to be safe, don't do anything if a custom block view dir already exists for the new handle
				if (!is_dir($new_dir)) {
					//IF the old dir exists, rename it to the new one...
					if (is_dir($old_dir)) {
						rename($old_dir, $new_dir);
					//BUT if the old dir didn't exist for some reason, just create a new one from scratch
					} else {
						$generator->createOverrideView($btHandle);
					}
				}
			}
			//end "view override" stuff
				
		}
		
		return self::getBlockTypeInfoByHandle($btHandle);
	}
	//Internal helper function for save(), which returns an array of non-deleted fields sorted by display order
	private static function collateFieldsForSave($fields, $field_type_options) {
		$collated_fields = array();
		
		foreach ($fields as $field) {
			if (!$field['isDeleted']) {
				$collated_fields[$field['displayOrder']] = array(
					'handle' => $field['handle'],
					'type' => $field['type'],
					'label' => $field['label'],
					'options' => self::massageOptionsForSave($field, $field_type_options),
				);
			}
		}
		
		ksort($collated_fields);
		$collated_fields = array_values($collated_fields); //we don't need the keys now that we've sorted the array
		
		return $collated_fields;
	}
	//Internal helper function for save(), which massages certain option types into the desired and consistent format:
	// * For "checkbox" option types, convert "1" to boolean true, and don't write anything at all for falsey values
	// * For "select options" of "select" field types, convert the handle:label\nhandle:label\netc. text format to a proper array
	//
	//Note that the $field_type_options arg should be the 2nd result of getAvailableFieldTypeLabelsAndOptions()
	// (passing it in is an optimization, so the caller can retrieve that info one time and pass it back here as needed).
	private static function massageOptionsForSave(&$field, &$field_type_options) {
		if (empty($field['options'])) {
			return array();
		}
		
		$options_info = $field_type_options[$field['type']];
		
		$return_options = array();
		foreach ($field['options'] as $option_key => $option_value) {
			if ($options_info[$option_key]['type'] == 'checkbox') {
				if (!empty($option_value)) {
					$return_options[$option_key] = true;
				}
			} else if ($options_info[$option_key]['type'] == 'list_choices') {
				$return_options[$option_key] = self::convertSelectFieldOptionsFromTextToArray($option_value);
			} else {
				$return_options[$option_key] = $option_value;
			}
		}
		
		return $return_options;
	}
	
	public static function install($btHandle) {
		$btInfo = self::getBlockTypeInfoByHandle($btHandle);
		if (is_null($btInfo)) {
			return;
		}
		
		$pkg = Package::getByHandle(DCP_CONTAINER_PACKAGE_HANDLE);
		Loader::library('dcp_controller', 'designer_content_pro'); //must include this here, otherwise next line fails
		BlockType::installBlockTypeFromPackage($btHandle, $pkg);
	}
	
	public static function uninstall($btHandle) {
		if (!self::isBlockTypeInstalled($btHandle)) {
			return; //already uninstalled, so do nothing
		}
		
		//Uninstall
		BlockType::getByHandle($btHandle)->delete();
		
		//Delete its primary (db.xml) table too
		// (even though it means losing all entered data)
		//BECAUSE: users can do things to block definitions when it's not-installed
		// that they otherwise couldn't do when it is installed [e.g. change field handle])
		//SO: if they uninstall a block, change its definitions, then re-install it...
		//     we don't want leftover weird data hanging around!
		//(BESIDES... C5 will remove the association of that block with its areas anyway,
		//  so there's no realistic way for users to get their data back anyway).
		$sql = 'DROP TABLE ' . self::getBlockTypeDatabaseTableName($btHandle);
		Loader::db()->Execute($sql);
	}
	
	public static function delete($btHandle, $delete_custom_view_dir = true) {
		self::uninstall($btHandle);
		
		if ($delete_custom_view_dir) {
			self::rrmdir(self::getCustomViewDir($btHandle));
		}
		
		self::rrmdir(self::getPkgBlockDir($btHandle));
	}
	
		private function rrmdir($dir) {
			//Use c5's file helper (if available) to work within marketplace guidelines
			if (version_compare(APP_VERSION, '5.6.1', '<')) {
			    self::recursivelyRemoveDirectory($dir);
			} else {
				Loader::helper('file')->removeAll($dir);
			}
		}
		
		//This is an exact copy of the "removeAll" function from the C5 core 'file' helper in 5.6.2.1
		private function recursivelyRemoveDirectory($source) {
			$r = @glob($source);
			if (is_array($r)) {
				foreach($r as $file) {
					if (is_dir($file)) {
						self::recursivelyRemoveDirectory("$file/*");
						rmdir($file);
					} else {
						unlink($file);
					}
				}
			}
		}
	//END delete() helpers
}

//This class generates a detailed report of changes that will happen
// when the given data (which was POSTed by the user) is saved.
// (The code in here is a bit messy -- it is isolated into its own class
// in preparation for a future refactoring.)
class DcpBlockTypeChangeReport {
	
	//Returns an array, each item representing a change.
	//
	//Each "change" item is itself an array, with 2 elements:
	// -'impact': denotes if the change is "safe" in terms of data loss, etc.
	//            Possible values are:
	//            -'safe': this change results in no data loss and no file changes
	//            -'data': this change may result in data loss
	//            -'files': this change will result in files and/or directories being renamed on the server
	//            -'': empty string is used in lieu of 'safe' and 'data' when the blocktype isn't installed
	// -'desc': human-readable HTML string that explains the change.
	// -'note': human-readable HTML string with secondary info for user (usually about the side-effects of the change).
	//          The note will often be an empty string, but the description is always provided.
	// -'reindex': boolean, TRUE if we recommend a re-index of the search engine due to this change (otherwise FALSE)
	//
	//Note that we only report on edits to existing blocktypes.
	// (If this is a brand new blocktype we return an empty array.)
	public function getList($data) {
		$ret = array();
		$th = Loader::helper('text');
		
		//initialize empty field arrays if they don't exist (because jquery $.ajax() doesn't send empty arrays)
		$data['single_fields'] = (empty($data['single_fields']) ? array() : $data['single_fields']);
		$data['orig_single_fields'] = (empty($data['orig_single_fields']) ? array() : $data['orig_single_fields']);
		$data['repeating_item_fields'] = (empty($data['repeating_item_fields']) ? array() : $data['repeating_item_fields']);
		$data['orig_repeating_item_fields'] = (empty($data['orig_repeating_item_fields']) ? array() : $data['orig_repeating_item_fields']);

		
		$changes = self::getChanges($data);
		$is_installed = $changes['is_new'] ? false : DcpBlockTypeModel::isBlockTypeInstalled($data['orig_btHandle']);
		
		//load up descriptive info about each field type and field option,
		// so we can give more human-readable messages (i.e. we won't have to display type/option handles)
		list($field_type_labels, $field_type_options) = DcpBlockTypeModel::getAvailableFieldTypeLabelsAndOptions();
		
		if (!empty($changes['btHandle'])) {
			$ret[] = array(
				'impact' => 'files', //unlike 'safe' and 'data' changes, files will be changed regardless of whether or not the blocktype is installed
				'desc' => t("Change <b>blocktype handle</b> to <code>%s</code>", $changes['btHandle']['to']),
				'note' => t("The block <b>template directory</b> will be renamed<br>from <code>%s</code> to <code>%s</code><br>(in <code>%s</code>)", $changes['btHandle']['from'], $changes['btHandle']['to'], DIR_FILES_BLOCK_TYPES),
				'reindex' => false,
			);
		}
		
		if (!empty($changes['btName'])) {
			$ret[] = array(
				'impact' => ($is_installed ? 'safe' : ''),
				'desc' => t("Change <b>blocktype name</b> to <code>%s</code>", $changes['btName']['to']),
				'note' => '',
				'reindex' => false,
			);
		}
		
		if (!empty($changes['btDescription'])) {
			//btDescription is an optional field, so display placeholder text (e.g. "<blank>")
			// when changing to or from an empty description.
			$from = empty($changes['btDescription']['from']) ? t('&lt;blank&gt;') : $changes['btDescription']['from'];
			$to = empty($changes['btDescription']['to']) ? t('&lt;blank&gt;') : $changes['btDescription']['to'];
			$ret[] = array(
				'impact' => ($is_installed ? 'safe' : ''),
				'desc' => t("Change <b>blocktype description</b> to <code>%s</code>", $to),
				'note' => '',
				'reindex' => false,
			);
		}
		
		if (!empty($changes['block_options'])) {
			$block_option_labels = array(
				'help_text_block_header' => t('Block Header Instructions / Help Text'),
				'help_text_block_middle' => t('Block Middle Instructions / Help Text'),
				'help_text_block_footer' => t('Block Footer Instructions / Help Text'),
				'help_text_repeating_item_header' => t('Repeating Item Header Instructions / Help Text'),
				'help_text_repeating_item_footer' => t('Repeating Item Footer Instructions / Help Text'),
			);
			foreach ($changes['block_options'] as $block_option_handle => $block_option_change) {
				if (empty($block_option_change['from'])) {
					$desc = t('Added');
				} else if (empty($block_option_change['to'])) {
					$desc = t('Removed');
				} else {
					$desc = t('Changed');
				}
				$desc .= " <b>{$block_option_labels[$block_option_handle]}</b>";
				$ret[] = array(
					'impact' => ($is_installed ? 'safe' : ''),
					'desc' => $desc,
					'note' => '',
					'reindex' => false,
				);
			}
		}
		
		//Since the per-field changes are grouped into two sets (single vs repeating),
		// we will create a temporary array with the appropriate group key and message label
		// so we don't need to double-up the code for each change type.
		$field_groups = array(
			'single_fields' => t('single'),
			'repeating_item_fields' => t('repeating item'),
		);
		
		foreach ($field_groups as $group_key => $group_label) {
			foreach ($changes[$group_key]['added'] as $field_handle) {
				$ret[] = array(
					'impact' => ($is_installed ? 'safe' : ''),
					'desc' => t("Added new %s field <b>%s</b>.", $group_label, $field_handle),
					'note' => t("Remember to update the block's view.php so this field gets displayed.<br>(But keep in mind that existing blocks will be missing data for this field until they are edited.)"),
					'reindex' => $is_installed,
				);
			}
		}
		
		foreach ($field_groups as $group_key => $group_label) {
			foreach ($changes[$group_key]['removed'] as $field_handle) {
				$note = t("<b>You must remove this field from the block's view.php file to prevent PHP errors!</b>");
				if ($is_installed) {
					$note = '<ol><li style="font-weight: bold;">'
					      .	t('This will cause permanent data loss for this field in existing blocks!')
					      . '</li><li style="font-weight: bold;">'
					      . $note
					      . '</li></ol>';
				}
				$ret[] = array(
					'impact' => ($is_installed ? 'data' : ''),
					'desc' => t("Removed existing %s field <b>%s</b>", $group_label, $field_handle),
					'note' => $note,
					'reindex' => $is_installed,
				);
			}
		}
		
		foreach ($field_groups as $group_key => $group_label) {
			foreach ($changes[$group_key]['handle'] as $change) {
				if (!is_null($change['from']) && !is_null($change['to'])) {
					$ret[] = array(
						'impact' => ($is_installed ? 'safe' : ''),
						'desc' => t("Changed <b>%s field handle</b> to <code>%s</code>", $group_label, $change['to']),
						'note' => t("Remember to update the code in the block's view.php file accordingly."),
						'reindex' => false,
					);
				}
			}
		}
		
		foreach ($field_groups as $group_key => $group_label) {
			foreach ($changes[$group_key]['type'] as $field_handle => $change) {
				if (!is_null($change['from']) && !is_null($change['to'])) {
					$type_label_from = $field_type_labels[$change['from']];
					$type_label_to = $field_type_labels[$change['to']];
					$ret[] = array(
						'impact' => ($is_installed ? 'safe' : ''),
						'desc' => t("<b>%s</b>: changed <b>%s field type</b> to <code>%s</code>", $field_handle, $group_label, $type_label_to),
						'note' => t("You may need to change code in the block's view.php file (depending on which functions you are using to display the field)."),
						'reindex' => false, //no need to reindex because field_type cannot be changed on installed blocktypes
					);
				}
			}
		}
		
		foreach ($field_groups as $group_key => $group_label) {
			foreach ($changes[$group_key]['label'] as $field_handle => $change) {
				if (!is_null($change['from']) && !is_null($change['to'])) {
					$ret[] = array(
						'impact' => ($is_installed ? 'safe' : ''),
						'desc' => t("<b>%s</b>: changed <b>%s field label</b> to <code>%s</code>", $field_handle, $group_label, $change['to']),
						'note' => '',
						'reindex' => false,
					);
				}
			}
		}
		
		//In preparation for field option changes, map field handles to their field types
		// (so we can get human-readable option info for each option handle).
		$field_types_per_handle = array(
			'single_fields' => array(),
			'repeating_item_fields' => array(),
		);
		foreach ($field_groups as $group_key => $group_label) {
			foreach ($data[$group_key] as $field) {
				$field_types_per_handle[$group_key][$field['handle']] = $field['type'];
			}
		}
		
		//Now go through the field option changes
		foreach ($field_groups as $group_key => $group_label) {
			foreach ($changes[$group_key]['options'] as $field_handle => $option_change) {
			
				if (!in_array($field_handle, $changes[$group_key]['added']) && !in_array($field_handle, $changes[$group_key]['removed'])) { //don't report option changes for new fields or deleted fields.
				
					$available_options_info = $field_type_options[$field_types_per_handle[$group_key][$field_handle]];
					foreach ($option_change as $option_handle => $change) {
						$option_info = $available_options_info[$option_handle];
					
						if (is_null($change['from'])) {
						
							$desc = t("%s field <b>%s</b>: set option <code>%s</code>", $group_label, $field_handle, $option_info['label']);
						
							if ($option_info['type'] == 'select') {
								$desc .= t(" to <code>%s</code>", $option_info['choices'][$change['to']]);
							} else if ($option_info['type'] == 'textarea') {
								$desc .= t(" to <code>%s</code>", nl2br($th->entities($change['to'])));
							} else if ($option_info['type'] == 'list_choices') {
								$to_value = nl2br("\n" . $th->entities($change['to']));
							} else if ($option_info['type'] == 'checkbox') {
								//for checkboxes, we'll just say "set option x on field y"
								// (as opposed to "set option x to true" or "set option x to yes")
								//so do nothing here.
							} else {
								$desc .= t(" to <code>%s</code>", $change['to']);
							}
						
						} else if (is_null($change['to'])) {
					
							$desc = t("%s field <b>%s</b>: cleared option <code>%s</code>", $group_label, $field_handle, $option_info['label']);
					
						} else {
						
							if ($option_info['type'] == 'select') {
								$from_value = $option_info['choices'][$change['from']];
							} else if ($option_info['type'] == 'textarea') {
								$from_value = nl2br($th->entities($change['from']));
							} else if ($option_info['type'] == 'list_choices') {
								$from_value = nl2br("\n" . $th->entities($change['from']));
							} else {
								$from_value = $change['from'];
							}
							$from_value = empty($from_value) ? t('&lt;blank&gt;') : $from_value;
						
							if ($option_info['type'] == 'select') {
								$to_value = $option_info['choices'][$change['to']];
							} else if ($option_info['type'] == 'textarea') {
								$to_value = nl2br($th->entities($change['to']));
							} else if ($option_info['type'] == 'list_choices') {
								$to_value = nl2br("\n" . $th->entities($change['to']));
							} else {
								$to_value = $change['to'];
							}
							$to_value = empty($to_value) ? t('&lt;blank&gt;') : $to_value;
						
							$desc = t("%s field <b>%s</b>: changed option <b>%s</b> to <code>%s</code>", $group_label, $field_handle, $option_info['label'], $to_value);
						}
					
						$note = '';
						if ($option_handle == 'required') {
							if (is_null($change['from'])) {
								$note = t('This field will now be required when users add/edit blocks, but blocks that existed before this change may still contain missing data.');
							} else if (is_null($change['to'])) {
								$note = t("This field will no longer be required. You may want to update your code in the block's view.php file if it assumes this field's data will always exist (for example, you may now want to check if the field is empty before displaying it).");
							}
						}
					
						$ret[] = array(
							'impact' => ($is_installed ? 'safe' : ''),
							'desc' => $desc,
							'note' => $note,
							'reindex' => ($is_installed && ($option_handle == 'searchable')),
						);
					}
				}
			}
		}
		
		foreach ($field_groups as $group_key => $group_label) {
			$display_order_changed = false;
			foreach ($changes[$group_key]['displayOrder'] as $change) {
				if (!is_null($change['from']) && !is_null($change['to'])) {
					$display_order_changed = true;
					break;
				}
			}
			if ($display_order_changed) {
				$ret[] = array(
					'impact' => ($is_installed ? 'safe' : ''),
					'desc' => t("Changed the <b>display order</b> of %s fields.", $group_label),
					'note' => t("Note that this only affects the block add/edit dialog (not the view template)."),
					'reindex' => false,
				);
			}
		}
		
		return $ret;
	}
	
	//Returns an array of information about what needs to be changed for a particular edit.
	//This info can be used by the getChangeMessages() function so it knows what to say.
	//
	//Historical Note: originally both the getChangeMessages() and save() functions used this info,
	// but now save() always just overwrites everything so it doesn't care what changed.
	//Since we already did the work to abstract this logic out, though, we're keeping this function here.
	//
	//Note that the array is structured with the change operation at the top-level,
	// then the relevant fields below that (because this is how the save() function
	// needed the data... back when the save() function actually called this).
	private static function getChanges($data) {
		$changes = array(
			'is_new' => false,
			'is_installed' => false, //this one doesn't pertain to whether data was changed or not, but it is useful to have
			'btHandle' => array(),
			'btName' => array(),
			'btDescription' => array(),
			'block_options' => array(),
			'single_fields' => array(
				'added' => array(),
				'removed' => array(),
				'handle' => array(),
				'type' => array(),
				'label' => array(),
				'options' => array(),
				'displayOrder' => array(),
			),
			'repeating_item_fields' => array(
				'added' => array(),
				'removed' => array(),
				'handle' => array(),
				'type' => array(),
				'label' => array(),
				'options' => array(),
				'displayOrder' => array(),
			),
		);
		
		$is_new = empty($data['orig_btHandle']);
		$changes['is_new'] = $is_new;
		
		$is_installed = $is_new ? false : DcpBlockTypeModel::isBlockTypeInstalled($data['orig_btHandle']);
		$changes['is_installed'] = $is_installed;
		
		//btHandle change (only valid for uninstalled blocktypes -- we assume validate() prevents this for installed blocktypes)
		if ($data['btHandle'] != $data['orig_btHandle']) {
			$changes['btHandle'] = array(
				'from' => $is_new ? null : $data['orig_btHandle'],
				'to' => $data['btHandle'],
			);
		}
		
		//name change
		if ($data['btName'] != $data['orig_btName']) {
			$changes['btName'] = array(
				'from' => $is_new ? null : $data['orig_btName'],
				'to' => $data['btName'],
			);
		}
		
		//description change
		if ($data['btDescription'] != $data['orig_btDescription']) {
			$changes['btDescription'] = array(
				'from' => $is_new ? null : $data['orig_btDescription'],
				'to' => $data['btDescription'],
			);
		}
		
		//block option changes
		foreach ($data['block_options'] as $block_option_handle => $block_option_value) {
			if ($block_option_value != $data['orig_block_options'][$block_option_handle]) {
				$changes['block_options'][$block_option_handle] = array(
					'from' => $is_new ? null : $data['orig_block_options'][$block_option_handle],
					'to' => $block_option_value,
				);
			}
		}
		
		//per-field repeating item changes
		self::getFieldChanges($data['single_fields'], $data['orig_single_fields'], $changes['single_fields']);
		self::getFieldChanges($data['repeating_item_fields'], $data['orig_repeating_item_fields'], $changes['repeating_item_fields']);
		
		return $changes;
	}
	
	private static function getFieldChanges($fields, $orig_fields, &$changes) {
		//make orig_fields easily fetchable via id so we can compare data between current field and its original data
		$unkeyed_orig_fields = $orig_fields;
		$orig_fields = array();
		foreach ($unkeyed_orig_fields as $field) {
			$orig_fields[$field['id']] = $field;
		}

		//now loop through the fields and denote changes...
		foreach ($fields as $field) {
			$orig_field = empty($orig_fields[$field['id']]) ? null : $orig_fields[$field['id']];
			
			$is_new_field = is_null($orig_field);
			$is_deleted_field = $field['isDeleted'];
			
			//skip over fields that were newly-added and then deleted prior to save
			if ($is_new_field && $is_deleted_field) {
				continue;
			}
			
			//added fields
			if ($is_new_field) {
				$changes['added'][] = $field['handle'];
				
			}
			
			//deleted fields
			if ($is_deleted_field) {
				$changes['removed'][] = $field['handle'];
			}
			
			//edited field handles (only valid for uninstalled blocktypes -- we assume validate() prevents this for installed blocktypes)
			if ($is_new_field || ($orig_field['handle'] != $field['handle'])) {
				$changes['handle'][] = array(
					'from' => $is_new_field ? null : $orig_field['handle'],
					'to' => $is_deleted_field ? null : $field['handle'],
				);
			}
			
			//edited field types (only valid for uninstalled blocktypes -- we assume validate() prevents this for installed blocktypes)
			if ($is_new_field || ($orig_field['type'] != $field['type'])) {
				$changes['type'][$field['handle']] = array(
					'from' => $is_new_field ? null : $orig_field['type'],
					'to' => $is_deleted_field ? null : $field['type'],
				);
			}
			
			//edited field labels (no prob!)
			if ($is_new_field || ($orig_field['label'] != $field['label'])) {
				$changes['label'][$field['handle']] = array(
					'from' => $is_new_field ? null : $orig_field['label'],
					'to' => $is_deleted_field ? null : $field['label'],
				);
			}
			
			//changed display order
			if ($is_new_field || ($orig_field['displayOrder'] != $field['displayOrder'])) {
				$changes['displayOrder'][$field['handle']] = array(
					'from' => $is_new_field ? null : $orig_field['displayOrder'],
					'to' => $is_deleted_field ? null : $field['displayOrder'],
				);
			}
			
			//edited field options:
			$options = empty($field['options']) ? array() : $field['options'];
			$orig_options = empty($orig_field['options']) ? array() : $orig_field['options'];
			foreach ($options as $option_handle => $value) {
				if (!array_key_exists($option_handle, $orig_options)) {
					$changes['options'][$field['handle']][$option_handle] = array(
						'from' => null,
						'to' => $value,
					);
				} else if ($orig_options[$option_handle] != $options[$option_handle]) {
					$changes['options'][$field['handle']][$option_handle] = array(
						'from' => $orig_options[$option_handle],
						'to' => $options[$option_handle],
					);
				}
			}
			foreach ($orig_options as $option_handle => $value) {
				if (!array_key_exists($option_handle, $options)) {
					$changes['options'][$field['handle']][$option_handle] = array(
						'from' => $option_handle,
						'to' => null,
					);
				}
			}
		}

	}
	
}