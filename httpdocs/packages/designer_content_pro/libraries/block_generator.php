<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/***************************************************************************
 * Copyright (C) Web Concentrate - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Written by Jordan Lev <jordan@webconcentrate.com>, January-August 2013
 ***************************************************************************/


Loader::model('dcp_block_type', 'designer_content_pro');

class DesignerContentProBlockGenerator {

	public function createPkgBlockType($btHandle, $btName, $btDescription, $btTable, $single_fields, $repeating_item_fields, $block_options) {
		$dir = $this->createPkgDir($btHandle);
		$this->createPkgAddPhp($dir);
		$this->createPkgControllerPhp($dir, $btHandle, $btName, $btDescription, $btTable, $single_fields, $repeating_item_fields, $block_options);
		$this->createPkgDbXml($dir, $btTable);
		$this->createPkgEditPhp($dir);
		$this->createPkgTools($dir);
		$this->createPkgViewPhp($dir, $btHandle);
	}
	
	public function createOverrideView($btHandle) {
		$dir = $this->createOverrideDir($btHandle);
		$this->createOverrideViewPhp($dir);
	}
	
	public function updatePkgBlockTypeController($btHandle, $btName, $btDescription, $btTable, $single_fields, $repeating_item_fields, $block_options) {
		$dir = DcpBlockTypeModel::getPkgBlockDir($btHandle);
		$this->createPkgControllerPhp($dir, $btHandle, $btName, $btDescription, $btTable, $single_fields, $repeating_item_fields, $block_options);
		//Note that we don't delete the existing file first,
		// because this class's outputFile() function uses
		// file_put_contents() (which overwrites if the file already exists).
	}

	public function updatePkgBlockTypeDbXml($btHandle, $btTable, $use_text_type_for_repeating_item_data) {
		$dir = DcpBlockTypeModel::getPkgBlockDir($btHandle);
		$this->createPkgDbXml($dir, $btTable, $use_text_type_for_repeating_item_data);
	}
	
	public function createPkgToolsLabelLookupIfNotExists($btHandle) {
		$dir = DcpBlockTypeModel::getPkgBlockDir($btHandle);
		$tools_dir = $dir . '/tools';
		$this->createPkgToolsLabelLookup($tools_dir);
	}

/*** GENERATOR FUNCTIONS ***/
	private function createPkgDir($btHandle) {
		$dir = DcpBlockTypeModel::getPkgBlockDir($btHandle);
		$this->outputDir($dir);
		return $dir;
	}
	
	private function createOverrideDir($btHandle) {
		$dir = DcpBlockTypeModel::getCustomViewDir($btHandle);
		$this->outputDir($dir, true);
		return $dir;
	}
	
	private function createPkgAddPhp($dir) {
		$contents = $this->getC5Header() . PHP_EOL . PHP_EOL
		          . $this->getHeaderComments() . PHP_EOL . PHP_EOL
		          . '$this->inc(\'edit.php\');' . PHP_EOL;
		$path = $dir . '/add.php';
		$this->outputFile($path, $contents);
	}
	
	private function createPkgControllerPhp($dir, $btHandle, $btName, $btDescription, $btTable, $single_fields, $repeating_item_fields, $block_options) {
		$escaped_btName = $this->esc($btName);
		$escaped_btDescription = $this->esc($btDescription);
		$escaped_btTable = $this->esc($btTable);
		$escaped_dcp_version = $this->esc(Package::getByHandle('designer_content_pro')->getPackageVersion());
		
		$contents = $this->getC5Header()
		          . PHP_EOL . PHP_EOL
		          . $this->getHeaderComments(false, true, $btHandle)
		          . PHP_EOL . PHP_EOL
		          . 'Loader::library(\'dcp_controller\', \'designer_content_pro\');'
		          . PHP_EOL . PHP_EOL
		          . 'class ' . DcpBlockTypeModel::getBlockTypeControllerName($btHandle) . ' extends DcpController {'
		          . PHP_EOL . PHP_EOL
		          . "\tprotected \$btHandle = '{$btHandle}';\n"
		          . "\tprotected \$btName = '{$escaped_btName}';\n"
		          . "\tprotected \$btDescription = '{$escaped_btDescription}';\n"
		          . "\tprotected \$btTable = '{$escaped_btTable}';\n"
		          . PHP_EOL
		          . "\tprotected \$btCacheBlockRecord = true;\n"
		          . "\tprotected \$btCacheBlockOutput = true;\n"
		          . "\tprotected \$btCacheBlockOutputOnPost = true;\n"
		          . "\tprotected \$btCacheBlockOutputForRegisteredUsers = false;\n"
		          . "\tprotected \$btCacheBlockOutputLifetime = CACHE_LIFETIME;\n"
		          . PHP_EOL
		          . "\tpublic \$btDCProGeneratorVersion = '{$escaped_dcp_version}'; //" . t("Don't change this!") . "\n"
		          . PHP_EOL
		          ;
		
		$contents .= "\tpublic \$btDCProBlockOptions = array(";
		if (!empty($block_options)) {
			$contents .= "\n" . $this->getControllerBlockOptionDefinitions($block_options) . "\t";
		}
		$contents .= ");\n\n";
		
		$contents .= "\tpublic \$btDCProSingleFields = array(";
		if (!empty($single_fields)) {
			$contents .= "\n" . $this->getControllerFieldDefinitions($single_fields) . "\t";
		}
		$contents .= ");\n\n";
		
		$contents .= "\tpublic \$btDCProRepeatingItemFields = array(";
		if (!empty($repeating_item_fields)) {
			$contents .= "\n" . $this->getControllerFieldDefinitions($repeating_item_fields) . "\t";
		}
		$contents .= ");\n\n";
		
		$contents .= "}\n";
		
		$path = $dir . '/controller.php';
		$this->outputFile($path, $contents);
	}
	private function getControllerFieldDefinitions($fields) {
		$contents = '';
		foreach ($fields as $field) {
			$escaped_label = $this->esc($field['label']);
			$contents .= "\t\t'{$field['handle']}' => array("
			           . "'type' => '{$field['type']}'"
			           . ", 'label' => '{$escaped_label}'"
			           . ", 'options' => array("
			           ;
			
			$option_strings = array();
			foreach ($field['options'] as $key => $val) {
				if ($val === true) { //if we don't do this then booleans TRUE gets written as string '1'
					$option_strings[] = "'{$key}' => true";
				} else if ($val === false) { //if we don't do this then booleans FALSE gets written as string '0'
					$option_strings[] = "'{$key}' => false";
				} else if (is_array($val)) {
					$escaped_option_keyvals = array();
					foreach ($val as $subkey => $subval) {
						$escaped_option_keyvals[] = "'" . $this->esc($subkey) . "' => '" . $this->esc($subval) . "'";
					}
					$option_strings[] = "'{$key}' => array(" . implode(', ', $escaped_option_keyvals) . ")";
				} else if (strpos($val, "\n") !== false) {
					$escaped_option_value = $this->esc_double_quotes_and_newlines($val);
					$option_strings[] = "'{$key}' => \"{$escaped_option_value}\"";
				} else {
					$escaped_option_value = $this->esc($val);
					$option_strings[] = "'{$key}' => '{$escaped_option_value}'";
				}
			}
			$contents .= implode(', ', $option_strings); //yes this is extra anal-retentive :)
			$contents .= ")),\n";
		}
		return $contents;
	}
	private function getControllerBlockOptionDefinitions($block_options) {
		$contents = '';
		foreach ($block_options as $option_handle => $option_value) {
			$escaped_option_value = $this->esc_double_quotes_and_newlines($option_value);
			$contents .= "\t\t'{$option_handle}' => \"{$escaped_option_value}\",\n";
		}
		return $contents;
	}
	
	private function esc($str) {
		$str = str_replace('\\', '\\\\', $str);
		$str = str_replace("'", "\\'", $str);
		return $str;
	}
	
	private function esc_double_quotes_and_newlines($str) {
		$str = str_replace('\\', '\\\\', $str);
		$str = str_replace('"', '\\"', $str);
		$str = str_replace('$', '\\$', $str);
		$str = str_replace("\n", '\\n', $str);
		return $str;
	}
	
	//The $use_text_type_for_repeating_item_data arg is to maintain backwards-compatibility:
	// Prior to v1.2.3, we stored repeatingItemData as LONGTEXT but due to some edge cases
	// (when the db table encoding was set to latin/ascii (non-utf8) that caused problems
	// with php's serialize function) the field type was switched to LONGBLOB in v1.2.3.
	//Now as of v2.0 we will be updating the db schema of existing tables to add the singleFieldData
	// field, but we CANNOT change existing repeatingItemData from type "X2" to "B"
	// because it will cause all existing data to be un-redable!
	private function createPkgDbXml($dir, $table, $use_text_type_for_repeating_item_data = false) {
		//NOTE: The C5 marketplace will blindly change all less-than-signs-followed-by-question-mark
		//      into opening php tags, so the characters must be separated below!
		$contents = '<' . '?' . 'xml version="1.0"?>' . PHP_EOL . PHP_EOL
		          . $this->getHeaderComments(true) . PHP_EOL . PHP_EOL
		          . '<schema version="0.3">' . PHP_EOL
		          . "\t" . '<table name="' . $table . '">' . PHP_EOL
		          . "\t\t" . '<field name="bID" type="I"><key /><unsigned /></field>' . PHP_EOL
		          . "\t\t" . '<field name="singleFieldData" type="B"></field>' . PHP_EOL
		          . "\t\t" . '<field name="repeatingItemData" type="' . ($use_text_type_for_repeating_item_data ? 'X2' : 'B') . '"></field>' . PHP_EOL
		          . "\t" . '</table>' . PHP_EOL
		          . '</schema>' . PHP_EOL;
		
		$path = $dir . '/db.xml';
		$this->outputFile($path, $contents);
	}
	
	private function createPkgEditPhp($dir) {
		$contents = $this->getC5Header() . PHP_EOL . PHP_EOL
		          . $this->getHeaderComments() . PHP_EOL . PHP_EOL
		          . 'Loader::element(\'block_edit\', array(\'controller\' => $controller), \'designer_content_pro\');' . PHP_EOL;
		$path = $dir . '/edit.php';
		$this->outputFile($path, $contents);
	}
	
	private function createPkgTools($dir) {
		$dir = $dir . '/tools';
		$this->outputDir($dir);
		
		$contents = $this->getC5Header() . PHP_EOL . PHP_EOL
		          . $this->getHeaderComments() . PHP_EOL . PHP_EOL
		          . '$btHandle = Request::get()->getBlock();' . PHP_EOL
		          . 'if (!empty($btHandle)) {' . PHP_EOL
		          . "\t" . '$btClass = Loader::helper(\'text\')->camelcase($btHandle) . \'BlockController\';' . PHP_EOL
		          . "\t" . '$controller = new $btClass;' . PHP_EOL
		          . "\t" . 'if (is_object($controller)) {' . PHP_EOL
		          . "\t\t" . 'Loader::element(\'block_edit_repeating_item\', array(\'controller\' => $controller), \'designer_content_pro\');' . PHP_EOL
		          . "\t" . '}' . PHP_EOL
		          . '}' . PHP_EOL;
		
		$path = $dir . '/edit_repeating_item.php';
		$this->outputFile($path, $contents);
		
		$this->createPkgToolsLabelLookup($dir);
	}
	
	//This is a separate function from createPkgTools()
	// because this file is part of the DCP 2.1 update,
	// so we want to be able to run this independently of all the other stuff
	// (to add this file to existing blocks on package update).
	//That's also why we do a lot of checking to see if things exist already
	// before writing the file (in case the DCP package is subsequently
	// updated again after v2.1, we don't want to overwrite what's already there).
	private function createPkgToolsLabelLookup($tools_dir) {
		if (!file_exists($tools_dir)) {
			return; //not sure how this could happen, but just in case
		}
		
		$path = $tools_dir . '/lookupedit_repeating_item_label.php';
		if (!file_exists($path)) {
			$contents = $this->getC5Header() . PHP_EOL . PHP_EOL
			          . $this->getHeaderComments() . PHP_EOL . PHP_EOL
			          . '$btHandle = Request::get()->getBlock();' . PHP_EOL
			          . 'if (!empty($btHandle)) {' . PHP_EOL
			          . "\t" . '$btClass = Loader::helper(\'text\')->camelcase($btHandle) . \'BlockController\';' . PHP_EOL
			          . "\t" . '$controller = new $btClass;' . PHP_EOL
			          . "\t" . 'if (is_object($controller) && !empty($_POST)) {' . PHP_EOL
			          . "\t\t" . 'echo $controller->getRepeatingItemLabelValue($_POST);' . PHP_EOL
			          . "\t" . '}' . PHP_EOL
			          . '}' . PHP_EOL;
			$this->outputFile($path, $contents);
		}
	}
	
	private function createPkgViewPhp($dir, $btHandle) {
		$contents = $this->getC5Header() . PHP_EOL . PHP_EOL
		          . $this->getHeaderComments() . PHP_EOL . PHP_EOL
                  . '?>'
		          . PHP_EOL . PHP_EOL
		          . '<p style="color: white; background-color: red;">' . PHP_EOL
		          . "\t" . t('This Designer Content Pro block view must be overridden in %s/blocks/%s/view.php', ($this->getPhpTag() . 'echo DIR_BASE; ?>'), $btHandle)
		          . PHP_EOL . '</p>'
		          . PHP_EOL;
		
		$path = $dir . '/view.php';
		$this->outputFile($path, $contents);
	}
	
	private function createOverrideViewPhp($dir) {
		$contents = $this->getC5Header() . ' ?>'
		          . PHP_EOL
		          . $this->getPhpTag() . '/* ' . t('This block was made with Designer Content Pro. Visit %s for documentation.', 'http://theblockery.com/dcp') . ' */ ?>'
		          . PHP_EOL . PHP_EOL . PHP_EOL
		          . '<!-- ' . "\n"
		          . t('Replace this comment with your markup.') . "\n"
		          . t('Sample code can be found in the Designer Content Pro dashboard page.') . "\n"
		          . "\n"
		          . t('For further instructions, visit http://theblockery.com/dcp') . "\n"
		          . '-->' . "\n"
		          ;
		$path = $dir . '/view.php';
		$this->outputFile($path, $contents, true);
	}

/*** UTILITY FUNCTIONS ***/
	private function getPhpTag() {
		return '<' . '?' . 'p' . 'h' . 'p' . ' '; //outsmart c5 marketplace process that inserts extra spaces after every php opening tag
	}

	private function getC5Header() {
		return $this->getPhpTag() . 'defined(\'C5_EXECUTE\') or die(_("Access Denied."));';
	}
	
	private function outputDir($path, $allow_server_permission_fix = false) {
		$path = rtrim($path, '/'); //remove trailing slash
		mkdir($path);

		if ($allow_server_permission_fix && defined('DCP_SERVER_PERMISSION_FIX') && DCP_SERVER_PERMISSION_FIX) {
			chmod($path, 0777);
		}
	}

	private function outputFile($path, $contents, $allow_server_permission_fix = false) {
		//Normally we'd just call: file_put_contents($path, $contents);
		//But we want to use C5's 'file' helper instead
		// to work within marketplace guidelines.
		//Note that the c5 helper only offers file_put_contents() in APPEND mode,
		// so first we must clear the file (if it exists), then append to the empty file.
		$fh = Loader::helper('file');
		$fh->clear($path);
		$fh->append($path, $contents);
		
		if ($allow_server_permission_fix && defined('DCP_SERVER_PERMISSION_FIX') && DCP_SERVER_PERMISSION_FIX) {
			chmod($path, 0666);
		}
		
	}
	
	//Pass in a btHandle for controller.php, otherwise leave it empty.
	private function getHeaderComments($is_dbxml = false, $is_controllerphp = false, $btHandle = '') {
		
		if ($is_controllerphp) {
			if (empty($btHandle)) {
				throw new Exception(t('DESIGNER CONTENT PRO ERROR: You must provide a btHandle when calling DesignerContentProBlockGenerator::getHeaderComments() for the controller.php file'));
			}
			$comments = 
t('/**
 * WARNING: This is generated code.
 * Anything in this file may get overwritten or deleted without warning.
 * Do NOT edit, rename, move, or delete this file (doing so will cause errors).
 *
 * To customize this controller, first copy it to SITEROOT/blocks/%s/controller.php
 *
 * Visit %s for documentation.
 */', $btHandle, 'http://theblockery.com/dcp');

		} else if ($is_dbxml) {
			$comments =
t('<!--
WARNING: This is generated code.
Anything in this file may get overwritten or deleted without warning.
Do NOT edit, rename, move, or delete this file (doing so will cause errors).
-->');

		} else {
			$comments =
t('/**
 * WARNING: This is generated code.
 * Anything in this file may get overwritten or deleted without warning.
 * Do NOT edit, rename, move, or delete this file (doing so will cause errors).
 */');
		}
		
		return $comments;
	}
	
}