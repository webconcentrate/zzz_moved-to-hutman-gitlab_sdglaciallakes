<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/***************************************************************************
 * Copyright (C) Web Concentrate - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Written by Jordan Lev <jordan@webconcentrate.com>, January-August 2013
 ***************************************************************************/


Loader::library('dcp_field_display', 'designer_content_pro');

abstract class DcpFieldType {
	protected $names; //array of input field names (most field types will just have one input field, but some are "compound" types that are made up of 2 or 3 -- e.g. "file" and "link")
	protected $label; //there's just one label for the whole field, even if there are multiple input fields (so it's really more of a "group heading" than a field label)
	protected $options; //array of options (differs from fieldtype to fieldtype -- e.g. "required", "searchable", "has_editable_text")
	protected $fh; //form helper -- used by outputEditFields function a lot so just instantiate it in the constructor
	protected $th; //text helper -- for htmlspecialchars
	
	public static function getGeneratorLabel() {
		//we want this to be an abstract static method,
		//but not all versions of php support that.
		//So fake it with an exception here in the base class:
		throw new Exception(t('DESIGNER CONTENT PRO ERROR: A DcpFieldType class does not have the "getGeneratorLabel" static method implemented!'));
	}
	
	public static function getGeneratorOptions() {
		//we want this to be an abstract static method,
		//but not all versions of php support that.
		//So fake it with an exception here in the base class:
		throw new Exception(t('DESIGNER CONTENT PRO ERROR: A DcpFieldType class does not have the "getGeneratorOptions" static method implemented!'));
	}
	
	public function __construct($handle, $label, $options = array()) {
		$this->names = array('text' => $handle . '_text');
		$this->label = $label;
		$this->options = $options;
		$this->fh = Loader::helper('form');
		$this->th = Loader::helper('text');
	}
	
	public function repeatingItemLabelFieldName() {
		return $this->names['text'];
	}
	public function repeatingItemLabelIsLookup() {
		return false;
	}
	public function repeatingItemLabelLookup($item_data) {
		//Put fieldtype-specific login in here for retrieving the label
		// from the given field value (or just return the field's value
		// if there is no lookup).
		$field_name = $this->repeatingItemLabelFieldName();
		if (!is_array($item_data) || !array_key_exists($field_name, $item_data)) {
			return '';
		} else {
			return $item_data[$field_name];
		}
	}
	
	public function repeatingItemThumbFieldName() {
		//Override this in your sub-class if you can provide a file id (e.g. File and Image types)
		return '';
	}
	
	public function outputJQTmplHiddenFields() {
		foreach ($this->names as $name) {
			//Hidden fields will be outputted to a jquery template,
			// so we bracketize the name (because it will be one of many repeated rows in the edit form),
			// and the "value" is really just the name wrapped in "${...}" (because it's a jquery-tmpl placeholder).
			//Also note that we can't use C5's form helper to output the hidden field because it always adds dom id's,
			// but we can't have that since these fields will be repeated for every repeating item row in the edit form.
			echo '<input type="hidden" name="' . $name . '[]" value="${' . $name . '}" />';
		}
	}
	
	abstract public function outputEditFields($single_vs_repeating, $data);
	
	protected function outputEditFieldsWrapperStart($display_required_asterisk = false) {
		echo '<fieldset>';
		echo '<legend>';
		echo $this->label;
		if ($display_required_asterisk) {
			echo '<span style="font-size: 8px;">&nbsp;</span><span style="color: red;">*</span>';
		}
		echo '</legend>';
		
		$help_text = $this->option('help_text');
		if ($help_text) {
			echo '<div class="dcp-help-text">' . nl2br($this->th->entities($help_text)) . '</div>';
		}
	}
	protected function outputEditFieldsWrapperEnd() {
		echo '</fieldset>';
	}
	
	protected function outputEditFieldRequiredValidation($type, $handle) {
		if ($this->option('required')) {
			echo '<input type="hidden" data-validation-rule="required-' . $type . '" data-validation-field="' . $handle . '" />';
		}
	}
	
	//Extract just the data pertaining to this field from $_POST.
	//Also ensures that a specific default value is provided when no data exists
	// for each of this field's input components (because repeating item fields
	// should default to an empty array so the caller can just assume it is always looping
	// through arrays without having to call empty() on everything, but single fields
	// should *not* default to empty arrays because that causes the wrong value type to be serialized
	// which causes subsequent random problems down the road).
	public function getPOSTDataPerInput($default_value_if_missing = array()) {
		$data = array();
		foreach ($this->names as $name) {
			$data[$name] = empty($_POST[$name]) ? $default_value_if_missing : $_POST[$name];
		}
		return $data;
	}
	
	//Sub-classes usually don't need to override this.
	// The only reason is if they want to alter the data (e.g. wysiwyg).
	// Note that if you do override it, it must return an array!
	public function getDataArrayForEdit($record) {
		$data = array();
		foreach ($this->names as $name) {
			$data[$name] = $record[$name];
		}
		return $data;
	}
	
	//This must be implemented, and should return the appropriate displayObject.
	abstract public function getDisplayObjectForView($record);
	
	public function getSearchableContent($record) {
		return $this->option('searchable') ? $record[$this->names['text']] : '';
	}
	
	public function validate($args) {
		if ($this->option('required') && empty($args[$this->names['text']])) {
			return t("'%s' is required.", $this->label);
		}
	}
	
	//Utility function for checking options without worrying about whether or not they exist
	protected function option($key, $default_if_missing = null) {
		return array_key_exists($key, $this->options) ? $this->options[$key] : $default_if_missing;
	}
	
	//Utility function for determining if this is an "add new record" or "edit record" situation
	protected function isNewVsEdit($single_vs_repeating) {
		if ($single_vs_repeating) {
			return (empty($_GET['btask']) || $_GET['btask'] != 'edit');
		} else {
			return !empty($_POST['isNew']);
		}
	}
	
}

class DcpFieldType_File extends DcpFieldType {
	
	public static function getGeneratorLabel() {
		return t('File');
	}
	
	public static function getGeneratorOptions() {
		return array(
			'help_text' => array('type' => 'textarea', 'label' => t('Help Text...'), 'description' => t('Help Text will be displayed next to this field in the block add/edit dialog.')),
			'searchable' => array('type' => 'checkbox', 'label' => t('Searchable'), 'description' => t('Check this box to include this field\'s file title in the site search index.')),
			'required' => array('type' => 'checkbox', 'label' => t('Required'), 'description' => t('Check this box to make this field required when adding/editing blocks.')),
			'has_editable_text' => array('type' => 'checkbox', 'label' => t('Editable Title'), 'description' => t('Check this box to provide a textbox for the file title when adding/editing blocks (if provided, this title will be used instead of the file\'s title attribute).')),
			'is_item_label' => array('type' => 'checkbox', 'label' => t('Use as label in edit mode'), 'description' => t('Check this box to use this field\'s editable title (if Editable Text is checked) or chosen file title as the label for repeating items when listed in the block add/edit dialog.')),
		);
	}

	public function __construct($handle, $label, $options = array()) {
		parent::__construct($handle, $label, $options);
		
		$this->names = array('fID' => $handle . '_fID');
		
		if ($this->option('has_editable_text')) {
			$this->names['text'] = $handle . '_title'; //for downloadable files this will be link text; for images this will be alt text
		}
	}
	
	public function repeatingItemLabelFieldName() {
		return $this->option('has_editable_text') ? $this->names['text'] : $this->names['fID'];
	}
	public function repeatingItemLabelIsLookup() {
		return !$this->option('has_editable_text');
	}
	public function repeatingItemLabelLookup($item_data) {
		if ($this->option('has_editable_text')) {
			return parent::repeatingItemLabelLookup($item_data);
		}
		
		$field_name = $this->names['fID'];
		if (!is_array($item_data) || !array_key_exists($field_name, $item_data)) {
			return '';
		}
		
		$fID = $item_data[$field_name];
		if (!$fID) {
			return '';
		}
		
		$file = File::getByID($fID);
		if (!$file || $f->error) {
			return '';
		}
		
		return $file->getTitle() ?: $file->getFileName();
	}
	
	public function outputEditFields($single_vs_repeating, $data, $restrict_to_filetype = null, $text_field_label = null) { //<--$text_field_label defaults to t('Title')
		$this->outputEditFieldsWrapperStart($this->option('required'));
		
		$fID_name = $this->names['fID'];
		$file = empty($data[$fID_name]) ? null : File::getByID($data[$fID_name]);
		
		$al = Loader::helper('concrete/asset_library');
		$args = array();
		if (!empty($restrict_to_filetype)) {
			$args['fType'] = $restrict_to_filetype; //passing FileType::T_IMAGE into $al->file() is the same as calling $al->image()
		}
		echo $al->file($fID_name, $fID_name, $this->label, $file, $args);
		
		if (!$single_vs_repeating) {
			$this->outputEditFieldRequiredValidation('chooser', $fID_name);
		}
		
		if ($this->option('has_editable_text')) {
			$text_name = $this->names['text'];
			$text_field_label = is_null($text_field_label) ? t('Title') : $text_field_label; //do this here instead of having a default value for the function param -- because php doesn't allow function calls [e.g. t()] as param defaults
			echo $this->fh->label($text_name, "{$text_field_label}: ");
			echo $this->fh->text($text_name, $data[$text_name], array('class' => 'file-alt-text'));
		}
		
		$this->outputEditFieldsWrapperEnd();		
	}
	
	public function getDisplayObjectForView($record) {
		$fID = $record[$this->names['fID']];
		$text = $this->option('has_editable_text') ? $record[$this->names['text']] : null; //pass null to the Display object if there's no "editable text" field so it knows to retrieve file title instead
		return new DcpFieldDisplay_File($fID, $text);
	}
	
	public function getSearchableContent($record) {
		return $this->option('searchable') ? $this->getDisplayObjectForView($record)->getTitle() : '';
	}
	
	public function validate($args) {
		if ($this->option('required') && empty($args[$this->names['fID']])) {
			return t("'%s' is required.", $this->label);
		}
	}
}

class DcpFieldType_Image extends DcpFieldType_File {
	
	public static function getGeneratorLabel() {
		return t('Image');
	}
	
	public static function getGeneratorOptions() {
		return array(
			'help_text' => array('type' => 'textarea', 'label' => t('Help Text...'), 'description' => t('Help Text will be displayed next to this field in the block add/edit dialog.')),
			'searchable' => array('type' => 'checkbox', 'label' => t('Searchable'), 'description' => t('Check this box to include this field\'s file title in the site search index.')),
			'required' => array('type' => 'checkbox', 'label' => t('Required'), 'description' => t('Check this box to make this field required when adding/editing blocks.')),
			'has_editable_text' => array('type' => 'checkbox', 'label' => t('Editable Alt Text'), 'description' => t('Check this box to provide a textbox for the image alt text when adding/editing blocks (if provided, this alt text will be used instead of the file\'s description attribute).')),
			'is_item_thumb' => array('type' => 'checkbox', 'label' => t('Use as thumbnail in edit mode'), 'description' => t('Check this box to use this field\'s image as the thumbnail for repeating items when listed in the block add/edit dialog.')),
			'is_item_label' => array('type' => 'checkbox', 'label' => t('Use as label in edit mode'), 'description' => t('Check this box to use this field\'s image file title as the label for repeating items when listed in the block add/edit dialog.')),
		);
	}
	
	public function repeatingItemThumbFieldName() {
		return $this->names['fID'];
	}
	
	public function repeatingItemLabelFieldName() {
		return $this->names['fID'];
	}
	public function repeatingItemLabelIsLookup() {
		return true;
	}
	public function repeatingItemLabelLookup($item_data) {
		//Don't just fallback to the "File" fieldtype's function
		// because that one checks the "has_editable_text" option,
		// which we never want to use (because it's for alt text, not titles)
		
		$field_name = $this->names['fID'];
		if (!is_array($item_data) || !array_key_exists($field_name, $item_data)) {
			return '';
		}
		
		$fID = $item_data[$field_name];
		if (!$fID) {
			return '';
		}
		
		$file = File::getByID($fID);
		if (!$file || $f->error) {
			return '';
		}
		
		return $file->getTitle() ?: $file->getFileName();
	}
	
	public function outputEditFields($single_vs_repeating, $data) {
		parent::outputEditFields($single_vs_repeating, $data, FileType::T_IMAGE, t('Alt Text'));
	}
	
	public function getDisplayObjectForView($record) {
		$fID = $record[$this->names['fID']];
		$text = $this->option('has_editable_text') ? $record[$this->names['text']] : null; //pass null to the Display object if there's no "editable text" field so it knows to retrieve file description instead
		return new DcpFieldDisplay_Image($fID, $text);
	}
}

class DcpFieldType_Textbox extends DcpFieldType {
	
	public static function getGeneratorLabel() {
		return t('Textbox');
	}
	
	public static function getGeneratorOptions() {
		return array(
			'help_text' => array('type' => 'textarea', 'label' => t('Help Text...'), 'description' => t('Help Text will be displayed next to this field in the block add/edit dialog.')),
			'searchable' => array('type' => 'checkbox', 'label' => t('Searchable'), 'description' => t('Check this box to include this field\'s text in the site search index.')),
			'required' => array('type' => 'checkbox', 'label' => t('Required'), 'description' => t('Check this box to make this field required when adding/editing blocks.')),
			'is_item_label' => array('type' => 'checkbox', 'label' => t('Use as label in edit mode'), 'description' => t('Check this box to use this field\'s text as the label for repeating items when listed in the block add/edit dialog.')),
		);
	}
	
	public function outputEditFields($single_vs_repeating, $data) {
		$this->outputEditFieldsWrapperStart($this->option('required'));
		
		$text_name = $this->names['text'];
		echo $this->fh->text($text_name, $data[$text_name], array('class' => 'input-xxlarge'));
		
		if (!$single_vs_repeating) {
			$this->outputEditFieldRequiredValidation('text', $text_name);
		}
		
		$this->outputEditFieldsWrapperEnd();
	}
	
	public function getDisplayObjectForView($record) {
		return new DcpFieldDisplay_Textbox($record[$this->names['text']]);
	}
}

class DcpFieldType_Textarea extends DcpFieldType {
	
	public static function getGeneratorLabel() {
		return t('Text Area');
	}
	
	public static function getGeneratorOptions() {
		return array(
			'help_text' => array('type' => 'textarea', 'label' => t('Help Text...'), 'description' => t('Help Text will be displayed next to this field in the block add/edit dialog.')),
			'searchable' => array('type' => 'checkbox', 'label' => t('Searchable'), 'description' => t('Check this box to include this field\'s text in the site search index.')),
			'required' => array('type' => 'checkbox', 'label' => t('Required'), 'description' => t('Check this box to make this field required when adding/editing blocks.')),
			'is_item_label' => array('type' => 'checkbox', 'label' => t('Use as label in edit mode'), 'description' => t('Check this box to use this field\'s text as the label for repeating items when listed in the block add/edit dialog.')),
		);
	}
	
	public function outputEditFields($single_vs_repeating, $data) {
		$this->outputEditFieldsWrapperStart($this->option('required'));
		
		$text_name = $this->names['text'];
		echo $this->fh->textarea($text_name, $data[$text_name], array('class' => 'input-xxlarge'));
		
		if (!$single_vs_repeating) {
			$this->outputEditFieldRequiredValidation('text', $text_name);
		}
		
		$this->outputEditFieldsWrapperEnd();
	}
	
	public function getDisplayObjectForView($record) {
		return new DcpFieldDisplay_Textarea($record[$this->names['text']]);
	}
}

class DcpFieldType_Wysiwyg extends DcpFieldType {
	protected $wh; //wysiwyg helper
	
	public static function getGeneratorLabel() {
		return t('WYSIWYG');
	}
	
	public static function getGeneratorOptions() {
		return array(
			'help_text' => array('type' => 'textarea', 'label' => t('Help Text...'), 'description' => t('Help Text will be displayed next to this field in the block add/edit dialog.')),
			'searchable' => array('type' => 'checkbox', 'label' => t('Searchable'), 'description' => t('Check this box to include this field\'s content in the site search index.')),
			'required' => array('type' => 'checkbox', 'label' => t('Required'), 'description' => t('Check this box to make this field required when adding/editing blocks.')),
		);
	}
	
	public function __construct($handle, $label, $options = array()) {
		parent::__construct($handle, $label, $options);
		$this->names = array('text' => $handle . '_content');
		$this->wh = Loader::helper('wysiwyg_link_replacement', 'designer_content_pro');
	}
	
	public function outputEditFields($single_vs_repeating, $data) {
		$this->outputEditFieldsWrapperStart($this->option('required'));
		
		Loader::element('editor_controls');
		$text_name = $this->names['text'];
		//Note that we use a different class name depending on whether this is a "single field" or a repeating item field,
		// because otherwise the tinyMCE initialization code for repeating item fields tramples the existing "single field" tinyMCE instances.
		echo $this->fh->textarea($text_name, $data[$text_name], array('class' => ($single_vs_repeating ? 'dcp-single-fields-editor' : 'ccm-advanced-editor'), 'id' => $text_name));
		
		if (!$single_vs_repeating) {
			$this->outputEditFieldRequiredValidation('text', $text_name);
		}
		
		$this->outputEditFieldsWrapperEnd();
	}
	
	public function getPOSTDataPerInput($default_value_if_missing = array()) {
		$text_name = $this->names['text'];
		$data = empty($_POST[$text_name]) ? $default_value_if_missing : $this->wh->translateTo($_POST[$text_name]); //DEV NOTE: this is kosher because translateTo uses preg_replace, which *does* return an array if you pass it an array (which $_POST[$text_name] is for repeating fields, because it contains this field's values across all repeating items)
		return array($text_name => $data);
	}
	
	public function getSearchableContent($record) {
		return $this->option('searchable') ? strip_tags($record[$this->names['text']]) : '';
	}
	
	public function getDataArrayForEdit($record) {
		$text_name = $this->names['text'];
		$content = $record[$text_name];
		$translatedContent = $this->wh->translateFromEditMode($content);
		return array($text_name => $translatedContent);
	}
	
	public function getDisplayObjectForView($record) {
		$text_name = $this->names['text'];
		$content = $record[$text_name];
		$translatedContent = $this->wh->translateFrom($content);
		return new DcpFieldDisplay_Wysiwyg($translatedContent);
	}
}

class DcpFieldType_Link extends DcpFieldType {
	private $original_handle;
	private static function availableControlTypes() { //ideally this would just be a private member, but we can't do that because we need to use the t() function.
		return array(
			'combo' => t('Combo'),
			'page' => t('Page Chooser'),
			'url' => t('External URL'),
		);
	}
	
	public static function getGeneratorLabel() {
		return t('Link');
	}
	
	public static function getGeneratorOptions() {
		return array(
			'help_text' => array('type' => 'textarea', 'label' => t('Help Text...'), 'description' => t('Help Text will be displayed next to this field in the block add/edit dialog.')),
			'control_type' => array('type' => 'select', 'label' => t('Type'), 'choices' => self::availableControlTypes(), 'description' => t('The type of field to provide in the block add/edit dialog:<br>sitemap page chooser only,<br>external url textbox only,<br>or both (combo).')),
			'searchable' => array('type' => 'checkbox', 'label' => t('Searchable'), 'description' => t('Check this box to include this field\'s link text in the site search index.')),
			'required' => array('type' => 'checkbox', 'label' => t('Required'), 'description' => t('Check this box to make this field required when adding/editing blocks.')),
			'has_editable_text' => array('type' => 'checkbox', 'label' => t('Editable Link Text'), 'description' => t('Check this box to provide a textbox for the link text when adding/editing blocks (if provided, this text will be used instead of the page title or external url).')),
			'is_item_label' => array('type' => 'checkbox', 'label' => t('Use as label in edit mode'), 'description' => t('Check this box to use this field\'s editable title (if Editable Text is checked), chosen page title (if Type is Page Chooser or Combo), or URL (if Type is External URL) as the label for repeating items when listed in the block add/edit dialog.')),
		);
	}
	
	public function __construct($handle, $label, $options = array()) {
		parent::__construct($handle, $label, $options);
		
		//check that controltype was provided and that it's one of the valid options
		if (!in_array($this->option('control_type'), array_keys(self::availableControlTypes()))) {
			$this->options['control_type'] = 'combo'; //default to 'combo' if not provided or invalid
		}
		
		$this->names = array(
			'cID' => $handle . '_cID',
			'url' => $handle . '_url',
		);
		if ($this->option('has_editable_text')) {
			$this->names['text'] = $handle . '_text';
		}

		$this->original_handle = $handle; //hold on to the original handle -- we'll need it for some temp fields later on
	}
	
	public function repeatingItemLabelFieldName() {
		if ($this->option('has_editable_text')) {
			return $this->names['text'];
		} else if ($this->option('control_type') == 'url') {
			return $this->names['url'];
		} else {
			//Note that we are unable to have the "combo" type
			// dynamically use either page or url depending on value
			// (because we don't know the value at this time),
			// so "combo" type will only use chosen page.
			return $this->names['cID'];
		}
	}
	public function repeatingItemLabelIsLookup() {
		if ($this->option('has_editable_text')) {
			return false;
		} else if ($this->option('control_type') == 'url') {
			return false;
		} else {
			return true;
		}
	}
	public function repeatingItemLabelLookup($item_data) {
		if ($this->option('has_editable_text')) {
			return parent::repeatingItemLabelLookup($item_data);
		} else if ($this->option('control_type') == 'url') {
			return $item_data[$this->names['url']];
		}
		
		$field_name = $this->names['cID'];
		if (!is_array($item_data) || !array_key_exists($field_name, $item_data)) {
			return '';
		}
		
		$cID = $item_data[$field_name];
		if (!$cID) {
			return '';
		}
		
		$page = Page::getByID($cID);
		if (!$page || !$page->getCollectionID()) {
			return '';
		}
		
		return $page->getCollectionName();
	}
	
	public function outputEditFields($single_vs_repeating, $data) {
		$this->outputEditFieldsWrapperStart($this->option('required'));
		
		$control_type_name = $this->original_handle . '_control_type'; //temporary field, never saved to db (so it's not in $this->names)
		$ps_wrapper_name = $this->original_handle . '_ps_wrapper'; //ditto
		$cID_name = $this->names['cID'];
		$url_name = $this->names['url'];
		if ($this->option('has_editable_text')) {
			$text_name = $this->names['text'];
		}
		
		$control_type = $this->option('control_type'); //no need to provide default fallback value here because the constructor already did that for us
		
		echo $this->fh->label($control_type_name, 'Link To: ', array('class' => 'link-to-label'));
		
		if ($control_type == 'url') {
			$default_control_type_is_page = false;
		} else if ($control_type == 'page') {
			$default_control_type_is_page = true;
		} else { //'combo'
			$default_control_type_is_page = empty($data[$url_name]); //note that this makes pageselector the default for new records
		}

		if ($control_type == 'combo') {
			//output a dropdown for 'combo' controltype:
			$select_options = array('page' => t('Page in Site'), 'url' => t('External URL'));
			$select_value = ($default_control_type_is_page ? 'page' : 'url');
			$select_onchange = "$(this).siblings('#{$ps_wrapper_name}').css('display', ($(this).val() === 'page') ? 'inline-block' : 'none');"
			                 . "$(this).siblings('#{$url_name}').css('display', ($(this).val() === 'url') ? 'inline-block' : 'none');";
			echo $this->fh->select($control_type_name, $select_options, $select_value, array('class' => 'link-to-type', 'onchange' => $select_onchange));
		
			echo ' ';
		} else if ($control_type == 'page') {
			echo $this->fh->hidden($control_type_name, 'page');
		} else if ($control_type == 'url') {
			echo $this->fh->hidden($control_type_name, 'url');
		}
		
		if ($control_type == 'url') {
			echo $this->fh->hidden($cID_name, '0');
		} else { //output a page selector for 'page' and 'combo' controltypes:
			echo '<div id="' . $ps_wrapper_name . '" class="link-to-ps-wrapper" style="display:' . ($default_control_type_is_page ? 'inline-block' : 'none') . ';">';
			$ps = Loader::helper('form/page_selector');
			echo $ps->selectPage($cID_name, $data[$cID_name]);
			echo '</div>';
		}
		
		if ($control_type == 'page') {
			echo $this->fh->hidden($url_name, '');
		} else { //output a textfield for 'url' and 'combo' controltypes:
			echo $this->fh->text($url_name, $data[$url_name], array('class' => 'link-to-url', 'style' => 'display:' . ($default_control_type_is_page ? 'none' : 'inline-block') . ';'));
		}
		
		if ($this->option('has_editable_text')) {
			echo '<br>';
			echo $this->fh->label($text_name, t('Link Text: '));
			echo $this->fh->text($text_name, $data[$text_name], array('class' => 'link-text-input'));
		}
		
		if (!$single_vs_repeating && $this->option('required')) {
			echo '<input type="hidden"'
			   . ' name="' . $this->original_handle . '_validation"'
			   . ' data-validation-rule="required-link"'
			   . ' data-validation-field-type="' . $control_type_name . '"'
			   . ' data-validation-field-page="' . $cID_name . '"'
			   . ' data-validation-field-url="' . $url_name . '"'
			   . ' />';
		}
		
		//nullify the non-chosen control type value
		// (for example, if user chooses a page, but then changes to external url,
		// we want the external url being saved and the chosen page should be empty)
		echo "\n" . "<script>$(document).on('dcp_block_edit_repeating_item_save dcp_block_edit_single_fields_save', function(e) {" . "\n";
		echo "\t" . "if ($('#{$control_type_name}').val() == 'page') {" . "\n";
		echo "\t\t" . "$('input[name=\"{$url_name}\"]').val('');" . "\n";
		echo "\t" . "} else if ($('#{$control_type_name}').val() == 'url') {" . "\n";
		echo "\t\t" . "$('input[name=\"{$cID_name}\"]').val('0');" . "\n";
		echo "\t" . "}" . "\n";
		echo "});</script>" . "\n";
		
		
		$this->outputEditFieldsWrapperEnd();
	}
	
	public function getDisplayObjectForView($record) {
		$text = $this->option('has_editable_text') ? $record[$this->names['text']] : null; //null indicates that the text should be ignored (it will use the collection name or external url instead)
		return new DcpFieldDisplay_Link($record[$this->names['cID']], $record[$this->names['url']], $text);
	}
	
	public function getSearchableContent($record) {
		return $this->option('searchable') ? $this->getDisplayObjectForView($record)->getText() : '';
	}
	
	public function validate($args) {
		if ($this->option('required') && empty($args[$this->names['cID']]) && empty($args[$this->names['url']])) {
			return t("You must provide a link for '%s'", $this->label);
		}
		// if ($this->option('required')) {
		// 	$control_type_name = $this->original_handle . '_control_type'; //temporary field, never saved to db (so it's not in $this->names)
		// 	$missing_required_page = ($args[$control_type_name] == 'page' && empty($args[$this->names['cID']]));
		// 	$missing_required_url = ($args[$control_type_name] == 'url' && empty($args[$this->names['url']]));
		// 	if ($missing_required_page || $missing_required_url) {
		// 		return t("You must provide a link for '%s'", $this->label);
		// 	}
		// }
	}
}

class DcpFieldType_Page extends DcpFieldType {
	
	public static function getGeneratorLabel() {
		return t('Page');
	}
	
	public static function getGeneratorOptions() {
		return array(
			'help_text' => array('type' => 'textarea', 'label' => t('Help Text...'), 'description' => t('Help Text will be displayed next to this field in the block add/edit dialog.')),
			'required' => array('type' => 'checkbox', 'label' => t('Required'), 'description' => t('Check this box to make this field required when adding/editing blocks.')),
			'page_type' => array('type' => 'select', 'label' => t('Allow Page Type'), 'choices' => self::pageTypeOptions(), 'description' => t('Restrict choice to a specific page type.')),
			'is_item_label' => array('type' => 'checkbox', 'label' => t('Use as label in edit mode'), 'description' => t('Check this box to use this field\'s chosen page title as the label for repeating items when listed in the block add/edit dialog.')),
		);
	}
	
	private static function pageTypeOptions() {
		$options = array('' => '&lt;All&gt;');
		$th = Loader::helper('text'); //we can't rely on $this->th because this is a static method
		foreach (CollectionType::getList() as $ct) {
			$options[$ct->getCollectionTypeID()] = $th->entities($ct->getCollectionTypeName());
		}
		return $options;
	}
	
	public function __construct($handle, $label, $options = array()) {
		parent::__construct($handle, $label, $options);
		$this->names = array('cID' => $handle . '_cID');
	}
	
	public function repeatingItemLabelFieldName() {
		return $this->names['cID'];
	}
	public function repeatingItemLabelIsLookup() {
		return true;
	}
	public function repeatingItemLabelLookup($item_data) {
		$field_name = $this->names['cID'];
		if (!is_array($item_data) || !array_key_exists($field_name, $item_data)) {
			return '';
		}
		
		$cID = $item_data[$field_name];
		if (!$cID) {
			return '';
		}
		
		$page = Page::getByID($cID);
		if (!$page || !$page->getCollectionID()) {
			return '';
		}
		
		return $page->getCollectionName();
	}
	
	public function outputEditFields($single_vs_repeating, $data) {
		$this->outputEditFieldsWrapperStart($this->option('required'));
		
		$cID_name = $this->names['cID'];

		$page_type_id = (int)$this->option('page_type');
		if (empty($page_type_id)) {
			$ps_wrapper_name = $cID_name . '_ps_wrapper'; //temporary field, never saved to db (so it's not in $this->names)

			echo '<div id="' . $ps_wrapper_name . '" class="link-to-ps-wrapper">';
			$ps = Loader::helper('form/page_selector');
			echo $ps->selectPage($cID_name, $data[$cID_name]);
			echo '</div>';
		} else {
			$page_options = $this->getPageOptions($this->option('page_type'));
			echo $this->fh->select($cID_name, $page_options, $data[$cID_name], array(
				'data-placeholder' => t('Choose a page...'), //for the "chosen" widget (it displays this in the widget when nothing has been selected yet)
			));
			echo "\n";
			echo "<script>$('#{$cID_name}').chosen({\n";
			echo "\t'disable_search_threshold': 10,\n";
			echo "\t'allow_single_deselect': true\n";
			echo "});</script>\n";
		}
		
		if (!$single_vs_repeating) {
			$this->outputEditFieldRequiredValidation('chooser', $cID_name);
		}
		
		$this->outputEditFieldsWrapperEnd();
	}
	
	private function getPageOptions($page_type_id) {
		$pl = new PageList;
		$pl->filterByCollectionTypeID($page_type_id);
		$pl->sortByPublicDateDescending();
		$pages = $pl->get();
		
		$options = array('' => ''); //leave the label blank so the "chosen" widget can use data-placeholder attribute
		foreach ($pages as $page) {
			$options[$page->getCollectionID()] = $this->th->entities($page->getCollectionName());
		}
		return $options;
	}
	
	public function getDisplayObjectForView($record) {
		return new DcpFieldDisplay_Page($record[$this->names['cID']]);
	}
	
	public function validate($args) {
		if ($this->option('required') && empty($args[$this->names['cID']])) {
			return t("'%s' is required.", $this->label);
		}
	}
}

class DcpFieldType_Date extends DcpFieldType {
	
	public static function getGeneratorLabel() {
		return t('Date');
	}
	
	public static function getGeneratorOptions() {
		return array(
			'help_text' => array('type' => 'textarea', 'label' => t('Help Text...'), 'description' => t('Help Text will be displayed next to this field in the block add/edit dialog.')),
			'required' => array('type' => 'checkbox', 'label' => t('Required'), 'description' => t('Check this box to make this field required when adding/editing blocks.')),
			'default_to_today' => array('type' => 'checkbox', 'label' => t('Default to Today'), 'description' => t('Check this box to pre-populate this field with today\'s date when adding new blocks.')),
		);
	}
	
	public function __construct($handle, $label, $options = array()) {
		parent::__construct($handle, $label, $options);
		$this->names = array('text' => $handle . '_date');
	}
	
	public function outputEditFields($single_vs_repeating, $data) {
		$this->outputEditFieldsWrapperStart($this->option('required'));
		
		$text_name = $this->names['text'];
		
		$is_new = $this->isNewVsEdit($single_vs_repeating);
		if ($is_new && empty($data[$text_name]) && $this->option('default_to_today')) {
			$val = date('Y-m-d'); //the $dth->date() call below will use strtotime() on this val... and for some weird reason there are certain date formats (e.g. 'j/n/Y', which is used in some non-U.S. countries) that strtotime() doesn't recognize properly (?!)
		} else if (!empty($data[$text_name])) {
			$date = $this->normalizeEuropeanDates($data[$text_name]);
			$val = date('Y-m-d', strtotime($date)); //don't just let $dth->date() pick up the value rom $_REQUEST, because while it *does* format the date when you pass in a value, it does *not* format the date when it grabs it directly from $_REQUEST (and we want the date formatted). WTF!!
		}
		unset($_REQUEST[$text_name]); //<--must do this otherwise C5's datetime helper sees it and overwrites our desired value with this! (which is just an empty string)
		
		$dth = Loader::helper('form/date_time');
 		echo $dth->date($text_name, $val);
		
		if (!$single_vs_repeating) {
			$this->outputEditFieldRequiredValidation('text', $text_name);
		}
		
		$this->outputEditFieldsWrapperEnd();
	}
	
	public function getPOSTDataPerInput($default_value_if_missing = array()) {
		//convert the dates submitted by the datepicker widget to Y-m-d format
		// (and also treat nonsensical dates as empty values)
		//note that we don't make use of the $default_value_if_missing arg
		// (but need it because it's in the parent class function we're overriding)
		$data = array();
		$text_name = $this->names['text'];
		if (!empty($_POST[$text_name])) {
			//input data may or may not be an array (depending on if it's a repeating item field or a single field)
			if (is_array($_POST[$text_name])) {
				foreach ($_POST[$text_name] as $i => $val) {
					$data[$i] = $this->sanitizeInputDate($val);
				}
			} else {
				$data = $this->sanitizeInputDate($_POST[$text_name]);
			}
		}
		return array($text_name => $data);
	}
	
	private function sanitizeInputDate($date) {
		$date = $this->normalizeEuropeanDates($date);
		$date_ts = empty($date) ? 0 : strtotime($date);
		$sanitized_date = empty($date_ts) ? '' : date('Y-m-d', $date_ts);
		return $sanitized_date;
	}
	
	private function normalizeEuropeanDates($date) {
		//strtotime can't differentiate between m/d/Y and d/m/Y
		// (see http://stackoverflow.com/a/2891949/477513 ),
		// so it looks to see if the separators are slashes
		// (in which case it assumes American format of m/d/Y)
		// or dashes or dots (in which case it assumes European format of d/m/Y).
		//SO we need to special-case based on the DATE_APP_GENERIC_MDY definition...
		// (ideally we'd use date_parse_from_format(), but that requires PHP 5.3+)
		$parts = explode('/', DATE_APP_GENERIC_MDY);
		if (!empty($parts) && (count($parts) == 3)) {
			$day_is_first = ($parts[0] == 'j' || $parts[0] == 'd');
			$month_is_second = ($parts[1] == 'n' || $parts[1] == 'm');
			$year_is_third = ($parts[2] == 'y' || $parts[2] == 'Y');
			if ($day_is_first && $month_is_second && $year_is_third) {
				//convert slashes to dashes so strtotime will recognize the day first and month second
				$date = str_replace('/', '-', $date);
			}
		}
		return $date;
	}
	
	public function getDisplayObjectForView($record) {
		return new DcpFieldDisplay_Date($record[$this->names['text']]);
	}
}

class DcpFieldType_Checkbox extends DcpFieldType {

	public static function getGeneratorLabel() {
		return t('Checkbox');
	}
	
	public static function getGeneratorOptions() {
		return array(
			'help_text' => array('type' => 'textarea', 'label' => t('Help Text...'), 'description' => t('Help Text will be displayed next to this field in the block add/edit dialog.')),
		);
	}
	
	public function __construct($handle, $label, $options = array()) {
		parent::__construct($handle, $label, $options);
		$this->names = array('check' => $handle . '_checked');
	}
	
	public function outputEditFields($single_vs_repeating, $data) {
		echo '<fieldset>';
		echo '<label>';
		$check_name = $this->names['check'];
		echo $this->fh->checkbox($check_name, 1, !empty($data[$check_name]), array('style' => 'display: inline;'));
		echo ' <b>' . $this->label . '</b>';
		$help_text = $this->option('help_text');
		if ($help_text) {
			echo '<div class="dcp-help-text" style="margin: 2px 0 10px 0;">' . nl2br($this->th->entities($help_text)) . '</div>';
		}
		echo '</fieldset>';
	}
	
	public function getDisplayObjectForView($record) {
		return new DcpFieldDisplay_Checkbox($record[$this->names['check']]);
	}
}

class DcpFieldType_Select extends DcpFieldType {
	private $original_handle;
	private static function availableControlTypes() { //ideally this would just be a private member, but we can't do that because we need to use the t() function.
		return array(
			'dropdown' => t('Dropdown List'),
			'checklist' => t('Checkbox List'),
		);
	}

	public static function getGeneratorLabel() {
		return t('Select List');
	}
	
	public static function getGeneratorOptions() {
		return array(
			'help_text' => array('type' => 'textarea', 'label' => t('Help Text...'), 'description' => t('Help Text will be displayed next to this field in the block add/edit dialog.')),
			'select_options' => array('type' => 'list_choices', 'label' => t('Select Options...'), 'description' => t('List choices, one per line,<br>in \'handle: label\' format.<br>For example:<br>:--Choose One--<br>first_handle: First Label<br>second_handle: Second Label<br>etc: And so on...')),
			'control_type' => array('type' => 'select', 'label' => t('Type'), 'choices' => self::availableControlTypes(), 'description' => t('The type of field to provide in the block add/edit dialog:<br>dropdown list (single choice),<br>or checkbox list (multiple choice).')),
			'required' => array('type' => 'checkbox', 'label' => t('Required'), 'description' => t('Check this box to make this field required when adding/editing blocks.<br>(For checkbox lists, this means at least one box must be checked.)')),
		);
	}
	
	public function __construct($handle, $label, $options = array()) {
		parent::__construct($handle, $label, $options);
		
		//check that controltype was provided and that it's one of the valid options
		if (!in_array($this->option('control_type'), array_keys(self::availableControlTypes()))) {
			$this->options['control_type'] = 'dropdown'; //default to 'dropdown' if not provided or invalid
		}
		
		$this->names = array('selections' => $handle . '_selections');
		
		$this->original_handle = $handle; //hold on to the original handle -- we'll need it for some temp fields later on
	}
	
	public function outputEditFields($single_vs_repeating, $data) {
		$this->outputEditFieldsWrapperStart($this->option('required'));
		
		$selections_name = $this->names['selections'];
		$control_type = $this->option('control_type'); //no need to provide default fallback value here because the constructor already did that for us
		$select_options = $this->option('select_options', array());
		
		//Multiple selections are stored as a comma-separated string,
		// which we need to split into an array for our purposes here.
		//Note that we're also doing this for dropdowns, because
		// although they only store a single value, it's possible
		// that the designer changed an existing field's "control type"
		// from checklist to dropdown, in which case we want to gracefully
		// handle existing data by pulling out the first value in the list.
		$existing_selections = array();
		if (!empty($data[$selections_name])) {
			$existing_selections = explode(',', $data[$selections_name]);
		}
		
		//Note: we *cannot* use c5's form helpers,
		// because it ignores the passed-in value we want to explicitly check
		// (because it does too much magic by looking at the $_REQUEST array,
		// so it winds up getting confused by our own "magic" of comma-separated strings etc.)
		if ($control_type == 'checklist') {
			//For checkbox lists, the checkboxes themselves get a suffix ("_chk")
			// and we use a single hidden field to store their concatenated values
			// (so the repeating item dialog can send the single value back to the block edit dialog for serialization).
			foreach ($select_options as $handle => $label) {
				echo '<label>';
				$checked = in_array($handle, $existing_selections) ? ' checked="checked"' : '';
				echo '<input type="checkbox" name="' . $selections_name . '_chk" value="' . $handle . '" style="display: inline;" ' . $checked . ' />';
				echo ' ' . $this->th->entities($label);
				echo '</label><br>';
			}
			echo '<input type="hidden" name="' . $selections_name . '" value="" />';

			//Rewrite the hidden field's value whenever boxes are checked/unchecked
			//(Historical note: we used to do this once prior to validation,
			// but that didn't work for the "single fields" because the "cleanup step"
			// happens after validation for repeating items but before validation for single fields.)
			echo "\n" . "<script>$(document).on('change', 'input[name=\"{$selections_name}_chk\"]', function(e) {" . "\n";
			echo "\t" . "var tmp_{$selections_name} = [];" . "\n";
			echo "\t" . "$('input[name=\"{$selections_name}_chk\"]').each(function() {" . "\n";
			echo "\t\t" . "if ($(this).prop('checked')) {" . "\n";
			echo "\t\t\t" . "tmp_{$selections_name}.push($(this).val());" . "\n";
			echo "\t\t" . "}" . "\n";
			echo "\t" . "});" . "\n";
			echo "\t" . "$('#single-fields-edit input[name=\"{$selections_name}\"], #repeating-item-edit input[name=\"{$selections_name}\"]').val(tmp_{$selections_name}.join(','));" . "\n";
			echo "});" . "\n";
			echo "$('input[name=\"{$selections_name}_chk\"]').trigger('change');" . "\n";
			echo "</script>" . "\n";

		} else {
			$dropdown_selection = empty($existing_selections) ? '' : $existing_selections[0];
			echo '<select name="' . $selections_name . '">';
			foreach ($select_options as $handle => $label) {
				$selected = ($dropdown_selection == $handle) ? 'selected="selected"' : '';
				echo '<option value="' . $handle . '" ' . $selected . '>' . $this->th->entities($label) . '</option>';
			}
			echo '</select>';
		}
		
		if (!$single_vs_repeating) {
			//We can use the default "text" validation,
			// because jquery validate's generic "required" rule
			// is smart enough to know how to handle checkbox lists!
			$this->outputEditFieldRequiredValidation('text', $selections_name);
		}
		
		$this->outputEditFieldsWrapperEnd();
	}
	
	public function getDisplayObjectForView($record) {
		return new DcpFieldDisplay_Select($record[$this->names['selections']], $this->option('select_options', array()));
	}
	
	public function validate($args) {
		if ($this->option('required') && empty($args[$this->names['selections']])) {
			return t("'%s' is required.", $this->label);
		}
	}
			
}

class DcpFieldType_Fileset extends DcpFieldType {
	public static function getGeneratorLabel() {
		return t('File Set');
	}
	
	public static function getGeneratorOptions() {
		return array(
			'help_text' => array('type' => 'textarea', 'label' => t('Help Text...'), 'description' => t('Help Text will be displayed next to this field in the block add/edit dialog.')),
			'searchable' => array('type' => 'checkbox', 'label' => t('Searchable'), 'description' => t('Check this box to include chosen file set names in the site search index.')),
			'required' => array('type' => 'checkbox', 'label' => t('Required'), 'description' => t('Check this box to make this field required when adding/editing blocks.')),
		);
	}
	
	public function __construct($handle, $label, $options = array()) {
		parent::__construct($handle, $label, $options);
		$this->names = array('fsID' => $handle . '_fsID');
	}
	
	public function outputEditFields($single_vs_repeating, $data) {
		$this->outputEditFieldsWrapperStart($this->option('required'));
		
		$fsID_name = $this->names['fsID'];
		
		$fileset_options = array('' => t('Choose a file set...'));
		foreach (FileSet::getMySets() as $fs) {
			$fileset_options[$fs->fsID] = $this->th->entities($fs->fsName);
		}
		
		echo $this->fh->select($fsID_name, $fileset_options, $data[$fsID_name]);
		
		if (!$single_vs_repeating) {
			$this->outputEditFieldRequiredValidation('text', $fsID_name); //yes, the 'text' validation type works for select lists
		}
		
		$this->outputEditFieldsWrapperEnd();
	}
	
	public function getSearchableContent($record) {
		if ($this->option('searchable')) {
			$fsID = (int)$record[$this->names['fsID']];
			if ($fsID) {
				$fs = FileSet::getByID($fsID);
				if ($fsID) {
					return $fs->fsName;
				}
			}
		}
		
		return '';
	}
	
	public function getDisplayObjectForView($record) {
		return new DcpFieldDisplay_Fileset($record[$this->names['fsID']]);
	}
	
	public function validate($args) {
		if ($this->option('required') && empty($args[$this->names['fsID']])) {
			return t("'%s' is required.", $this->label);
		}
	}
}

class DcpFieldType_Sdglmemberssubcategory extends DcpFieldType {
	
	public static function getGeneratorLabel() {
		return t('SDGL Members Subcategory');
	}
	
	public static function getGeneratorOptions() {
		return array(
			'help_text' => array('type' => 'textarea', 'label' => t('Help Text...'), 'description' => t('Help Text will be displayed next to this field in the block add/edit dialog.')),
			'required' => array('type' => 'checkbox', 'label' => t('Required'), 'description' => t('Check this box to make this field required when adding/editing blocks.')),
			'is_item_label' => array('type' => 'checkbox', 'label' => t('Use as label in edit mode'), 'description' => t('Check this box to use this field\'s chosen subcategory title as the label for repeating items when listed in the block add/edit dialog.')),
		);
	}
	
	public function __construct($handle, $label, $options = array()) {
		parent::__construct($handle, $label, $options);
		$this->names = array('subcategory_id' => $handle . '_subcategory_id');
	}
	
	public function repeatingItemLabelFieldName() {
		return $this->names['subcategory_id'];
	}
	public function repeatingItemLabelIsLookup() {
		return true;
	}
	public function repeatingItemLabelLookup($item_data) {
		$field_name = $this->names['subcategory_id'];
		if (!is_array($item_data) || !array_key_exists($field_name, $item_data)) {
			return '';
		}
		
		$subcategory_id = $item_data[$field_name];
		if (!$subcategory_id) {
			return '';
		}
		
		Loader::model('subcategory/query', 'sdgl_members');
		$subcategory = SubcategoryQuery::byId($subcategory_id);
		if (!$subcategory) {
			return '';
		}
		
		return $subcategory->title;
	}
	
	public function outputEditFields($single_vs_repeating, $data) {
		$this->outputEditFieldsWrapperStart($this->option('required'));
		
		$subcategory_id_name = $this->names['subcategory_id'];

		Loader::model('subcategory/query', 'sdgl_members');
		$subcategories_per_category = SubcategoryQuery::allGroupedByCategory();
		
		echo '<select name="' . $subcategory_id_name . '" id="' . $subcategory_id_name . '" class="input-xxlarge">';
		echo '<option value="">Choose a category...</option>';
		foreach ($subcategories_per_category as $category) {
			echo '<optgroup label="' . $category->title . '">';
			foreach ($category->subcategories as $subcategory) {
				echo '<option value="' . $subcategory->id . '">' . $subcategory->title . '</option>';
			}
			echo '</optgroup>';
		}
		echo '</select>';
		
		if (!$single_vs_repeating) {
			$this->outputEditFieldRequiredValidation('chooser', $subcategory_id_name);
		}
		
		$this->outputEditFieldsWrapperEnd();
	}
	
	public function getDisplayObjectForView($record) {
		return new DcpFieldDisplay_Sdglmemberssubcategory($record[$this->names['subcategory_id']]);
	}
	
	public function validate($args) {
		if ($this->option('required') && empty($args[$this->names['subcategory_id']])) {
			return t("'%s' is required.", $this->label);
		}
	}
}

class DcpFieldType_Sdglmemberslocation extends DcpFieldType {
	
	public static function getGeneratorLabel() {
		return t('SDGL Members Location');
	}
	
	public static function getGeneratorOptions() {
		return array(
			'help_text' => array('type' => 'textarea', 'label' => t('Help Text...'), 'description' => t('Help Text will be displayed next to this field in the block add/edit dialog.')),
			'required' => array('type' => 'checkbox', 'label' => t('Required'), 'description' => t('Check this box to make this field required when adding/editing blocks.')),
			'is_item_label' => array('type' => 'checkbox', 'label' => t('Use as label in edit mode'), 'description' => t('Check this box to use this field\'s chosen location title as the label for repeating items when listed in the block add/edit dialog.')),
		);
	}
	
	public function __construct($handle, $label, $options = array()) {
		parent::__construct($handle, $label, $options);
		$this->names = array('location_id' => $handle . '_location_id');
	}
	
	public function repeatingItemLabelFieldName() {
		return $this->names['location_id'];
	}
	public function repeatingItemLabelIsLookup() {
		return true;
	}
	public function repeatingItemLabelLookup($item_data) {
		$field_name = $this->names['location_id'];
		if (!is_array($item_data) || !array_key_exists($field_name, $item_data)) {
			return '';
		}
		
		$location_id = $item_data[$field_name];
		if (!$location_id) {
			return '';
		}
		
		Loader::model('location/query', 'sdgl_members');
		$location = LocationQuery::byId($location_id);
		if (!$location) {
			return '';
		}
		
		$label = $location->title;
		if (!$location->is_active) {
			$label .= ' [DISABLED / WILL NOT BE SHOWN]';
		}
		
		return $label;
	}
	
	public function outputEditFields($single_vs_repeating, $data) {
		$this->outputEditFieldsWrapperStart($this->option('required'));
		
		$location_id_name = $this->names['location_id'];

		Loader::model('location/query', 'sdgl_members');
		$location_options = LocationQuery::allAsSelectOptions();
		$location_options = array('' => '') + $location_options;
		echo $this->fh->select($location_id_name, $location_options, $data[$location_id_name], array(
			'data-placeholder' => t('Choose a member...'), //for the "chosen" widget (it displays this in the widget when nothing has been selected yet)
			'style' => 'width: 100% !important;',
		));
		echo "\n";
		echo "<script>$('#{$location_id_name}').chosen({\n";
		echo "\t'disable_search_threshold': 10,\n";
		echo "\t'allow_single_deselect': true\n";
		echo "});</script>\n";
		
		if (!$single_vs_repeating) {
			$this->outputEditFieldRequiredValidation('chooser', $location_id_name);
		}
		
		$this->outputEditFieldsWrapperEnd();
	}
	
	public function getDisplayObjectForView($record) {
		return new DcpFieldDisplay_Sdglmemberslocation($record[$this->names['location_id']]);
	}
	
	public function validate($args) {
		if ($this->option('required') && empty($args[$this->names['location_id']])) {
			return t("'%s' is required.", $this->label);
		}
	}
}
