<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/***************************************************************************
 * Copyright (C) Web Concentrate - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Written by Jordan Lev <jordan@webconcentrate.com>, January-August 2013
 ***************************************************************************/

Loader::model('dcp_block_type', 'designer_content_pro'); //makes the DCP_CONTAINER_PACKAGE_HANDLE definition available

class DesignerContentProContainerPackageGenerator {
	
	public function createAndInstallContainerPkg() {
		if (!$this->containerPkgExists()) {
			$this->createContainerPkg();
		}
		
		$maybe_pkg = Loader::package(DCP_CONTAINER_PACKAGE_HANDLE);
		if (is_null($maybe_pkg) || !is_object($maybe_pkg)) {
			throw new Exception(t('Designer Content Pro Error: Could not create custom block storage package.'));
		} else {
			//craziness...
			$installed_pkg = Package::getByHandle(DCP_CONTAINER_PACKAGE_HANDLE);
			if (is_null($installed_pkg) || !is_object($installed_pkg) || !$installed_pkg->isPackageInstalled()) {
				$maybe_pkg->install();
			}

			$pkg = Package::getByHandle(DCP_CONTAINER_PACKAGE_HANDLE);
			if (is_null($pkg) || !is_object($pkg) || !$pkg->isPackageInstalled()) {
				throw new Exception(t('Designer Content Pro Error: Could not install custom block storage package.'));
			}
		}
	}
	
	public function containerPkgExists() {
		$pkgs = Package::getAvailablePackages(false); //pass false to retrieve ALL packages (not just "awaiting installation")
		foreach ($pkgs as $pkg) {
			if ($pkg->getPackageHandle() == DCP_CONTAINER_PACKAGE_HANDLE) {
				return true;
			}
		}
		return false;
	}
	
	public function uninstallContainerPkg() {
		$pkg = Package::getByHandle(DCP_CONTAINER_PACKAGE_HANDLE);
		if (!is_null($pkg) && is_object($pkg) && $pkg->isPackageInstalled()) {
			$pkg->uninstall();
		}
	}
	
/*** Generator Functions ***/
	private function createContainerPkg() {
		$pkg_dir = $this->createPkgDir();
		
		$this->createController($pkg_dir);
		$this->createPkgBlocksDir($pkg_dir);
		$this->copyPkgIcon($pkg_dir);
		
		$elements_dashboard_dir = $this->createPkgElementsDashboardDir($pkg_dir);
		$this->createPkgInstallElement($elements_dashboard_dir);
		$this->createPkgUninstallElement($elements_dashboard_dir);
	}
	
	private function createPkgDir() {
		$pkg_dir = DIR_PACKAGES . '/' . DCP_CONTAINER_PACKAGE_HANDLE;
		$this->outputDir($pkg_dir);
		return $pkg_dir;
	}
	
	private function createController($pkg_dir) {
		$pkgName = t('Designer Content Pro - Custom Block Storage');
		$pkgDescription = t('Stores custom blocktypes created with Designer Content Pro.');
		
		$escaped_pkgHandle = $this->esc(DCP_CONTAINER_PACKAGE_HANDLE);
		$escaped_pkgName = $this->esc($pkgName);
		$escaped_pkgDescription = $this->esc($pkgDescription);
		
		$contents = $this->getC5Header()
		          . PHP_EOL . PHP_EOL
		          . "/**\n"
		          . " * " . t('This package was automatically created by the Designer Content Pro addon.') . "\n"
		          . " * \n"
 		          . " * " . t('The custom blocktypes you create with Designer Content Pro are saved here,') . "\n"
		          . " * " . t('so that they won\'t get erased by C5 when the Designer Content Pro addon is upgraded.') . "\n"
 		          . " */"
		          . PHP_EOL . PHP_EOL
		          . 'class DesignerContentProBlocksPackage extends Package {'
		          . PHP_EOL . PHP_EOL
		          . "\tprotected \$pkgHandle = '{$escaped_pkgHandle}';\n"
		          . "\tprotected \$pkgName = '{$escaped_pkgName}';\n"
		          . "\tprotected \$pkgDescription = '{$escaped_pkgDescription}';\n"
		          . "\tprotected \$appVersionRequired = '5.6.0';\n"
		          . "\tprotected \$pkgVersion = '1.0';\n"
		          . "\n}";
		
		$path = $pkg_dir . '/controller.php';
		$this->outputFile($path, $contents);
	}
	
	private function createPkgBlocksDir($pkg_dir) {
		$path = $pkg_dir . '/blocks';
		$this->outputDir($path);
	}
	
	private function copyPkgIcon($pkg_dir) {
		$dcp_pkg_dir = Package::getByHandle('designer_content_pro')->getPackagePath();
		$source = $dcp_pkg_dir . '/images/container_package_icon.png';
		$dest = $pkg_dir . '/icon.png';
		copy($source, $dest);
	}
	
	private function createPkgElementsDashboardDir($pkg_dir) {
		$path = $pkg_dir . '/elements';
		$this->outputDir($path);
		$path = $path . '/dashboard';
		$this->outputDir($path);
		return $path;
	}
	
	private function createPkgInstallElement($elements_dashboard_dir) {
		$exception_msg = t('This addon cannot be installed on its own -- you must install the Designer Content Pro addon (which will then cause this addon to be installed).');
		$escaped_exception_msg = $this->esc($exception_msg);
		
		$contents = $this->getC5Header()
		          . PHP_EOL . PHP_EOL
		          . "echo '<h1 style=\"color: red; font-size: 36px; padding-bottom: 20px;\">{$escaped_exception_msg}</h1>';"
		          . PHP_EOL . PHP_EOL
		          . "throw new Exception('{$escaped_exception_msg}');"
		          ;
		$path = $elements_dashboard_dir . '/install.php';
		$this->outputFile($path, $contents);
	}
	
	private function createPkgUninstallElement($elements_dashboard_dir) {
		$exception_msg = t('This addon cannot be uninstalled on its own -- you must uninstall the Designer Content Pro addon (which will then cause this addon to be uninstalled).');
		$escaped_exception_msg = $this->esc($exception_msg);
		
		$contents = $this->getC5Header()
		          . PHP_EOL . PHP_EOL
		          . "echo '<h1 style=\"color: red; font-size: 36px; padding-bottom: 20px;\">{$escaped_exception_msg}</h1>';"
		          . PHP_EOL . PHP_EOL
		          . "throw new Exception('{$escaped_exception_msg}');"
		          ;
		$path = $elements_dashboard_dir . '/uninstall.php';
		$this->outputFile($path, $contents);
	}
	
	
/*** Utility Functions ***/
	private function esc($str) {
		$str = str_replace('\\', '\\\\', $str);
		$str = str_replace("'", "\\'", $str);
		return $str;
	}

	private function getPhpTag() {
		return '<' . '?' . 'p' . 'h' . 'p' . ' '; //outsmart c5 marketplace process that inserts extra spaces after every php opening tag
	}

	private function getC5Header() {
		return $this->getPhpTag() . 'defined(\'C5_EXECUTE\') or die(_("Access Denied."));';
	}
	
	private function outputFile($path, $contents) {
		//Normally we'd just call: file_put_contents($path, $contents);
		//But we want to use C5's 'file' helper instead
		// to work within marketplace guidelines.
		//Note that the c5 helper only offers file_put_contents() in APPEND mode,
		// so first we must clear the file (if it exists), then append to the empty file.
		$fh = Loader::helper('file');
		$fh->clear($path);
		$fh->append($path, $contents);
	}
	
	private function outputDir($path) {
		$path = rtrim($path, '/'); //remove trailing slash
		mkdir($path);
	}
	
}