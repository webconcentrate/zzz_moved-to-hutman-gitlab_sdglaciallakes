<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/***************************************************************************
 * Copyright (C) Web Concentrate - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Written by Jordan Lev <jordan@webconcentrate.com>, January-August 2013
 ***************************************************************************/


abstract class DcpFieldDisplay {
	// abstract public function __construct(); //accept data for the object as instantiation params
	//DEV NOTE: As of PHP 5.4, constructor method signatures can no longer differ between abstract base classes and their subclasses.
	//          Since each of our subclass accepts different kinds of data (and a different number of variables),
	//          we can no longer put the constructor in the abstract base class.

	abstract public function getDisplay(); //returns the "most common" HTML for this data (e.g. if image field, an <img> tag, if link, an <a> tag... etc.)
	public function display() { //override this in child classes when getDisplay() has params (display() params should always match getDisplay() params so they can be called interchangeably)
		echo $this->getDisplay();
	}
	public function __toString() {
		return $this->getDisplay();
	}
	
	abstract public function isEmpty();
	public function isNotEmpty() {
		return !$this->isEmpty();
	}
}

class DcpFieldDisplay_File extends DcpFieldDisplay {
	protected $fID;
	protected $fileObj = null; //will be lazy-loaded
	protected $title_raw;
	protected $title_escaped;
	
	//As an optimization, if you already have a file object loaded,
	// you can pass that in for $fID (to avoid us having to re-query the db).
	public function __construct($fID, $title) {
		if (is_object($fID)) {
			$this->fileObj = $fID;
			$this->fID = $fID->getFileID();
		} else {
			$this->fID = $fID;
		}
		
		if (empty($title)) {
			$file = $this->getFileObj();
			if (!is_null($file)) {
				$title = $this->getTitleForFileObj($file);
			}
		}
		$this->title_raw = $title;
		$this->title_escaped = is_null($title) ? '' : Loader::helper('text')->entities($title);
	}
	
	protected function getTitleForFileObj($file) { //so Image class can override this without having to override the whole constructor
		return is_null($file) ? null : $file->getTitle();
	}

	public function display($force_download = true) {
		echo $this->getDisplay($force_download);
	}
	public function getDisplay($force_download = true) {
		$output = '';
		
		if ($this->isNotEmpty()) {
			$output .= '<a href="' . $this->getHref($force_download) . '">';
			$output .= $this->getTitle();
			$output .= '</a>';		
		}
		
		return $output;
	}
	
	public function isEmpty() {
		return empty($this->fID);
	}
	
	public function getFileObj() {
		if ($this->isEmpty()) {
			return null;
		}
		
		if (is_null($this->fileObj)) {
			$this->fileObj = File::getByID($this->fID);
		}
		
		if (!is_null($this->fileObj)) {
			$fp = new Permissions($this->fileObj);
			if (!$fp->canViewFile()) {
				return null;
			}
		}
				
		return $this->fileObj;
	}
	public function getFileObject() {
		return $this->getFileObj();
	}
	
	public function getTitle($escaped = true) {
		return $escaped ? $this->title_escaped : $this->title_raw;
	}
	
	public function getHref($force_download = true) {
		if ($this->isEmpty()) {
			return '';
		}
		
		$path = '/download_file' . ($force_download ? '/force' : '/view_inline');
		$cID = Page::getCurrentPage()->getCollectionID();
		return View::url($path, $this->fID, $cID); //the cID is used for site statistics (so you know which page the file was downloaded from -- see file properties in the dashboard file manager)
	}
}

class DcpFieldDisplay_Image extends DcpFieldDisplay_File {
	
	protected function getTitleForFileObj($file) {
		if (is_null($file)) {
			return null;
		} else {
			//If file has a description, use that for alt text.
			//Otherwise return nothing (do *not* fallback to title,
			// because C5 populates those with the filename...
			// which is worse than useless as a description
			// of what an image portrays).
			$description = $file->getDescription();
			$description = trim($description);
			return empty($description) ? null : $description;
		}
	}

	public function display($width = 0, $height = 0, $crop = false) {
		echo $this->getDisplay($width, $height, $crop);
	}
	public function getDisplay($width = 0, $height = 0, $crop = false) {
		$output = '';
		
		if ($this->isNotEmpty()) {
			$img = $this->getImageObj($width, $height, $crop);
			if (!is_null($img)) {
				$output .= '<img';
				$output .= ' src="' . $img->src . '"';
				$output .= ' width="' . $img->width . '"';
				$output .= ' height="' . $img->height . '"';
				$output .= ' alt="' . $this->getAltText() . '"';
				$output .= ' />';
			}
		}
		
		return $output;
	}
	
	public function isEmpty() {
		return empty($this->fID);
	}
	
	public function getImageObj($width = 0, $height = 0, $crop = false) {
		$file = $this->getFileObj();
		if (is_null($file)) {
			return null;
		}
		
		$oWidth = $file->getAttribute('width');
		$oHeight = $file->getAttribute('height');
		
		$generate_thumbnail = true;
		if (empty($width) && empty($height)) {
			$generate_thumbnail = false;
		} else if ($width >= $oWidth && $height >= $oHeight) {
			$generate_thumbnail = false;
		}
		
		if ($generate_thumbnail) {
			$width = empty($width) ? 9999 : $width;
			$height = empty($height) ? 9999 : $height;
			$image = Loader::helper('image')->getThumbnail($file, $width, $height, $crop);
		} else {
			$image = new stdClass;
			$image->src = $file->getRelativePath();
			$image->width = $oWidth;
			$image->height = $oHeight;
		}
		
		return $image;
	}
	public function getImageObject($width = 0, $height = 0, $crop = false) {
		return $this->getImageObj($width, $height, $crop);
	}
	
	public function getAltText($escaped = true) {
		return $this->getTitle($escaped);
	}
	
}

class DcpFieldDisplay_Wysiwyg extends DcpFieldDisplay {
	protected $content;
	
	public function __construct($content) {
		$this->content = $content;
	}
	
	public function getDisplay() {
		return $this->getContent();
	}	
	
	public function isEmpty() {
		return empty($this->content);
	}
	
	public function getContent() {
		return $this->content;
	}
}

class DcpFieldDisplay_Textbox extends DcpFieldDisplay {
	protected $text_raw;
	protected $text_escaped;
	
	public function __construct($text) {
		$this->text_raw = $text;
		$this->text_escaped = Loader::helper('text')->entities($text);
	}
	
	public function getDisplay() {
		return $this->getText();
	}
	
	public function isEmpty() {
		return empty($this->text_raw);
	}
	
	public function getText($escaped = true) {
		return $escaped ? $this->text_escaped : $this->text_raw;
	}
}

class DcpFieldDisplay_Textarea extends DcpFieldDisplay_Textbox {
	
	public function getText($escaped = true) {
		return $escaped ? nl2br($this->text_escaped) : $this->text_raw;
	}
}

class DcpFieldDisplay_Link extends DcpFieldDisplay {
	protected $cID;
	protected $pageObj = null; //will be lazy-loaded
	protected $url_raw;
	protected $url_normalized;
	protected $text_raw;
	protected $text_escaped;
	
	public function __construct($cID, $url, $text) {
		$this->cID = $cID;
		$this->url_raw = $url;
		$this->url_normalized = $this->normalizeUrl($url);
		
		if (empty($text)) {
			if (!empty($cID)) {
				$page = $this->getPageObj();
				$text = is_null($page) ? '' : $page->getCollectionName();
			} else if (!empty($url)) {
				$text = $this->getHref();
			} else {
				$text = null;
			}
		}
		
		$this->text_raw = $text;
		$this->text_escaped = is_null($text) ? '' : Loader::helper('text')->entities($text);
	}
	
	//pass true to always open link in new window,
	//pass false to never open link in new window,
	//pass null (or don't pass anything) to open external links in new window and internal page links in same window
	public function display($open_in_new_window = null) {
		echo $this->getDisplay($open_in_new_window = null);
	}

	public function isEmpty() {
		$href = $this->getHref();
		return empty($href);
	}

	public function getDisplay($open_in_new_window = null) {
		$output = '';
		
		if ($this->isNotEmpty()) {
			$open_in_new_window = is_null($open_in_new_window) ? empty($this->cID) : $open_in_new_window;
			$target = $open_in_new_window ? ' target="_blank"' : '';
			
			$output .= '<a href="' . $this->getHref() . '"' . $target . '>';
			$output .= $this->getText();
			$output .= '</a>';
		}
		
		return $output;
	}
	
	public function getPageObj() {
		if (empty($this->cID)) {
			return null;
		}
		
		if (is_null($this->pageObj)) {
			$this->pageObj = Page::getByID($this->cID);
		}
		
		//Watch out: Page::getByID() returns a page object even if cID doesn't exist!
		if (is_object($this->pageObj) && ($this->pageObj->getCollectionID() == 0)) {
			return null;
		}
		
		return $this->pageObj;
	}
	public function getPageObject() {
		return $this->getPageObj();
	}
	
	//Returns the full url to whichever link type is appropriate (either internal page or external url)
	public function getHref() {
		if (empty($this->cID)) {
			return $this->url_normalized;
		} else {
			$page = $this->getPageObj();
			return is_null($page) ? '' : Loader::helper('navigation')->getLinkToCollection($page);
		}
	}
	
	public function getText($escaped = true) {
		return $escaped ? $this->text_escaped : $this->text_raw;
	}
	
	//For "combo" fields, this tells you whether the user provided a site page or a text url.
	//Returns TRUE if user chose a site page from the page chooser control.
	//Returns FALSE if user entered a text url (aka "external url").
	//Also returns FALSE if user left both fields blank.
	public function isPageLink() {
		return !empty($this->cID);
	}
	
	//internal helper function -- attempts to create a full and valid url from potentially incomplete data
	private function normalizeUrl($url) {
		if (empty($url)) {
			return '';
		} else if ($this->colonExistsBeforeDot($url)) {
			return $url; //probably already has "http://", "https://", "mailto:", "tel:", etc.
		} else if (strpos($url, '@') !== false) {
			return 'mailto:' . $url;
		} else if (strpos($url, '/') === 0) {
			return View::url($url); //site path (not an external url)
		} else if (strpos($url, '#') === 0) {
			return $url; //anchor link
		} else {
			return 'http://' . $url;
		}
	}
	//internal helper function -- checks if a colon (:) exists in the string AND it appears before any dots (.)
	//The purpose of this is to guess whether or not a string contains a URI scheme (http://, mailto:, tel:)
	// (since colons can legitemately appear in the path of URL's, we can't just check for their existence).
	private function colonExistsBeforeDot($str) {
		$colonPos = strpos($str, ':');
		$dotPos = strpos($str, '.');
		if ($colonPos === false) {
			return false;
		} else if ($dotPos === false) {
			return true;
		} else {
			return $colonPos < $dotPos;
		}
	}
}

class DcpFieldDisplay_Page extends DcpFieldDisplay {
	protected $cID;
	protected $pageObj = null; //will be lazy-loaded
	
	public function __construct($cID) {
		$this->cID = $cID;
	}
	
	public function getDisplay() {
		$output = '';
		
		if ($this->isNotEmpty()) {
			$page = $this->getPageObj();
			$href = Loader::helper('navigation')->getLinkToCollection($page);
			$text = $page->getCollectionName();
			$output = '<a href="' . $href . '">' . $text . '</a>';
		}
		
		return $output;
	}
	
	public function isEmpty() {
		$page = $this->getPageObj();
		return is_null($page);
	}
	
	public function __call($name, $arguments) {
		if ($this->isNotEmpty()) {
			$page = $this->getPageObj();
			if (is_callable(array($page, $name))) {
				return call_user_func_array(array($page, $name), $arguments);
			} else {
				//throw an exception with a slightly more helpful error message than what call_user_func_array gives
				// (this message will be confusing as well, but at least it will say the line number that caused the error
				// in the block view.php file itself, not the underlying DCP library file)
				$trace = debug_backtrace(); //from http://www.php.net/manual/en/language.oop5.overloading.php#example-214
				trigger_error(t('Call to undefined method Page::%s in %s on line %s', $name, $trace[0]['file'], $trace[0]['line']), E_USER_WARNING);
			}
		}
	}
	
	public function getPageObj() {
		if (empty($this->cID)) {
			return null;
		}
		
		if (is_null($this->pageObj)) {
			$this->pageObj = Page::getByID($this->cID);
		}
		
		//Watch out: Page::getByID() returns a page object even if cID doesn't exist!
		if (is_object($this->pageObj) && ($this->pageObj->getCollectionID() == 0)) {
			return null;
		}
		
		return $this->pageObj;
	}
	public function getPageObject() {
		return $this->getPageObj();
	}
	
}

class DcpFieldDisplay_Date extends DcpFieldDisplay {
	protected $date_Ymd;
	
	public function __construct($date_Ymd) {
		$this->date_Ymd = $date_Ymd;
	}
	
	public function display($format = DATE_APP_GENERIC_MDY) {
		echo $this->getDisplay($format);
	}

	public function getDisplay($format = DATE_APP_GENERIC_MDY) {
		return $this->getDate($format);
	}

	public function isEmpty() {
		return empty($this->date_Ymd);
	}
	
	public function getDate($format = DATE_APP_GENERIC_MDY) {
		$date_ts = $this->isEmpty() ? 0 : strtotime($this->date_Ymd);
		return empty($date_ts) ? '' : date($format, $date_ts);
	}
}

class DcpFieldDisplay_Checkbox extends DcpFieldDisplay {
	protected $checked;
	
	public function __construct($checked) {
		$this->checked = $checked;
	}
	
	public function display($checked_output = null, $unchecked_output = '') {
		echo $this->getDisplay($checked_output, $unchecked_output);
	}
	
	//Default for $checked_output is actually "yes"
	// (even though the function param defaults to null...
	// because we need to use the t() function for translation,
	// but php does not allow functions to be used as default params).
	public function getDisplay($checked_output = null, $unchecked_output = '') {
		if (is_null($checked_output)) {
			$checked_output = t('yes');
		}
		$output = $this->isChecked() ? $checked_output : $unchecked_output;
		return Loader::helper('text')->entities($output);
	}
	
	public function isEmpty() {
		return empty($this->checked);
	}
	
	public function isChecked() {
		return $this->isNotEmpty();
	}
	
}

class DcpFieldDisplay_Select extends DcpFieldDisplay {
	protected $selected_handles; //array of selected handles
	protected $all_options; //array of all available list choices
	private $th; //text helper
	
	public function __construct($selected_handles_as_comma_separated_string, $all_options_as_array) {
		$this->selected_handles = empty($selected_handles_as_comma_separated_string) ? array() : explode(',', $selected_handles_as_comma_separated_string);
		$this->all_options = $all_options_as_array;
		$this->th = Loader::helper('text');
	}
	
	public function getDisplay($separator = ', ') {
		return implode($separator, $this->getSelectedLabels());
	}
	
	public function isEmpty() {
		return empty($this->selected_handles);
	}
	
	public function isSelected($handle) {
		return in_array($handle, $this->selected_handles);
	}
	
	public function getSelectedHandle() {
		return empty($this->selected_handles) ? '' : reset($this->selected_handles); //`reset` rewinds the array and returns first value
	}
	
	public function getSelectedHandles() {
		return $this->selected_handles;
	}
	
	public function getSelectedLabel($escaped = true) {
		$selected_handle = $this->getSelectedHandle();
		if (empty($selected_handle)) {
			return '';
		}
		
		$selected_label = $this->all_options[$selected_handle];
		return $escaped ? $this->th->entities($selected_label) : $selected_label;
	}
	
	public function getSelectedLabels($escaped = true) {
		return array_values($this->getSelections($escaped));
	}
	
	public function getSelections($escaped = true) {
		$selections = array();
		foreach ($this->selected_handles as $selected_handle) {
			$selected_label = $this->all_options[$selected_handle];
			$selections[$selected_handle] = $escaped ? $this->th->entities($selected_label) : $selected_label;
		}
		return $selections;
	}
	
	public function getAllOptions() {
		return $this->all_options;
	}
	
}

class DcpFieldDisplay_Fileset extends DcpFieldDisplay {
	protected $fsID;
	protected $filesetObj = null; //will be lazy-loaded
	protected $files = null; //will be lazy-loaded
	private $th; //text helper

	public function __construct($fsID) {
		$this->fsID = $fsID;
		$this->th = Loader::helper('text');
	}
	
	public function display($force_download = true) {
		echo $this->getDisplay($force_download);
	}
	public function getDisplay($force_download = true) {
		$output = '';
		
		if ($this->isNotEmpty()) {
			$output .= '<ul>';
			foreach ($this->getFiles() as $fileObj) {
				$output .= '<li>';
				$output .= '<a href="' . $fileObj->getHref($force_download) . '">';
				$output .= $fileObj->getTitle();
				$output .= '</a>';		
				$output .= '</li>';
			}
			$output .= '</ul>';
		}
		
		return $output;
	}
	
	public function isEmpty() {
		return empty($this->fsID);
	}
	
	public function getFilesetObj() {
		if ($this->isEmpty()) {
			return null;
		}
		
		if (is_null($this->filesetObj)) {
			$this->filesetObj = FileSet::getByID($this->fsID);
		}
		
		return $this->filesetObj;
	}
	public function getFilesetObject() {
		return $this->getFilesetObj();
	}
	
	public function getTitle($escaped = true) {
		$fs = $this->getFilesetObj();
		$title = $fs ? $fs->fsName : '';
		return $escaped ? $this->th->entities($title) : $title;
	}
	
	/**
	 * $restrict_to_type can be one of these constants
	 * (as defined in /concrete/core/libraries/file/types.php):
	 *   FileType::T_IMAGE
	 *   FileType::T_VIDEO
	 *   FileType::T_TEXT
	 *   FileType::T_AUDIO
	 *   FileType::T_DOCUMENT
	 *   FileType::T_APPLICATION
	 */
	protected function getFilesetFileObjs($restrict_to_type = null) {
		$fs = $this->getFilesetObj();
		if (!$fs) {
			return array();
		}
		
		$fl = new FileList();		
		$fl->filterBySet($fs);
		if ($restrict_to_type) {
			$fl->filterByType($restrict_to_type);
		}
		$fl->sortByFileSetDisplayOrder();
		
		$all_files = $fl->get();
		$permitted_files = array();
		foreach ($all_files as $f) {
			$fp = new Permissions($f);
			if ($fp->canRead()) {
				$permitted_files[] = $f;
			}
		}
		
		return $permitted_files;	
	}
	
	//See comments above `getFilesetFileObjs()` function for info on `$restrict_to_type`
	public function getFiles($restrict_to_type = null) {
		$dcp_file_objs = array();
		foreach ($this->getFilesetFileObjs($restrict_to_type) as $f) {
			$dcp_file_objs[] = new DcpFieldDisplay_File($f, null);
		}
		return $dcp_file_objs;
	}
	
	public function getImages() {
		$dcp_image_objs = array();
		foreach ($this->getFilesetFileObjs(FileType::T_IMAGE) as $f) {
			$dcp_image_objs[] = new DcpFieldDisplay_Image($f, null);
		}
		return $dcp_image_objs;
	}
}

class DcpFieldDisplay_Sdglmemberssubcategory extends DcpFieldDisplay {
	private $subcategory_id;
	
	private $subcategory = null; //will be lazy-loaded
	private $locations = null; //ditto
	
	public function __construct($subcategory_id) {
		$this->subcategory_id = $subcategory_id;
	}
	
	public function getDisplay() {
		$output = '';
		
		if ($this->isNotEmpty()) {
			$subcategory = $this->getSubcategory();
			$output = '<a href="' . $subcategory->directory_url . '">' . $subcategory->title . '</a>';
		}
		
		return $output;
	}
	
	public function isEmpty() {
		$subcategory = $this->getSubcategory();
		return is_null($subcategory);
	}
	
	public function getSubcategory() {
		if (empty($this->subcategory_id)) {
			return null;
		}
		
		if (is_null($this->subcategory)) {
			Loader::model('subcategory/query', 'sdgl_members');
			$this->subcategory = SubcategoryQuery::byId($this->subcategory_id);
		}
		
		return $this->subcategory;
	}
	
	public function getLocations() {
		if (empty($this->subcategory_id)) {
			return null;
		}
		
		if (is_null($this->locations)) {
			Loader::model('location/query', 'sdgl_members');
			$this->locations = LocationQuery::activeBySubcategoryId($this->subcategory_id);
		}
		
		return $this->locations;
	}
}

class DcpFieldDisplay_Sdglmemberslocation extends DcpFieldDisplay {
	private $location_id;
	
	private $location = null; //will be lazy-loaded
	
	public function __construct($location_id) {
		$this->location_id = $location_id;
	}
	
	public function getDisplay() {
		$output = '';
		
		if ($this->isNotEmpty()) {
			$location = $this->getLocation();
			$output = '<a href="' . $location->details_page_url . '">' . $location->title . '</a>';
		}
		
		return $output;
	}
	
	public function isEmpty() {
		$location = $this->getLocation();
		return is_null($location);
	}
	
	public function getLocation() {
		if (empty($this->location_id)) {
			return null;
		}
		
		if (is_null($this->location)) {
			Loader::model('location/query', 'sdgl_members');
			$this->location = LocationQuery::byId($this->location_id);
		}
		
		return $this->location;
	}
}
