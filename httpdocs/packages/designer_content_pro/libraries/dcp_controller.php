<?php defined('C5_EXECUTE') or die(_("Access Denied."));

/***************************************************************************
 * Copyright (C) Web Concentrate - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Written by Jordan Lev <jordan@webconcentrate.com>, January-August 2013
 ***************************************************************************/


Loader::library('dcp_field_types', 'designer_content_pro');
Loader::model('dcp_block_type', 'designer_content_pro');

//NOTE: Do *NOT* name this class DcpBlockController
// because having the words "BlockController" in the class name
// confuses C5's auto-loader, and weird errors ensue (but only
// under certain circumstances... like it happens with APC enabled
// but not without... under 5.6.x... maybe).
class DcpController extends BlockController {
	
	protected $btWrapperClass = 'ccm-ui'; //so twitter bootstrap styles are available in edit dialog
	protected $btInterfaceWidth = 800;
	protected $btInterfaceHeight = 500;
	
	private $btSingleFieldDataColName = 'singleFieldData'; //this field must exist (should be "X2" type) in block's db.xml file
	private $btSingleFieldObjects = null; //Array of "DcpFieldType" objects that represent the "single fields" schema
	private $btSingleFieldDataRecord = null; //Holds the unserialized "single field" data. USE getSingleFieldDataRecord() TO ACCESS THIS!
	
	private $btRepeatingItemDataColName = 'repeatingItemData'; //this field must exist (should be "X2" type) in block's db.xml file
	private $btRepeatingItemDataFieldObjects = null; //Array of "DcpFieldType" objects that represents the repeating item data's schema
	private $btRepeatingItemDataRecords = null; //Holds the unserialized repeating item data. USE getRepeatingItemDataRecords() TO ACCESS THIS!
	private $btRepeatingItemLabelField = ''; //field name containing text to use as label in edit dialog's repeating item list (or leave blank to use placeholder text)
	private $btRepeatingItemThumbField = ''; //field name containing fID of image to use as thumbnail in edit dialog's repeating item list (or leave blank to use placeholder image)
	
	//The following 3 properties should be overridden in each blocktype's controller sub-class:
	public $btDCProBlockOptions = array();
	public $btDCProSingleFields = array(); //this one is especially important to be defined here in the base class -- it prevents errors when a blocktype was generated from an older version of DCP (prior to 2.0) that didn't have this property
	public $btDCProRepeatingItemFields = array();
	
	public function __construct($obj = null) {
		
		parent::__construct($obj);
		
		//Set up our field schemas...
		if (is_null($this->btSingleFieldObjects) && is_null($this->btRepeatingItemDataFieldObjects)) { //just in case we've already been instantiated (yeah, I don't know how the constructor would be called twice, but this seems to prevent some random errors)
			$this->initializeSchema(); //Convert the controller arrays that describe the field schema into arrays of DcpFieldType_xxx objects
		}
		
		//Set "single fields" data as variables for the view
		// (do this here in the constructor instead of the view() or on_page_view() methods
		// because those are the two most likely methods to get implemented
		// if the block controller gets overridden, and people will inevitably
		// fail to call parent::view() or parent::on_page_view() because it is non-standard to do so).
		//Note that C5 always sets a bunch of its own variables in block views,
		// and also relies on some of them between controller instantiation and display...
		// so in order to avoid collisions we validate single field handles when creating/editing blocktypes
		// (see DcpBlockTypeModel::validateSingleFieldHandleIsNotC5BlockViewVarName()).
		foreach (get_object_vars($this->getSingleFields()) as $handle => $field) {
			$this->set($handle, $field);
		}
	}
	
	private function initializeSchema() {
		$this->btSingleFieldObjects = array();
		if (property_exists($this, 'btDCProSingleFields')) { //blocktypes that were defined in older versions of DCP might not have this property
			foreach ($this->btDCProSingleFields as $field_handle => $field_info) {
				$this->btSingleFieldObjects[$field_handle] = $this->instantiateFieldTypeObject($field_info['type'], $field_handle, $field_info['label'], $field_info['options']);
			}
		}
		
		$this->btRepeatingItemDataFieldObjects = array();
		if (property_exists($this, 'btDCProRepeatingItemFields')) { //this probably always exists, but let's check anyway just to be safe
			foreach ($this->btDCProRepeatingItemFields as $field_handle => $field_info) {
				$this->btRepeatingItemDataFieldObjects[$field_handle] = $this->instantiateFieldTypeObject($field_info['type'], $field_handle, $field_info['label'], $field_info['options']);
			
				if ($field_info['options']['is_item_label']) {
					$this->btRepeatingItemLabelField = $field_handle;
				}
				if ($field_info['options']['is_item_thumb']) {
					$this->btRepeatingItemThumbField = $field_handle;
				}
			}
		}
	}
	private function instantiateFieldTypeObject($type, $handle, $label, $options) {
		$class = 'DcpFieldType_' . ucfirst(strtolower($type));
		
		if (!class_exists($class)) {
			throw new Exception(t('DESIGNER CONTENT PRO ERROR: Invalid field type "%s" declared for Block Type "%s"', $type, $this->btHandle));
		}
		
		$object = new $class($handle, $label, $options);
		
		return $object;
	}
	
	private function getSingleFieldDataRecord() {
		if (is_null($this->btSingleFieldDataRecord)) {
			$serialized_data = property_exists($this, $this->btSingleFieldDataColName) ? $this->{$this->btSingleFieldDataColName} : array(); //blocktypes that were defined in older versions of DCP might not have this
			$this->btSingleFieldDataRecord = empty($serialized_data) ? array() : unserialize($serialized_data);
		}
		return $this->btSingleFieldDataRecord;
	}
	
	private function getRepeatingItemDataRecords() {
		if (is_null($this->btRepeatingItemDataRecords)) {
			$serialized_data = property_exists($this, $this->btRepeatingItemDataColName) ? $this->{$this->btRepeatingItemDataColName} : array(); //this probably always exists, but let's check anyway just to be safe
			$this->btRepeatingItemDataRecords = empty($serialized_data) ? array() : unserialize($serialized_data);
		}
		return $this->btRepeatingItemDataRecords;
	}
	
	private function getSingleFieldDataFromPOST() {
		$return_data = array();
		
		foreach ($this->btSingleFieldObjects as $field) {
			$post_data_per_input = $field->getPOSTDataPerInput('');
			foreach ($post_data_per_input as $input_name => $input_data) {
				$return_data[$input_name] = $input_data;
			}
		}
		
		return $return_data;
	}
	
	private function getRepeatingItemDataFromPOST() {
		//$_POST will contain an array like this:
		// ['file1_cID'][0] = #
		// ['file1_cID'][1] = #
		// ['file1_cID'][2] = #
		// ['file1_title'][0] = '...'
		// ['file1_title'][1] = '...'
		// ['file1_title'][2] = '...'
		// ['textbox1_text'][0] = '...'
		// ['textbox1_text'][1] = '...'
		// ['textbox1_text'][2] = '...'
		// ['textbox2_text'][0] = '...'
		// ['textbox2_text'][1] = '...'
		// ['textbox2_text'][2] = '...'
		//
		//We want to convert that to this (for serialization):
		// [0]['file1_cID'] = #
		// [0]['file1_title'] = '...'
		// [0]['textbox1_text'] = '...'
		// [0]['textbox2_text'] = '...'
		// [1]['file1_cID'] = #
		// [1]['file1_title'] = '...'
		// [1]['textbox1_text'] = '...'
		// [1]['textbox2_text'] = '...'
		// [2]['file1_cID'] = #
		// [2]['file1_title'] = '...'
		// [2]['textbox1_text'] = '...'
		// [2]['textbox2_text'] = '...'
		//
		//BUT, $_POST will also contain other stuff we're not concerned with,
		// so we can't just do this to every $_POST field -- instead we need
		// to go through the field definitions to know which $_POST items we want.
		
		$return_data = array();
		
		foreach ($this->btRepeatingItemDataFieldObjects as $field) {
			$post_data_per_input = $field->getPOSTDataPerInput();
			foreach ($post_data_per_input as $input_name => $input_data) {
				for ($i = 0, $cnt = count($input_data); $i < $cnt; $i++) {
					$return_data[$i][$input_name] = $input_data[$i];
				}
			}
		}
		
		return $return_data;
	}
	
	/*************************************************************************/
	
	public function save($args) {
		$args[$this->btSingleFieldDataColName] = serialize($this->getSingleFieldDataFromPOST());
		$args[$this->btRepeatingItemDataColName] = serialize($this->getRepeatingItemDataFromPOST());
		
		parent::save($args);
	}
	
	public function validate($args) {
		$e = Loader::helper('validation/error');
		
		foreach ($this->btSingleFieldObjects as $field) {
			$error = $field->validate($args);
			if (!empty($error)) {
				$e->add($error);
			}
		}
		
		return $e;
	}
	
	final public function getSearchableContent() { //use "final" to prevent block controllers from bypassing this
		$content = array();
		
		$record = $this->getSingleFieldDataRecord();
		foreach ($this->btSingleFieldObjects as $field) {
			$content[] = $field->getSearchableContent($record);
		}
		
		foreach ($this->getRepeatingItemDataRecords() as $record) {
			foreach ($this->btRepeatingItemDataFieldObjects as $field) {
				$content[] = $field->getSearchableContent($record);
			}
		}
		
		$content = array_filter($content); //removes empty items from array
		$content = implode(' - ', $content);
		return $content;
	}
	
	//Note that we use this for both the intended "translatable strings" functionality,
	// *and* as a means of passing php variables to javascript.
	final public function getJavaScriptStrings() { //use "final" to prevent block controllers from bypassing this
		return array(
			//translatable strings
			'edit_repeating_item_dialog_title_prefix' => t('Edit Item #'),
			'edit_repeating_item_default_label' => t('click to edit'),
			'edit_repeating_item_error_missing_link' => t('You must provide a link'),
			//php vars to send to js
			'repeatingItemLabelField' => $this->getRepeatingItemLabelFieldName(),
			'repeatingItemLabelIsLookup' => ($this->getRepeatingItemLabelIsLookup() ? '1' : ''),
			'repeatingItemThumbField' => $this->getRepeatingItemThumbFieldName(),
			'editRepeatingItemDialogURL' => REL_DIR_FILES_TOOLS_BLOCKS . '/' . $this->btHandle . '/edit_repeating_item', //much easier than calling getBlockTypeToolsURL() in the concrete/urls helper
			'repeatingItemLabelLookupURL' => REL_DIR_FILES_TOOLS_BLOCKS . '/' . $this->btHandle . '/lookupedit_repeating_item_label', //ditto
		);
	}
	
	/*************************************************************************/
	
	public function outputAllSingleFields($data) {
		foreach ($this->btSingleFieldObjects as $field) {
			$field->outputEditFields(true, $data);
		}
	}
	
	public function outputAllRepeatingItemJQTmplHiddenFields() {
		foreach ($this->btRepeatingItemDataFieldObjects as $field) {
			$field->outputJQTmplHiddenFields();
		}
	}
	
	public function outputAllRepeatingItemEditFields($data) {
		foreach ($this->btRepeatingItemDataFieldObjects as $field) {
			$field->outputEditFields(false, $data);
		}
	}
	
	public function getRepeatingItemLabelFieldName() {
		return empty($this->btRepeatingItemLabelField) ? '' : $this->btRepeatingItemDataFieldObjects[$this->btRepeatingItemLabelField]->repeatingItemLabelFieldName();
	}
	
	public function getRepeatingItemLabelIsLookup() {
		return empty($this->btRepeatingItemLabelField) ? '' : $this->btRepeatingItemDataFieldObjects[$this->btRepeatingItemLabelField]->repeatingItemLabelIsLookup();
	}
	
	public function getRepeatingItemThumbFieldName() {
		return empty($this->btRepeatingItemThumbField) ? '' : $this->btRepeatingItemDataFieldObjects[$this->btRepeatingItemThumbField]->repeatingItemThumbFieldName();
	}
	
	public function getRepeatingItemLabelValue($item_data) {
		return $this->btRepeatingItemDataFieldObjects[$this->btRepeatingItemLabelField]->repeatingItemLabelLookup($item_data);
	}
	
	public function getRepeatingItemThumbSrc($repeating_item) {
		$field_name = $this->getRepeatingItemThumbFieldName();
		if (empty($field_name) || empty($repeating_item[$field_name])) {
			return '';
		} else {
			return File::getByID($repeating_item[$field_name])->getThumbnailSRC(1);
		}
	}
	
	/*************************************************************************/
	
	public function hasSingleFields() {
		return !empty($this->btSingleFieldObjects);
	}
	
	public function hasRepeatingItemFields() {
		return !empty($this->btRepeatingItemDataFieldObjects);
	}
	
	public function getSingleFields($mode = 'view') {
		if ($mode == 'edit') {
			return $this->getSingleFieldsForEdit();
		} else {
			return $this->getSingleFieldsForView();
		}
	}
	
	public function getRepeatingItems($mode = 'view') {
		if ($mode == 'edit') {
			return $this->getRepeatingItemsForEdit();
		} else {
			return $this->getRepeatingItemsForView();
		}
	}
	
	//returns single fields as an associative array (for easy json_encoding)
	private function getSingleFieldsForEdit() {
		$single_fields = array();
		
		$record = $this->getSingleFieldDataRecord();
		foreach ($this->btSingleFieldObjects as $fieldObject) {
			$fieldDataArray = $fieldObject->getDataArrayForEdit($record);
			foreach ($fieldDataArray as $key => $val) {
				$single_fields[$key] = $val;
			}
		}
		
		return $single_fields;
	}
	
	//returns single fields as display objects (for use in block view.php templates)
	private function getSingleFieldsForView() {
		$single_fields = new StdClass;
		
		$record = $this->getSingleFieldDataRecord();
		foreach ($this->btSingleFieldObjects as $fieldName => $fieldObject) {
			$single_fields->$fieldName = $fieldObject->getDisplayObjectForView($record);
		}
		
		return $single_fields;
	}
	
	//returns repeating items as associative arrays (for easy json_encoding)
	private function getRepeatingItemsForEdit() {
		$repeating_items = array();
		
		foreach ($this->getRepeatingItemDataRecords() as $record) {
			$repeating_item = array();
			foreach ($this->btRepeatingItemDataFieldObjects as $fieldObject) {
				$fieldDataArray = $fieldObject->getDataArrayForEdit($record);
				foreach ($fieldDataArray as $key => $val) {
					$repeating_item[$key] = $val;
				}
			}
			$repeating_items[] = $repeating_item;
		}
		
		return $repeating_items;
	}
	
	//returns repeating items as display objects (for use in block view.php templates)
	private function getRepeatingItemsForView() {
		$repeating_items = array();
		
		foreach ($this->getRepeatingItemDataRecords() as $record) {
			$repeating_item = new StdClass;
			foreach ($this->btRepeatingItemDataFieldObjects as $fieldName => $fieldObject) {
				$repeating_item->$fieldName = $fieldObject->getDisplayObjectForView($record);
			}
			$repeating_items[] = $repeating_item;
		}
		
		return $repeating_items;
	}
	
}
