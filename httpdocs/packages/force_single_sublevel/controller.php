<?php  defined('C5_EXECUTE') or die(_("Access Denied."));

class ForceSingleSublevelPackage extends Package {

	protected $pkgHandle = 'force_single_sublevel';
	protected $appVersionRequired = '5.3.3';
	protected $pkgVersion = '1.0.1';

	public function getPackageName() {
		return t("Force Single Sublevel");
	}

	public function getPackageDescription() {
		return t("Prevent accidental creation of sub-sub-pages by restricting certain top-level pages to a single sublevel only.");
	}

	public function on_start() {
		if (!defined('ENABLE_APPLICATION_EVENTS')) {
			define('ENABLE_APPLICATION_EVENTS', true);
		}

		Events::extend('on_page_add', 'ForceSingleSublevel', 'on_page_add', 'packages/force_single_sublevel/libraries/force_single_sublevel.php');
	}

	public function install() {
		$pkg = parent::install();

		Loader::model('collection_attributes');
		$at = AttributeType::getByHandle('boolean');
		$akSettings = array(
			'akHandle' => 'force_single_sublevel',
			'akName' => t('Force Single Sublevel'),
			'akIsSearchable' => false
		);
		$ak = CollectionAttributeKey::add($at, $akSettings, $pkg);

		//Add the attribute to the "Navigation and Indexing" set
		// (I think this was added in 5.5??)...
		if (version_compare(APP_VERSION, '5.5.0', '>=')) {
			$as = AttributeSet::getByHandle('navigation');
			if (!is_null($as)) {
				$as->addKey($ak);
			}
		}
	}

}