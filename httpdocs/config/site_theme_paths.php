<?php defined('C5_EXECUTE') or die(_("Access Denied."));

$v = View::getInstance();

//NOTE that our theme view.php file does *NOT* output the header/footer/errors,
// so you must override each system single_page in the list below
// to include those elements (via `include($this->getThemeDirectory() . '/elements/header.php')`, etc.)

$v->setThemeByPath('/login', 'custom');
$v->setThemeByPath('/page_not_found', 'custom');
$v->setThemeByPath('/page_forbidden', 'custom');
$v->setThemeByPath('/maintenance_mode', 'custom');
