<?php

/*==============================================================================================================================
 SERVER SPECIFIC SETTINGS
 Some settings differ between server environments (local, stage, live, etc). So we conditionally set them here.
==============================================================================================================================*/

// Local server
if (strpos($_SERVER['SERVER_NAME'], 'lndo.site') !== false) {

	define('DB_SERVER', 'database');
	define('DB_USERNAME', 'lamp');
	define('DB_PASSWORD', 'lamp');
	define('DB_DATABASE', 'lamp');
	
	define('DEC_PUBLIC_SUBMISSIONS_NOTIFICATION_EMAIL_TO', 'dan@webconcentrate.com');

	// Custom 49x49 image that displays in C5 toolbar
	define('WHITE_LABEL_LOGO_SRC', substr($_SERVER['SCRIPT_NAME'], 0, stripos($_SERVER['SCRIPT_NAME'], 'index.php')) . 'themes/custom/images/c5-toolbar-icon.png');

	// Number (must be a number!) used for file-based cache busting. Works in tandem w/ .htaccess rules that route requests for '/css/main.12345.css' to '/css/main.css'.
	define('WC_ASSET_CACHE_BUST_NUMBER', time());

	// Flag that can be used when hardcoding tracking codes into the site. Usually we only want to include analytics tracking codes on the live site.
	define('INCLUDE_ANALYTICS_TRACKING_CODES', false);
	

// Stage server
} else if (strpos($_SERVER['SERVER_NAME'], 'webconcentrate.com') !== false) {

	// MySQL settings
	define('DB_SERVER', 'localhost');
	define('DB_USERNAME', 'sdglaciallakes');
	define('DB_PASSWORD', 'lYXhaK2yEQwcJzkvr1iMX8NpMRRtSCYWpqSmm7qZ');
	define('DB_DATABASE', 'sdglaciallakesc5');

	// Custom 49x49 image that displays in C5 toolbar
	define('WHITE_LABEL_LOGO_SRC', '/themes/custom/images/c5-toolbar-icon.png');

	// Number (must be a number!) used for file-based cache busting. Works in tandem w/ .htaccess rules that route requests for '/css/main.12345.css' to '/css/main.css'.
	define('WC_ASSET_CACHE_BUST_NUMBER', time());

	// Flag that can be used when hardcoding tracking codes into the site. Usually we only want to include analytics tracking codes on the live site.
	define('INCLUDE_ANALYTICS_TRACKING_CODES', false);

	define('DEC_PUBLIC_SUBMISSIONS_NOTIFICATION_EMAIL_TO', 'support@webconcentrate.com');
	
	//The following settings are used by our /libraries/file/importer.php override.
	//ImageMagick CLI program ("convert") must be installed on the server (*not* via php!).
	//If deploying to a non-webconcentrate server, comment out the following 3 lines.
	define('WC_IMAGE_RESIZE_ON_UPLOAD_IMAGEMAGICK_COMMAND', '/usr/bin/convert');
	define('WC_IMAGE_RESIZE_ON_UPLOAD_WIDTH', 2000);
	define('WC_IMAGE_RESIZE_ON_UPLOAD_HEIGHT', 2000);
	
// Live server
} else {

	// MySQL settings
	define('DB_SERVER', 'localhost');
	define('DB_USERNAME', 'sdglaciallakes');
	define('DB_PASSWORD', '7qybGNspSsArHOiR_BAzv5MIYLF8OMsjSlAO3ufM');
	define('DB_DATABASE', 'sdglaciallakesc5');

	// Custom 49x49 image that displays in C5 toolbar
	define('WHITE_LABEL_LOGO_SRC', '/themes/custom/images/c5-toolbar-icon.png');

	// Number (must be a number!) used for file-based cache busting. Works in tandem w/ .htaccess rules that route requests for '/css/main.12345.css' to '/css/main.css'.
	define('WC_ASSET_CACHE_BUST_NUMBER', 4);

	// Flag that can be used when hardcoding tracking codes into the site. Usually we only want to include analytics tracking codes on the live site.
	define('INCLUDE_ANALYTICS_TRACKING_CODES', true);

	define('DEC_PUBLIC_SUBMISSIONS_NOTIFICATION_EMAIL_TO', 'contact@sdglaciallakes.com');
	
	//The following settings are used by our /libraries/file/importer.php override.
	//ImageMagick CLI program ("convert") must be installed on the server (*not* via php!).
	//If deploying to a non-webconcentrate server, comment out the following 3 lines.
	define('WC_IMAGE_RESIZE_ON_UPLOAD_IMAGEMAGICK_COMMAND', '/usr/bin/convert');
	define('WC_IMAGE_RESIZE_ON_UPLOAD_WIDTH', 2000);
	define('WC_IMAGE_RESIZE_ON_UPLOAD_HEIGHT', 2000);

}


/*==============================================================================================================================
 SERVER INDEPENDENT SETTINGS
 These settings are the same no matter the server environment.
==============================================================================================================================*/

// Ensure C5 uses pretty urls for dashboard, too.
define('URL_REWRITING_ALL', true);

// Custom page title. "%1$s" is site name, "%2$s" is page title.
define('PAGE_TITLE_FORMAT', '%2$s | %1$s');

// Override system default of "ccm_paging_p".
define('PAGING_STRING', 'p');

// C5 stats aren't very useful, so we disable them (no need to bloat the database).
define('STATISTICS_TRACK_PAGE_VIEWS', false);

// Our sites are intentionally very custom, so we like to limit customers from getting themselves in trouble installing random addons.
define('ENABLE_MARKETPLACE_SUPPORT', false);

// The concrete5.org help search results aren't useful for our typical clients, so we disable it.
define('ENABLE_INTELLIGENT_SEARCH_HELP', false);

// C5's newsflow isn't useful for our typical clients, so we disable it.
define('ENABLE_NEWSFLOW_OVERLAY', false);

// C5 news isn't useful for our typical clients, so we disable it.
define('ENABLE_APP_NEWS', false);

// Background images in the dashboard seem unnecessary (and sometimes unprofessional), so we prefer no image.
define('WHITE_LABEL_DASHBOARD_BACKGROUND_SRC', 'none');
define('DASHBOARD_BACKGROUND_INFO', false);

// Don't output C5 version number (it's a potential security risk)
define('APP_VERSION_DISPLAY_IN_HEADER', false);

// Instead of the default (and predicatable) "admin" superuser, define our own.
define('USER_SUPER', 'hutman');

// Override email settings used by C5.
define('EMAIL_DEFAULT_FROM_ADDRESS', 'no-reply@sdglaciallakes.com');
define('EMAIL_DEFAULT_FROM_NAME', 'SD Glacial Lakes and Prairies');
//define('EMAIL_ADDRESS_REGISTER_NOTIFICATION', 'example1@example.com, example2@example.com');
define('EMAIL_ADDRESS_REGISTER_NOTIFICATION_FROM', EMAIL_DEFAULT_FROM_ADDRESS);
define('EMAIL_ADDRESS_FORGOT_PASSWORD', EMAIL_DEFAULT_FROM_ADDRESS);
define('EMAIL_ADDRESS_VALIDATE', EMAIL_DEFAULT_FROM_ADDRESS);
define('FORM_BLOCK_SENDER_EMAIL', EMAIL_DEFAULT_FROM_ADDRESS);

// Override the name of C5's session cookie name.
//define('SESSION', 'CONCRETE5');


//NOTE: SESSION_MAX_LIFETIME HAS NO EFFECT ON WC'S PLESK SERVERS
// BECAUSE PLESK DISABLES PHP'S BUILT-IN SESSION CLEANUP METHOD!
// Override browser session time (seconds of inactivity before user is logged out, or 0 for never).
//define('SESSION_MAX_LIFETIME', 7200);


//NOTE: As of 2016-12-16, we have decided *NOT* to set APP_TIMEZONE,
// and instead just set the php timezone (via `date_default_timezone_set()`)
// because some C5 code is wonky in how it converts dates
// (so in some places it uses this APP_TIMEZONE but in others it uses
// the "system" [server] timezone, which results in user saving data
// from a date/time picker as one date/time but it getting saved to the db
// as a few hours different. This only happens in some situations
// (like custom attributes of the Date/Time type) but not others
// (like the page properties "Public Date" when you're first adding a page).
// See /blocks/wc_expiring_content/controller.php for more details and sample code.
/*
// Adjust timestamps throughout C5 to a specific time zone (instead of the server's default).
//define('APP_TIMEZONE', 'America/New_York'); // Eastern Time
//define('APP_TIMEZONE', 'America/Chicago'); // Central Time
//define('APP_TIMEZONE', 'America/Denver'); // Mountain Time
//define('APP_TIMEZONE', 'America/Phoenix'); // Mountain Time (no DST)
//define('APP_TIMEZONE', 'America/Los_Angeles'); // Pacific Time
//define('APP_TIMEZONE', 'America/Anchorage'); // Alaska Time
//define('APP_TIMEZONE', 'America/Adak'); // Hawaii-Aleutian
//define('APP_TIMEZONE', 'Pacific/Honolulu'); // Hawaii-Aleutian Time (no DST)
*/
date_default_timezone_set('America/Chicago');

// Enable advanced permissions in C5.
//define('PERMISSIONS_MODEL', 'advanced');

// Disable use of area layouts in C5.
//define('ENABLE_AREA_LAYOUTS', false);

// Disable use of custom design options in C5.
//define('ENABLE_CUSTOM_DESIGN', false);

// Google maps API keys
define('GOOGLE_MAPS_API_KEY_FRONTEND', 'AIzaSyDB4WUPSGfKIdBgB5Bldd2EwF4SnK144cE'); // Frontend maps
define('GOOGLE_MAPS_API_KEY_BACKEND', 'AIzaSyBsc3AHUIyt1WhqY-2PPYJpQ5D0Ta4Wmac'); // Lookup lat/lng for member locations in dashboard edit interface

// Google reCAPTCHA keys
define('WC_RECAPTCHA_SITE_KEY', '6LeQvv0ZAAAAAH2tQvehhSMYOgO1EoxC-4dXHDFG');
define('WC_RECAPTCHA_SECRET_KEY', '6LeQvv0ZAAAAAEOvZeWSNyKmXFPYRVXqoIL0LS3D');

// Hook up our `custom_contact_form` addon to akismet (we use the same API key for all WC clients)
//define('CUSTOM_CONTACT_FORM_AKISMET_API_KEY', '2de6cad40c87');
// Hook up our `custom_contact_form` addon to reCAPTCHA
define('CUSTOM_CONTACT_FORM_RECAPTCHA_SITE_KEY', WC_RECAPTCHA_SITE_KEY);
define('CUSTOM_CONTACT_FORM_RECAPTCHA_SECRET_KEY', WC_RECAPTCHA_SECRET_KEY);
define('CUSTOM_CONTACT_FORM_RECAPTCHA_ENABLED', (defined('CUSTOM_CONTACT_FORM_RECAPTCHA_SITE_KEY') && CUSTOM_CONTACT_FORM_RECAPTCHA_SITE_KEY && defined('CUSTOM_CONTACT_FORM_RECAPTCHA_SECRET_KEY') && CUSTOM_CONTACT_FORM_RECAPTCHA_SECRET_KEY));

// Hook up our `dec_public_submissions` addon to reCAPTCHA
define('DEC_PUBLIC_SUBMISSIONS_RECAPTCHA_SITE_KEY', WC_RECAPTCHA_SITE_KEY);
define('DEC_PUBLIC_SUBMISSIONS_RECAPTCHA_SECRET_KEY', WC_RECAPTCHA_SECRET_KEY);
define('DEC_PUBLIC_SUBMISSIONS_RECAPTCHA_ENABLED', (defined('DEC_PUBLIC_SUBMISSIONS_RECAPTCHA_SITE_KEY') && DEC_PUBLIC_SUBMISSIONS_RECAPTCHA_SITE_KEY && defined('DEC_PUBLIC_SUBMISSIONS_RECAPTCHA_SECRET_KEY') && DEC_PUBLIC_SUBMISSIONS_RECAPTCHA_SECRET_KEY));
