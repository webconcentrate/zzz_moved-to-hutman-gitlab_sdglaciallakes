<?php defined('C5_EXECUTE') or die("Access Denied.");

/* Override core model so TinyMCE editor looks for /css/tinymce.css instead of typography.css */

class PageThemeEditableStyle extends Concrete5_Model_PageThemeEditableStyle {}
class PageThemeEditableStyleFont extends Concrete5_Model_PageThemeEditableStyleFont {}
class PageThemeFile extends Concrete5_Model_PageThemeFile {}
class PageTheme extends Concrete5_Model_PageTheme {
 public function getThemeEditorCSS() {
  return $this->ptURL . '/css/tinymce.' . WC_ASSET_CACHE_BUST_NUMBER . '.css';
 }
}
