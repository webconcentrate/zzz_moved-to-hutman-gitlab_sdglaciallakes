<?php

/* Override default # of items to show in search results list */

defined('C5_EXECUTE') or die("Access Denied."); 
class UserList extends Concrete5_Model_UserList {
	
	protected $itemsPerPage = 100;
	
}