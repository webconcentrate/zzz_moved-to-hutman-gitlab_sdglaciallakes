<?php defined('C5_EXECUTE') or die("Access Denied.");

class MailHelper extends Concrete5_Helper_Mail {
	
	//Some transactional email services (e.g. SparkPost, MailGun)
	// fail when an email address has a space before or after it,
	// and this will be the case when a comma-separated list of email addresses
	// is provided by the user (since most human beings put spaces
	// after commas in lists).
	//SO: trim all surrounding whitespace from each individual email address
	// in comma-separated lists before passing it up to the parent class...
	public function to($email, $name = null) {
		if (strpos($email, ',') > 0) {
			$email = explode(',', $email);
			$email = array_map('trim', $email);
			$email = implode(',', $email);
		}
		return parent::to($email, $name);
	}
}