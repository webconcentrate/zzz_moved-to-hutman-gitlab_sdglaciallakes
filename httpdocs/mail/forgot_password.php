<?php
defined('C5_EXECUTE') or die("Access Denied.");

$subject = t("Forgot Password");
$body = t("

Dear %s,

You have requested a new password for the site %s 

Your username is: %s

You may change your password at the following address:

%s

NOTE: for security reasons, this link is temporary and will expire. If you receive an error message after clicking the link, please try the password reset form again.

", $uName, SITE, $uName, $changePassURL);

?>