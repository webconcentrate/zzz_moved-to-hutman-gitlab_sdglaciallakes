<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<?php 
	// To keep this global area out of the way while editing, we're making it only editable through stacks in the dashboard.
	$a = new GlobalArea('Crumbs Nav');
	$a->disableControls();
	$a->display();
?>