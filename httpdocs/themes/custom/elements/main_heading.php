<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<?php
	if ($page_is_home) {
		$main_heading = SITE . ' Home';
	} else if ($c->getAttribute('main_heading_override')) {
		$main_heading = $c->getAttribute('main_heading_override');
	} else {
		$main_heading = $page_title;
	}
?>

<div class="main-heading">
	<h1><?=$main_heading?></h1>
</div>

