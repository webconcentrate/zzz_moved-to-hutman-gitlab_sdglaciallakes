<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<?php
	if (isset($error) && $error != '') {
		if ($error instanceof Exception) {
			$_error[] = $error->getMessage();
		} else if ($error instanceof ValidationErrorHelper) {
			$_error = $error->getList();
		} else if (is_array($error)) {
			$_error = $error;
		} else if (is_string($error)) {
			$_error[] = $error;
		}
	}
?>

<?php if (!empty($_error)): ?>
		
	<div class="c5-errors">
		<div class="c5-errors__heading">
			<h1>Please correct the following error(s):</h1>
		</div>
		<ul class="c5-errors__list">
			<?php foreach($_error as $e): ?>
				<li class="c5-errors__item">
					<?php echo $e?>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
	
<?php endif; ?>

