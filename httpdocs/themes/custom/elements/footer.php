<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
			<?php if (!$hide_footer) { ?>
				<div class="the-page__footer">
					<div class="the-page__footer-liner">
						<div class="the-page__footer-ctas">
							<?php
								$a = new GlobalArea('Footer CTAs');
								$a->setBlockLimit(1);
								$a->display();
							?>
						</div>
						<div class="the-page__footer-misc-content">
							<div class="the-page__footer-misc-content-item the-page__footer-misc-content-item--1">
								<?php
									$a = new GlobalArea('Footer Misc Content 1');
									$a->display();
								?>
							</div>
							<div class="the-page__footer-misc-content-item the-page__footer-misc-content-item--2">
								<?php
									$a = new GlobalArea('Footer Misc Content 2');
									$a->display();
								?>
							</div>
							<div class="the-page__footer-misc-content-item the-page__footer-misc-content-item--3">
								<?php
									$a = new GlobalArea('Footer Misc Content 3');
									$a->display();
								?>
							</div>
							<div class="the-page__footer-misc-content-item the-page__footer-misc-content-item--4">
								<?php
									$a = new GlobalArea('Footer Misc Content 4');
									$a->display();
								?>
							</div>
							<div class="the-page__footer-misc-content-item the-page__footer-misc-content-item--5">
								<?php
									$a = new GlobalArea('Footer Misc Content 5');
									$a->display();
								?>
							</div>
							<div class="the-page__footer-misc-content-item the-page__footer-misc-content-item--6">
								<?php
									$a = new GlobalArea('Footer Misc Content 6');
									$a->display();
								?>
							</div>
						</div>
						<div class="the-page__footer-social">
							<?php
								$a = new GlobalArea('Footer Social');
								$a->setBlockLimit(1);
								$a->display();
							?>
						</div>
						<div class="the-page__footer-copyright">
							<?php
								$a = new GlobalArea('Footer Copyright');
								$a->setBlockLimit(1);
								$a->display();
							?>
						</div>
						<div class="the-page__footer-credits">
							<a href="http://webconcentrate.com" target="_blank">Made from Web Concentrate</a>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
		<script src="<?=$theme_path?>/js/main.<?=WC_ASSET_CACHE_BUST_NUMBER?>.js"></script>
		<?php Loader::element('footer_required'); ?>
	</body>
</html>