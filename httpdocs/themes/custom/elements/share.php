<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<div class="share">
	<div class="share__heading">
		<h2>Share:</h2>
	</div>
	<ul class="share__link-list">
		<li class="share__link-item">	
			<a class="share__link share__link--facebook" href="https://facebook.com/sharer/sharer.php?u=<?=rawurlencode($page_canonical_url)?>" target="_blank" rel="noopener">
				<span class="share__link-text">Facebook</span>
			</a>
		</li>
		<li class="share__link-item">	
			<a class="share__link share__link--twitter" href="https://twitter.com/intent/tweet/?text=<?=rawurlencode($page_title . ' - ' . SITE)?>&amp;url=<?=rawurlencode($page_canonical_url)?>" target="_blank" rel="noopener">
				<span class="share__link-text">Twitter</span>
			</a>										
		</li>
		<li class="share__link-item">	
			<a class="share__link share__link--linkedin" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?=rawurlencode($page_canonical_url)?>&amp;title=<?=rawurlencode($page_title . ' - ' . SITE)?>&amp;summary=<?=(!empty($post) ? $post->getTeaser() : '')?>" target="_blank" rel="noopener">
				<span class="share__link-text">LinkedIn</span>
			</a>
		</li>
		<li class="share__link-item">	
			<a class="share__link share__link--email" href="mailto:?subject=<?=rawurlencode($page_title . ' - ' . SITE)?>&amp;body=<?=rawurlencode($page_canonical_url)?>" target="_blank" rel="noopener">
				<span class="share__link-text">Share by E-Mail</span>
			</a>
		</li>
	</ul>
</div>