<?php 
	defined('C5_EXECUTE') or die("Access Denied.");
	// Have page reload on any attribute changes (https://github.com/jordanlev/c5_reload_on_attribute_change)
	Loader::packageElement('wc_reload_on_attribute_change', 'wc_utils');
	// Gather common page data and put into simple variables
	include('header_vars.php');
?><!doctype html>
<html class="no-js <?=($page_has_admin_toolbar ? 'yes-c5-toolbar' : '')?>" lang="en">
	<head>
		<?php if (INCLUDE_ANALYTICS_TRACKING_CODES) { ?>
			<!-- Global site tag (gtag.js) - Google Analytics -->
			<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127983437-1"></script>
			<script>
			  window.dataLayer = window.dataLayer || [];
			  function gtag(){dataLayer.push(arguments);}
			  gtag('js', new Date());
			
			  gtag('config', 'UA-127983437-1');
			</script>
			<!-- End Global site tag (gtag.js) - Google Analytics -->
		<?php } ?>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<?php Loader::element('header_required', array('pageTitle' => $page_title_for_header_element, 'pageDescription' => $page_description_for_header_element)); ?>
		<meta name="viewport" content="width=device-width, initial-scale=1">		
		<link rel="canonical" href="<?=$page_canonical_url?>">
		<link rel="apple-touch-icon" sizes="180x180" href="<?=DIR_REL?>/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?=DIR_REL?>/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?=DIR_REL?>/favicon-16x16.png">
		<link rel="manifest" href="<?=DIR_REL?>/manifest.json">
		<link rel="mask-icon" href="<?=DIR_REL?>/safari-pinned-tab.svg" color="#092954">
		<meta name="theme-color" content="#ffffff">
		<link rel="stylesheet" href="<?=$theme_path?>/css/main.<?=WC_ASSET_CACHE_BUST_NUMBER?>.css">
		<!--[if lt IE 9]><script src="<?=$theme_path?>/js/html5shiv-printshiv.min.js"></script><![endif]-->
		<!--[if lt IE 9]><script src="<?=$theme_path?>/js/respond.min.js"></script><![endif]-->
		<script>document.documentElement.className = document.documentElement.className.replace(/(^|\s)no-js(\s|$)/, '$1js$2');</script>
	</head>
	<body>
		<div class="js-the-page the-page">
			<?php if ($include_skip_to_main_link) { ?>
				<a class="js-skip-to-main-content the-page__skip-to-main-link" href="#main-content">Skip to main content</a>
			<?php } ?>
			<?php if (!$hide_header) { ?>
				<div class="the-page__notification">
					<?php
						$a = new GlobalArea('Notification');
						$a->setBlockLimit(1);
						$a->display();
					?>
				</div>
				<div class="the-page__header" role="banner">
					<div class="the-page__header-liner">
						<div class="the-page__header-branding">
							<div class="header-branding">
								<a href="<?=DIR_REL?>/"><?=SITE?> Home</a>
							</div>
						</div>
						<div class="the-page__header-menu-toggle">	
							<a class="js-header-menu-toggle header-menu-toggle" href="#header-menu">
								Menu
							</a>
						</div>
						<div class="the-page__header-menu">
							<div id="header-menu" class="js-header-menu header-menu">
								<div class="header-menu__search">
									<div class="site-search">
										<form class="site-search__form" novalidate method="get" action="<?=View::url('/search')?>">
											<div class="js-site-search-input site-search__input">
												<div class="js-site-search-input-label site-search__input-label">
													<label for="site-search-query">Search</label>
												</div>	
												<div class="js-site-search-input-field site-search__input-field">
													<input type="text" name="query" id="site-search-query" value="">
												</div>
											</div>
											<div class="site-search__submit">
												<button>Submit</button>
											</div>
										</form>
									</div>
								</div>
								<div class="header-menu__utility-nav">
									<?php
										// To keep this global area out of the way while editing, we're making it only editable through stacks in the dashboard.
										$a = new GlobalArea('Utility Nav');
										$a->disableControls();
										$a->display();
									?>
								</div>
								<div class="header-menu__primary-nav">
									<?php
										// To keep this global area out of the way while editing, we're making it only editable through stacks in the dashboard.
										$a = new GlobalArea('Primary Nav');
										$a->disableControls();
										$a->display();
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>