<?php defined('C5_EXECUTE') or die("Access Denied.");

// This file should be included by the theme's header element so that these variables are available to all page type templates.

$nh = Loader::helper('navigation');
$ih = Loader::helper('image');
$theme_path = $this->getThemePath();

$page_id = $c->getCollectionID();
$page_type = $c->getCollectionTypeHandle();
$page_is_home = (HOME_CID == $page_id);
$page_is_defaults = ($c->getMasterCollectionID() == $page_id);
$page_is_single = $c->isGeneratedCollection();
$page_in_edit_mode = $c->isEditMode();
$p = new Permissions($c);
$page_has_admin_toolbar = $p->canViewToolbar();

$page_title = isset($pageTitle) ? $pageTitle : $c->getCollectionName(); //allow single_pages to override page title by setting the $pageTitle var
$page_title = h($page_title);

$page_title_for_header_element = isset($pageTitle) ? sprintf(PAGE_TITLE_FORMAT, SITE, $pageTitle) : null; //we need a separate var to pass to C5's "header_required" element that includes the site name when the title is overridden by a controller

$page_meta_title = $c->getAttribute('meta_title');
$page_meta_title = (empty($page_meta_title) ? $page_title : $page_meta_title);
$page_meta_title = h($page_meta_title);

$page_meta_description = isset($pageDescription) ? $pageDescription : $c->getAttribute('meta_description'); //allow single_pages to override page meta description by setting the $pageDescription var
$page_meta_description = (empty($page_meta_description) ? $c->getCollectionDescription() : $page_meta_description);
$page_meta_description = h($page_meta_description);

$page_description_for_header_element = isset($pageDescription) ? $pageDescription : null; //we need a separate var to pass to C5's "header_required" element because it does its own html-escaping so we don't want to escape what we pass to it

$page_timestamp = strtotime($c->getCollectionDatePublic());

$page_canonical_url = isset($pageCanonicalUrl) ? $pageCanonicalUrl : Loader::helper('wc_canonical_link', 'wc_utils')->getForPage($c, false); //allow single_pages to override canonical url by setting the $pageCanonicalUrl var.
$page_canonical_url = h($page_canonical_url);

$page_path = $c->getCollectionPath();
if (substr_count($page_path, '/') > 1) {
	$page_path_parts = explode('/', $page_path);
	$page_section_path = "/{$page_path_parts[1]}"; //0th element is empty string (because of leading slash)
	$page_is_section = false;
} else {
	$page_section_path = $page_path;
	$page_is_section = true;
}
$page_section_c = Page::getByPath($page_section_path);
$page_section_url = $nh->getLinkToCollection($page_section_c);
$page_section_title = h($page_section_c->getCollectionName());
