<?php
	defined('C5_EXECUTE') or die("Access Denied.");
	$include_skip_to_main_link = true; // Tells header.php that this template has a main content section to jump to.
	include('elements/header.php');
?>

	<div class="the-page__main">
		<div class="the-page__main-liner">
				
			<div class="page-main-landing">
				<div class="page-main-landing__heading">
					<?php
						$a = new Area('Landing Heading');
						$a->setBlockLimit(1);
						$a->display($c);
					?>
				</div>
				<div class="page-main-landing__crumbs">
					<?php include('elements/global_area_crumbs.php'); ?>
				</div>
				<div class="page-main-landing__content">
					<div class="page-main-landing__main">
						<a name="main-content" style="display: block;"></a>
						<?php
							$a = new Area('Main');
							$a->display($c);
						?>
					</div>
				</div>
			</div>
				
		</div>
	</div>

<?php include('elements/footer.php'); ?>