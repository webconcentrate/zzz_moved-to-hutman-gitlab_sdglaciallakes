<?php
	defined('C5_EXECUTE') or die("Access Denied.");
	
	// NOTE: we're doing something non-standard here. Instead of this file including the site's header and footer, 
	// we're giving single pages the flexiblity to have their own header, footer, layout, error message, etc.
	echo $innerContent;