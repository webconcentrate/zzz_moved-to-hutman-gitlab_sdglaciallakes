<?php
	defined('C5_EXECUTE') or die("Access Denied.");
	include('elements/header.php'); 
	
	Loader::model('blog', 'designer_blog');
	$post = new BlogPost($c);
	$ih = Loader::helper('image');
?>

	<div class="the-page__main" role="main">
		<div class="the-page__main-liner">
				
			<div class="page-main-basic">
				<div class="page-main-basic__crumbs">
					<?php include('elements/global_area_crumbs.php'); ?>
				</div>
				<div class="page-main-basic__heading">
					<?php include('elements/main_heading.php'); ?>		
				</div>
				<div class="page-main-basic__main">	
					
					<div class="db-post">
						<div class="db-post__main">
							<div class="db-post__meta-before">
								<div class="db-post__date">
									<?=$post->getDate('F j, Y')?>
								</div>
								<?php /*
								<div class="db-post__categories">
									<?php $post_categories = $post->getMultiClassification('category'); ?>
									<?php if (!empty($post_categories)) { ?>
										<?php 
											$post_categories_count = count($post_categories);
											$post_categories_index = 1;
										?>
										<?php foreach ($post_categories as $category) { ?>
											<a href="<?=BlogHelper::getClassificationUrl('category',$category)?>"><?=$category?></a><?=($post_categories_index < $post_categories_count ? ',' : '')?>
											<?php $post_categories_index++; ?>
										<?php } ?>
									<?php } ?>
								</div>
								*/ ?>
							</div>
							<div class="db-post__content">
								<?php
									$a = new Area('Main');
									$a->display($c);
								?>
							</div>
							<div class="db-post__share">
								<?php include('elements/share.php'); ?>		
							</div>
							<?php /*
							<div class="db-post__related">
								<?php Loader::packageElement('blog_related_posts', 'designer_blog', array('post' => $post, 'classification' => 'category', 'count' => 3)); ?>
							</div>
							*/ ?>
						</div>
						<div class="db-post__posts-nav">
							<div class="db-post__posts-nav-categories">
								<?php Loader::packageElement('blog_classification_nav', 'designer_blog', array('classification' => 'post_category', 'title' => 'Categories')); ?>
							</div>
							<?php /*
							<div class="db-post__posts-nav-featured">
								<?php Loader::packageElement('blog_featured', 'designer_blog'); ?>
							</div>
							*/ ?>
							<?php /*	
							<div class="db-post__posts-nav-authors">
								<?php Loader::packageElement('blog_author_nav', 'designer_blog'); ?>
							</div>
							*/ ?>
							<?php /*
							<div class="db-post__posts-nav-archives">
								<?php Loader::packageElement('blog_date_nav', 'designer_blog'); ?>
							</div>
							*/ ?>
							<?php /*
							<div class="db-post__posts-nav-subscribe">
								<?php Loader::packageElement('blog_subscribe', 'designer_blog'); ?>
							</div>
							*/ ?>
						</div>
					</div>
				
				</div>
			</div>
				
		</div>
	</div>	
	
<?php include('elements/footer.php'); ?>