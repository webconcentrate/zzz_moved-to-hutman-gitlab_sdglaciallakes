<?php 
	defined('C5_EXECUTE') or die("Access Denied.");
	include('elements/header.php'); 
?>
	
	<div class="the-page__main">
		<div class="the-page__main-liner">
							
			<div class="page-main-home">
				<div class="page-main-home__heading">
					<?php include('elements/main_heading.php'); ?>
				</div>		
				<div class="page-main-home__feature">
					<?php
						$a = new Area('Home Feature');
						$a->setBlockLimit(1);
						$a->display($c);
					?>
				</div>
				<div class="page-main-home__main-and-ads">
					<div class="page-main-home__main">
						<?php
							$a = new Area('Main');
							$a->display($c);
						?>
					</div>
					<div class="page-main-home__ads">
						<?php
							$a = new Area('Ads');
							$a->display($c);
						?>
					</div>
				</div>
			</div>
				
		</div>
	</div>
	
<?php include('elements/footer.php'); ?>