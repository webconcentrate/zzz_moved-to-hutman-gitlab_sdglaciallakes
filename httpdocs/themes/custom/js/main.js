/*==============================================================================================================================
 HEADER MENU
==============================================================================================================================*/

$(document).ready(function() {

	var $headerMenuToggle = $('.js-header-menu-toggle');
	var $headerMenu = $('.js-header-menu');
	$headerMenuToggle.on('click', function(event) {
		event.preventDefault();
		$headerMenuToggle.toggleClass('header-menu-toggle--expanded');
		$headerMenu.toggleClass('header-menu--expanded');
	});
	
});


/*==============================================================================================================================
 EXPANDABLE CONTENT
==============================================================================================================================*/

$(document).ready(function() {

	$('.js-expandable-content-toggle').on('click', function(event) {
		event.preventDefault();
		$(this).closest('.js-expandable-content-item').toggleClass('expandable-content__item--expanded');
	});
	
});


/*==============================================================================================================================
 PHOTO SLIDER
==============================================================================================================================*/

$(document).ready(function() {
	
	$('.js-photo-slider').each(function() {
		var $thisSlider = $(this);
		var popupEnabled = $thisSlider.data('popup-enabled');
		var $sliderItems = $('.js-photo-slider-items', $thisSlider);
		var $sliderLinks = $('.js-photo-slider-image-link', $sliderItems);
		
		// Setup magnific popup gallery		
		if (popupEnabled) {
			$sliderLinks.magnificPopup({
				type: 'image',
				image: {
					titleSrc: 'data-caption'
				},
				gallery: {
					enabled: true,
					navigateByImgClick: true
				},
				prependTo: '.js-the-page'
			});
		}
		
		// Make flicky slider (if more than one item)
		if ($sliderItems.children().length > 1 && !CCM_EDIT_MODE) {
			
			// Initialize
			var $flickityInstance = $sliderItems.flickity({
				wrapAround: true,
				autoPlay: false,
				prevNextButtons: false,
				pageDots: false,
				lazyLoad: true,
				lazyLoad: 2
			});
			
			// Previous/Next
			$('.js-photo-slider-previous', $thisSlider).on('click keydown', function(event) {
				// Tap/Click
				if (event.type == 'click') {
					event.preventDefault();
					$flickityInstance.flickity('previous');
				}
				// Enter key
				if (event.type == 'keydown') {
					var code = event.keyCode || event.which;
					if (code == 13) {
						event.preventDefault();
						$flickityInstance.flickity('previous');
					}
				}
			});
			$('.js-photo-slider-next', $thisSlider).on('click keydown', function(event) {
				// Tap/Click
				if (event.type == 'click') {
					event.preventDefault();
					$flickityInstance.flickity('next');
				}
				// Enter key
				if (event.type == 'keydown') {
					var code = event.keyCode || event.which;
					if (code == 13) {
						event.preventDefault();
						$flickityInstance.flickity('next');
					}
				}
			});
			
			// Pager
			var $pagerList = $('.js-photo-slider-pagination-items', $thisSlider);
			var $pagerItems = $('.js-photo-slider-pagination-item', $pagerList);
			$pagerItems.eq($flickityInstance.data('flickity').selectedIndex).addClass('photo-slider__pagination-item--active');
			$flickityInstance.on('cellSelect.flickity', function() {
				$pagerItems.removeClass('photo-slider__pagination-item--active');
				$pagerItems.eq($flickityInstance.data('flickity').selectedIndex).addClass('photo-slider__pagination-item--active');
			});
			$pagerList.on('click keydown', '.js-photo-slider-pagination-item', function(event) {
				// Stop the auto play once user starts taking over
				$flickityInstance.flickity('stopPlayer');
				var index = $(this).index();
				// Tap/Click
				if (event.type == 'click') {
					event.preventDefault();
					$flickityInstance.flickity('select', index);
				}
				// Enter key
				if (event.type == 'keydown') {
					var code = event.keyCode || event.which;
					if (code == 13) {
						event.preventDefault();
						$flickityInstance.flickity('select', index);
					}
				}
			});
			
			if (popupEnabled) {
				// Clear "click" event (to get rid of magnific's default poppup trigger)
				$sliderLinks.off('click');
				// Don't follow slider links
				$sliderLinks.on('click', function(event) {
					event.preventDefault();
				});
				// Use flickity's "staticClick" to open the popups instead of link click (otherwise drags can sometimes trigger 'click')
				$flickityInstance.on('staticClick.flickity', function( event, pointer, cellElement, cellIndex) {
					$sliderLinks.magnificPopup('open', cellIndex);
				});
			}
			
		}
	});
	
});


/*==============================================================================================================================
 VIDEO POPUP LINK
==============================================================================================================================*/

$(document).ready(function() {
	$('.js-video-popup-link').each(function() {
		$(this).magnificPopup({
			type: 'iframe',
			removalDelay: 160,
			preloader: false,
			prependTo: '.js-the-page'
		});
	});
});


/*==============================================================================================================================
 SKIP TO MAIN CONTENT 
==============================================================================================================================*/

$('.js-skip-to-main-content').on('click', function(event) {
	event.preventDefault();
	scrollToElement('[name="main-content"]', 300, 0);
});


/*==============================================================================================================================
 SITE SEARCH
==============================================================================================================================*/

$(document).ready(function() {
	
	// Hide/show search label.
	$('.js-site-search-input-field input').on('focus', function() {
		$(this).parents('.js-site-search-input').find('.js-site-search-input-label').addClass('site-search__input-label--hidden');
	});
	$('.js-site-search-input-field input').on('focusout', function() {
		if (!$(this).val()) {
			$(this).parents('.js-site-search-input').find('.js-site-search-input-label').removeClass('site-search__input-label--hidden');
		}
	});
	
});


/*==============================================================================================================================
 HELPERS
==============================================================================================================================*/

// Scroll helper function
function scrollToElement(target, speed, buffer, setFocus) {

	// Get vars or set defaults
	var $target = (target instanceof jQuery) ? target : $(target);
	var speed = (typeof speed !== 'undefined') ? speed : 200;
	var buffer = (typeof buffer !== 'undefined') ? buffer : 0;
	var setFocus = (typeof setFocus !== 'undefined') ? setFocus : true;
	
	// Scroll
	if ($target.length) {
		$('body,html').animate({scrollTop : ($target.offset().top - buffer)}, speed);
		// Set focus on target (more info: https://css-tricks.com/smooth-scrolling-accessibility/)
		if (setFocus) {
			$target.focus();
			if (!$target.is(':focus')) { // Checking if the target was focused
				$target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
				$target.focus(); // Setting focus
				$target.css('outline', 'none'); // Get rid of outline (probably not desired on elements that aren't normally focusable).
			};
		}
	}
	
}