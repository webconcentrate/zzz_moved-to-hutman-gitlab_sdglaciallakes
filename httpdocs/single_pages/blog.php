<?php
	defined('C5_EXECUTE') or die("Access Denied.");
	include($this->getThemeDirectory() . '/elements/header.php');
?>

	<div class="the-page__main" role="main">
		<div class="the-page__main-liner">
				
			<div class="page-main-basic">
				<div class="page-main-basic__crumbs">
					<?php include($this->getThemeDirectory() . '/elements/global_area_crumbs.php'); ?>
				</div>
				<div class="page-main-basic__heading">
					<?php include($this->getThemeDirectory() . '/elements/main_heading.php'); ?>
				</div>
				<div class="page-main-basic__main">	
					
					<div class="db-posts-index">
						<div class="db-posts-index__posts">
							<?php
								$posts_heading_classes = 'db-posts-index__posts-heading';
								if ($archive_month) {
									$posts_heading = 'Archives for ' . date('F, Y', $archive_month);
								} else if ($archive_author) {
									$posts_heading = 'Posts by ' . $archive_author_name;
								} else if ($archive_classification) {
									$posts_heading = 'Posts with ' . $archive_classification_name . ': ' . $archive_classification_value;
								} else {
									$posts_heading = 'Recent Posts';
									$posts_heading_classes .= ' db-posts-index__posts-heading--hidden';
								}
							?>
							<div class="<?=$posts_heading_classes?>">
								<h2><?=$posts_heading?></h2>
							</div>
							<?php if ($posts) { ?>
								<ol class="db-posts-index__posts-list">
									<?php foreach ($posts as $index=>$post) { ?>
										<li class="db-posts-index__posts-item">
											<?php if ($post->getThumbnail()) { ?>
												<div class="db-posts-index__posts-item-image">
													<a href="<?=$post->getUrl()?>">
														<?php $post->displayThumbnail(800, 500, true); ?>
													</a>
												</div>
											<?php } ?>
											<div class="db-posts-index__posts-item-copy">
												<div class="db-posts-index__posts-item-title">
													<h3>
														<a href="<?=$post->getUrl()?>"><?=$post->getTitle()?></a>
													</h3>
												</div>
												<div class="db-posts-index__posts-item-date">
													<?=$post->getDate('F j, Y')?>
												</div>
												<div class="db-posts-index__posts-item-teaser">
													<p>
														<?=$post->getTeaser()?>
														<a href="<?=$post->getUrl()?>">Read Full Post</a>
													</p>
												</div>
											</div>
										</li>
									<?php } ?>
								</ol>
								<?php if ($paginator->getTotalPages() > 1) { ?>
									<div class="db-posts-index__posts-pagination">
										<span class="db-posts-index__posts-pagination-previous"><?=$paginator->getPrevious('Newer')?></span>
										<span class="db-posts-index__posts-pagination-status">
											Page 
											<form action="" method="get">
												<input onfocus="this.select()" type="text" name="p" value="<?=($paginator->getCurrentPage() + 1)?>">
											</form>
											of 
											<?=$paginator->getTotalPages()?>
										</span>
										<span class="db-posts-index__posts-pagination-next"><?=$paginator->getNext('Older')?></span>
									</div>
								<?php } ?>
							<?php } else { ?>
								<p><em>No posts to display</em></p>
							<?php } ?>
						</div>
						<div class="db-posts-index__nav">
							<div class="db-posts-index__posts-nav-categories">
								<?php Loader::packageElement('blog_classification_nav', 'designer_blog', array('classification' => 'post_category', 'title' => 'Categories', 'active_value' => ($archive_classification == 'category' ? $archive_classification_value : ''))); ?>
							</div>
							<?php /*
							<div class="db-posts-index__posts-nav-featured">
								<?php Loader::packageElement('blog_featured', 'designer_blog'); ?>
							</div>
							*/ ?>
							<?php /*
							<div class="db-posts-index__posts-nav-authors">
								<?php Loader::packageElement('blog_author_nav', 'designer_blog'); ?>
							</div>
							*/ ?>
							<?php /*
							<div class="db-posts-index__posts-nav-archives">
								<?php Loader::packageElement('blog_date_nav', 'designer_blog', array('active_month' => $archive_month)); ?>
							</div>
							*/ ?>
							<?php /*
							<div class="db-posts-index__posts-nav-subscribe">
								<?php Loader::packageElement('blog_subscribe', 'designer_blog'); ?>
							</div>
							*/ ?>
						</div>
					</div>
					
				</div>
			</div>
								
		</div>
	</div>

<?php include($this->getThemeDirectory() . '/elements/footer.php'); ?>