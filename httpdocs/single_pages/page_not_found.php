<?php
	defined('C5_EXECUTE') or die("Access Denied.");
	$include_skip_to_main_link = true; // Tells header.php that this template has a main content section to jump to.
	include($this->getThemeDirectory() . '/elements/header.php');
?>

	<div class="the-page__main">
		<div class="the-page__main-liner">
				
			<div class="page-main-basic">
				<div class="page-main-basic__crumbs">
					<?php include($this->getThemeDirectory() . '/elements/global_area_crumbs.php'); ?>
				</div>
				<div class="page-main-basic__heading">
					<?php include($this->getThemeDirectory() . '/elements/main_heading.php'); ?>
				</div>
				<div class="page-main-basic__main">	
					<a name="main-content" style="display: block;"></a>
					<p>
						Sorry, the page you requested does not exist.
					</p>
					<p>
						Try starting over at the <a href="<?=DIR_REL?>/">homepage</a>.
					</p>
				</div>
			</div>
								
		</div>
	</div>

<?php include($this->getThemeDirectory() . '/elements/footer.php'); ?>