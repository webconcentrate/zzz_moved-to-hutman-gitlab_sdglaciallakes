<?php 
	defined('C5_EXECUTE') or die("Access Denied.");
	Loader::library('authentication/open_id');
	$form = Loader::helper('form');
	
	// Determine what to show the user
	if ($changePasswordForm) {
		$showChangePasswordForm = true;
	} else if ($validated) {
		$showEmailValidatedMessage = true;
	} else if (isset($_SESSION['uOpenIDError']) && isset($_SESSION['uOpenIDRequested'])) {
		$showOpenId = true;
	} else if ($invalidRegistrationFields) {
		$showRegistrationFields = true;
	} else {
		$showStandardLogin = true;
	}
	$include_skip_to_main_link = true; // Tells header.php that this template has a main content section to jump to.
	include($this->getThemeDirectory() . '/elements/header.php');
?>

	<div class="the-page__main">
		<div class="the-page__main-liner">
				
			<div class="page-main-basic">
				<div class="page-main-basic__crumbs">
					<?php include($this->getThemeDirectory() . '/elements/global_area_crumbs.php'); ?>
				</div>
				<div class="page-main-basic__heading">
					<?php include($this->getThemeDirectory() . '/elements/main_heading.php'); ?>
				</div>
				<div class="page-main-basic__main">
					<a name="main-content" style="display: block;"></a>
					
					<?php include($this->getThemeDirectory() . '/elements/c5-errors.php'); ?>
					
					<div class="c5-login">
	
						<?php if (isset($intro_msg)) { ?>
							<div class="c5-login__intro-message"><p><?php echo $intro_msg?></p></div>
						<?php } ?>
						
						<?php if ($passwordChanged) { ?>
							<div class="c5-login__password-changed-message"><p><?php echo t('Password changed. Please login to continue. ') ?></p></div>
						<?php } ?>
						
						<?php if ($showEmailValidatedMessage) { ?>
							<div class="c5-login__email-validated-message">
								<p>
									<?php echo t('The email address <b>%s</b> has been verified and you are now a fully validated member of this website.', $uEmail)?>
								</p>
								<p>
									<a href="<?php echo $this->url('/')?>"><?php echo t('Continue to Site')?></a>
								</p>
							</div>
						<?php } ?>
						
						<?php if ($showChangePasswordForm) { ?>
							<div class="c5-login__password-change">
								<p><?php echo t('Enter your new password below.') ?></p>
								<form class="c5-login__form" method="post" action="<?php echo $this->url( '/login', 'change_password', $uHash )?>">
									<div class="input-items">
										<div class="input-item">
											<div class="input-label-wrapper">
												<label for="uPassword" class="input-label">
													<?php echo t('New Password')?>
												</label>
											</div>
											<div class="input-field-wrapper">
												<input class="input-text" type="password" name="uPassword" id="uPassword" />
											</div>
										</div>
										<div class="input-item">
											<div class="input-label-wrapper">
												<label for="uPasswordConfirm" class="input-label">
													<?php echo t('Confirm Password')?>
												</label>
											</div>
											<div class="input-field-wrapper">
												<input class="input-text" type="password" name="uPasswordConfirm" id="uPasswordConfirm" />
											</div>
										</div>
										<div class="input-item">
											<div class="input-buttons-wrapper">
												<button type="submit" class="input-submit"><?php echo t('Log In'); ?></button>
											</div>
										</div>
									</div>
								</form>
							</div>
						<?php } ?>
						
							
						<?php /* wctodo: make work maybe someday? if ($showOpenId) { ?>
							<div class="ccm-form">
							
							<?php switch($_SESSION['uOpenIDError']) {
								case OpenIDAuth::E_REGISTRATION_EMAIL_INCOMPLETE: ?>
							
									<form method="post" action="<?php echo $this->url('/login', 'complete_openid_email')?>">
										<p><?php echo t('To complete the signup process, you must provide a valid email address.')?></p>
										<label for="uEmail"><?php echo t('Email Address')?></label><br/>
										<?php echo $form->text('uEmail')?>
											
										<div class="ccm-button">
										<?php echo $form->submit('submit', t('Sign In') . ' &gt;')?>
										</div>
									</form>
							
								<?php break;
								case OpenIDAuth::E_REGISTRATION_EMAIL_EXISTS:
								
								$ui = UserInfo::getByID($_SESSION['uOpenIDExistingUser']);
								
								?>
							
							<form method="post" action="<?php echo $this->url('/login', 'do_login')?>">
								<p><?php echo t('The OpenID account returned an email address already registered on this site. To join this OpenID to the existing user account, login below:')?></p>
								<label for="uEmail"><?php echo t('Email Address')?></label><br/>
								<div><strong><?php echo $ui->getUserEmail()?></strong></div>
								<br/>
								
								<div>
								<label for="uName"><?php if (USER_REGISTRATION_WITH_EMAIL_ADDRESS == true) { ?>
									<?php echo t('Email Address')?>
								<?php } else { ?>
									<?php echo t('Username')?>
								<?php } ?></label><br/>
								<input type="text" name="uName" id="uName" <?php echo (isset($uName)?'value="'.$uName.'"':'');?> class="ccm-input-text">
								</div>			<div>
							
								<label for="uPassword"><?php echo t('Password')?></label><br/>
								<input type="password" name="uPassword" id="uPassword" class="ccm-input-text">
								</div>
							
								<div class="ccm-button">
								<?php echo $form->submit('submit', t('Sign In') . ' &gt;')?>
								</div>
							</form>
							
							<?php break;
							
							}
							?>
							
							</div>
						<?php } */ ?>
						
						
						<?php if ($showRegistrationFields) { ?>
							<?php /* wctodo: style this section */ ?>
							<div class="ccm-form">
								<p><?php echo t('You must provide the following information before you may login.')?></p>
								
								<form method="post" action="<?php echo $this->url('/login', 'do_login')?>">
									<?php 
									$attribs = UserAttributeKey::getRegistrationList();
									$af = Loader::helper('form/attribute');
									
									$i = 0;
									foreach($unfilledAttributes as $ak) { 
										if ($i > 0) { 
											print '<br/><br/>';
										}
										print $af->display($ak, $ak->isAttributeKeyRequiredOnRegister());	
										$i++;
									}
									?>
									
									<?php echo $form->hidden('uName', Loader::helper('text')->entities($_POST['uName']))?>
									<?php echo $form->hidden('uPassword', Loader::helper('text')->entities($_POST['uPassword']))?>
									<?php echo $form->hidden('uOpenID', $uOpenID)?>
									<?php echo $form->hidden('completePartialProfile', true)?>
								
									<div class="ccm-button">
										<?php echo $form->submit('submit', t('Sign In'))?>
										<?php echo $form->hidden('rcID', $rcID); ?>
									</div>
									
								</form>
							</div>			
						<?php } ?>
						
						
						<?php if ($showStandardLogin) { ?>
						
							<?php $rcID = isset($_REQUEST['rcID']) ? Loader::helper('text')->entities($_REQUEST['rcID']) : $rcID; ?>
						
							<div class="c5-login__login">
								<h2><?php echo t('Login'); ?></h2>
								<form class="c5-login__form" method="post" action="<?php echo $this->url('/login', 'do_login')?>">	
									<input type="hidden" name="rcID" value="<?php echo $rcID?>" />
								
									<div class="input-items">
										<div class="input-item">
											<div class="input-label-wrapper">
												<label for="uName" class="input-label">
													<?php echo (USER_REGISTRATION_WITH_EMAIL_ADDRESS == true) ? t('Email Address') : t('Username'); ?>
												</label>
											</div>
											<div class="input-field-wrapper">
												<input class="input-text" type="text" name="uName" id="uName" value="<?php echo (isset($uName) ? $uName : '');?>" />
											</div>
										</div>
										<div class="input-item">
											<div class="input-label-wrapper">
												<label for="uPassword" class="input-label">
													<?php echo t('Password')?>
												</label>
											</div>
											<div class="input-field-wrapper">
												<input class="input-text" type="password" name="uPassword" id="uPassword" />
											</div>
										</div>
														
										<?php /* wctodo: make work maybe someday? if (OpenIDAuth::isEnabled()) { ?>
											<fieldset>
								
									<legend><?php echo t('OpenID')?></legend>
								
									<div class="control-group">
										<label for="uOpenID" class="control-label"><?php echo t('Login with OpenID')?>:</label>
										<div class="controls">
											<input type="text" name="uOpenID" id="uOpenID" <?php echo (isset($uOpenID)?'value="'.$uOpenID.'"':'');?> class="ccm-input-openid">
										</div>
									</div>
									</fieldset>
										<?php } */?>
								
										<?php if (isset($locales) && is_array($locales) && count($locales) > 0) { ?>
											<div class="input-item">
												<div class="input-label-wrapper">
													<label for="USER_LOCALE" class="input-label">
														<?php echo t('Language')?>
													</label>
												</div>
												<div class="input-field-wrapper">
													<?php echo $form->select('USER_LOCALE', $locales, null, array('class' => 'input-select')); ?>
												</div>
											</div>						
										<?php } ?>
										<div class="input-item">
											<div class="input-field-wrapper">
												<label class="input-checkbox"><?php echo $form->checkbox('uMaintainLogin', 1)?> <?php echo t('Keep me logged in')?></label>
											</div>
										</div>				
									
										<div class="input-item">
											<div class="input-buttons-wrapper">
												<button type="submit" class="input-submit"><?php echo t('Log In'); ?></button>
											</div>
										</div>
									</div>
								</form>
							</div>
								
							<div class="c5-login__password-forgot">
								<h2><?php echo t('Forgot Your Password?'); ?></h2>
								<p><?php echo t("Enter your email address below. We will send you instructions to reset your password.")?></p>
								<form class="c5-login__form" method="post" action="<?php echo $this->url('/login', 'forgot_password')?>">
									<input type="hidden" name="rcID" value="<?php echo $rcID?>" />				
									<div class="input-items">
										<div class="input-item">
											<div class="input-field-wrapper">
												<input class="input-text" type="text" name="uEmail" id="uEmail" value="" />
											</div>
										</div>
										<div class="input-item">
											<div class="input-buttons-wrapper">
												<button type="submit" class="input-submit"><?php echo t('Send'); ?></button>
											</div>
										</div>
									</div>
								</form>
							</div>
							
							<?php if (ENABLE_REGISTRATION == 1) { ?>
								<aside class="c5-login__register">
									<h1><?php echo t('Need An Account?') ?></h1>
									<p><?php echo t('Create an account for use on this website.')?></p>					
									<form class="c5-login__form" method="post" action="<?php echo $this->url('/register')?>">
										<div class="input-items">
											<div class="input-item">
												<div class="input-buttons-wrapper">
													<button type="submit" class="input-submit"><?php echo t('Create Account'); ?></button>
												</div>
											</div>
										</div>
									</form>
								</aside>
							<?php  } ?>
					
						<?php } ?>	
						
						<script type="text/javascript">
							$(function() {
								$("input[name=uName]").focus();
							});
						</script>
						
					</div>
				</div>
				
			</div>
		</div>
	</div>

<?php include($this->getThemeDirectory() . '/elements/footer.php'); ?>