<?php defined('C5_EXECUTE') or die("Access Denied."); ?><!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Down for Maintenance</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body>
		<h1>Down for Maintenance</h1>
		<p>
			This site is currently down for maintenance.
		</p>
	</body>
</html>