<?php
defined('C5_EXECUTE') or die(_("Access Denied."));

/* Modified copy of /concrete/elements/system_errors.php
 *
 * Changes from core version:
 * 1) Checks if the error object is empty before outputting markup (otherwise empty divs/ul's get outputted when there are no errors).
 * 2) Checks if current page is "edit profile" or eCommerce "checkout" -- if so, don't output errors (because those two already output errors in their page content directly).
 * 3) Checks if the $format var is set before checking if it's "block" (this $format thing was added in 5.5 core... but it's very possible no $format var is passed in here, which would generate a PHP warning if we didn't check isset($format) first).
 * 4) Outputs an error header message, so it is clearer to users what's going on.
 * 
 * INSTALLATION:
 * Just drop this file in your site's top-level "elements" directory, and will be picked up by the system automatically.
 * Don't forget to put this line of code in your theme's view.php file (above the "<?php echo $innerContent; ?>" line):
 * 
 *     <?php Loader::element('system_errors', array('error' => $error)); ?>
 * 
 **********************************************************************************************************************/


if (isset($error) && $error != '') {
	if ($error instanceof Exception) {
		$_error[] = $error->getMessage();
	} else if ($error instanceof ValidationErrorHelper) {
		$_error = $error->getList();
	} else if (is_array($error)) {
		$_error = $error;
	} else if (is_string($error)) {
		$_error[] = $error;
	}
}
?>

<?php if (!empty($_error)): ?>
	
	<?php if (View::section('checkout') || View::section('profile/edit')): ?>
		
		<div class="ccm-error">Cannot proceed -- please correct the errors below.</div>
		
	<?php elseif (isset($format) && ($format == 'block')): ?>
		
		<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
			<?php foreach($_error as $e): ?>
				<?php echo $e?><br/>
			<?php endforeach; ?>
		</div>
		
	<?php else: ?>
		
		<div class="ccm-error">Please correct the following error(s):</div>
		
		<ul class="ccm-error">
		<?php foreach($_error as $e): ?>
			<li><?php  echo $e?></li>
		<?php endforeach; ?>
		</ul>
		
	<?php endif; ?>
	
<?php endif; ?>

