<?php defined('C5_EXECUTE') or die("Access Denied.");
	
//Expects the following variables to be provided:
// `$post` (the current page's blog post)
// `$classification` (the attribute handle for the "related category")
// `$count` (the number of related posts you want displayed)

Loader::model('blog', 'designer_blog');

$categories = $post->getMultiClassification($classification);

$pl = new PageList;
$pl->filterBySelectAttribute($classification, $categories);
$pl->filterByCollectionTypeHandle('post');
$pl->filter('p1.cID', $post->getID(), '<>');
$pl->sortBy('RAND()');
$pages = $pl->get($count);
$related_posts = array();
foreach ($pages as $page) {
	$related_posts[] = new BlogPost($page);
}
?>


<?php if (!empty($related_posts)) { ?>
	<nav class="db-related-posts">
		<div class="db-related-posts__heading">
			<h2>You Might Also Like</h2>
		</div>
		<ul class="db-related-posts__list">
			<?php foreach ($related_posts as $index=>$post) { ?>
				<?php 
					$item_classes = 'db-related-posts__item';							
					$item_classes .= ($index % 2 == 0 ? ' db-related-posts__item--1-if-2-per-group' : '');
					$item_classes .= ($index % 2 == 1 ? ' db-related-posts__item--2-if-2-per-group' : '');
					$item_classes .= ($index % 3 == 0 ? ' db-related-posts__item--1-if-3-per-group' : '');
					$item_classes .= ($index % 3 == 1 ? ' db-related-posts__item--2-if-3-per-group' : '');
					$item_classes .= ($index % 3 == 2 ? ' db-related-posts__item--3-if-3-per-group' : '');
				?>
				
				<li class="<?=$item_classes?>">
					<?php if ($post->getThumbnail()) { ?>
						<div class="db-related-posts__item-image">
							<a href="<?=$post->getUrl()?>">
								<?php $post->displayThumbnail(300, 175, true); ?>
							</a>
						</div>
					<?php } ?>
					<div class="db-related-posts__item-copy">
						<div class="db-related-posts__item-title">
							<h3>
								<a href="<?=$post->getUrl()?>"><?=$post->getTitle()?></a>
							</h3>
						</div>
						<div class="db-related-posts__item-date">
							<?=$post->getDate('F j, Y')?>
						</div>
						<div class="db-related-posts__item-teaser">
							<p>
								<?=$post->getTeaser()?><br>
								<a href="<?=$post->getUrl()?>">Read Full Post</a>
							</p>
						</div>
					</div>
				</li>
			<?php } ?>
		</ul>
	</nav>
<?php } ?>