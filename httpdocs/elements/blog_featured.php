<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<?php
	$nh = Loader::helper('navigation');
	Loader::model('page_list');
	$pl = new PageList();
	$pl->filterByCollectionTypeHandle('post');
	$pl->filterByAttribute('post_featured', 1);
	$posts = $pl->get();
?>

<?php if (!empty($posts)) { ?>
	<nav class="db-featured-nav">
		<div class="db-featured-nav__heading">
			<h2>Featured</h2>
		</div>
		<ul class="db-featured-nav__list">
			<?php foreach ($posts as $post) { ?>
				<li class="db-featured-nav__item ">
					<a class="db-featured-nav__link" href="<?=$nh->getLinkToCollection($post)?>"><?=$post->getCollectionName()?></a>
				</li>
			<?php } ?>
		</ul>
	</nav>
<?php } ?>