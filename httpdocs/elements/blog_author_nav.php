<?php 
	defined('C5_EXECUTE') or die("Access Denied.");
	Loader::model('blog', 'designer_blog');
	$authors = BlogHelper::getAuthors();
?>

<nav class="db-author-nav">
	<div class="db-author-nav__heading">
		<h2>Authors</h2>
	</div>
	<ul class="db-author-nav__list">
		<?php foreach ($authors as $author): ?>
			<li class="db-author-nav__item <?php echo ($option->value == $active_value) ? 'db-author-nav__item--current-page' : ''; ?>">
				<a class="db-author-nav__link <?php echo ($option->value == $active_value) ? 'db-author-nav__link--current-page' : ''; ?>" href="<?php echo $author->url; ?>"><?php echo $author->display_name; ?></a>
			</li>
		<?php endforeach; ?>
	</ul>
</nav>