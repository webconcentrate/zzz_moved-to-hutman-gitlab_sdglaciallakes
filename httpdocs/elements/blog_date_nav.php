<?php 
	defined('C5_EXECUTE') or die("Access Denied.");
	
	// Determine the "active" month that should be expanded/hilighted by default in the nav structure...
	// No "active_month" arg was passed in, so use the current page's public date:
	if (!isset($active_month)) {
		$c = Page::getCurrentPage();
		$active_month = $c->getCollectionDatePublic('m');
		$active_year = $c->getCollectionDatePublic('Y');

	// An "active_month" arg of 0 was passed in, so use today's date:
	} else if (empty($active_month)) {
		$active_month = date('m');
		$active_year = date('Y');

	// An "active_month" arg higher than 0 was passed in, so use that as a timestamp:
	} else {
		$active_month_timestamp = $active_month; //store this in another variable so it doesn't get overwritten in the next line
		$active_month = date('m', $active_month_timestamp);
		$active_year = date('Y', $active_month_timestamp);
	}
	
	Loader::model('blog_date_nav', 'designer_blog');
	$nav = new BlogDateNav($active_year, $active_month);
?>

<nav class="db-date-nav">
	<div class="db-date-nav__heading">
		<h2>Archive</h2>
	</div>
	<ul class="db-date-nav__years">
		<?php foreach ($nav as $year): ?>
			<li class="db-date-nav__year">
				<span class="db-date-nav__year-name <?php echo $year->active ? 'db-date-nav__year-name--expanded' : ''; ?>"><?php echo "{$year} ({$year->count})"; ?></span>
				<ul class="db-date-nav__months <?php echo $year->active ? 'db-date-nav__months--expanded' : ''; ?>">
					<?php foreach ($year as $month): ?>
						<li class="db-date-nav__month">
							<a class="db-date-nav__month-name" href="<?php echo $month->url; ?>"><?php echo "{$month->name} ({$month->count})"; ?></a>
						</li>
					<?php endforeach; ?>
				</ul>
			</li>
		<?php endforeach ?>
	</ul>
</nav>

<script>
	$(document).ready(function() {
		
		$('.db-date-nav__year-name').on('click', function() {
		
			$(this).toggleClass('db-date-nav__year-name--expanded');
			$(this).parents('.db-date-nav__year').find('.db-date-nav__months').toggleClass('db-date-nav__months--expanded');
			
		});

	});
</script>
