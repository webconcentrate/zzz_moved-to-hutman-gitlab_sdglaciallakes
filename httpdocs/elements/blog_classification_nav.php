<?php 
	defined('C5_EXECUTE') or die("Access Denied.");
	$options = BlogHelper::getClassificationOptions($classification);
?>

<nav class="db-classification-nav">
	<div class="db-classification-nav__heading">
		<h2><?php echo $title; ?></h2>
	</div>
	<ul class="db-classification-nav__list">
		<li class="db-classification-nav__item <?php echo (isset($active_value) && empty($active_value)) ? 'db-classification-nav__item--current-page' : ''; ?>">
			<a class="db-classification-nav__link <?php echo (isset($active_value) && empty($active_value)) ? 'db-classification-nav__link--current-page' : ''; ?>" href="<?=BlogHelper::getIndexUrl()?>">All</a>
		</li>
		<?php foreach ($options as $option): ?>
			<li class="db-classification-nav__item <?php echo ($option->value == $active_value) ? 'db-classification-nav__item--current-page' : ''; ?>">
				<a class="db-classification-nav__link <?php echo ($option->value == $active_value) ? 'db-classification-nav__link--current-page' : ''; ?>" href="<?php echo $option->url; ?>"><?php echo $option->display_name; ?></a>
			</li>
		<?php endforeach; ?>
	</ul>
</nav>