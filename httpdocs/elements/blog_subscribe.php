<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<nav class="db-subscribe">
	<div class="db-subscribe__heading">
		<h2>Subscribe</h2>
	</div>
	<div class="db-subscribe__copy">
		<p>
			Get notified of new posts via <a href="">Email</a> or <a href="<?=BlogHelper::getRssUrl()?>">RSS</a>.
		</p>
	</div>
</nav>