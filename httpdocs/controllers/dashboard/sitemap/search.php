<?php

/* Override default # of items to show in search results list */

defined('C5_EXECUTE') or die("Access Denied.");
class DashboardSitemapSearchController extends Concrete5_Controller_Dashboard_Sitemap_Search {
	
	public function view() {
		if (empty($_SESSION['PageListSearchFields']['numResults'])) {
			$_SESSION['PageListSearchFields']['numResults'] = 100;
		}
		
		parent::view();
	}
}