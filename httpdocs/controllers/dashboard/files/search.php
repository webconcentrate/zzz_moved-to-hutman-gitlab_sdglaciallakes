<?php

/* Override default # of items to show in search results list */

defined('C5_EXECUTE') or die("Access Denied.");
class DashboardFilesSearchController extends Concrete5_Controller_Dashboard_Files_Search {
	public function view() {
		if (empty($_SESSION['FileListSearchFields']['numResults'])) {
			$_SESSION['FileListSearchFields']['numResults'] = 100;
		}
		parent::view();
	}
}