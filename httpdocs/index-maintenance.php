<?php	
	// Set header to be SEO friendly (https://yoast.com/http-503-site-maintenance-seo/)
	$protocol = 'HTTP/1.0';
	if ( $_SERVER['SERVER_PROTOCOL'] === 'HTTP/1.1' ) {
		$protocol = 'HTTP/1.1';
	}
	header( $protocol . ' 503 Service Unavailable', true, 503 );
	header( 'Retry-After: 3600' );
?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Undergoing Maintenance</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<style>
			body {
				background-color: #ebebeb;
				font-family: sans-serif;
			}
				.message {
					max-width: 650px;
					margin: 0 auto;
					padding: 50px;
					text-align: center;
				}
		</style>
	</head>
	<body>
		<div class="message">
			<h1>Undergoing Maintenance</h1>
			<p>
				This site is currently undergoing maintenance. Please check back shortly. 
			</p>
		</div>
	</body>
</html>