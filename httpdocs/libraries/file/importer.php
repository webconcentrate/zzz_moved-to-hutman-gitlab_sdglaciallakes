<?php defined('C5_EXECUTE') or die("Access Denied.");

/*** WEBCONCENTRATE CUSTOMIZATION:
 Override the "import" function in the core file importer library to
 automatically resize uploaded images if they exceed a certain width/height.

 NOTE that this requires that ImageMagick be installed on the server
 (as a command-line utility that we call out to -- NOT part of PHP!)

 NOTE that 3 constants must be defined in config/site.php in order for this to take effect:
  WC_IMAGE_RESIZE_ON_UPLOAD_IMAGEMAGICK_COMMAND
  WC_IMAGE_RESIZE_ON_UPLOAD_WIDTH
  WC_IMAGE_RESIZE_ON_UPLOAD_HEIGHT

 NOTE that the "import" function below was taken from Concrete version 5.6.3.1.
 You very well might need to update it (or downgrade it) for future (or past) versions of C5!

 See http://www.concrete5.org/community/forums/customizing_c5/automatic-resize-on-upload-of-images/
***/

class FileImporter extends Concrete5_Library_FileImporter {

	/** 
	 * Imports a local file into the system. The file must be added to this path
	 * somehow. That's what happens in tools/files/importers/.
	 * If a $fr (FileRecord) object is passed, we assign the newly imported FileVersion
	 * object to that File. If not, we make a new filerecord.
	 * @param string $pointer path to file
	 * @param string $filename
	 * @param FileRecord $fr
	 * @return number Error Code | FileVersion
	 */
	public function import($pointer, $filename = false, $fr = false) {
		
		if ($filename == false) {
			// determine filename from $pointer
			$filename = basename($pointer);
		}
		
		$fh = Loader::helper('validation/file');
		$fi = Loader::helper('file');
		$sanitized_filename = $fi->sanitize($filename);
		
		// test if file is valid, else return FileImporter::E_FILE_INVALID
		if (!$fh->file($pointer)) {
			return FileImporter::E_FILE_INVALID;
		}
		
		if (!$fh->extension($filename)) {
			return FileImporter::E_FILE_INVALID_EXTENSION;
		}

		//START WEBCONCENTRATE CUSTOMIZATION: resize files that are larger than 2000px (or whatever) in width or height
		if (defined('WC_IMAGE_RESIZE_ON_UPLOAD_IMAGEMAGICK_COMMAND') && defined('WC_IMAGE_RESIZE_ON_UPLOAD_WIDTH') && defined('WC_IMAGE_RESIZE_ON_UPLOAD_HEIGHT')) {
			list($width, $height) = getimagesize($pointer);
			if ($width > WC_IMAGE_RESIZE_ON_UPLOAD_WIDTH || $height > WC_IMAGE_RESIZE_ON_UPLOAD_HEIGHT) {
				//Apparently GD chokes on large images due to memory usage
				//(see http://www.concrete5.org/community/forums/usage/upload-image-problem-no-thumbnail-is-created/#50468)
				//So this often doesn't work:
				// Loader::helper('image')->create($pointer, $pointer, 2000, 1500);
				//
				//Instead, call out to the command-line ImageMagick utility:
				$cmd_option_size = WC_IMAGE_RESIZE_ON_UPLOAD_WIDTH . 'x' . WC_IMAGE_RESIZE_ON_UPLOAD_HEIGHT; //"2000x2000"
				$quality = AL_THUMBNAIL_JPEG_COMPRESSION; //integer between 1 and 100 (100 = highest quality, least compression; 1 = most compression, worst quality)
				$cmd = WC_IMAGE_RESIZE_ON_UPLOAD_IMAGEMAGICK_COMMAND . " '{$pointer}' -resize {$cmd_option_size} -quality {$quality} '{$pointer}'";
				exec($cmd);
			}
		}
		//END WEBCONCENTRATE CUSTOMIZATION
		
		$prefix = $this->generatePrefix();
		
		// do save in the FileVersions table
		
		// move file to correct area in the filesystem based on prefix
		$response = $this->storeFile($prefix, $pointer, $sanitized_filename, $fr);
		if (!$response) {
			return FileImporter::E_FILE_UNABLE_TO_STORE;
		}
		
		if (!($fr instanceof File)) {
			// we have to create a new file object for this file version
			$fv = File::add($sanitized_filename, $prefix, array('fvTitle'=>$filename));
			$fv->refreshAttributes();
			$fr = $fv->getFile();
		} else {
			// We get a new version to modify
			$fv = $fr->getVersionToModify(true);
			$fv->updateFile($sanitized_filename, $prefix);
			$fv->refreshAttributes();
		}

		$fr->refreshCache();
		return $fv;
	}

}
