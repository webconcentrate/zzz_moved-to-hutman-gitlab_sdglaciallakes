<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php /* This block was made with Designer Content Pro. Visit http://theblockery.com/dcp for documentation. */ ?>

<?php 
	// Get repeating items 
	$items = $controller->getRepeatingItems();
	
?>


<div class="footer-nav">
	<div class="footer-nav__heading">
		<h2>
			<?php if ($heading_link->isNotEmpty()) { ?>
				<a href="<?=$heading_link->getHref()?>" <?=($heading_link_new_window->isNotEmpty() ? 'target="_blank" rel="noopener"' : '')?>>
			<?php } ?>
				<?=$heading?>
			<?php if ($heading_link->isNotEmpty()) { ?>
				</a>
			<?php } ?>
		</h2>
	</div>
	<?php if (!empty($items)) { ?>
		<ul class="footer-nav__list">
			<?php foreach ($items as $item) { ?>
				<?php
					// Array of classes to add to keep HTML output logic cleaner later on.
					$item_classes = array('footer-nav__item');
					$link_classes = array('footer-nav__link');
					
					if ($item->icon->isNotEmpty()) {
						$link_classes[] = 'footer-nav__link--has-icon';
						$link_classes[] = 'footer-nav__link--' . $item->icon->getSelectedHandle();
					}
					
					// Put all classes together into one space-separated string.
					$item_classes = implode(" ", $item_classes);
					$link_classes = implode(" ", $link_classes);
				?>
				<li class="<?=$item_classes?>">
					<a href="<?=$item->link->getHref()?>" class="<?=$link_classes?>" <?=($item->new_window->isNotEmpty() ? 'target="_blank" rel="noopener"' : '')?>>
						<span class="footer-nav__link-text"><?=$item->link->getText()?></span>
					</a>
				</li>
			<?php } ?>
		</ul>
	<?php } ?>
</div>
