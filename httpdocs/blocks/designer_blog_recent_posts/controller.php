<?php defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('blog', 'designer_blog');

class DesignerBlogRecentPostsBlockController extends BlockController {

	public function getBlockTypeName() { return t('Recent Blog Posts'); }
	public function getBlockTypeDescription() { return t('Displays a few recent blog posts'); }
	
	protected $btInterfaceWidth = 400;
	protected $btInterfaceHeight = 150;
	protected $btWrapperClass = 'ccm-ui'; //for 5.5, which uses bootstrap v1 styles
	
	protected $btTable = 'btDesignerBlogRecentPosts';
	
	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = false;
	protected $btCacheBlockOutputOnPost = false;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;
	
	private $default_post_count = 3;
	
	public function add() {
		//set default values for new blocks
		$this->set('post_count', $this->default_post_count);
		
		$this->edit();
	}
	
	public function edit() {
		//populate dropdown lists
		
		$filter_type_options = $this->getFilterTypeOptions();
		$filter_value_options = array();
		foreach ($filter_type_options as $handle => $display_name) {
			$filter_value_options[$handle] = $this->getFilterValueOptions($handle);
		}
		
		$this->set('filter_type_options', $filter_type_options);
		$this->set('filter_value_options', $filter_value_options);
	}
	
	private function getFilterTypeOptions() {
		$options = array('' => t('None (display all types of posts)'));
		
		$authors_group_id = BlogConfig::getAuthorsGroupID();
		if (!empty($authors_group_id)) {
			$options['authors'] = t('By Author');
		}
		
		$classifications = BlogHelper::getAllClassifications();
		foreach ($classifications as $handle => $display_name) {
			$options[$handle] = t('By %s', $display_name);
		}
		
		return $options;
	}
	
	private function getFilterValueOptions($filter_type_handle) {
		$options = array('' => t('&lt;Choose&gt'));
		
		if (empty($filter_type_handle)) {
			return array();
		} else if ($filter_type_handle == 'authors') {
			$authors = BlogHelper::getAuthors();
			foreach ($authors as $author) {
				$options[$author->user_name] = $author->display_name;
			}
		} else if (!empty($filter_type_handle)) {
			$classification_options = BlogHelper::getClassificationOptions($filter_type_handle);
			foreach ($classification_options as $co) {
				$options[$co->value] = $co->display_name;
			}
		}
		
		return $options;
	}
	
	public function save($args) {
		//save the appropriate filter value based on the chosen filter type
		//NOTE: If we want to explicitly clear out a previously-saved value,
		// we CANNOT set it to null! (Because C5 uses isset() to determine
		// if it should even pass a value to the db to be saved or not,
		// and isset() returns false if a variable is set to null!!)
		$args['filter_value'] = '';
		if (!empty($args['filter_type'])) {
			$filter_value_key = 'filter_value_' . $args['filter_type'];
			if (empty($args[$filter_value_key])) {
				$args['filter_type'] = '';
			} else {
				$args['filter_value'] = $args[$filter_value_key];
			}
		}
		
		//use default if post_count is invalid (instead of showing error message to user)
		$args['post_count'] = ctype_digit($args['post_count']) ? (int)$args['post_count'] : $this->default_post_count;
		
		parent::save($args);
	}
	
	public function view() {
		$this->set('posts', $this->getBlogPosts());
		$this->set('blog_url', $this->getBlogUrl());
	}
	
	private function getBlogPosts() {
		if (empty($this->filter_type)) {
			$bl = BlogList::getAll($this->post_count);
		} else if ($this->filter_type == 'authors') {
			$bl = BlogList::getByAuthor($this->filter_value, $this->post_count);
		} else {
			$bl = BlogList::getByClassification($this->filter_type, $this->filter_value, $this->post_count);
		}
		
		return $bl->posts;
	}
	
	private function getBlogUrl() {
		if (empty($this->filter_type)) {
			return BlogHelper::getIndexUrl();
		} else if ($this->filter_type == 'authors') {
			return BlogHelper::getAuthorUrl($this->filter_value);
		} else {
			return BlogHelper::getClassificationUrl($this->filter_type, $this->filter_value);
		}
	}
}