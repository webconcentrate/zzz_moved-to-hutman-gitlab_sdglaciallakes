<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php /* This block was made with Designer Content Pro. Visit http://theblockery.com/dcp for documentation. */ ?>


<div class="footer-ctas">
	<div class="footer-ctas__cta-1">
		<a class="footer-ctas__cta-1-link" href="<?=$cta_1_link->getHref()?>">
			<div class="footer-ctas__cta-1-heading">
				<h2><?=$cta_1_heading?></h2>
			</div>
			<div class="footer-ctas__cta-1-image">
				<div class="footer-ctas__cta-1-image-liner">
					<img src="<?=$cta_1_image->getImageObj(300, 390, true)->src?>" alt="">
				</div>
			</div>
		</a>
	</div>
	<div class="footer-ctas__cta-2">
		<div class="footer-ctas__cta-2-heading">
			<h2><?=$cta_2_heading?></h2>
		</div>
		<form class="footer-ctas__cta-2-form" novalidate action="<?=DIR_REL?>/plan-your-trip/newsletter/" method="post">
			<div class="footer-ctas__cta-2-email">
				<input name="email" value="" type="text" placeholder="email@example.com">
			</div>
			<div class="footer-ctas__cta-2-submit">
				<button type="submit">Submit</button>
			</div>
		</form>
	</div>
</div>