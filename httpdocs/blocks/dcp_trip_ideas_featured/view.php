<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php /* This block was made with Designer Content Pro. Visit http://theblockery.com/dcp for documentation. */ ?>

<?php
	// Get repeating items
	$items = $controller->getRepeatingItems();
	
	// Load image helper
	$ih = Loader::helper('image');
	$nh = Loader::helper('navigation');
?>

<div class="trip-ideas">
	
	<?php if ($heading->isNotEmpty()) { ?>
		<div class="trip-ideas__heading-and-link">
			<?php if ($heading->isNotEmpty()) { ?>
				<div class="trip-ideas__heading">
					<h2><?=$heading?></h2>	
				</div>
			<?php } ?>
			<?php if ($link->isNotEmpty()) { ?>
				<div class="trip-ideas__link">
					<a href="<?=$link->getHref()?>"><?=$link->getText()?></a>
				</div>
			<?php } ?>
		</div>
	<?php } ?>
	<ul class="trip-ideas__items">		
		<?php foreach ($items as $item) { ?>
			<?php $trip_idea = $item->trip_idea->getPageObj(); ?>
			<li class="trip-ideas__item">
				<a class="trip-ideas__item-link" href="<?=$nh->getLinkToCollection($trip_idea)?>">
					<div class="trip-ideas__item-image">
						<?php $trip_idea_thumbnail = $trip_idea->getAttribute('trip_idea_thumbnail'); ?>
						<?php if (!empty($trip_idea_thumbnail)) { ?>
							<?php $trip_idea_thumbnail_cropped = $ih->getThumbnail($trip_idea_thumbnail, 300, 400, true); ?>
							<img src="<?=$trip_idea_thumbnail_cropped->src?>" width="<?=$trip_idea_thumbnail_cropped->width?>" height="<?=$trip_idea_thumbnail_cropped->height?>" alt="" />
						<?php } ?>
					</div>
					<div class="trip-ideas__item-text">
						<?=$trip_idea->getCollectionName()?>
					</div>
				</a>
			</li>
		<?php } ?>
	</ul>
		
</div>