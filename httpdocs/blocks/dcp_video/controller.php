<?php defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::library('dcp_controller', 'designer_content_pro');

class DcpVideoBlockController extends DcpController {

	protected $btHandle = 'dcp_video';
	protected $btName = 'YouTube/Vimeo Video';
	protected $btDescription = '';
	protected $btTable = 'btDcpVideo';

	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;

	public $btDCProGeneratorVersion = '2.0'; //Don't change this!

	public $btDCProBlockOptions = array(
		'help_text_block_header' => "",
		'help_text_block_middle' => "",
		'help_text_block_footer' => "",
		'help_text_repeating_item_header' => "",
		'help_text_repeating_item_footer' => "",
	);

	public $btDCProSingleFields = array(
		'video_url' => array('type' => 'link', 'label' => 'YouTube/Vimeo URL', 'options' => array('control_type' => 'url', 'required' => true)),
		'image' => array('type' => 'image', 'label' => 'Image', 'options' => array('help_text' => 'The play button will be automatically overlaid (no need to add beforehand). This image will be automatically downscaled and cropped to the dimensions you enter below (or 600x400 pixels if not specifically set). The image will not upscale, so make sure the original image is at least as large as the dimensions you want.', 'required' => true)),
		'width' => array('type' => 'textbox', 'label' => 'Width', 'options' => array('help_text' => 'Optional. This is the number of pixels to crop the image to. If left blank, "600" will be used. Make sure you enter a number (no units) otherwise cropping will not work.')),
		'height' => array('type' => 'textbox', 'label' => 'Height', 'options' => array('help_text' => 'Optional. This is the number of pixels to crop the image to. If left blank, "400" will be used. Make sure you enter a number (no units) otherwise cropping will not work.')),
		'alignment' => array('type' => 'select', 'label' => 'Alignment', 'options' => array('control_type' => 'dropdown', 'select_options' => array('1' => 'Left', '2' => 'Center', '3' => 'Right'), 'help_text' => 'If the image doesn\'t fill the screen, how should the video be aligned?', 'required' => true)),
	);

	public $btDCProRepeatingItemFields = array();
	
	public function on_page_view() {
		$html = Loader::helper('html');
		$this->addHeaderItem($html->css('magnific-popup.css'));
		$this->addFooterItem($html->javascript('jquery.magnific-popup.min.js'));
	}
	
	public function getCleanVideoUrl($video_url) {
		$youtube_video_id = $this->extractYoutubeIdFromUrl($video_url);
		$vimeo_video_id = $this->extractVimeoIdFromUrl($video_url);
		$clean_video_url = '';
		if ($youtube_video_id) {
			$clean_video_url = 'https://www.youtube.com/watch?v=' . $youtube_video_id;
		} else if ($vimeo_video_id) {
			$clean_video_url = 'https://vimeo.com/' . $vimeo_video_id;
		}
		return $clean_video_url;
	}
	
	private function extractYoutubeIdFromUrl($url) {
		// http://stackoverflow.com/a/6121972/477513
		$regex = "#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#";
		if (preg_match($regex, $url, $matches)) {
			return $matches[0];
		} else {
			return '';
		}
	}
	
	private function extractVimeoIdFromUrl($url) {
		// https://gist.github.com/wwdboer/4943672
		$regex = '~(?:<iframe [^>]*src=")?(?:https?:\/\/(?:[\w]+\.)*vimeo\.com(?:[\/\w]*\/videos?)?\/([0-9]+)[^\s]*)"?(?:[^>]*></iframe>)?(?:<p>.*</p>)?~ix';
		if (preg_match($regex, $url, $matches)) {
			return $matches[1];
		} else {
			return '';
		}
	}

}
