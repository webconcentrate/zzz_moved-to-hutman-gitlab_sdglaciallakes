<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php /* This block was made with Designer Content Pro. Visit http://theblockery.com/dcp for documentation. */ ?>

<?php
	$image_width = 600;
	$image_height = 400;
	if ($width->isNotEmpty()) {
		$image_width = intval($width->getText());
	}
	if ($height->isNotEmpty()) {
		$image_height = intval($height->getText());
	}
	$resulting_image = $image->getImageObj($image_width, $image_height, true);
?>

<div class="video video--alignment-<?=$alignment->getSelectedHandle()?>">
	<div class="video__video">
		<div class="video__image">
			<img src="<?=$resulting_image->src?>" width="<?=$resulting_image->width?>" height="<?=$resulting_image->height?>" alt="<?=$image->getAltText()?>">
		</div>
		<div class="video__link">
			<a class="js-video-popup-link" href="<?=$controller->getCleanVideoUrl($video_url->getHref())?>" target="_blank" rel="noopener">Play Video</a>
		</div>
	</div>
</div>