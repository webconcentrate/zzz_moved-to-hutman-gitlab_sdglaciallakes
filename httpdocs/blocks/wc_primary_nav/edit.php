<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<div class="control-group">
	<label class="control-label" for="max_depth"># of Levels</label>
	<div class="controls">
		<?=$form->text('max_depth', $max_depth, array('style' => 'text-align: center; width: 20px;', 'maxlength' => '1'))?>
		<i>1 = no dropdown, 2 = single-level dropdown, etc.</i>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="include_home_page">Include Home Page</label>
	<div class="controls">
		<?=$form->select('include_home_page', array(
			'1' => 'Yes, include the home page',
			'0' => 'No, do not include the home page',
		), $include_home_page, array('style' => 'width: auto;'))?>
	</div>
</div>
