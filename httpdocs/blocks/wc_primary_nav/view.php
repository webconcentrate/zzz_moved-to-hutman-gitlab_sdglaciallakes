<?php defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::model('category/query', 'sdgl_members');
Loader::model('subcategory/query', 'sdgl_members');

if (!function_exists('wcPrimaryNavOutputMenu')) {
	function wcPrimaryNavOutputMenu($items, $level = 1) { ?>
		
		<ul class="js-primary-nav-list primary-nav__list primary-nav__list--level-<?=$level?>">

			<?php foreach ($items as $item) {
				
				//custom for SDGL
				$item_is_attractions = ($item->path == '/attractions');
				$item_is_where_to_stay = ($item->path == '/where-to-stay');
				if ($item_is_attractions || $item_is_where_to_stay) {
					$item->has_children = true;
				}

				// Determine css classes first to keep HTML output logic cleaner later on...
				$item_classes = array();
				$link_and_toggle_wrapper_classes = array();
				$link_classes = array();
				$children_toggle_classes = array();
				
				// JS classes
				$item_classes[] = 'js-primary-nav-item';
				//$link_and_toggle_wrapper_classes[] = 'js-primary-nav-link-and-toggle';
				$link_classes[] = 'js-primary-nav-link';
				$children_toggle_classes[] = 'js-primary-nav-children-toggle';
				
				// BEM classes
				$item_classes[] = 'primary-nav__item';
				$link_and_toggle_wrapper_classes[] = 'primary-nav__link-and-toggle';
				$link_classes[] = 'primary-nav__link';
				$children_toggle_classes[] = 'primary-nav__children-toggle';
				
				if ($item->is_home) {
					// Class for the homepage
					$item_classes[] = 'primary-nav__item--homepage';
					$link_classes[] = 'primary-nav__link--homepage';
				}
				
				if ($item->is_current) {
					// Class for the page currently being viewed
					$item_classes[] = 'primary-nav__item--current-page';
					$link_classes[] = 'primary-nav__link--current-page';
				}

				if ($item->in_path) {
					// Class for parent items of the page currently being viewed
					$item_classes[] = 'primary-nav__item--in-current-page-path';
					$link_classes[] = 'primary-nav__link--in-current-page-path';
				}

				if ($item->has_children) {
					// Class for items that have sub-menus
					
					// JS classes
					$item_classes[] = 'js-primary-nav-item-has-children';
					$link_classes[] = 'js-primary-nav-link-has-children';
					
					// BEM classes
					$item_classes[] = 'primary-nav__item--has-children';
					$link_classes[] = 'primary-nav__link--has-children';
				}
				
				if ($item_is_attractions) {
					// Class for parent items of the page currently being viewed
					$item_classes[] = 'primary-nav__item--full-width-child';
				}
				
				// Output depth of nav item
				$item_classes[] = 'primary-nav__item--level-' . $level;
				$link_and_toggle_wrapper_classes[] = 'primary-nav__link-and-toggle--level-' . $level;
				$link_classes[] = 'primary-nav__link--level-' . $level;
				$children_toggle_classes[] = 'primary-nav__children-toggle--level-' . $level;
				

				// Put all classes together into one space-separated string
				$item_classes = implode(" ", $item_classes);
				$link_and_toggle_wrapper_classes = implode(" ", $link_and_toggle_wrapper_classes);
				$link_classes = implode(" ", $link_classes);
				$children_toggle_classes = implode(" ", $children_toggle_classes);

				// NOW OUTPUT THE HTML...
				?>

				<li class="<?=$item_classes?>">
					
					<?php if ($item->has_children) { ?>
						<div class="<?=$link_and_toggle_wrapper_classes?>">
					<?php } ?>
					
							<a href="<?=$item->url?>" <?=$item->target_attr?> class="<?=$link_classes?>">
		
								<?=$item->title?>
		
							</a>
							
							<?php if ($item->has_children) { ?>
								<button class="<?=$children_toggle_classes?>">Toggle child links of <?=$item->title?></button>
							<?php } ?>
							
					<?php if ($item->has_children) { ?>
						</div>
					<?php } ?>
					
					<?php if ($item_is_attractions) {
						$attractions = SubcategoryQuery::attractionsGroupedByCategory();
						?>
						
						<ul class="js-primary-nav-list primary-nav__list primary-nav__list--level-2 primary-nav__list--full-width">
							<?php foreach ($attractions as $category) { ?>
								<li class="js-primary-nav-item js-primary-nav-item-has-children primary-nav__item primary-nav__item--has-children primary-nav__item--level-2">
									<div class="primary-nav__link-and-toggle primary-nav__link-and-toggle--level-2">
										<a class="js-primary-nav-link js-primary-nav-link-has-children primary-nav__link primary-nav__link--has-children primary-nav__link--level-2" href="<?=$category->directory_url?>"><?=$category->title?></a>
										<button class="js-primary-nav-children-toggle primary-nav__children-toggle primary-nav__children-toggle--level-2">Toggle child links of <?=$category->title?></button>
									</div>
									<ul class="js-primary-nav-list primary-nav__list primary-nav__list--level-3">
										<?php foreach ($category->subcategories as $subcategory) { ?>
											<li class="js-primary-nav-item primary-nav__item primary-nav__item--level-3">
												<a class="js-primary-nav-link primary-nav__link primary-nav__link--level-3" href="<?=$category->directory_url($subcategory->id)?>"><?=$subcategory->title?></a>
											</li>
										<?php } ?>
									</ul>
								</li>
							<?php } ?>
						</ul>
						
					<?php } else if ($item_is_where_to_stay) {
						$category = CategoryQuery::byUrlSlug('where-to-stay');
						if ($category) {
							$subcategories = SubcategoryQuery::byCategoryId($category->id);
							?>
							
							<ul class="js-primary-nav-list primary-nav__list primary-nav__list--level-2">
								<?php foreach ($subcategories as $subcategory) { ?>
									<li class="js-primary-nav-item primary-nav__item primary-nav__item--level-2">
										<a class="js-primary-nav-link primary-nav__link primary-nav__link--level-2" href="<?=$category->directory_url($subcategory->id)?>">
											<?=$subcategory->title?>
										</a>
									</li>
								<?php } ?>
							</ul>
							
						<?php } ?>
					
					<?php } else if ($item->has_children) { ?>
						<?=wcPrimaryNavOutputMenu($item->children, $level + 1)?>
					<?php } ?>
				</li>

			<?php } ?>
		</ul>
	<?php } ?>
<?php } ?>

<nav class="js-primary-nav primary-nav">

	<div class="js-primary-nav-responsive-tester-narrow primary-nav__responsive-tester primary-nav__responsive-tester--narrow"></div>
	<div class="js-primary-nav-responsive-tester-wide primary-nav__responsive-tester primary-nav__responsive-tester--wide"></div>
									
	<div class="primary-nav__heading">
		<h2>Menu</h2>
	</div>

	<?=wcPrimaryNavOutputMenu($items)?>

</nav>
