/*==============================================================================================================================
 PRIMARY NAV
==============================================================================================================================*/

$(document).ready(function() {

	$('.js-primary-nav').each(function() {
		
		// Variables to cache DOM lookups and make things more readable
		var $thisNav = $(this);
		var $lists = $('.js-primary-nav-list', $thisNav);
		var $itemsWithChildren = $('.js-primary-nav-link-has-children', $thisNav);
		var $childrenToggles = $('.js-primary-nav-children-toggle', $thisNav);
		var $responsiveTesterNarrow = $('.js-primary-nav-responsive-tester-narrow');
		var $responsiveTesterWide = $('.js-primary-nav-responsive-tester-wide');
		
		// Make toggle click toggle visiblity of the list of links.
		$childrenToggles.on('click', function(event) {
			var $targetList = $(this).closest('.js-primary-nav-item').children('.js-primary-nav-list');
			var $targetListParentLists = $targetList.parents('.js-primary-nav-list');
			var $targetListChildLists = $targetList.find('.js-primary-nav-list');
			// Toggle target list.
			$targetList.toggleClass('primary-nav__list--expanded');
			// On narrow nav, collapse child lists (always a fresh start with child lists), but keep other lists open to prevent jumpy nav.
			if ($responsiveTesterNarrow.is(':visible')) {
				$targetListChildLists.removeClass('primary-nav__list--expanded');
			}
			// On wide nav, collapse all lists besides target list and parent lists.
			if ($responsiveTesterWide.is(':visible')) {
				$lists.not($targetList).not($targetListParentLists).removeClass('primary-nav__list--expanded');
			} 
		});
			
		// Make clicking outside nav close all lists
		$(document).on('click', function(event) {
			if ($thisNav.has(event.target).length === 0) {
				$lists.removeClass('primary-nav__list--expanded');
			}
		});
		
		// On wide nav, enable doubletaptogo (so touch devices can trigger dropdown)
		if ($responsiveTesterWide.is(':visible')) {
			$thisNav.doubleTapToGo();
		}
		
	});
	
});
