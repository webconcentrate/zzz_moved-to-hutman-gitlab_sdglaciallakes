<?php defined('C5_EXECUTE') or die("Access Denied.");

/* version 2015-08-21 */

class WcPrimaryNavBlockController extends BlockController {

	protected $btName = 'Primary Nav Menu';
	protected $btTable = 'btWcPrimaryNav';
	
	protected $btWrapperClass = 'ccm-ui'; //for twitter bootstrap styles in edit dialog
	protected $btInterfaceWidth = "450";
	protected $btInterfaceHeight = "150";
	
	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btCacheBlockOutputForRegisteredUsers = true;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;
	
	public function add() {
		//default values for new blocks...
		$this->set('max_depth', 2);
		$this->set('include_home_page', 1);
	}
	
	public function validate($args) {
		$e = Loader::helper('validation/error');
		
		if ((int)$args['max_depth'] < 1) {
			$e->add('# of Levels must be a number greater than 0');
		}
		
		return $e;
	}
	
	public function on_page_view() {
		// If multi-level, include necessary javascript
		if ($this->max_depth > 1) {
			$html = Loader::helper('html');
			$bv = new BlockView();
			$bv->setBlockObject($this->getBlockObject());
			$blockURL = $bv->getBlockURL();
			$this->addFooterItem($html->javascript('jquery.dcd.doubletaptogo.min.js'));
			$this->addFooterItem('<script src="' . $blockURL . '/primary-nav.' . WC_ASSET_CACHE_BUST_NUMBER . '.js"></script>');
		}	
	}
	
	public function view() {
		$pages = $this->getPages();
		$nav = $this->getHierarchicalArray($pages);

		//Ditch the homepage as root node
		// (just send array of top-level items to the view)
		$items = $nav->children;
		
		if ($this->include_home_page) {
			//insert homepage at first position of top-level items
			// (not as a parent, so empty its children array too)
			$nav->children = array();
			$items = array('' => $nav) + $items;
		}

		$current_page_path = Page::getCurrentPage()->getCollectionPath();
		$this->addPositionalPropertiesToNav($items, $current_page_path);
		
		$this->set('items', $items);
	}
	
	private function getPages() {
		$pl = new PageList;
		
		//filter out excluded pages and children of excluded parents...
		$pl->filter(false, '(ak_exclude_nav = 0 or ak_exclude_nav is null)');
		foreach ($this->getExcludedPaths() as $exclude_path) {
			$pl->filter('PagePaths.cPath', $exclude_path . '/%', 'NOT LIKE');
		}
		
		//filter out pages beneath a certain depth
		// (determine "depth" by counting the number of slashes in the path)...
		$slash_count_sql = "(CHAR_LENGTH(PagePaths.cPath) - CHAR_LENGTH(replace(PagePaths.cPath, '/', '')))"; // http://stackoverflow.com/a/1860478/477513
		$depth_condition = "({$slash_count_sql} <= {$this->max_depth})";
		$pl->filter(false, $depth_condition);
		
		//The above filters will likely exclude the home page for various reasons
		// (e.g. the "path NOT LIKE..." always matches homepage path because it's null)...
		// but they also might not (if they are never triggered... e.g. if no pages in the sitemap
		// are marked exclude_nav)... so to keep things simple we just explicitly exclude the homepage
		// from the query, and add it back manually after we run the query.
		$pl->filter(false, 'p1.cID != ' . (int)HOME_CID);
		
		//Don't worry about sorting -- that will be done manually later
		// (because "display order" needs to be compared only to other pages
		// under the same parent).
		
		$pages = $pl->get();
		
		//Add the homepage in (we do this regardless of $this->include_home_page
		// because the code that generates the hierarchy needs a root node -- it will be 
		// moved or removed as needed later on, but for now always include it in the results).
		$pages[] = Page::getByID(HOME_CID);
		
		return $pages;
	}
	
	private function getExcludedPaths() {
		$pl = new PageList;
		$pl->filter(false, '(ak_exclude_nav = 1 OR ak_exclude_subpages_from_nav = 1)');
		$pages = $pl->get();
		
		$paths = array();
		foreach ($pages as $page) {
			$paths[] = $page->getCollectionPath();
		}
		
		return $paths;
	}
	
	private function getHierarchicalArray($pages) {
		//First sort all the pages so all top-level pages are first,
		// then all 2nd level pages, then all 3rd-level pages, etc.
		// (otherwise items get added in the wrong order
		// down below when we add "nodes" to the tree).
		usort($pages, array($this, 'usortPages'));
		
		$nav = new WcPrimaryNavNode;
		
		foreach ($pages as $page) {
			$node = &$nav;
			$path_parts = explode('/', $page->getCollectionPath());
			foreach ($path_parts as $slug) {
				if (!array_key_exists($slug, $node->children)) {
					$node->children[$slug] = new WcPrimaryNavNode;
				}
				$node = &$node->children[$slug];
			}
			
			$node->setPage($page);
		}
		
		//The structure comes out as a dummy root node
		// with children being an array of 1 item (the home page).
		//But we want to return the homepage as the root node:
		return $nav->children[''];
	}
	
	//callback for usort() in getHierarchicalArray (hence it must be public)
	public function usortPages($page_a, $page_b) {
		$level_a = substr_count($page_a->getCollectionPath(), '/');
		$level_b = substr_count($page_b->getCollectionPath(), '/');
		if ($level_a != $level_b) {
			return ($level_a < $level_b) ? -1 : 1;
		}
		
		$display_order_a = $page_a->getCollectionDisplayOrder();
		$display_order_b = $page_b->getCollectionDisplayOrder();
		if ($display_order_a != $display_order_b) {
			return ($display_order_a < $display_order_b) ? -1 : 1;
		}
		
		return 0;
	}
	
	private function addPositionalPropertiesToNav(&$children, $current_page_path) {
		$max_index = (count($children) - 1);
		$current_index = 0;
		foreach ($children as &$node) {
			$node->is_first = ($current_index == 0);
			$node->is_last = ($current_index == $max_index);
			$node->has_children = (bool)count($node->children);
			$node->is_current = ($current_page_path == $node->path);
			if ($node->is_home) {
				$node->in_path = $node->is_current; //even though technically the homepage is in EVERY page's path, it doesn't make sense for the purposes of the primary nav menu... so only consider it "in the path" if it is the active page
			} else {
				$node->in_path = (strpos($current_page_path, $node->path) !== false && strpos($current_page_path, $node->path) == 0);
			}

			if ($node->replace_link_with_first_in_nav && $node->has_children) {
				$node->url = $node->children[0]->url;
			}

			$this->addPositionalPropertiesToNav($node->children, $current_page_path);
			
			$current_index++;
		}
	}
}

class WcPrimaryNavNode {
	public $cID;
	public $path;
	public $url;
	public $title;
	public $target;
	public $target_attr;
	public $is_home;
	public $replace_link_with_first_in_nav;

	public $is_first;
	public $is_last;
	public $has_children;
	public $is_current;
	public $level;
	public $in_path;
	
	public $children = array();
	
	public function setPage($page) {
		$this->cID = $page->getCollectionID();
		$this->path = $page->getCollectionPath();
		$this->url = Loader::helper('navigation')->getLinkToCollection($page);
		$this->title = h($page->getCollectionName());
		$this->target = (($page->cPointerExternalLink != '') && $page->cPointerExternalLinkNewWindow) ? '_blank' : $page->getAttribute('nav_target');
		$this->target_attr = empty($this->target) ? '' : 'target="' . $this->target . '"';
		$this->is_home = ($page->cID == HOME_CID);
		$this->replace_link_with_first_in_nav = $page->getAttribute('replace_link_with_first_in_nav');
	}
	
}