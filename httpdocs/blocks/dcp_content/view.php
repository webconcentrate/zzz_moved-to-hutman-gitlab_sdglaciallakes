<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php /* This block was made with Designer Content Pro. Visit http://theblockery.com/dcp for documentation. */ ?>

<?php
	// Replace placeholder strings with dynamic value.
	$content = str_replace('{YEAR}', date('Y'), $content);
	$content = str_replace('{SITE_NAME}', SITE, $content);
?>

<div class="content <?=($width->isNotEmpty() ? 'content--width-' . $width->getSelectedHandle() : '')?>">
	<?=$content?>	
</div>