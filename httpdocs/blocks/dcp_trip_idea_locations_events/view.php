<?php defined('C5_EXECUTE') or die(_("Access Denied."));

$locations = array();
foreach ($controller->getRepeatingItems() as $item) {
	$location = $item->location->getLocation();
	if ($location && $location->is_active) {
		$locations[] = $location;
	}
}

if ($locations) {
	Loader::model('event', 'designer_event_calendar');
	$location_ids = Loader::helper('wc_utils', 'wc_utils')->arrayColumn($locations, 'id');
	$location_events = EventOccurrence::getByCount(date(DEC_DATETIMEPICKER_DATE_FORMAT), 5, null, 365, $location_ids);
} else if (Page::getCurrentPage()->isEditMode()) {
	echo '<p><b><i>Click here to add members to this trip idea</i></b></p>';
	return;
} else {
	return;
}
?>

<div class="member-list">
	<div class="member-list__heading">
		<h2>Featured Attractions</h2>
	</div>
	<ul class="member-list__list">
		<?php foreach ($locations as $location) { ?>
			<li class="member-list__item">
				<a class="member-list__item-link" href="<?=$location->details_page_url?>">
					<div class="member-list__item-copy <?=(!empty($location->thumbnail_photo) ? 'member-list__item-copy--has-image' : '')?>">
						<div class="member-list__item-title">
							<?=$location->title?>
						</div>
						<div class="member-list__item-location">
							<?php if ($location->address_city && $location->address_state) { ?>
								<?=$location->address_city?>, <?=$location->address_state?>
							<?php } else if ($location->address_city || $location->address_state) { ?>
								<?=$location->address_city?> <?=$location->address_state?>
							<?php } ?>
						</div>
					</div>
					<?php if (!empty($location->thumbnail_photo)) { ?>
						<div class="member-list__item-image">
							<img src="<?=$location->thumbnail_photo->src(400, 300, true)?>" alt="">
						</div>
					<?php } ?>
				</a>
			</li>
		<?php } ?>
	</ul>
</div>

<?php if ($location_events) { ?>
	<div class="spacer spacer--size-10 spacer--style-3"></div>


	<div class="event-list">
		<div class="event-list__heading">
			<h2>Featured Events</h2>
		</div>
		<ul class="event-list__list">
			<?php foreach ($location_events as $event) { ?>
				<li class="event-list__item">
					<div class="event-list__item-summary-date">
						<div class="event-list__item-summary-month">
							<?=$event->getStartFormatted('M')?>
						</div>
						<div class="event-list__item-summary-day">
							<?=$event->getStartFormatted('j')?>
						</div>
					</div>
					<div class="event-list__item-details">
						<div class="event-list__item-title">
							<?php if ($event->isLinkToNothing()) { ?>
								<span><?=$event->getTitle()?></span>
							<?php } else { ?>
								<a href="<?=$event->getLinkToDetailsPage()?>">
									<?=$event->getTitle()?>
								</a>
							<?php } ?>
						</div>
						<div class="event-list__item-date">
							<?php
								$start_date = $event->getStartFormatted('l, F d, Y');
								$start_time = $event->getStartFormatted('g:ia');
								$end_date = $event->getEndFormatted('l, F d, Y');
								$end_time = $event->getEndFormatted('g:ia');
								
								if ($event->isAllDay()) {
									echo 'All day';
								} else {
									echo $start_time;
									if (!$event->isEndTimeHidden()) {
										echo ' - ' . $end_time;
									}
								}
								
								if ($event->isMultiDay()) {
									echo ' (thru ' . $end_date . ')';
								}
							?>
						</div>
						
						<?php if ($event->hasSubtitle()) { ?>
							<div class="event-list__item-location">
								<?=$event->getSubtitle()?>
							</div>
						<?php } ?>
						
					</div>
				</li>
			<?php } ?>
		</ul>
	</div>
<?php } ?>
