<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php /* This block was made with Designer Content Pro. Visit http://theblockery.com/dcp for documentation. */ ?>

<?php
	$items = $controller->getRepeatingItems();
?>

<?php if (!empty($items)) { ?>
	<div class="social-nav">
		<ul class="social-nav__list">
			<?php foreach ($items as $item) { ?>
				<?php
					// Array of classes to add to keep HTML output logic cleaner later on.
					$item_classes = array('social-nav__item');
					$link_classes = array('social-nav__link');
					
					if ($item->icon->isNotEmpty()) {
						$link_classes[] = 'social-nav__link--has-icon';
						$link_classes[] = 'social-nav__link--' . $item->icon->getSelectedHandle();
					}
					
					// Put all classes together into one space-separated string.
					$item_classes = implode(" ", $item_classes);
					$link_classes = implode(" ", $link_classes);
				?>
				<li class="<?=$item_classes?>">
					<a href="<?=$item->link->getHref()?>" class="<?=$link_classes?>" <?=($item->new_window->isNotEmpty() ? 'target="_blank" rel="noopener"' : '')?>>
						<span class="social-nav__link-text"><?=$item->link->getText()?></span>
					</a>
				</li>
			<?php } ?>
		</ul>
	</div>
<?php } ?>