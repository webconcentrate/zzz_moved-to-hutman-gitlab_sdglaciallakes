<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php /* This block was made with Designer Content Pro. Visit http://theblockery.com/dcp for documentation. */ ?>

<?php
	// Get items
	$items = $controller->getRepeatingItems();
?>

<?php 
	// We want at least one image. Otherwise this will look bad.
	if (!empty($items)) { 
?>
	<?php
		// Randomize items so we get a random image.
		shuffle($items);
	?>
	<div class="home-feature">
		<div class="home-feature__imagery">
			<?php if ($items[0]->image_narrow->isNotEmpty()) { ?>
				<div class="home-feature__image home-feature__image--narrow" style="background-image: url(<?=$items[0]->image_narrow->getImageObj(700, 500, true)->src?>)"></div>
			<?php } else if ($items[0]->image_wide->isNotEmpty()) { ?>
				<div class="home-feature__image home-feature__image--narrow" style="background-image: url(<?=$items[0]->image_wide->getImageObj(700, 500, true)->src?>)"></div>
			<?php } ?>
			<?php if ($items[0]->image_wide->isNotEmpty()) { ?>
				<div class="home-feature__image home-feature__image--wide" style="background-image: url(<?=$items[0]->image_wide->getImageObj(1500, 600, true)->src?>)"></div>
			<?php } ?>
			<?php if ($heading->isNotEmpty()) { ?>
				<div class="home-feature__heading">
					<h2><?=$heading?></h2>
				</div>
			<?php } ?>
		</div>
		<div class="home-feature__cta">
			<div class="home-feature__cta-liner">
				<div class="home-feature__cta-copy">
					<div class="home-feature__cta-heading">
						<h3><?=$cta_heading?></h3>
					</div>
					<div class="home-feature__cta-message">
						<?=$cta_message?>
					</div>
				</div>
				<div class="home-feature__cta-link">
					<a href="<?=$cta_link->getHref()?>"><?=$cta_link->getText()?></a>
				</div>
				<div class="home-feature__cta-image">
					<div class="home-feature__cta-image-liner">
						<img src="<?=$cta_image->getImageObj(300, 390, true)->src?>" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>