<?php defined('C5_EXECUTE') or die(_("Access Denied."));

Loader::library('dcp_controller', 'designer_content_pro');

class DcpHomeFeatureBlockController extends DcpController {

	protected $btHandle = 'dcp_home_feature';
	protected $btName = 'Home Feature';
	protected $btDescription = '';
	protected $btTable = 'btDcpHomeFeature';

	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = false;
	protected $btCacheBlockOutputOnPost = false;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;

	public $btDCProGeneratorVersion = '2.1'; //Don't change this!

	public $btDCProBlockOptions = array(
		'help_text_block_header' => "",
		'help_text_block_middle' => "",
		'help_text_block_footer' => "",
		'help_text_repeating_item_header' => "",
		'help_text_repeating_item_footer' => "",
	);

	public $btDCProSingleFields = array(
		'heading' => array('type' => 'textbox', 'label' => 'Photo Overlay Message', 'options' => array('searchable' => true, 'help_text' => '', 'required' => true)),
		'cta_heading' => array('type' => 'textbox', 'label' => 'CTA Heading', 'options' => array('searchable' => true, 'required' => true)),
		'cta_message' => array('type' => 'textbox', 'label' => 'CTA Message', 'options' => array('searchable' => true, 'required' => true)),
		'cta_link' => array('type' => 'link', 'label' => 'CTA Button', 'options' => array('control_type' => 'combo', 'searchable' => true, 'has_editable_text' => true, 'required' => true)),
		'cta_image' => array('type' => 'image', 'label' => 'CTA Image', 'options' => array('help_text' => 'This image will be automatically downscaled and cropped to 300x390 pixels. This image should be these dimensions or larger.', 'required' => true)),
	);

	public $btDCProRepeatingItemFields = array(
		'image_wide' => array('type' => 'image', 'label' => 'Image (wide screen)', 'options' => array('help_text' => 'This image will be automatically downscaled and cropped to 1500x600 pixels. This image should be these dimensions or larger. If possible, sizing to these dimensions and optimizing to a low file size in Photoshop prior to uploading will yield the best results.', 'is_item_thumb' => true, 'required' => true, 'is_item_label' => true)),
		'image_narrow' => array('type' => 'image', 'label' => 'Image (narrow screen)', 'options' => array('help_text' => 'This image will be automatically downscaled and cropped to 700x500 pixels. This image should be these dimensions or larger. If possible, sizing to these dimensions and optimizing to a low file size in Photoshop prior to uploading will yield the best results. If blank, wide image will be used. ')),
	);

}
