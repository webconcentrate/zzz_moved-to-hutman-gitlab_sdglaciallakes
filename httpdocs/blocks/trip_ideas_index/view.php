<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>


<?php	
	// Load image helper
	$ih = Loader::helper('image');
	$nh = Loader::helper('navigation');
?>

<?php if (!empty($pages)) { ?>
	<div class="trip-ideas">
		
		<div class="trip-ideas__items">		
			<?php foreach ($pages as $page) { ?>
				<?php $trip_idea = $page; ?>
				<div class="trip-ideas__item">
					<a class="trip-ideas__item-link" href="<?=$nh->getLinkToCollection($trip_idea)?>">
						<div class="trip-ideas__item-image">
							<?php $trip_idea_thumbnail = $trip_idea->getAttribute('trip_idea_thumbnail'); ?>
							<?php if (!empty($trip_idea_thumbnail)) { ?>
								<?php $trip_idea_thumbnail_cropped = $ih->getThumbnail($trip_idea_thumbnail, 300, 400, true); ?>
								<img src="<?=$trip_idea_thumbnail_cropped->src?>" width="<?=$trip_idea_thumbnail_cropped->width?>" height="<?=$trip_idea_thumbnail_cropped->height?>" alt="" />
							<?php } ?>
						</div>
						<div class="trip-ideas__item-text">
							<?=$trip_idea->getCollectionName()?>
						</div>
					</a>
				</div>
			<?php } ?>
		</div>
			
	</div>
<?php } ?>