<?php defined('C5_EXECUTE') or die("Access Denied.");

class TripIdeasIndexBlockController extends BlockController {

	protected $btName = 'Trip Ideas Index';
	
	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = false;
	protected $btCacheBlockOutputOnPost = false;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;
	
	public function view() {
		
		//$current_page = Page::getCurrentPage();
		//$current_page_cID = $current_page->getCollectionID();

		$pl = new PageList;
		$pl->filterByCollectionTypeHandle(array('trip_idea'));
		//$pl->filterByParentID($current_page_cID);
		$pages = $pl->get(0);
		
		$this->set('pages', $pages);
		
	}
}
