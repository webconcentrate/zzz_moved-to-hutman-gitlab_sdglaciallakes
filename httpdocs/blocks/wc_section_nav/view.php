<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<nav class="section-nav">
		
	<ul class="section-nav__list section-nav__list--level-1">
		
		<?php foreach ($children as $item) { ?>
			
			<li class="section-nav__item section-nav__item--level-1">
				<a class="section-nav__item-link section-nav__item-link--level-1" href="<?=$item->url?>" class="<?=$link_classes?>">
					
					<?=$item->title?>
				
				</a>
			</li>
		
		<?php } ?>
		
	</ul>
	
</nav>
