<?php defined('C5_EXECUTE') or die("Access Denied.");

/* version 2015-03-20 */

class WcSectionNavBlockController extends BlockController {

	protected $btName = 'Section Nav';
	
	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btCacheBlockOutputForRegisteredUsers = true;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;
	
	private $current_page;
	private $current_path;
	
	public function view() {
		$this->current_page = Page::getCurrentPage();
		$this->current_path = $this->current_page->getCollectionPath();
		
		$section_parent_page = $this->getSectionParentPage();
		$section_child_pages = $this->getSectionChildPages($section_parent_page);

		$section_parent_display_object = $this->getDisplayObjectFromPage($section_parent_page);
		$section_children_display_objects = $this->getDisplayObjectsFromPages($section_child_pages);
		
		$this->set('parent', $section_parent_display_object);
		$this->set('children', $section_children_display_objects);
	}
	
	private function getSectionParentPage() {
		if (substr_count($this->current_path, '/') > 1) {
			$current_path_parts = explode('/', $this->current_path);
			$section_path = "/{$current_path_parts[1]}"; //0th element is empty string (because of leading slash)
			$section_page = Page::getByPath($section_path);
		} else {
			$section_page = $this->current_page;
		}
		
		return $section_page;
	}
	
	private function getSectionChildPages($section_parent_page) {
		$pl = new PageList;
		$pl->filterByParentID($section_parent_page->getCollectionID());
		$pl->filter(false, '(ak_exclude_nav = 0 or ak_exclude_nav is null)');
		$pl->sortByDisplayOrder();
		return $pl->get();
	}
		
	private function getDisplayObjectFromPage($page) {
		$page_path = $page->getCollectionPath();
		
		$display_object = (object)array(
			'url' => Loader::helper('navigation')->getLinkToCollection($page),
			'title' => h($page->getCollectionName()),
			'is_current' => ($page_path == $this->current_path),
			'in_path' => (strpos($this->current_path, $page_path) !== false && strpos($this->current_path, $page_path) == 0),
		);
		
		return $display_object;
	}
	
	private function getDisplayObjectsFromPages($pages) {
		$display_objects = array();
		foreach ($pages as $page) {
			$display_objects[] = $this->getDisplayObjectFromPage($page);
		}
		return $display_objects;
	}
	
}
