<?php defined('C5_EXECUTE') or die("Access Denied.");

Loader::model('blog', 'designer_blog');

class DesignerBlogFeaturedPostsBlockController extends BlockController {

	protected $btName = 'Featured Posts';
	protected $btTable = 'btDesignerBlogFeaturedPosts';
	
	protected $btWrapperClass = 'ccm-ui'; //for twitter bootstrap styles in edit dialog
	protected $btInterfaceWidth = "450";
	protected $btInterfaceHeight = "250";
	
	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = false;
	protected $btCacheBlockOutputForRegisteredUsers = false;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;
	
	public function add() {
		$this->setPostSelectOptions();
	}
	public function edit() {
		$this->setPostSelectOptions();
	}
	private function setPostSelectOptions() {
		$pl = new PageList;
		$pl->filterByCollectionTypeHandle('post');
		$pl->sortByPublicDateDescending();
		$pages = $pl->get();
		
		$options = array('' => ''); //leave the label blank so the "chosen" widget can use data-placeholder attribute
		foreach ($pages as $page) {
			$options[$page->getCollectionID()] = h($page->getCollectionName());
		}
		
		$this->set('post_select_options', $options);
	}
	
	public function validate($args) {
		$e = Loader::helper('validation/error');
		
		if (empty($args['post_1_cID'])) {
			$e->add('You must choose a post 1');
		}
		
		if (empty($args['post_2_cID'])) {
			$e->add('You must choose a post 2');
		}
		
		if (empty($args['post_3_cID'])) {
			$e->add('You must choose a post 3');
		}
		
		return $e;
	}
	
	public function view() {
		$posts = array();
		$posts[0] = new BlogPost(Page::getByID($this->post_1_cID));
		$posts[1] = new BlogPost(Page::getByID($this->post_2_cID));
		$posts[2] = new BlogPost(Page::getByID($this->post_3_cID));
		$this->set('posts', $posts);
		$this->set('blog_url', BlogHelper::getIndexUrl());
	}
}
