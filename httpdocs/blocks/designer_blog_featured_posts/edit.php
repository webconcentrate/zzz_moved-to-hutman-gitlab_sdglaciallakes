<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<div class="control-group">
	<label class="control-label" for="post_1_cID">Post 1:</label>
	<div class="controls">
		<?=$form->select('post_1_cID', $post_select_options, $post_1_cID, array('data-placeholder' => 'Choose a post...' /* for the "chosen" widget (it displays this in the widget when nothing has been selected yet) */))?>
		<script>$('#post_1_cID').chosen({width: "100%"});</script>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="post_2_cID">Post 2:</label>
	<div class="controls">
		<?=$form->select('post_2_cID', $post_select_options, $post_2_cID, array('data-placeholder' => 'Choose a post...' /* for the "chosen" widget (it displays this in the widget when nothing has been selected yet) */))?>
		<script>$('#post_2_cID').chosen({width: "100%"});</script>
	</div>
</div>

<div class="control-group">
	<label class="control-label" for="post_3_cID">Post 3:</label>
	<div class="controls">
		<?=$form->select('post_3_cID', $post_select_options, $post_3_cID, array('data-placeholder' => 'Choose a post...' /* for the "chosen" widget (it displays this in the widget when nothing has been selected yet) */))?>
		<script>$('#post_3_cID').chosen({width: "100%"});</script>
	</div>
</div>
