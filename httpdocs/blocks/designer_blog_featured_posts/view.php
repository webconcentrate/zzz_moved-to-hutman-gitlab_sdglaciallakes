<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<div class="db-featured-posts">
	<div class="db-featured-posts__heading">
		<h2>Featured Posts</h2>
	</div>
	<ol class="db-featured-posts__list">
		<?php foreach ($posts as $index=>$post) { ?>
			<li class="db-featured-posts__item">
				<?php if ($post->getThumbnail()) { ?>
					<div class="db-featured-posts__item-image">
						<a href="<?=$post->getUrl()?>">
							<?php $post->displayThumbnail(200, 200, true); ?>
						</a>
					</div>
				<?php } ?>
				<div class="db-featured-posts__item-copy">
					<div class="db-featured-posts__item-title">
						<h3> 
							<a href="<?=$post->getUrl()?>"><?=$post->getTitle()?></a>
						</h3>
					</div>
					<div class="db-featured-posts__item-date"><?=$post->getDate('F j, Y')?></div>
					<div class="db-featured-posts__item-teaser">
						<p>
							<?=$post->getTeaser()?>
							<a href="<?=$post->getUrl()?>">Read Full Post</a>
						</p>
					</div>
				</div>
			</li>
		<?php } ?>
	</ol>
	<div class="db-featured-posts__more">
		<a href="<?=$blog_url?>">View More Posts &raquo;</a>
	</div>
</div>