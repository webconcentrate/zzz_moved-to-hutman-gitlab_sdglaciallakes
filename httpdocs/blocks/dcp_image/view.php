<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php /* This block was made with Designer Content Pro. Visit http://theblockery.com/dcp for documentation. */ ?>

<?php
	$image_width = $image->getImageObj()->width;
	$image_height = $image->getImageObj()->height;
	if (isset($size) && $size->isNotEmpty()) {
		$size_pieces = explode("_", $size->getSelectedHandle());
		$image_width = intval($size_pieces[0]);
		$image_height = intval($size_pieces[1]);
		if (intval($size_pieces[2] == 'crop')) {
			$image_crop = true;
		} else {
			$image_crop = false;	
		}
	} else {
		if ($width->isNotEmpty()) {
			$image_width = intval($width->getText());
		}
		if ($height->isNotEmpty()) {
			$image_height = intval($height->getText());
		}	
		$image_crop = $crop->isChecked();
	}
	$resulting_image = $image->getImageObj($image_width, $image_height, $image_crop);
?>

<div class="image image--alignment-<?=$alignment->getSelectedHandle()?>">
	<?php if ($link->isNotEmpty()) { ?>
		<a href="<?=$link->getHref()?>" <?=($new_window->isChecked() ? 'target="_blank" rel="noopener"' : '')?>>
	<?php } ?>
		<img src="<?=$resulting_image->src?>" width="<?=$resulting_image->width?>" height="<?=$resulting_image->height?>" alt="<?=$image->getAltText()?>">
	<?php if ($link->isNotEmpty()) { ?>
		</a>
	<?php } ?>
</div>