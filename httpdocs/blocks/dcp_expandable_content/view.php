<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php /* This block was made with Designer Content Pro. Visit http://theblockery.com/dcp for documentation. */ ?>

<?php
	// Create a uniquie id in case this block is on the page multiple times.
	$unique_id = $bID;
	
	// Get repeating items
	$items = $controller->getRepeatingItems();	
?>

<?php if (!empty($items)) { ?>
	<div class="expandable-content">
		<?php foreach ($controller->getRepeatingItems() as $index=>$item) { ?>
			<div class="js-expandable-content-item expandable-content__item">
				<div class="expandable-content__heading">
					<h2>
						<a class="js-expandable-content-toggle" href="#expandable-content-<?=$unique_id?>=<?=$index?>"><?=$item->heading?></a>
					</h2>
				</div>
				<div id="expandable-content-<?=$unique_id?>=<?=$index?>" class="expandable-content__content">
					<?=$item->content?>
				</div>
			</div>
		<?php } ?>
	</div>
<?php } ?>