<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php /* This block was made with Designer Content Pro. Visit http://theblockery.com/dcp for documentation. */ ?>

<div class="cta-box">
	<div class="cta-box__heading-and-content">
		<div class="cta-box__heading">
			<h2><?=$heading?></h2>
		</div>
		<div class="cta-box__content">
			<?=$content?>
		</div>
	</div>
	<?php if ($link->isNotEmpty()) { ?>
		<div class="cta-box__cta">
			<a href="<?=$link->getHref()?>" <?=($new_window->isChecked() ? 'target="_blank" rel="noopener"' : '')?>><?=$link->getText()?></a>
		</div>
	<?php } ?>
</div>