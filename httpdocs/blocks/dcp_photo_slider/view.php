<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php /* This block was made with Designer Content Pro. Visit http://theblockery.com/dcp for documentation. */ ?>

<?php
	// Create a uniquie id in case this block is on the page multiple times.
	$unique_id = $bID;
	
	// Determine if popup is enabled
	$popup_enabled = ($popup->getSelectedHandle() == '1' ? true : false);
	
	// Get repeating items
	$items = $controller->getRepeatingItems();	
	
?>

<div class="js-photo-slider photo-slider" data-popup-enabled="<?=$popup_enabled?>">
	<div class="photo-slider__liner">
		<div class="photo-slider__image-list-and-controls">
			<div class="js-photo-slider-items photo-slider__image-list">
				<?php foreach ($items as $index=>$item) { ?>
					<div id="photo-slider-item-<?=$index?>-<?=$unique_id?>" class="js-photo-slider-item photo-slider__image-item <?=($index < 1 ? 'is-selected' : '')?>">
						<?php if ($popup_enabled) { ?>
							<a class="js-photo-slider-image-link photo-slider__image-link" href="<?=$item->image->getImageObj(1200, 1200, false)->src?>" data-caption="<?=$item->caption?>">
						<?php } ?>
							<?php if ($index < 1) { ?>
								<img src="<?=$item->image->getImageObj(1200, 600, true)->src?>" alt="<?=$item->image->getAltText()?>">
							<?php } else { ?>
								<img src="<?=DIR_REL?>/images/placeholder.svg" data-flickity-lazyload="<?=$item->image->getImageObj(1200, 600, true)->src?>" alt="<?=$item->image->getAltText()?>">
							<?php } ?>
						<?php if ($popup_enabled) { ?>
							</a>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
			<?php if (count($items) > 1) { ?>
				<div class="photo-slider__controls">
					<button class="js-photo-slider-previous photo-slider__controls-previous">Previous</button>	
					<button class="js-photo-slider-next photo-slider__controls-next">Next</button>
				</div>
			<?php } ?>
		</div>
		<?php if (count($items) > 1) { ?>
			<div class="photo-slider__pagination">
				<ol class="js-photo-slider-pagination-items photo-slider__pagination-list">
					<?php foreach ($items as $index=>$item) { ?>
						<li class="js-photo-slider-pagination-item photo-slider__pagination-item">
							<a class="photo-slider__pagination-link" href="#photo-slider-item-<?=$index?>-<?=$unique_id?>">
								<span>
									<?=$index?>
								</span>
							</a>
						</li>
					<?php } ?>
				</ol>
			</div>
		<?php } ?>
	</div>
</div>