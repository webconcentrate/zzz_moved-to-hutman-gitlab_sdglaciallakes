<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php /* This block was made with Designer Content Pro. Visit http://theblockery.com/dcp for documentation. */ ?>

<?php
	// Get repeating items
	$items = $controller->getRepeatingItems();		
?>

<?php if (!empty($items)) { ?>
	<div class="ads">
		<?php foreach ($items as $index=>$item) { ?>
			<?php
				$size_pieces = explode("_", $item->size->getSelectedHandle());
				$image_width = intval($size_pieces[0]);
				$image_height = intval($size_pieces[1]);
				if (intval($size_pieces[2] == 'crop')) {
					$image_crop = true;
				} else {
					$image_crop = false;	
				}
				$resulting_image = $item->image->getImageObj($image_width, $image_height, $image_crop);
			?>
			
			<div class="ads__ad">
				<?php if ($item->link->isNotEmpty()) { ?>
					<a href="<?=$item->link->getHref()?>" <?=($item->new_window->isChecked() ? 'target="_blank" rel="noopener"' : '')?>>
				<?php } ?>
					<img src="<?=$resulting_image->src?>" width="<?=$resulting_image->width?>" height="<?=$resulting_image->height?>" alt="<?=$item->image->getAltText()?>">
				<?php if ($item->link->isNotEmpty()) { ?>
					</a>
				<?php } ?>
			</div>
		<?php } ?>
	</div>
<?php } ?>