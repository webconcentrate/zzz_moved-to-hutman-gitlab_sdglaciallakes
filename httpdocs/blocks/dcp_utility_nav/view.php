<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php /* This block was made with Designer Content Pro. Visit http://theblockery.com/dcp for documentation. */ ?>

<?php 
	// Get repeating items 
	$items = $controller->getRepeatingItems();
?>


<div class="utility-nav">
	<div class="utility-nav__heading">
		<h2>
			Utility Nav
		</h2>
	</div>
	<?php if (!empty($items)) { ?>
		<ul class="utility-nav__list">
			<?php foreach ($items as $item) { ?>
				<?php
					// Array of classes to add to keep HTML output logic cleaner later on.
					$item_classes = array('utility-nav__item');
					$link_classes = array('utility-nav__link');
					
					if ($item->icon->isNotEmpty()) {
						$link_classes[] = 'utility-nav__link--has-icon';
						$link_classes[] = 'utility-nav__link--' . $item->icon->getSelectedHandle();
					}
					
					if ($item->hide_link_text->isChecked()) {
						$link_classes[] = 'utility-nav__link--text-hidden';
					}
					
					// Put all classes together into one space-separated string.
					$item_classes = implode(" ", $item_classes);
					$link_classes = implode(" ", $link_classes);
				?>
				<li class="<?=$item_classes?>">
					<a href="<?=$item->link->getHref()?>" class="<?=$link_classes?>" <?=($item->new_window->isNotEmpty() ? 'target="_blank" rel="noopener"' : '')?>>
						<span class="utility-nav__link-text"><?=$item->link->getText()?></span>
					</a>
				</li>
			<?php } ?>
		</ul>
	<?php } ?>
</div>
