<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<?php if (!empty($items)) { ?>
	<nav class="crumbs">
		<div class="crumbs__heading">
			<h2>You are here</h2>
		</div>
		<ol class="crumbs__list">
			<?php foreach($items as $item) { ?>
				<li class="crumbs__item <?=($item->is_current ? 'crumbs__item--current-page' : '')?>">
					<a class="crumbs__link <?=($item->is_current ? 'crumbs__link--current-page' : '')?>" href="<?=$item->url?>"><?=$item->title?></a>
				</li>
			<?php } ?>
		</ol>
	</nav>
<?php } ?>