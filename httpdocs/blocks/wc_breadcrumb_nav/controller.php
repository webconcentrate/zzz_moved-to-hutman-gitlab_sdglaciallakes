<?php defined('C5_EXECUTE') or die("Access Denied.");

/* version 2015-03-20 */

class WcBreadcrumbNavBlockController extends BlockController {

	protected $btName = 'Breadcrumb Nav';
	
	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btCacheBlockOutputForRegisteredUsers = true;
	protected $btCacheBlockOutputLifetime = CACHE_LIFETIME;
	
	public function view() {
		$current_page = Page::getCurrentPage();
		$parents = Loader::helper('navigation')->getTrailToCollection($current_page);
		$breadcrumbs = array_reverse($parents);
		$breadcrumbs[] = $current_page;
		
		$display_objects = $this->getDisplayObjectsFromPages($breadcrumbs);
		$this->set('items', $display_objects);
	}
	
	private function getDisplayObjectsFromPages($pages) {
		$display_objects = array();
		
		$nh = Loader::helper('navigation');
		
		foreach ($pages as $page) {
			$object = (object)array(
				'title' => h($page->getCollectionName()),
				'url' => $nh->getLinkToCollection($page),
				'is_current' => false,
			);
			
			$display_objects[] = $object;
		}
		
		//The leftover object is the last one in the loop,
		// which we assume is the current page...
		$object->is_current = true;
		
		return $display_objects;
	}
	
}
