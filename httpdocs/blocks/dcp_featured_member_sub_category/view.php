<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php /* This block was made with Designer Content Pro. Visit http://theblockery.com/dcp for documentation. */ ?>


<div class="featured-member-sub-category">
	<div class="featured-member-sub-category__heading-and-link">
		<div class="featured-member-sub-category__heading">
			<h2><?=$sub_category->getSubcategory()->title?></h2>
		</div>
		<div class="featured-member-sub-category__link">
			<a href="<?=$sub_category->getSubcategory()->directory_url?>">See all</a>
		</div>
	</div>
	<ul class="featured-member-sub-category__items">		
		<?php 
			$sub_category_locations = $sub_category->getLocations();
			shuffle($sub_category_locations);
			$sub_category_locations = array_slice($sub_category_locations, 0, 4);
		?>
		<?php foreach ($sub_category_locations as $location) { ?>
			<li class="featured-member-sub-category__item">
				<a class="featured-member-sub-category__item-link" href="<?=$location->details_page_url?>">
					<div class="featured-member-sub-category__item-image">
						<?php if (!empty($location->thumbnail_photo)) { ?>
							<img src="<?=$location->thumbnail_photo->src(400, 300, true)?>" alt="">
						<?php } ?>
					</div>
					<div class="featured-member-sub-category__item-text-1">
						<?=$location->title?>
					</div>
					<div class="featured-member-sub-category__item-text-2">
						<?php
							if ($location->address_city) {
								echo $location->address_city;
								if ($location->address_state) {
									echo ', ' . $location->address_state;
								}
							}
						?>
					</div>
				</a>
			</li>
		<?php } ?>
	</ul>
</div>