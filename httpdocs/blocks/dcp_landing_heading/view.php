<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php /* This block was made with Designer Content Pro. Visit http://theblockery.com/dcp for documentation. */ ?>

<?php
	if (!$heading->isNotEmpty()) {
		$c = Page::getCurrentPage();
		if ($c->getMasterCollectionID() != $c->getCollectionId()) {
			$heading = $c->getCollectionName();
		} else {
			$heading = 'Page Title';
		}
	}
?>

<div class="landing-heading">
	<?php if ($image_narrow->isNotEmpty()) { ?>
		<div class="landing-heading__image landing-heading__image--narrow" style="background-image: url(<?=$image_narrow->getImageObj(700, 500, true)->src?>)"></div>
	<?php } else if ($image_wide->isNotEmpty()) { ?>
		<div class="landing-heading__image landing-heading__image--narrow" style="background-image: url(<?=$image_wide->getImageObj(700, 500, true)->src?>)"></div>
	<?php } ?>
	<?php if ($image_wide->isNotEmpty()) { ?>
		<div class="landing-heading__image landing-heading__image--wide" style="background-image: url(<?=$image_wide->getImageObj(1500, 600, true)->src?>)"></div>
	<?php } ?>
	<div class="landing-heading__heading">
		<h1><?=$heading?></h1>
	</div>
</div>
