<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<?php if (isset($error)) { ?>
	<?php echo $error?><br/><br/>
<?php } ?>

<form action="<?php echo $this->url( $resultTargetURL )?>" method="get" class="ccm-search-block-form">

	<?php if( strlen($title)>0){ ?><h3><?php echo $title?></h3><?php } ?>
	
	<?php if(strlen($query)==0){ ?>
	<input name="search_paths[]" type="hidden" value="<?php echo htmlentities($baseSearchPath, ENT_COMPAT, APP_CHARSET) ?>" />
	<?php } else if (is_array($_REQUEST['search_paths'])) { 
		foreach($_REQUEST['search_paths'] as $search_path){ ?>
			<input name="search_paths[]" type="hidden" value="<?php echo htmlentities($search_path, ENT_COMPAT, APP_CHARSET) ?>" />
	<?php  }
	} ?>
	
	<input name="query" type="text" value="<?php echo htmlentities($query, ENT_COMPAT, APP_CHARSET)?>" class="ccm-search-block-text" />
	
	<input name="submit" type="submit" value="<?php echo $buttonText?>" class="ccm-search-block-submit" />

<?php 
$tt = Loader::helper('text');
if ($do_search) { ?>
	
	<?php /* START Members & Events ***********************************************************************************************************************************/ ?><?php /* END Members & Events ***********************************************************************************************************************************/ ?>
	<?php 
		$search_terms = array_key_exists('query', $_GET) ? $_GET['query'] : '';
		$search_terms = trim($search_terms);
		
		$has_search_terms = (strlen($search_terms) > 3);
		if ($has_search_terms) {
			Loader::model('location/query', 'sdgl_members');
			$members = LocationQuery::activeBySearchTerms($search_terms);
			
			Loader::model('event', 'designer_event_calendar');
			$events = EventRecord::getByTitleSearch($search_terms);
		}
	?>
	
	<?php if ($has_search_terms) { ?>
	
		<div class="spacer spacer--size-10 spacer--style-3"></div>
			
		<div class="member-list">
			<div class="member-list__heading">
				<h2>Members</h2>
			</div>
			<?php if ($members) { ?>
				<ul class="member-list__list member-list__list--scroll">
					<?php foreach ($members as $location) { ?>
						<li class="member-list__item">
							<a class="member-list__item-link" href="<?=$location->details_page_url?>">
								<div class="member-list__item-copy <?=(!empty($location->thumbnail_photo) ? 'member-list__item-copy--has-image' : '')?>">
									<div class="member-list__item-title">
										<?=$location->title?>
									</div>
									<div class="member-list__item-location">
										<?php if ($location->address_city && $location->address_state) { ?>
											<?=$location->address_city?>, <?=$location->address_state?>
										<?php } else if ($location->address_city || $location->address_state) { ?>
											<?=$location->address_city?> <?=$location->address_state?>
										<?php } ?>
									</div>
								</div>
								<?php if (!empty($location->thumbnail_photo)) { ?>
									<div class="member-list__item-image">
										<img src="<?=$location->thumbnail_photo->src(400, 300, true)?>" alt="">
									</div>
								<?php } ?>
							</a>
						</li>
					<?php } ?>
				</ul>
			<?php } else { ?>
				<i>No attractions match the search terms<i>
			<?php } ?>
		</div>	
		
		<div class="spacer spacer--size-10 spacer--style-3"></div>
		<div class="event-list">
			<div class="event-list__heading">
				<h2>Event Calendar</h2>
			</div>
			<?php if ($events) { ?>
				<ul class="event-list__list event-list__list--multicolumn event-list__list--scroll">
					<?php foreach ($events as $event) { ?>
	
							<li class="event-list__item">
								<div class="event-list__item-summary-date">
									<div class="event-list__item-summary-month">
										<?=$event->getStartFormatted('M')?>
									</div>
									<div class="event-list__item-summary-day">
										<?=$event->getStartFormatted('j')?>
									</div>
								</div>
								<div class="event-list__item-details">
									<div class="event-list__item-title">
										<?php if ($event->isLinkToNothing()) { ?>
											<span><?=$event->getTitle()?></span>
										<?php } else { ?>
											<a href="<?=$event->getLinkToDetailsPage()?>">
												<?=$event->getTitle()?>
											</a>
										<?php } ?>
									</div>
									<div class="event-list__item-date">
										<?php
											$start_date = $event->getStartFormatted('l, F d, Y');
											$start_time = $event->getStartFormatted('g:ia');
											$end_date = $event->getEndFormatted('l, F d, Y');
											$end_time = $event->getEndFormatted('g:ia');
											
											if ($event->isAllDay()) {
												echo 'All day';
											} else {
												echo $start_time;
												if (!$event->isEndTimeHidden()) {
													echo ' - ' . $end_time;
												}
											}
											
											if ($event->isMultiDay()) {
												echo ' (thru ' . $end_date . ')';
											}
										?>
									</div>
									
									<?php if ($event->hasSubtitle()) { ?>
										<div class="event-list__item-location">
											<?=$event->getSubtitle()?>
										</div>
									<?php } ?>
									
								</div>
							</li>
					<?php } ?>
				</ul>
			<?php } else { ?>
				<i>No calendar events match the search terms<i>
			<?php } ?>
		</div>
		
	<?php } ?>
	
	<?php /* END Members & Events ***********************************************************************************************************************************/ ?>
	
	<div class="spacer spacer--size-10 spacer--style-3"></div>
	
	<h2>Plan Your Trip</h2>
	<div style="background-color: #fff; padding: 20px;">
		<?php if(count($results)==0){ ?>
			<i>No trip planning content matches the search terms</i>
		<?php }else{ ?>
			<div id="searchResults">
			<?php foreach($results as $r) { 
				$currentPageBody = $this->controller->highlightedExtendedMarkup($r->getBodyContent(), $query);?>
				<div class="searchResult">
					<h3><a href="<?php echo $r->getPath()?>"><?php echo $r->getName()?></a></h3>
					<p>
						<?php if ($r->getDescription()) { ?>
							<?php  echo $this->controller->highlightedMarkup($tt->shortText($r->getDescription()),$query)?><br/>
						<?php } ?>
						<?php echo $currentPageBody; ?>
						<a href="<?php  echo $r->getPath(); ?>" class="pageLink"><?php  echo $this->controller->highlightedMarkup($r->getPath(),$query)?></a>
					</p>
				</div>
			<?php 	}//foreach search result ?>
			</div>
			
			<?php
			if($paginator && strlen($paginator->getPages())>0){ ?>	
			<div class="ccm-pagination">	
				 <span class="ccm-page-left"><?php echo $paginator->getPrevious()?></span>
				 <?php echo $paginator->getPages()?>
				 <span class="ccm-page-right"><?php echo $paginator->getNext()?></span>
			</div>	
			<?php } ?>
	
		<?php } //results found ?>
	</div>
<?php } ?>

</form>