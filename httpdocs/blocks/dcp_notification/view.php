<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php /* This block was made with Designer Content Pro. Visit http://theblockery.com/dcp for documentation. */ ?>

<?php
	$enabled = true;
	
	if ($disable->isChecked()) {
		$enabled = false;	
	}
	
	if ($date->isNotEmpty()) {
		$today = strtotime("today");
		$expire_date = strtotime($date);
		if ($today > $expire_date) {
			$enabled = false;
		}
	}
?>
<?php if ($enabled) { ?>
	<div class="notification">
		<div class="notification__message">
			<?=$message?>
		</div>
		<div class="notification__link">
			<a href="<?=$link->getHref()?>" <?=($new_window->isChecked ? 'target="_blank" rel="noopener"' : '')?>><?=$link->getText()?></a>
		</div>
	</div>	
<?php } ?>