<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>
<?php /* This block was made with Designer Content Pro. Visit http://theblockery.com/dcp for documentation. */ ?>

<div class="photo-cta">
	<div class="photo-cta__liner">
		<?php if ($image_narrow->isNotEmpty()) { ?>
			<div class="photo-cta__image photo-cta__image--narrow" style="background-image: url(<?=$image_narrow->getImageObj(700, 400, true)->src?>)"></div>
		<?php } else if ($image_wide->isNotEmpty()) { ?>
			<div class="photo-cta__image photo-cta__image--narrow" style="background-image: url(<?=$image_wide->getImageObj(700, 400, true)->src?>)"></div>
		<?php } ?>
		<?php if ($image_wide->isNotEmpty()) { ?>
			<div class="photo-cta__image photo-cta__image--wide" style="background-image: url(<?=$image_wide->getImageObj(1300, 500, true)->src?>)"></div>
		<?php } ?>
		<?php if ($heading->isNotEmpty() || $link->isNotEmpty()) { ?>
			<div class="photo-cta__content">
				<div class="photo-cta__content-liner">
					<?php if ($heading->isNotEmpty()) { ?>
						<div class="photo-cta__heading">
							<h2><?=$heading?></h2>
						</div>
					<?php } ?>
					<?php if ($link->isNotEmpty()) { ?>
						<div class="photo-cta__link">
							<a href="<?=$link->getHref()?>" <?=($new_window->isChecked() ? 'target="_blank" rel="noopener"' : '')?>><?=$link->getText()?></a>
						</div>
					<?php } ?>
				</div>
			</div>
		<?php } ?>
	</div>
</div>