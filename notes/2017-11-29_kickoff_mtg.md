# Home page

 - check latest fishing report: can just be hardcoded (no need for wc_notification block).
   (just give editors the ability to show/hide it... SO I GUESS JUST A PLAIN OLD CONTENT BLOCK?!)

 * trip ideas (thumbnails + links): hand-picked from available trip ideas.
 * state parks (thumbnails + links): "category" block (well, a sub-category I guess). Cap it at a max amt (just random for now).
 * upcoming events (hero): just a single link block I guess
 * get your free travel guide: link to a page in the site
 * sign up for our newsletter: TRAV will let us know what that is (mailchimp? constant contact? just notify admins? etc)
 * footer nav: manual links
 * bottom-right of footer: hover over regions of the state to link to other sites
  
## primary nav menu

 - trip ideas: collections of attractions, and/or "bundles" of attactions?
 - attractions: more of a phone book-esque directory
   **note** each attaction does have a category (outdoors, family, shopping) and subcategory (hunting, fishing).
    BUT an attraction can be in more than 1 category!

# overall design of templates
 ~ top-level interior pages have big hero image and the intro text block (but children of those don't?)

# Trip ideas top-level page
 - list all the trip ideas. Order (by sitemap I guess? some kind of manual order)
 
## Trip idea detail page
 - large hero photo
 - rich text intro
 - sidebar
 	~ "plan your trip" section is just manual text links
 	~ ad units below... for now, just plain old blocks (maybe in the future we do something more robust, but not now)
 - popular hunting attractions
   NEED TO FIGURE OUT HOW TO MANAGE EDITING OF THIS. (In the page? in the dashboard? If in the page, how do we query? But if in the dashboard, how is the page generated?)
   ~ the "View more hunting attractions >" link at the bottom... I guess a manually-entered link? (and could be 0, 1, or more)
   SO in effect, the upper list is of specific attractions, the lower list is of sub-categories
 - FEATURED EVENTS
 	~ list all calendar events that are associated with attractions that are listed in this trip idea bundle
 	- MAYBE cap it at a certain number of events or a timeframe?
 	! HEY: FOR MULTIDAY EVENTS, CAN WE ONLY SHOW IT ON THE FIRST DAY? (Or does it already do this?). AND/OR WHAT ABOUT REPEATING EVENTS? Hrmmm...


# Attractions
 - plain old list view (just like primary nav)
 - map view ("directory") , navigated to from the list view.
   Each directory (map view) page is for a toplevel category. If user clicked a subcategory in the plain text list view, then just prepopulate the filter in the parent directory.
   "Listings Near" dropdown: is a list of CITY NAMES.
    (WIll need a picklist of bigger towns to populate this, THEN when editing an attraction, you choose which bigtown(s) is nearby.)
    ^^^MAYBE HIDE IT FROM EDITORS THOUGH BECAUSE WE DONT WANT THEM OVERPOPULATING IT? <--so could just be database table for now with no editing interface
  MAP: yeah, do it. Needs to show pins, and we prolly want lat/lng for each one (but could be auto-guessed from address in dashboard)
  
## Attraction details page
	- may or may not have images
	- may or may not have events
	- may or may not have ads

# Events
 ~ events ONLY exist in association with attractions! <--JORDAN THINK ABOUT THIS!
 ~ the "category" filter on the calendar should be CATEGORY OF ATTRACTIONS THAT EVENTS ARE ASSOCIATED WITH
 ~ also then add a "events nearby..." filter to the events page
 ! Note that the design has an events map, but we are NOT implementing this!

**HEY** WE NEED A PUBLIC SUBMISSIONS FORM AS WELL!

#Where to stay:
 note that these are just attractions, but some kind of side-category of 'em or something
 (not sure if there's an uber-meta-category ("attractions" vs "where to stay"), or if "where to stay" is a toplevel attraction (on same level as "Outdoors", "Family", etc) but is somehow marked as not being included in normal attractions thingy?
 SO: on the where-to-stay toplevel page, there are "Hotels & Motels", "Resorts", "Lodges"... each of these is a SUB-CATEGORY of the toplevel WHERETOSTAY category. But the listing style of those is the style of toplevel categories on the toplevel attractions page.
 
(ALSO A "TRAVEL SERVICES" side-category) <--so... maybe just a "hide from attractions" flag on toplevel categories? Then we hardocde the output of those in other places?
 
 
# Plan Your Trip

## Free Travel Guide page
 - some kind of form where people can request these
 - just submit to email notification / dashboard (custom_contact_form)
 ~ copy fields and options from http://www.sdglaciallakes.com/about-us/free-visitors-guide
 TRAV: awaiting answer on if this form is for individuals wanting a mailing, OR wholesale places wanting a bunch of them for their display.
 
## Blog
 - categories (shown in sidebar)

## Travel Deals
 - accordion content block

## Travel Services
 - another attraction category (like where-to-stay)

## Maps
 - use the accordion (like travel deals), but note that it could have PDF links in it!

## Fishing Reports
 - section of pages. Each lake gets its own page (content gets overwritten each year, so not like a blog where there's new posts for every report)
   ~ be able to categorize lakes by geographical area.
   ~ andrew may or may not design jump links or filtering or whatever
     (but there might be a lot of them so maybe pagination??)
   SEE http://www.sdglaciallakes.com/fishing-reports
   
   (BUT MAYBE ALL ONE PAGE, BECAUSE EACH REPORT IS JUST 1 PARAGRAPH? BUT MAYBE IT'S NOT DURING SUMMER SEASON?)
    hrmmm.... FOR NOW JUST 1 PAGE WITH CONTENT BLOCKS. Later we could do something else but not for now.
    
## Pheasant Reports, Lake Reports, Road Reports:
 - each just generic content page (for now)

##Counties
 - also attractions (like where-to-stay). pin the county seat on the map (I guess?).


#About our organization
	(not part of primary nav, but has utility nav link to it)
	TODO: Await andrew finishing up design (but in general it's generic content)

# Contact
 I presume it's just a generic contact form?


JORDAN DO EXTRAS:
 1) Beef up DCP to show better text in list view
 2) vue.js dashboard interface